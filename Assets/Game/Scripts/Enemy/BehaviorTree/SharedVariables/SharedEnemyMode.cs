﻿///-------------------------------------------------------------------------------------------------
// File: SharedEnemyMode.cs
//
// Author: Dakshvir Singh Rehill
// Date: 17/7/2020
//
// Summary:	Enemy Mode Enum created to allow for checking of Deactivate and Frozen from one conditional
///-------------------------------------------------------------------------------------------------
using BehaviorDesigner.Runtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum EnemyMode
{
	None,
	Deactivate,
	Frozen,
	Attack
}

[System.Serializable]
public class SharedEnemyMode : SharedVariable<EnemyMode>
{
	public override string ToString() { return mValue.ToString(); }
	public static implicit operator SharedEnemyMode(EnemyMode value) { return new SharedEnemyMode { mValue = value }; }
}
