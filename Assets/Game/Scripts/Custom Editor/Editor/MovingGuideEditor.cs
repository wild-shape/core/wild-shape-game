﻿///-------------------------------------------------------------------------------------------------
// File: MovingGuideEditor.cs
//
// Author: Aditya Dinesh
// Date: 28/7/2020
//
// Summary:	Custom Inspector display for Moving Guide
///-------------------------------------------------------------------------------------------------

using UnityEditor;

[CustomEditor(typeof(MovingGuide))]
public class MovingGuideEditor : Editor
{
    SerializedProperty mControlType;
    SerializedProperty mDynamicConversationDialogs;
    SerializedProperty mAnimSpeedMultiplier;
    SerializedProperty mAnimationListener;
    SerializedProperty mRigidbody;
    SerializedProperty mSteeringAgent;
    SerializedProperty mMovingBehaviour;
    SerializedProperty mCollectible;

    MovingGuide mMovingGuide = null;

    private void OnEnable()
    {
        mMovingGuide = target as MovingGuide;

        mControlType = serializedObject.FindProperty("mControlType");
        mDynamicConversationDialogs = serializedObject.FindProperty("mDynamicConversationDialogs");
        mAnimSpeedMultiplier = serializedObject.FindProperty("mAnimSpeedMultiplier");
        mAnimationListener = serializedObject.FindProperty("mAnimationListener");
        mRigidbody = serializedObject.FindProperty("mRigidbody");
        mSteeringAgent = serializedObject.FindProperty("mSteeringAgent");
        mMovingBehaviour = serializedObject.FindProperty("mMovingBehaviour");
        mCollectible = serializedObject.FindProperty("mCollectible");
    }

    public override void OnInspectorGUI()
    {
        if (mMovingGuide != null)
        {
            EditorGUILayout.PropertyField(mControlType);
            EditorGUILayout.PropertyField(mDynamicConversationDialogs);
            EditorGUILayout.PropertyField(mAnimSpeedMultiplier);
            EditorGUILayout.PropertyField(mAnimationListener);
            EditorGUILayout.PropertyField(mRigidbody);
            EditorGUILayout.PropertyField(mSteeringAgent);
            EditorGUILayout.PropertyField(mMovingBehaviour);
            EditorGUILayout.PropertyField(mCollectible);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
