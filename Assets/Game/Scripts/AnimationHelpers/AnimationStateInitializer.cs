﻿///-------------------------------------------------------------------------------------------------
// File: AnimationStateInitializer.cs
//
// Author: Aditya Dinesh
// Date: 3/8/2020
//
// Summary:	Sets initial state of the Animator based on the selected Enum state
///-------------------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationStateInitializer : MonoBehaviour
{
    public enum AnimationState
    {
        Sleep,
        Sit,
        Dead
    };

    public AnimationState mAnimState = AnimationState.Sit;
    public Animator mAnimator;

    void Start()
    {
        if (mAnimator != null)
        {
            switch (mAnimState)
            {
                case AnimationState.Sleep:
                    mAnimator.SetTrigger("sleep");
                    break;

                case AnimationState.Sit:
                    mAnimator.SetTrigger("sit");
                    break;

                case AnimationState.Dead:
                    mAnimator.SetTrigger("dead");
                    break;
            }
        }
    }
}
