﻿///-------------------------------------------------------------------------------------------------
// File: IsInspectPointSet.cs
//
// Author: Dakshvir Singh Rehill
// Date: 26/6/2020
//
// Summary:	Returns success if inspect point count is greater than zero
///-------------------------------------------------------------------------------------------------
using BehaviorDesigner.Runtime.Tasks;

[TaskCategory("Enemy")]
[TaskDescription("Returns success if inspect point count is greater than zero")]
public class IsInspectPointSet : EnemyConditionalBase
{
    public override TaskStatus OnUpdate()
    {
        //check list count and return success if there is anything in list
        return mEnemyInstance.mInspectPoints.Count > 0 ? TaskStatus.Success : TaskStatus.Failure;
    }
}
