///-------------------------------------------------------------------------------------------------
// File: Deactivate.cs
//
// Author: Dakshvir Singh Rehill
// Date: 27/5/2020
//
// Summary:	Called when the player is deactivated and will be used for playing the deactivate anim
///-------------------------------------------------------------------------------------------------
using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

[TaskCategory("Enemy")]
[TaskDescription("Called when the player is deactivated and will be used for playing the deactivate anim")]
public class Deactivate : EnemyActionBase
{
	[BehaviorDesigner.Runtime.Tasks.Tooltip("The Enemies that have been deactivated in the entire game")]
	[SerializeField] SharedEnemyList mDeactivatedEnemies;

	public override void OnStart()
	{
		base.OnStart();
	}

	public override TaskStatus OnUpdate()
	{
		if(!mDeactivatedEnemies.Value.Contains(mEnemyInstance))
        {
			mDeactivatedEnemies.Value.Add(mEnemyInstance);
        }
		return TaskStatus.Success;
	}
}