﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FleeSteeringBehaviour : SteeringBehaviourBase
{
	public float fleeDistance = 5.0f;
	public Transform enemyTarget;

	public override Vector3 calculateForce()
	{
		if (enemyTarget != null)
		{
			mTarget = enemyTarget.position;
        }
		

		// check the distance return zero if we are far away
		float distance = (mTarget - transform.parent.position).magnitude;
		if (distance > fleeDistance)
		{
			return Vector3.zero;
		}

		Vector3 desiredVelocity = (transform.parent.position - mTarget).normalized;
		desiredVelocity = desiredVelocity * mSteeringAgent.mMaxSpeed;
		return desiredVelocity - mSteeringAgent.mVelocity;
	}

	private void OnDrawGizmos()
	{
		DebugExtension.DrawCircle(transform.parent.position, fleeDistance);
		DebugExtension.DebugWireSphere(mTarget, Color.red);
	}

}
