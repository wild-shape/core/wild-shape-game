﻿///-------------------------------------------------------------------------------------------------
// File: PressurePlate.cs
//
// Author: Dakshvir Singh Rehill
// Date: 30/5/2020
//
// Summary:	Responsible for activating doors and moving platforms
///-------------------------------------------------------------------------------------------------
using System.Collections.Generic;
using UnityEngine;

public class PressurePlate : MonoBehaviour, IFreezable
{
    [Tooltip("The Render Object that is going to be displayed as the button")]
    [SerializeField] GameObject mBtnRenderer;
    [Tooltip("The Normal Position of the render object")]
    [SerializeField] Transform mNormalPosition;
    [Tooltip("The Pressed Position of the render object")]
    [SerializeField] Transform mPressedPosition;
    [Tooltip("The Button Press Effect Time")]
    [SerializeField] float mEffectTime = 0.5f;
    [Tooltip("The Button Press Effect Delay")]
    [SerializeField] float mEffectDelay = 0.2f;
    [Tooltip("The Sound Effect for Button Press")]
    [SerializeField] string mButtonPressSFX = "PressButton";
    [Tooltip("The Sound Effect for Button Release")]
    [SerializeField] string mButtonReleaseSFX = "ButtonOff";
    [SerializeField] [ColorUsage(true,true)]Color mPressedColor;
    [SerializeField] [ColorUsage(true,true)] Color mUnpressedColor;
    [Tooltip("The Renderer with the Emissive Material")]
    [SerializeField] GameObject mRenderObject;
    List<MeshRenderer> mEmissiveRenderers;
    /// <summary>
    /// The Emmissive Material copy of this interactable
    /// </summary>
    Material mEmissiveMaterial;
    /// <summary>
    /// All the objects controlled by this pressure plate
    /// </summary>
    public readonly List<IButtonControllable> mControlledObjects = new List<IButtonControllable>();
    /// <summary>
    /// Boolean to check whether its pressed or not
    /// </summary>
    bool mPressed = false;
    /// <summary>
    /// current presser
    /// </summary>
    CanPress mCurrentCanPress = null;

    public bool mFrozen { get; set; }

    void Start()
    {
        mEmissiveRenderers = new List<MeshRenderer>(mRenderObject.GetComponentsInChildren<MeshRenderer>());
        if (mEmissiveRenderers.Count <= 0)
        {
            Debug.LogError("No Emmissive Renderers Attached");
            Destroy(gameObject);
            return;
        }
        mEmissiveMaterial = new Material(mEmissiveRenderers[0].material);
    }

    void SetColor()
    {
        mEmissiveMaterial.SetColor("_EmissionColor", mPressed ? mPressedColor : mUnpressedColor);
        foreach (MeshRenderer aRenderer in mEmissiveRenderers)
        {
            aRenderer.material = mEmissiveMaterial;
        }
    }


    void Update()
    {
        if(mFrozen || !mPressed)
        {
            return;
        }
        if (mCurrentCanPress != null) // to check for shapeshift and disable pressure
        {
            if (!mCurrentCanPress.gameObject.activeInHierarchy)
            {
                mCurrentCanPress = null;
                mPressed = false;
                CallButtonFunctions();
            }
        }
        else
        {
            mPressed = false;
            CallButtonFunctions();
        }
    }


    void OnTriggerEnter(Collider pOther)
    {
        AddCanPress(ref pOther);
    }

    /// <summary>
    /// Function that checks if the plate is pressed in enter and stay
    /// </summary>
    /// <param name="pOther"></param>
    void AddCanPress(ref Collider pOther)
    {
        if(mFrozen)
        {
            return;
        }
        if (mCurrentCanPress != null)
        {
            return;
        }
        mCurrentCanPress = pOther.GetComponent<CanPress>();
        if (mCurrentCanPress != null)
        {
            mPressed = true;
            CallButtonFunctions();
        }

    }

    void OnTriggerStay(Collider pOther)
    {
        AddCanPress(ref pOther);
    }

    void OnTriggerExit(Collider pOther)
    {
        if (mCurrentCanPress == null)
        {
            return;
        }
        if (mCurrentCanPress.gameObject.GetInstanceID() == pOther.gameObject.GetInstanceID())
        {
            mCurrentCanPress = null;
        }
    }

    /// <summary>
    /// Calls button up and button down callbacks on all controllables
    /// </summary>
    void CallButtonFunctions()
    {
        foreach (IButtonControllable aControllable in mControlledObjects)
        {
            Vector3 aLocalPos = Vector3.zero;
            if (mPressed)
            {
                aControllable.ButtonDown(this);
                aLocalPos = mPressedPosition.localPosition;
                if (!string.IsNullOrEmpty(mButtonPressSFX))
                {
                    AudioManager.Instance.PlaySound(mButtonPressSFX);
                }
            }
            else
            {
                aControllable.ButtonUp(this);
                aLocalPos = mNormalPosition.localPosition;
                if (!string.IsNullOrEmpty(mButtonReleaseSFX))
                {
                    AudioManager.Instance.PlaySound(mButtonReleaseSFX);
                }
            }
            if (LeanTween.isTweening(mBtnRenderer))
            {
                LeanTween.cancel(mBtnRenderer);
            }
            LeanTween.moveLocal(mBtnRenderer, aLocalPos, mEffectTime).setDelay(mEffectDelay);
        }
        SetColor();
    }

}
