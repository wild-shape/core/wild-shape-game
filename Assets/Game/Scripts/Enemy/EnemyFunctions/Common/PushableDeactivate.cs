﻿///-------------------------------------------------------------------------------------------------
// File: PushableDeactivate.cs
//
// Author: Dakshvir Singh Rehill
// Date: 8/6/2020
//
// Summary:	Script to deactivate enemy when pushable object falls on top of them
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushableDeactivate : MonoBehaviour
{
    [Tooltip("The Enemy that will get deactivated if the player pushes the pushable object on top of it")]
    [SerializeField] Enemy mEnemy;
    [Tooltip("The collider of the deactivate zone of this enemy")]
    [SerializeField] BoxCollider mCollider;
    [Tooltip("The layermask of what all can deactivate by falling on the enemy")]
    [SerializeField] LayerMask mDeactivationObjects;
    void Update()
    {
        // find all colliders that the deactivate zone is coming in contact with if any
        Collider[] aColliders = Physics.OverlapBox(mCollider.bounds.center,
            mCollider.bounds.extents, mCollider.transform.rotation,
            mDeactivationObjects, QueryTriggerInteraction.Ignore);
        if (aColliders.Length > 0) // if there are deactivation object on top of the zone then deactivate enemy
        {
            foreach (Collider aCol in aColliders)
            {
                if (aCol.attachedRigidbody != null)
                {
                    Pushable aPushable = aCol.attachedRigidbody.gameObject.GetComponentInChildren<Pushable>();
                    if (aPushable != null)
                    {
                        if (aCol.attachedRigidbody.gameObject.transform.position.y >= transform.position.y)//check if block is abv or nt
                        {
                            aPushable.BreakPushable();
                            if (!mEnemy.mDeactivate)
                            {
                                IDeactivatable aDeactivatableInst = mEnemy;
                                aDeactivatableInst.mDeactivate = true;
                            }
                        }
                    }
                    break;
                }
            }
        }
    }
}
