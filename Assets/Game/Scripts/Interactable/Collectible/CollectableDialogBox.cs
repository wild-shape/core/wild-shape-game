﻿///-------------------------------------------------------------------------------------------------
// File: CollectableDialogBox.cs
//
// Author: Aditya Dinesh, Dakshvir SIngh Rehill
// Date: 7/8/2020
//
// Summary:	Class containing Collectable data which will be displayed on HUD prompts and Stats for Collectible
///-------------------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class CollectableDialogBox : MonoBehaviour
{
    [Header("Collectable DialogBox Parameters")]
    [Tooltip("The time(seconds) after which the Dialogue Box will close automatically")]
    public float mHideDelay = 2.0f;
    [Header("Colectable DialogBox UI elements")]
    [Tooltip("The text of the main heading")]
    public TextMeshProUGUI mCollectibleText;
    [Tooltip("Continue Container Image")]
    public GameObject mContinueContainer;
    [Tooltip("The Continue Button Text")]
    public TextMeshProUGUI mContinueTxt;
    [SerializeField] Image mContinueImg;
    [Tooltip("Collectible BG Img")]
    [SerializeField] Image mBGImg;
    [Tooltip("The Interact BG Img")]
    [SerializeField] Image mInteractBG;
    [Tooltip("The new menu classifer for the progress menu")]
    [SerializeField] MenuClassifier mProgressMenuClassifier;
    [Tooltip("The Text for Total in Scene")]
    [SerializeField] TextMeshProUGUI mTotalInScene;
    [Tooltip("The Text for Total collected")]
    [SerializeField] TextMeshProUGUI mTotalCollected;
    /// <summary>
    /// Dialog Menu reference that allows to display the collectable HUD prompt
    /// </summary>
    InGameDialogMenu mDialogMenu;
    /// <summary>
    /// Game action of the player that controls interaction with Collectable Dialog box
    /// </summary>
    GameActions mGameAction;
    /// <summary>
    /// Time of the timer
    /// </summary>
    float mCurrentTime = 0.0f;
    /// <summary>
    /// Default Alpha of BGImg
    /// </summary>
    float mDefaultAlpha = 0.0f;
    ProgressMenu mProgressMenu;
    /// <summary>
    /// Callback for changing controls
    /// </summary>
    UnityAction<ChangeableControls> mControlAction;

    void Awake()
    {
        mGameAction = new GameActions();
        GameManager.Instance.UpdateAllChangeableControls(mGameAction);
        mDefaultAlpha = mBGImg.color.a;
    }

    void Start()
    {
        gameObject.SetActive(false);
        mProgressMenu = MenuManager.Instance.GetMenu<ProgressMenu>(mProgressMenuClassifier);
        mControlAction = (pControls) => GameManager.Instance.OnControlsChanged(pControls, mGameAction);
        GameManager.Instance.mOnControlsChanged.AddListener(mControlAction);
    }

    void OnDestroy()
    {
        if (GameManager.IsValidSingleton())
        {
            GameManager.Instance.mOnControlsChanged.RemoveListener(mControlAction);
        }
        mGameAction.Dispose();
    }


    void OnEnable()
    {
        mGameAction.Enable();
        Color aColor = mBGImg.color;
        aColor.a = 0.0f;
        mBGImg.color = aColor;
    }

    void OnDisable()
    {
        mGameAction.Disable();
        mContinueContainer.SetActive(false);
        mContinueImg.gameObject.SetActive(false);
        mContinueTxt.gameObject.SetActive(false);
        Color aColor = mBGImg.color;
        aColor.a = mDefaultAlpha;
        mBGImg.color = aColor;
    }

    private void Update()
    {
        if (mCurrentTime > 0.0f)
        {
            mCurrentTime -= Time.deltaTime;

            if (mCurrentTime <= 0.0f)
            {
                HideDialogBox();
            }
        }
    }

    public void ShowInfo(InGameDialogMenu pMenu, int pIx)
    {
        mGameAction.PlayerBasic.Interact.performed += HideInput;
        CollectibleData aData;
        if (!mProgressMenu.mAllCollectibleData.TryGetValue(pIx, out aData))
        {
            Debug.LogErrorFormat("Collectible with id {0} not found", pIx);
            HideDialogBox();
            return;
        }
        mDialogMenu = pMenu;
        mCollectibleText.text = aData.mCollectibleStory;
        SetupContinueImage();
        int aCollected = 0;
        int aScene = 0;
        mProgressMenu.GetCollectedTotalInScene(out aCollected, out aScene);
        mTotalCollected.text = aCollected.ToString();
        mTotalInScene.text = aScene.ToString();
        gameObject.SetActive(true);
        mCurrentTime = mHideDelay;
    }

    void SetupContinueImage()
    {
        ChangeableControls aControlValues = GameManager.Instance.mSettingsData.mCurrentControls.GetControl(Controls.Interact);
        PromptContainer aPrompt;
        string aControlPath = InputPromptHandler.Instance.mActiveControlSchemeType != ControlSchemeType.KeyboardMouse ?
            aControlValues.mGamepadPath.Substring(aControlValues.mGamepadPath.LastIndexOf(">/") + 2) :
            aControlValues.mKeyboardPath.Substring(aControlValues.mKeyboardPath.LastIndexOf(">/") + 2);
        if (InputPromptHandler.Instance.mUIPrompts[InputPromptHandler.Instance.mActiveControlSchemeType]
            .TryGetValue(aControlPath, out aPrompt))
        {
            mInteractBG.sprite = InputPromptHandler.Instance.mInteractImg;
            Sprite aMBTNSprite = null;
            if (InputPromptHandler.Instance.mActiveControlSchemeType == ControlSchemeType.KeyboardMouse)
            {
                aMBTNSprite = aPrompt.mPromptSettings.mFallbackText.Contains("MBTN") ? InputPromptHandler.Instance.mMBtnImg[int.Parse(aPrompt.mPromptSettings.mFallbackText.Substring(4))] : null;
                if (aMBTNSprite != null)
                {
                    mInteractBG.sprite = aMBTNSprite;
                }
                else
                {
                    mInteractBG.sprite = aPrompt.mPromptSettings.mFallbackText.Length >= 3 ? InputPromptHandler.Instance.mLongKeyImg : InputPromptHandler.Instance.mKeyImg;
                }
            }
            mContinueContainer.SetActive(true);
            if (aMBTNSprite == null)
            {
                mContinueTxt.text = aPrompt.mPromptSettings.mFallbackText;
                mContinueImg.sprite = aPrompt.mPromptImage == null ? null : aPrompt.mPromptImage;
                if (mContinueImg.sprite != null)
                {
                    mContinueImg.gameObject.SetActive(true);
                }
                else
                {
                    mContinueTxt.gameObject.SetActive(true);
                }
            }
            else
            {
                mContinueTxt.text = "";
            }
        }
    }

    void HideInput(InputAction.CallbackContext pContext)
    {
        HideDialogBox();
    }

    void HideDialogBox()
    {
        gameObject.SetActive(false);
        if (mDialogMenu != null)
        {
            mDialogMenu.HideDialogBox();
        }
        mCurrentTime = 0.0f;
        mGameAction.PlayerBasic.Interact.performed -= HideInput;
    }
}
