﻿///-------------------------------------------------------------------------------------------------
// File: ModularControls.cs
//
// Author: Dakshvir Singh Rehill
// Date: 24/6/2020
//
// Summary:	Button to change control scheme of input
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ModularControls : MonoBehaviour,ISelectHandler
{
    [Tooltip("The Rect Transform of the Parent Object")]
    public RectTransform mSelfTransform;
    [Tooltip("The Button component of this control object")]
    public Button mButton;
    [Tooltip("The Name of the Action Pressed")]
    public TextMeshProUGUI mActionName;
    [Tooltip("The text component of the control")]
    public TextMeshProUGUI mControlTxt;
    [Tooltip("The Image Component of the Control")]
    public Image mControlImg;
    [HideInInspector] public int mIndex = -1;
    [HideInInspector] public ControlsMenu mControlsMenu;

    public void OnSelect(BaseEventData eventData)
    {
        mControlsMenu.mActiveIndex = mIndex;
    }
}
