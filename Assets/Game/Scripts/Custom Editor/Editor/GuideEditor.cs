﻿///-------------------------------------------------------------------------------------------------
// File: GuideEditor.cs
//
// Author: Aditya Dinesh
// Date: 28/7/2020
//
// Summary:	Custom Inspector display for Guide
///-------------------------------------------------------------------------------------------------

using UnityEditor;

[CustomEditor(typeof(Guide))]
public class GuideEditor : Editor
{
    SerializedProperty mControlType;
    SerializedProperty mNPCDialogue;
    SerializedProperty mNextSceneTrigger;
    SerializedProperty mGuideAnimator;
    SerializedProperty mFSMInstance;
    SerializedProperty mIdleFSMTriggerName;
    SerializedProperty mConvFSMTriggerName;

    Guide mGuide = null;

    private void OnEnable()
    {
        mGuide = target as Guide;

        mControlType = serializedObject.FindProperty("mControlType");
        mNPCDialogue = serializedObject.FindProperty("mNPCDialogue");
        mNextSceneTrigger = serializedObject.FindProperty("mNextSceneTrigger");
        mGuideAnimator = serializedObject.FindProperty("mGuideAnimator");
        mFSMInstance = serializedObject.FindProperty("mFSMInstance");
        mIdleFSMTriggerName = serializedObject.FindProperty("mIdleFSMTriggerName");
        mConvFSMTriggerName = serializedObject.FindProperty("mConvFSMTriggerName");
    }

    public override void OnInspectorGUI()
    {
        if (mGuide != null)
        {
            EditorGUILayout.PropertyField(mControlType);
            EditorGUILayout.PropertyField(mNPCDialogue);
            EditorGUILayout.PropertyField(mNextSceneTrigger);
            EditorGUILayout.PropertyField(mGuideAnimator);
            EditorGUILayout.PropertyField(mFSMInstance);
            EditorGUILayout.PropertyField(mIdleFSMTriggerName);
            EditorGUILayout.PropertyField(mConvFSMTriggerName);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
