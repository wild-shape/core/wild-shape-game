﻿///-------------------------------------------------------------------------------------------------
// File: SceneTransitionTriggerable.cs
//
// Author: Dakshvir Singh Rehill
// Date: 25/6/2020
//
// Summary:	Script for triggering the next scene to load after unloading this scene
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneTransitionTriggerable : MonoBehaviour
{
    [Tooltip("The index of the scene that needs to load when player enters this trigger")]
    [SerializeField] int mSceneIndex;
    [Tooltip("Boolean to determine whether the scene transition is active or not")]
    public bool mActive = true;
    /// <summary>
    /// Whether the trigger has been entered or not
    /// </summary>
    [HideInInspector] bool mTriggered = false;

    public void TriggerEnter(Collider pCollider)
    {
        if(!mActive)
        {
            return;
        }
        if(mTriggered)
        {
            return;
        }
        if(pCollider.GetComponent<PlayerCharacter>())
        {
            GameManager.Instance.LoadNextScene(mSceneIndex); //load scene
            mTriggered = true;
        }
    }
}
