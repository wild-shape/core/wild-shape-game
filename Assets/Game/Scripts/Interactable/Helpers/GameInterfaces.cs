﻿///-------------------------------------------------------------------------------------------------
// File: GameInterfaces.cs
//
// Author: Dakshvir Singh Rehill
// Date: 30/5/2020
//
// Summary:	This file consists of some interfaces and enums that have been used in the game
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Any object that can be freezed using Bear AOE Ability
/// </summary>
public interface IFreezable
{
    /// <summary>
    /// Boolean value which is true in case of the object being frozen
    /// </summary>
    bool mFrozen { get; set; }
}

/// <summary>
/// Any object that can be controlled by pressure plate press and release
/// </summary>
public interface IButtonControllable
{
    /// <summary>
    /// Function called to press a pressure controlled button
    /// </summary>
    /// <param name="pControllingPlate">The pressure plate that was pressed</param>
    void ButtonDown(PressurePlate pControllingPlate);
    /// <summary>
    /// Function called when pressure controlled button is unpressed
    /// </summary>
    /// <param name="pControllingPlate">The pressure plate that was unpressed</param>
    void ButtonUp(PressurePlate pControllingPlate);
}

/// <summary>
/// Enum to check if Button is Pressed, UnPressed or Idle
/// </summary>
public enum ButtonControllableState
{
    Idle,
    Pressed,
    Unpressed
}
/// <summary>
/// Objects that can be deactivated by bear deactivate
/// </summary>
public interface IDeactivatable
{
    /// <summary>
    /// Boolean value that is true when object is deactivated
    /// </summary>
    bool mDeactivate { get; set; }
}