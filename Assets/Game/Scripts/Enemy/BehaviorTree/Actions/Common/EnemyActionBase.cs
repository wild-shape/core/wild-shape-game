///-------------------------------------------------------------------------------------------------
// File: EnemyActionBase.cs
//
// Author: Dakshvir Singh Rehill
// Date: 8/6/2020
//
// Summary:	Action Base for both enemies
///-------------------------------------------------------------------------------------------------
using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public abstract class EnemyActionBase : Action
{
	/// <summary>
	/// The Instance of Enemy Monobehavior
	/// </summary>
	protected Enemy mEnemyInstance;

	public override void OnStart()
	{
		if(mEnemyInstance == null)
        {
			mEnemyInstance = GetComponent<Enemy>();
        }
	}

	public override TaskStatus OnUpdate()
	{
		return TaskStatus.Success;
	}
}