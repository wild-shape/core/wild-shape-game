﻿///-------------------------------------------------------------------------------------------------
// File: BearAOE.cs
//
// Author: Dakshvir Singh Rehill
// Date: 3/6/2020
//
// Summary:	Script Responsible for spawning AOE Freeze
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class BearAOE : SpecialAbility
{
    /// <summary>
    /// The current AOE freeze in the game
    /// </summary>
    AOEFreeze mActiveFreeze = null;
    /// <summary>
    /// Whether AOE freeze has been triggered or not
    /// </summary>
    bool mAOETriggered = false;
    [Tooltip("The Sound Effect for AOE Mode")]
    [SerializeField] string mAOEModeSFX = "AOEMode";
    [Tooltip("The Sound Effect for AOE Freeze")]
    [SerializeField] string mAOEFreezeSFX = "AOE";

    void OnDisable() //for protection against using ability when shapeshifting
    {
        if (mAbilityMode)
        {
            AbilityCanceled(new InputAction.CallbackContext());
        }
    }


    protected override void PerformAbility(InputAction.CallbackContext pObj)
    {
        mAOETriggered = false;
        GameObject aFreezeObject = ObjectPoolingManager.Instance.GetPooledObject(mAbilityPool.mPoolName);
        aFreezeObject.transform.position = transform.position;
        aFreezeObject.transform.rotation = transform.rotation;
        mActiveFreeze = aFreezeObject.GetComponent<AOEFreeze>();
        mActiveFreeze.mCharacter = mCharacter;
        mActiveFreeze.StartAOE();
        if (!string.IsNullOrEmpty(mAOEModeSFX))
        {
            AudioManager.Instance.PlaySound(mAOEModeSFX, true);
        }
    }

    protected override void AbilityCanceled(InputAction.CallbackContext pObj)
    {
        if(!mAbilityMode)
        {
            return;
        }
        base.AbilityCanceled(pObj);
        if(!mAOETriggered)
        {
            mActiveFreeze.ResetAOE();
        }
        mActiveFreeze = null;
        if (!string.IsNullOrEmpty(mAOEModeSFX) && AudioManager.Instance.IsSoundPlaying(mAOEModeSFX))
        {
            AudioManager.Instance.StopSound(mAOEModeSFX);
        }
    }

    protected override void ActivateAbility(InputAction.CallbackContext pObj)
    {
        if (!mAbilityMode)
        {
            return;
        }
        base.ActivateAbility(pObj);
        //AnalyticsManager.Instance.mPlayerAnalytics.mFreezeUsed++;
        mAOETriggered = true;
        if (!string.IsNullOrEmpty(mAOEFreezeSFX))
        {
            if(!string.IsNullOrEmpty(mAOEModeSFX) && AudioManager.Instance.IsSoundPlaying(mAOEModeSFX))
            {
                AudioManager.Instance.StopSound(mAOEModeSFX);
            }
            AudioManager.Instance.PlaySound(mAOEFreezeSFX);
        }
        mActiveFreeze.AOEAnimStart();
        AbilityCanceled(pObj);
    }

}