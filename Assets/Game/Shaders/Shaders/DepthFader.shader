Shader "WildShape/DepthFade"
{
	Properties
	{
		_DepthFade("Depth Fade", Range(0,1)) = 0
		_Color("Color", Color) = (1,1,1,1)
		_MaxHeight("Height", Range(1,100)) = 11
		_FadePercent("Fade", Range(0,1)) = 0.5
		_LowestColorVal("Lowest Color Position", vector) = (0,0,0,0) // The location of the lowest color value - will be set by script
	}

		SubShader
	{
		Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha
			LOD 200

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			uniform fixed _MaxHeight;
			uniform fixed _FadePercent;
			uniform fixed _DepthFade;
			uniform fixed4 _Color;
			uniform fixed4 _LowestColorVal;
			// Input to vertex shader
			struct vertexInput
			{
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};
			// Input to fragment shader
			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float4 position_in_world_space : TEXCOORD0;
				float4 tex : TEXCOORD1;
			};

			// VERTEX SHADER
			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;
				output.pos = UnityObjectToClipPos(input.vertex);
				output.position_in_world_space = mul(unity_ObjectToWorld, input.vertex);
				output.tex = input.texcoord;
				return output;
			}

			// FRAGMENT SHADER
			float4 frag(vertexOutput input) : COLOR
			{
				float4 aColor = _Color;
				if (_DepthFade < 1)
				{
					aColor.a = 1 - _FadePercent * (1- (input.position_in_world_space.y - _LowestColorVal.y) / _MaxHeight);
				}
				else
				{
					aColor.a = 1 - _FadePercent * ((input.position_in_world_space.y - _LowestColorVal.y) / _MaxHeight);
				}
				return aColor;
		}

		ENDCG
			}
	}
		FallBack "Diffuse"
}