﻿///-------------------------------------------------------------------------------------------------
// File: DialogMenu.cs
//
// Author: Dakshvir Singh Rehill, Adtya Dinesh
// Date: 24/6/2020
//
// Summary:	Menu class for multiple dialog boxes that can be opened throughout the game
///-------------------------------------------------------------------------------------------------
using UnityEngine;

public class InGameDialogMenu : Menu
{

    [HideInInspector] public DialogBoxStrings mDialogStrings;
    public InGameDialogBox mNPCDialogBoxUI;
    public InGameDialogBox mHUDPromptDialogBox;
    public CollectableDialogBox mCollectableHUDPromptDialogBox;
    [Tooltip("The Sfx for Dialogue Box Opens")]
    [SerializeField] string mOpenDialogueSFX = "DialogueBoxOpens";
    [Tooltip("The Sfx for Dialogue Box Closes")]
    [SerializeField] string mCloseDialogueSFX = "DialogueBoxClose";


    /// <summary>
    /// Function called to display NPC dialog box with the set dialog strings
    /// </summary>
    public void ShowNPCDialogueBox()
    {
        mNPCDialogBoxUI.ShowDialog(mDialogStrings);
        MenuManager.Instance.ShowMenu(mMenuClassifier);
    }

    /// <summary>
    /// Function called to display HUD Prompt dialog box with the set dialog strings
    /// </summary>
    /// <param name="pTutorialInfo">The strings that need to be set in the dialog box</param>
    public void ShowHUDPromptDialogueBox(TutorialInfo pTutorialInfo)
    {
        mHUDPromptDialogBox.ShowInfo(pTutorialInfo);
        MenuManager.Instance.ShowMenu(mMenuClassifier);
        if (!string.IsNullOrEmpty(mOpenDialogueSFX))
        {
            AudioManager.Instance.PlaySound(mOpenDialogueSFX);
        }
    }

    /// <summary>
    /// Function called to display Collectaable HUD Prompt dialog box with the set dialog strings
    /// </summary>
    /// <param name="pCollectableInfo">The strings that need to be set in the dialog box</param>
    public void ShowCollectableHUDPromptDialogueBox(int pID)
    {
        mCollectableHUDPromptDialogBox.ShowInfo(this, pID);
        MenuManager.Instance.ShowMenu(mMenuClassifier);
    }

    /// <summary>
    /// Hide Dialog Box and Dialog box menu using this function
    /// </summary>
    public void HideDialogBox()
    {
        mNPCDialogBoxUI.gameObject.SetActive(false);
        mHUDPromptDialogBox.gameObject.SetActive(false);
        mCollectableHUDPromptDialogBox.gameObject.SetActive(false);
        MenuManager.Instance.HideMenu(mMenuClassifier);
        if (!string.IsNullOrEmpty(mCloseDialogueSFX))
        {
            AudioManager.Instance.PlaySound(mCloseDialogueSFX);
        }
    }
}
