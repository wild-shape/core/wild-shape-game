﻿///-------------------------------------------------------------------------------------------------
// File: HUDEffectsMenu.cs
//
// Author: Dakshvir Singh Rehill
// Date: 11/6/2020
//
// Summary:	Menu for Vignette of Player
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDEffectsMenu : Menu
{
    [Tooltip("The Freeze Color if player is frozen")]
    [SerializeField] Color mFreezeColor;
    [Tooltip("The color of vignette for other effects")]
    [SerializeField] Color mEffectColor;
    [Tooltip("The Vignette image")]
    [SerializeField] Image mVignetteImage;
    [Tooltip("The time to which no ability effect to be shown")]
    [SerializeField] float mShowNoAbilityEffectTime = 0.5f;
    public void SetFreeze()
    {
        mVignetteImage.color = mFreezeColor;
        MenuManager.Instance.ShowMenu(mMenuClassifier);
    }

    public void SetNoAbility()
    {
        mVignetteImage.color = mEffectColor;
        if(LeanTween.isTweening(gameObject))
        {
            LeanTween.cancel(gameObject);
        }
        MenuManager.Instance.ShowMenu(mMenuClassifier);
        LeanTween.value(gameObject, 0, 1, mShowNoAbilityEffectTime).setOnComplete(() => MenuManager.Instance.HideMenu(mMenuClassifier));
    }

}
