﻿///-------------------------------------------------------------------------------------------------
// File: RangedEnemy.cs
//
// Author: Dakshvir Singh Rehill
// Date: 26/6/2020
//
// Summary:	Inherits Enemy but also stores some ranged specific variables
///-------------------------------------------------------------------------------------------------
using BehaviorDesigner.Runtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedEnemy : Enemy
{
    [Header("Ranged Specific Assignments")]
    [Tooltip("The Transform that actually rotates around in the Y Axis")]
    public Transform mYRotationPvt;
    [Tooltip("The Transform that actually rotates around in the XZ Axis")]
    public Transform mXZRotationPvt;
    [Tooltip("The object at the end of the laser that actually freezes the enemy")]
    public LaserEndPoint mLaserPtr;
    [Tooltip("Inspect Point parent for Ranged Enemy")]
    public GameObject mInspectPointParent;
    [Tooltip("Enemies that the Ranged Enemy can Alert if it spots the enemy")]
    public List<Enemy> mEnemiesToAlert;
    [Tooltip("Alerted Alpha for Laser")]
    [SerializeField] float mAlertedAlpha = 1.0f;
    [Tooltip("Normal Alpha for Laser")]
    [SerializeField] float mNormalAlpha = 0.75f;
    /// <summary>
    /// Boolean to check if enemy can see player
    /// </summary>
    SharedBool mCanSeePlayer;
    /// <summary>
    /// The maximum distance this enemy can see
    /// </summary>
    float mMaxDistance;

    protected override void Start()
    {
        base.Start();
        mCanSeePlayer = (SharedBool)mTree.GetVariable("mCanSee");
        mMaxDistance = float.MinValue;
        foreach (VisionConePoint aPoint in mVisionCones)
        {
            if (aPoint.mMaxViewingDistance > mMaxDistance)
            {
                mMaxDistance = aPoint.mMaxViewingDistance;
            }
        }
    }

    protected override void Update()
    {
        base.Update();
        if(!mDeactivate)
        {
            if (!mCanSeePlayer.Value)
            {
                mLaserPtr.gameObject.transform.position = mOrigin.position;
            }
        }
    }

    protected override void ChangeEnemyRenderColor(Color pColor, Color pConeColor)
    {
        base.ChangeEnemyRenderColor(pColor, pConeColor);
        pConeColor.a = (mCanSeePlayer.Value ? mAlertedAlpha : mNormalAlpha) * pConeColor.a;
    }

}
