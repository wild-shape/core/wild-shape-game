﻿///-------------------------------------------------------------------------------------------------
// File: LoadSequenceSettings.cs
//
// Author: Dakshvir Singh Rehill
// Date: 29/6/2020
//
// Summary:	Scriptable Asset used in Loading Sequence Editor Window
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadSequenceSettings : ScriptableObject
{
    [Tooltip("The Path to the core scene")]
    public SceneReference mCoreScenePath;
    [Tooltip("The current debug sequence that is set")]
    public LoadingSequence mCurrentLoadingSequence;
    [Tooltip("The final game sequence that needs to be reset")]
    public LoadingSequence mGameLoadingSequence;
}