﻿///-------------------------------------------------------------------------------------------------
// File: DoorLaserEffect.cs
//
// Author: Dakshvir Singh Rehill
// Date: 14/07/2020
//
// Summary:	This script handles the switching on and off of the laser that blocks certain places
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserEffectObject : MonoBehaviour
{
    [Tooltip("The Line Renderer Reference for the laser objects")]
    [SerializeField] LineRenderer mLaserObj;
    [Tooltip("The Start Point of the laser object")]
    [SerializeField] Transform mStartPoint;
    [Tooltip("The End Point of the laser object")]
    [SerializeField] Transform mEndPoint;
    [Tooltip("Laser On/Off Time")]
    [SerializeField] float mEffectTime = 0.5f;
    [Tooltip("Damaged Laser Particle Effect")]
    [SerializeField] List<ParticleSystem> mDamagedLasers;
    /// <summary>
    /// Boolean added to ensure that effect isn't called twice
    /// </summary>
    bool mActive = false;
    /// <summary>
    /// Reference to parent laser object to allow for deactivating and activating laser
    /// </summary>
    [HideInInspector] public DamageLaser mLaserParent;
    /// <summary>
    /// The Positions of the laser line renderer
    /// </summary>
    Vector3[] mLaserPoints;

    void Start()
    {
        mLaserObj.positionCount = 0;
        mLaserObj.transform.localPosition = mStartPoint.transform.localPosition;
        mLaserObj.positionCount = 2;
        mLaserPoints = new Vector3[]
        {
            Vector3.zero,
            Vector3.zero
        };
        if(!mActive)
        {
            ActivateDamagedLaser();
        }
    }

    void Update()
    {
        if(mLaserParent == null)
        {
            return;
        }
        if (!mActive && !mLaserParent.mDeactivate)
        {
            mActive = true;
            ChangeLaserState();
        }
        else if (mActive && mLaserParent.mDeactivate)
        {
            mActive = false;
            ChangeLaserState();
        }
    }

    /// <summary>
    /// Called when active state of the laser is changed
    /// </summary>
    void ChangeLaserState()
    {
        if(LeanTween.isTweening(gameObject))
        {
            LeanTween.cancel(gameObject);
        }
        float aFinalPos = mActive ? mEndPoint.localPosition.z - mStartPoint.localPosition.z : 0.0f;
        LeanTween.value(gameObject, mLaserPoints[1].z, aFinalPos, mEffectTime).
            setOnUpdate(ChangeLaserSecondPos).setOnComplete(ActivateDamagedLaser);
    }
    /// <summary>
    /// Function that changes the position of the laser end point according to the leantween effect
    /// </summary>
    /// <param name="pZPos"></param>
    void ChangeLaserSecondPos(float pZPos)
    {
        mLaserPoints[1] = new Vector3(mLaserPoints[1].x, mLaserPoints[1].y, pZPos);
        mLaserObj.SetPositions(mLaserPoints);
    }

    void ActivateDamagedLaser()
    {
        if(!mActive)
        {
            foreach(ParticleSystem aParticle in mDamagedLasers)
            {
                aParticle.Play();
            }
        }
    }

}
