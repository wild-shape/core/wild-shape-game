﻿///-------------------------------------------------------------------------------------------------
// File: CheckpointManager.cs
//
// Author: Aditya Dinesh
// Date: 11/6/2020
//
// Summary:	Singleton Class for Storing and Retrieving persistent data
///-------------------------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;


public class SaveGameManager : Singleton<SaveGameManager>
{
    [SerializeField] string mSaveGameFilename = "PlayerData.json";
    [SerializeField] string mSettingsFilename = "Settings.json";
    [HideInInspector] public bool mLoadExists = false;
    [HideInInspector] public bool mSettingsSaved = false;
    /// <summary>
    /// Loads the player data from saved file
    /// </summary>
    public void LoadPlayerData()
    {
        if(GameManager.Instance.mGameLoadingSequence.mDebugMode)
        {
            return;
        }
        //// Check if a save file exists to load data
        string aPath = Application.persistentDataPath + "/" + mSaveGameFilename;
        try
        {
            using (StreamReader aReader = new StreamReader(aPath))
            {
                string aJsonString = aReader.ReadToEnd();
                GameManager.Instance.mGameData = JsonUtility.FromJson<GameData>(aJsonString);
                GameManager.Instance.mGameData.UpdateDataVersion();
                mLoadExists = true;
            }
        }
        catch (Exception aE)
        {
            Debug.LogFormat("No Save File Exists\n{0}:{1}", aE.Message, aE.StackTrace);
        }
       
    }
    /// <summary>
    /// Saves the player data
    /// </summary>
    public void SavePlayerData()
    {
        //placed to ensure easy debugging
        if (GameManager.Instance.mGameLoadingSequence.mDebugMode)
        {
            return;
        }
        try
        {
            // Check if a save file exists and store the data. If a save file doesn't exist then create a new one
            string aPath = Application.persistentDataPath + "/" + mSaveGameFilename;
            using (StreamWriter aWriter = new StreamWriter(aPath))
            {
                string aPlayerJson = JsonUtility.ToJson(GameManager.Instance.mGameData, true);
                aWriter.WriteLine(aPlayerJson);
            }
        }
        catch (Exception aE)
        {
            Debug.LogErrorFormat("Couldn't Save File\n {0}:{1}", aE.Message, aE.StackTrace);
        }
    }

    /// <summary>
    /// Load Game Settings from Settings file
    /// </summary>
    public void LoadSettingsData()
    {
        //// Check if a save file exists to load data
        string aPath = Application.persistentDataPath + "/" + mSettingsFilename;
        try
        {
            using (StreamReader aReader = new StreamReader(aPath))
            {
                string aJsonString = aReader.ReadToEnd();
                GameManager.Instance.mSettingsData = JsonUtility.FromJson<GameSettingsData>(aJsonString);
                mSettingsSaved = true;
            }
        }
        catch (Exception aE)
        {
            Debug.LogFormat("No Save File Exists\n{0}:{1}", aE.Message, aE.StackTrace);
        }

    }
    /// <summary>
    /// Save Game Settings from Settings file
    /// </summary>
    public void SaveSettingsData()
    {
        try
        {
            // Check if a save file exists and store the data. If a save file doesn't exist then create a new one
            string aPath = Application.persistentDataPath + "/" + mSettingsFilename;
            using (StreamWriter aWriter = new StreamWriter(aPath))
            {
                string aPlayerJson = JsonUtility.ToJson(GameManager.Instance.mSettingsData, true);
                aWriter.WriteLine(aPlayerJson);
                mSettingsSaved = true;
            }
        }
        catch (Exception aE)
        {
            Debug.LogErrorFormat("Couldn't Save File\n {0}:{1}", aE.Message, aE.StackTrace);
        }
    }


}
