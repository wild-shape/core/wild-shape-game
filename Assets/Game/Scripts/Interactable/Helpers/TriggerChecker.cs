﻿///-------------------------------------------------------------------------------------------------
// File: TriggerChecker.cs
//
// Author: Dakshvir Singh Rehill
// Date: 17/6/2020
//
// Summary:	Sends callbacks for each trigger enter
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerChecker : MonoBehaviour
{
    [Tooltip("All Events that need to be called when collider enters this trigger")]
    [SerializeField] List<TriggerEvent> mTriggerEnterEvents;
    [Tooltip("All Events that need to be called when collider stays in this trigger")]
    [SerializeField] List<TriggerEvent> mTriggerStayEvents;
    [Tooltip("All Events that need to be called when collider exits this trigger")]
    [SerializeField] List<TriggerEvent> mTriggerExitEvents;
    void OnTriggerEnter(Collider pOther)
    {
        if(mTriggerEnterEvents != null)
        {
            foreach(TriggerEvent aEvent in mTriggerEnterEvents)
            {
                aEvent.Invoke(pOther);
            }
        }
    }

    void OnTriggerStay(Collider pOther)
    {
        if(mTriggerStayEvents != null)
        {
            foreach(TriggerEvent aEvent in mTriggerStayEvents)
            {
                aEvent.Invoke(pOther);
            }
        }
    }
    void OnTriggerExit(Collider pOther)
    {
        if (mTriggerExitEvents != null)
        {
            foreach (TriggerEvent aEvent in mTriggerExitEvents)
            {
                aEvent.Invoke(pOther);
            }
        }
    }

}
