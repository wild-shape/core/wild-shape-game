﻿///-------------------------------------------------------------------------------------------------
// File: RangedAttack.cs
//
// Author: Dakshvir Singh Rehill
// Date: 11/6/2020
//
// Summary:	Handles immobilize and alert logic of Ranged Enemy
///-------------------------------------------------------------------------------------------------
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

[TaskCategory("Enemy/Ranged")]
[TaskDescription("Handles immobilize and alert logic of ranged enemy")]
public class RangedAttack : EnemyActionBase
{
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The Timer that is set once enemy freeze player")]
    [SerializeField] SharedFloat mFreezeTimer;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The boolean that is true if enemy has shot player")]
    [SerializeField] SharedBool mShotPlayer;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The boolean that toggles between allowing and not allowing the enemy to reshoot")]
    [SerializeField] SharedBool mAllowReshoot = false;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The boolean that is true if enemy can see player")]
    [SerializeField] SharedBool mCanSee;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The minimum distance of the laser from the player")]
    [SerializeField] float mMinLaserDistance = 0.5f;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The last known position of the player")]
    [SerializeField] SharedVector3 mPlayerLastKnown;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The delay between the enemy reshooting the player after unfreeze")]
    [SerializeField] SharedFloat mShootDelay = 1.0f;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The delay that the enemy waits for after detecting the player")]
    [SerializeField] SharedFloat mFirstShotDelay = 1.0f;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The speed of the laser pointer for covering the distance to the player")]
    [SerializeField] SharedFloat mLaserSpeed = 5.0f;
    ///<summary>
    ///The current delay to wait for before shooting player
    ///</summary>
    float mCurrentShootDelay = 0.0f;
    /// <summary>
    /// The active delay that is compared to the current delay to shoot player
    /// </summary>
    float mActiveShootDelay = 0.0f;
    /// <summary>
    /// The instance of the ranged enemy
    /// </summary>
    RangedEnemy mRangedInstance;
    public override void OnStart()
    {
        base.OnStart();
        if (mRangedInstance == null)
        {
            mRangedInstance = (RangedEnemy)mEnemyInstance;
            mActiveShootDelay = mFirstShotDelay.Value;
        }
    }

    public override TaskStatus OnUpdate()
    {
        if (mShotPlayer.Value) //if shot player remove laser alert enemies and check for the frozen timer
        {
            if (mRangedInstance.mLaserPtr.mActive)
            {
                mCurrentShootDelay = 0.0f;
                mRangedInstance.mLaserPtr.transform.position = mRangedInstance.mOrigin.position;
                mRangedInstance.mLaserPtr.mActive = false;
                if (mRangedInstance.mEnemiesToAlert != null)//if enemies to alert then alert them
                {
                    foreach (Enemy aEnemy in mRangedInstance.mEnemiesToAlert)
                    {
                        if (!aEnemy.mInspectPoints.Contains(mPlayerLastKnown.Value))
                        {
                            aEnemy.mInspectPoints.Add(mPlayerLastKnown.Value);
                        }
                    }
                }
            }
            if (mAllowReshoot.Value)//if can reshoot
            {
                mFreezeTimer.Value -= Time.deltaTime; //reduce the freeze timer
                if (mFreezeTimer.Value <= 0.0f)//set shot player false and reset shoot delay
                {
                    mFreezeTimer.Value = 0.0f;
                    mShotPlayer.Value = false;
                    mActiveShootDelay = mShootDelay.Value;
                }
            }
            return TaskStatus.Running;
        }
        if (mActiveShootDelay > mCurrentShootDelay) //if delay still left return
        {
            mCurrentShootDelay += Time.deltaTime;
            if (!mCanSee.Value)//if can't see player return failure
            {
                return TaskStatus.Failure;
            }
        }
        else //shoot player
        {
            if(GameManager.Instance.mGameData.mPlayerData.mPlayerHealth <= 0.0f) //if player dead don't attack
            {
                return TaskStatus.Failure;
            }
            if (!mRangedInstance.mLaserPtr.mActive) //start shooting player
            {
                mRangedInstance.mLaserPtr.mActive = true;
                //EnemyAnalyticsData aData = AnalyticsManager.Instance.mEnemyAnalytics[mEnemyInstance.mEnemyID];
                //aData.mTimesAttacked++;
                //AnalyticsManager.Instance.mEnemyAnalytics[mEnemyInstance.mEnemyID] = aData;
            }
            Vector3 aDistanceToPlayer = mPlayerLastKnown.Value - mRangedInstance.mLaserPtr.transform.position;
            if (aDistanceToPlayer.magnitude > mMinLaserDistance) //set line renderer points
            {
                mRangedInstance.mLaserPtr.gameObject.transform.position += aDistanceToPlayer.normalized * mLaserSpeed.Value * Time.deltaTime;
            }
        }
        return TaskStatus.Running;
    }

    public override void OnEnd() //reset all delays so that enemy is allowed to do a quick reshoot next time
    {
        mShotPlayer.Value = false;
        mActiveShootDelay = mFirstShotDelay.Value;
        mCurrentShootDelay = 0.0f;
        mRangedInstance.mLaserPtr.mActive = false;
    }

}
