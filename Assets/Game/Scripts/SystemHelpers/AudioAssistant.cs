﻿///-------------------------------------------------------------------------------------------------
// File: AudioAssitant.cs
//
// Author: Dakshvir Singh Rehill
// Date: 25/6/2020
//
// Summary:	Script attached to all 3D sounds in the game
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]
public class AudioAssistant : MonoBehaviour
{
    AudioSource mAudioSource;
    string mSourceID;
    [Tooltip("The type of audio group this assistant belongs to")]
    [SerializeField] AudioGroup mAudioGroup = AudioGroup.Sfx;
    void Awake()
    {
        mAudioSource = GetComponent<AudioSource>();
        if(mAudioSource == null || mAudioGroup == AudioGroup.None)
        {
            Destroy(this);
            return;
        }
        mSourceID = System.Guid.NewGuid().ToString();
        AudioManager.Instance.AddAmbientSource(mSourceID, mAudioGroup, mAudioSource);
    }

    void OnDestroy()
    {
        if(AudioManager.IsValidSingleton())
        {
            AudioManager.Instance.RemoveAmbientSource(mSourceID);
        }
    }

    /// <summary>
    /// Play the sound with the name of the clip
    /// </summary>
    /// <param name="pSoundId">Name of the clip</param>
    /// <param name="pLoop">Whether you want to loop it or not</param>
    /// <param name="pDelay">Delay before starting the clip</param>
    public void PlaySound(string pSoundId, bool pLoop = false, float pDelay = 0)
    {
        AudioManager.Instance.PlaySound(pSoundId, pLoop, mSourceID, pDelay);
    }
}
