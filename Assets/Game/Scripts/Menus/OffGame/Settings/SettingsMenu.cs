﻿///-------------------------------------------------------------------------------------------------
// File: SettingsMenu.cs
//
// Author: Dakshvir Singh Rehill
// Date: 23/6/2020
//
// Summary:	Menu responsible for opening input settings/ audio settings/etc
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenu : Menu
{
    [Tooltip("Button to show audio settings")]
    [SerializeField] Button mAudioBtn;
    [Tooltip("Button to show video settings")]
    [SerializeField] Button mVideoBtn;
    [Tooltip("Button to show control settings")]
    [SerializeField] Button mControlsBtn;
    [SerializeField] Button mBackBtn;
    /// <summary>
    /// The menu that opened this menu to allow to go back to previous menu
    /// </summary>
    [HideInInspector] public MenuClassifier mPreviousMenu;
    [Tooltip("Classifier for Audio Menu")]
    [SerializeField] MenuClassifier mAudioSettings;
    [Tooltip("Classifier for video menu")]
    [SerializeField] MenuClassifier mVideoSettings;
    [Tooltip("Classifier for Controls Menu")]
    [SerializeField] MenuClassifier mControlsSettings;


    void OnEnable()
    {
        mAudioBtn.Select();
        mAudioBtn.interactable = mVideoBtn.interactable = mControlsBtn.interactable = mBackBtn.interactable = true;
    }


    public void ShowAudioSettings()
    {
        mAudioBtn.interactable = false;
        MenuManager.Instance.HideMenu(mMenuClassifier);
        MenuManager.Instance.ShowMenu(mAudioSettings);
    }

    public void ShowVideoSettings()
    {
        mVideoBtn.interactable = false;
        MenuManager.Instance.HideMenu(mMenuClassifier);
        MenuManager.Instance.ShowMenu(mVideoSettings);
    }

    public void ShowControlsSettings()
    {
        mControlsBtn.interactable = false;
        MenuManager.Instance.HideMenu(mMenuClassifier);
        MenuManager.Instance.ShowMenu(mControlsSettings);
    }



    public void BackToPreviousMenu()
    {
        mBackBtn.interactable = false;
        MenuManager.Instance.HideMenu(mMenuClassifier);
        MenuManager.Instance.ShowMenu(mPreviousMenu);
    }


}
