﻿///-------------------------------------------------------------------------------------------------
// File: MovingPlatform.cs
//
// Author: Dakshvir Singh Rehill
// Date: 30/5/2020
//
// Summary:	Attached to freezable and button controllable moving platform
///-------------------------------------------------------------------------------------------------
using UnityEngine;

public class MovingPlatform : MonoBehaviour, IButtonControllable, IFreezable
{
    /// <summary>
    /// The Corner of the moving platform that holds the plate 
    /// and lerp point as well as speed and wait time
    /// </summary>
    [System.Serializable]
    public struct PlatformCorner
    {
        [Tooltip("Pressure Plate that controls this side of platform")]
        public PressurePlate mControllingPlate;
        [Tooltip("The location the platform should go to and wait on this side of platform")]
        public Transform mLerpToLocation;
        [Tooltip("The wait time after reaching this corner")]
        public float mCornerWaitTime;
        [Tooltip("The speed of platfrom going towards this corner")]
        public float mLerpToSpeed;
        [Tooltip("The speed of platform going away from this corner")]
        public float mLerpFromSpeed;

    }
    public bool mFrozen { get; set; }
    /// <summary>
    /// The controlling state of this controllable object
    /// </summary>
    ButtonControllableState mState = ButtonControllableState.Idle;
    [Header("Platform Lerping Variables")]
    [Tooltip("Max error in between platform and lerp to location")]
    [SerializeField] float mDistanceDelta = 0.2f;
    [Tooltip("Platform Corner for side A")]
    [SerializeField] PlatformCorner mLocationA;
    [Tooltip("Platform Corner for side B")]
    [SerializeField] PlatformCorner mLocationB;
    [Tooltip("Midpoint of the Platform")]
    [SerializeField] Transform mMidPoint;
    [Tooltip("The speed to return back to mid point from either corner")]
    [SerializeField] float mReturnToCenterSpeed = 10;
    /// <summary>
    /// to determine which side the platform is traveling to/from
    /// </summary>
    bool mIsLocationB = false;
    /// <summary>
    /// to determine whether platform reached a corner during pressed
    /// </summary>
    bool mReachedWhilePressed = false;
    /// <summary>
    /// The current wait time of this object on either side
    /// </summary>
    float mWaitTime = 0.0f;
    /// <summary>
    /// If player is on the object then this is the player reference
    /// </summary>
    PlayerCharacter mCurrentShape;

    void Start()
    {
        mLocationA.mControllingPlate.mControlledObjects.Add(this);
        mLocationB.mControllingPlate.mControlledObjects.Add(this);
    }

    void OnDestroy()
    {
        if (mLocationA.mControllingPlate != null)
        {
            mLocationA.mControllingPlate.mControlledObjects.Remove(this);
        }
        if (mLocationB.mControllingPlate != null)
        {
            mLocationB.mControllingPlate.mControlledObjects.Remove(this);
        }
    }



    void FixedUpdate()
    {
        switch (mState)
        {
            case ButtonControllableState.Idle: //return to mid point
                if (!mFrozen)
                {
                    if (mWaitTime <= 0.0f)
                    {
                        if (Move(mReturnToCenterSpeed, mMidPoint.position))
                        {
                            mCurrentShape = null;
                        }
                    }
                    else
                    {
                        mWaitTime -= Time.deltaTime;
                    }
                }
                break;
            case ButtonControllableState.Pressed: //lerp to selected corner and stay there
                if (!mFrozen)
                {
                    if (mIsLocationB)
                    {
                        mReachedWhilePressed = Move(mLocationB.mLerpToSpeed, mLocationB.mLerpToLocation.position);
                    }
                    else
                    {
                        mReachedWhilePressed = Move(mLocationA.mLerpToSpeed, mLocationA.mLerpToLocation.position);
                    }
                }
                break;
            case ButtonControllableState.Unpressed: //lerp to the other corner, stay there, lerp back to idle
                if (!mFrozen)
                {
                    if (mWaitTime <= 0.0f)
                    {
                        if (mIsLocationB)
                        {
                            if (Move(mLocationB.mLerpFromSpeed, mLocationA.mLerpToLocation.position))
                            {
                                if (mCurrentShape == null)
                                {
                                    mState = ButtonControllableState.Idle;
                                    //mWaitTime = mLocationA.mCornerWaitTime;
                                }
                            }
                        }
                        else
                        {
                            if (Move(mLocationA.mLerpFromSpeed, mLocationB.mLerpToLocation.position))
                            {
                                if (mCurrentShape == null)
                                {
                                    mState = ButtonControllableState.Idle;
                                    //mWaitTime = mLocationB.mCornerWaitTime;
                                }
                            }
                        }
                    }
                    else
                    {
                        mWaitTime -= Time.deltaTime;
                    }
                }
                break;
        }
    }

    /// <summary>
    /// Function to move the platform and current shape
    /// </summary>
    /// <param name="pSpeed">Current movement speed</param>
    /// <param name="pDestination">Current movement destination</param>
    /// <returns>true if destination reached</returns>
    bool Move(float pSpeed, Vector3 pDestination)
    {
        Vector3 aDistance = pDestination - transform.parent.position;
        if (aDistance.magnitude > mDistanceDelta)
        {
            transform.parent.position += aDistance.normalized * pSpeed * Time.deltaTime;
            if (mCurrentShape != null)
            {
                mCurrentShape.mRigidbody.MovePosition(mCurrentShape.transform.position + aDistance.normalized * pSpeed * Time.deltaTime);
            }
            return false;
        }
        return true;
    }

    private void OnTriggerEnter(Collider pCollider)
    {
        PlayerCharacter aCharacter = pCollider.GetComponent<PlayerCharacter>();
        if (aCharacter != null)
        {
            mCurrentShape = aCharacter;
        }
    }

    private void OnTriggerExit(Collider pCollider)
    {
        if (mCurrentShape != null && pCollider.gameObject.GetInstanceID() == mCurrentShape.gameObject.GetInstanceID())
        {
            mCurrentShape = null;
        }
    }

    public void ButtonDown(PressurePlate pControllingPlate)
    {
        mState = ButtonControllableState.Pressed;
        mIsLocationB = pControllingPlate == mLocationB.mControllingPlate;
        mReachedWhilePressed = false;
        mWaitTime = 0.0f;
    }

    public void ButtonUp(PressurePlate pControllingPlate)
    {
        if (!mReachedWhilePressed)
        {
            mState = ButtonControllableState.Idle;
        }
        else
        {
            mState = ButtonControllableState.Unpressed;
            mWaitTime = mIsLocationB ? mLocationB.mCornerWaitTime : mLocationA.mCornerWaitTime;
        }
    }
}
