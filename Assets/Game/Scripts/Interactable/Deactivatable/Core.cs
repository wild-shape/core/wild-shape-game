﻿///-------------------------------------------------------------------------------------------------
// File: Core.cs
//
// Author: Dakshvir Singh Rehill
// Date: 19/6/2020
//
// Summary:	Class responsible for main energy core of base as well as mini energy cores in the level
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class Core : MonoBehaviour, IDeactivatable
{
    [System.Serializable]
    public struct CoreBreakable
    {
        [Tooltip("The Object that will be moving on this bezier curve")]
        public GameObject mMovingObject;
        [Tooltip("All the bezier points on this curve")]
        public List<BezierPoint> mBezierPoints;
        [Tooltip("The Delay in starting of the breaking animation")]
        public float mStartDelay;
        [Tooltip("The Total time or speed this animation will play for or by")]
        public float mAnimationTimeOrSpeed;
        [Tooltip("Set this to true if you want to use speed over time")]
        public bool mUseSpeed;
    }

    [System.Serializable]
    public struct BezierPoint
    {
        [Tooltip("This is the point on the curve")]
        public Transform mPoint;
        [Tooltip("This is the point that controls the turn of the curve")]
        public Transform mControl;
    }
    [Tooltip("Affordance Trigger")]
    [SerializeField] InteractableAffordance mAffordanceTrigger;
    [Tooltip("Check if this Core is the Main Core")]
    [SerializeField] bool mIsMainCore;
    [Tooltip("All the deactivable triggers that can deactivate this core, if it is a mini core, add the main core deactivable as well")]
    [SerializeField] List<Deactivatable> mDeactivators;
    [Tooltip("Audio Assistant component for 3D Hit Sfx")]
    [SerializeField] AudioAssistant mHitAudioAssistant;
    [Tooltip("Audio Assistant component for 3D Explode Sfx")]
    [SerializeField] AudioAssistant mExplodeAudioAssistant;
    [Tooltip("Sound Effect for Core Hit")]
    [SerializeField] string mCoreHitSFX = "CoreHit";
    [Tooltip("3D Sound for Mini Core")]
    [SerializeField] string mMiniCoreOnSFX = "MiniCoreOn";
    [Tooltip("3D Sound for Main Core")]
    [SerializeField] string mMainCoreOnSFX = "MainCoreOn";
    [Tooltip("3D Sound for Core Explosion")]
    [SerializeField] string mCoreExplosionSFX = "MainCoreExplosion";
    [Tooltip("The Particle System that stays active when the core is active")]
    [SerializeField] ParticleSystem mCoreActiveFX;
    [Tooltip("The Particle Effect to Play when core gets deactivated")]
    [SerializeField] ParticleSystem mCoreDeactivateFX;
    [Tooltip("The Core Pieces that need to break along with their animation settings")]
    [SerializeField] List<CoreBreakable> mBreakablePieces;
    [Tooltip("Set this to true for debug draw of all bezier paths")]
    [SerializeField] bool mDebugDraw = false;
    bool mStartTimer;
    float mCurTime = 0;
    float mWaitTime = 0.5f;
    /// <summary>
    /// Used to allow check for credits show
    /// </summary>
    bool mDeactivated;
    public bool mDeactivate
    {
        get
        {
            return mDeactivated;
        }
        set
        {
            mDeactivated = value;
            if (mDeactivated)
            {
                if (!string.IsNullOrEmpty(mCoreHitSFX) && mHitAudioAssistant != null) //play sound if music is set
                {
                    mHitAudioAssistant.PlaySound(mCoreHitSFX);
                    mStartTimer = true;
                }
                if(mCoreActiveFX != null)
                {
                    mCoreActiveFX.Stop();
                    mCoreActiveFX.gameObject.SetActive(false);
                    mAffordanceTrigger.StopAffordance();
                }
                if(mCoreDeactivateFX != null)
                {
                    mCoreDeactivateFX.Play();
                }
                foreach (CoreBreakable aBreakablePiece in mBreakablePieces)
                {
                    if (aBreakablePiece.mMovingObject == null)
                    {
                        continue;
                    }
                    if (aBreakablePiece.mBezierPoints.Count % 2 != 0)
                    {
                        continue;
                    }
                    int aStartPoint = 0;
                    List<Vector3> aBezierPoints = new List<Vector3>();
                    while ((aStartPoint + 1) < aBreakablePiece.mBezierPoints.Count)
                    {
                        BezierPoint aStartP = aBreakablePiece.mBezierPoints[aStartPoint];
                        BezierPoint aEndP = aBreakablePiece.mBezierPoints[(aStartPoint + 1)];
                        aBezierPoints.Add(aStartP.mPoint.position);
                        aBezierPoints.Add(aEndP.mControl.position);
                        aBezierPoints.Add(aStartP.mControl.position);
                        aBezierPoints.Add(aEndP.mPoint.position);
                        aStartPoint += 2;
                    }
                    if (aBezierPoints.Count % 4 == 0)
                    {
                        GameObject aDestructableObject = aBreakablePiece.mMovingObject;
                        if (aBreakablePiece.mUseSpeed)
                        {
                            LeanTween.move(aDestructableObject, aBezierPoints.ToArray(), aBreakablePiece.mAnimationTimeOrSpeed)
                                     .setOnComplete(() =>
                                     {
                                         aDestructableObject.SetActive(false);
                                     }).setSpeed(aBreakablePiece.mAnimationTimeOrSpeed).setDelay(aBreakablePiece.mStartDelay);
                        }
                        else
                        {
                            LeanTween.move(aDestructableObject, aBezierPoints.ToArray(), aBreakablePiece.mAnimationTimeOrSpeed)
                                     .setOnComplete(() =>
                                     {
                                         aDestructableObject.SetActive(false);
                                     }).setDelay(aBreakablePiece.mStartDelay);

                        }
                    }
                }
            }
        }
    }



    void Start()
    {
        if (mDeactivators != null) //add deactivators to the deactivables
        {
            foreach (Deactivatable aDeactivator in mDeactivators)
            {
                aDeactivator.mDeactivatables.Add(this);
            }
        }

        if(!mDeactivate)
        {
            if(mIsMainCore)
            {
                if(!string.IsNullOrEmpty(mMainCoreOnSFX) && mHitAudioAssistant != null)
                {
                    mHitAudioAssistant.PlaySound(mMainCoreOnSFX, true);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(mMiniCoreOnSFX) && mHitAudioAssistant != null)
                {
                    mHitAudioAssistant.PlaySound(mMiniCoreOnSFX, true);
                }
            }
        }
    }

    private void Update()
    {
        if(mStartTimer)
        {
            mCurTime += Time.deltaTime;

            if(mCurTime >= mWaitTime)
            {
                if (!string.IsNullOrEmpty(mCoreExplosionSFX) && mExplodeAudioAssistant != null) 
                {
                    mExplodeAudioAssistant.PlaySound(mCoreExplosionSFX);
                }
                mStartTimer = false;
            }
        }
    }

    void OnDrawGizmos()
    {
        if (!mDebugDraw)
        {
            return;
        }
        if (mBreakablePieces.Count == 0)
        {
            return;
        }
        foreach (CoreBreakable aBreakablePiece in mBreakablePieces)
        {
            if (aBreakablePiece.mMovingObject == null)
            {
                continue;
            }
            if (aBreakablePiece.mBezierPoints.Count % 2 != 0)
            {
                continue;
            }
            int aStartPoint = 0;
            List<Vector3> aBezierPoints = new List<Vector3>();
            while ((aStartPoint + 1) < aBreakablePiece.mBezierPoints.Count)
            {
                BezierPoint aStartP = aBreakablePiece.mBezierPoints[aStartPoint];
                BezierPoint aEndP = aBreakablePiece.mBezierPoints[(aStartPoint + 1)];
                if (aStartP.mPoint == null || aStartP.mControl == null || aEndP.mPoint == null || aEndP.mControl == null)
                {
                    break;
                }
                aBezierPoints.Add(aStartP.mPoint.position);
                aBezierPoints.Add(aEndP.mControl.position);
                aBezierPoints.Add(aStartP.mControl.position);
                aBezierPoints.Add(aEndP.mPoint.position);
                DebugExtension.DrawPoint(aStartP.mPoint.position, Color.green);
                DebugExtension.DrawPoint(aEndP.mPoint.position, Color.green);
                DebugExtension.DrawPoint(aStartP.mControl.position, Color.red);
                DebugExtension.DrawPoint(aEndP.mControl.position, Color.red);
                Debug.DrawLine(aStartP.mPoint.position, aStartP.mControl.position, Color.white);
                Debug.DrawLine(aEndP.mPoint.position, aEndP.mControl.position, Color.white);
                aStartPoint += 2;
            }
            if (aBezierPoints.Count % 4 == 0)
            {
#if UNITY_EDITOR
                aStartPoint = 0;
                while ((aStartPoint + 3) < aBezierPoints.Count)
                {
                    Handles.DrawBezier(
                        aBezierPoints[aStartPoint],
                        aBezierPoints[aStartPoint + 3],
                        aBezierPoints[aStartPoint + 2],
                        aBezierPoints[aStartPoint + 1], Color.blue, null, 2f);
                    aStartPoint += 4;
                }
#endif
            }
        }
    }
}
