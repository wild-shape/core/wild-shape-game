﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleAvoidanceBehaviour : SteeringBehaviourBase
{
    [System.Serializable]
    public class Feeler
    {
        public float mDistance;
        public Vector3 mOffset;
    }

    [Header("Obstacle Avoidance Varaibles")]
    public List<Feeler> mFeelers = new List<Feeler>();
    public LayerMask mLayerMask;
    public bool mDebugDraw;
    Color mFeelerColor = Color.blue;
    public override Vector3 calculateForce()
    {
        RaycastHit aHit;
        Ray aRay;

        foreach (Feeler aFeeler in mFeelers)
        {
            Vector3 aFeelerPosition = transform.rotation * aFeeler.mOffset + transform.position;
            aRay = new Ray(aFeelerPosition, transform.forward);
            if (Physics.Raycast(aRay, out aHit, aFeeler.mDistance, mLayerMask, QueryTriggerInteraction.Ignore))
            {
                Vector3 aOtherPostion = aHit.point;
                Vector3 aCollisionPoint = Vector3.Project(aOtherPostion - transform.position, transform.forward) + transform.position;
                float aStrength = 1.0f + ((aCollisionPoint.magnitude - aFeeler.mDistance) / aFeeler.mDistance);
                mFeelerColor = Color.red;
                return (aCollisionPoint - aOtherPostion).normalized * aStrength;
            }
        }
        mFeelerColor = Color.blue;
        return Vector3.zero;
    }

    private void OnDrawGizmos()
    {
        if (mDebugDraw)
        {
            foreach (Feeler aFeeler in mFeelers)
            {
                Vector3 aFeelerPosition = transform.rotation * aFeeler.mOffset + transform.position;
                Debug.DrawLine(aFeelerPosition, transform.forward * aFeeler.mDistance + aFeelerPosition, mFeelerColor);
            }
        }
    }
}
