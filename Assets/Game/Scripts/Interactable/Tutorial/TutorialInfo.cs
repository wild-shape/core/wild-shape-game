﻿///-------------------------------------------------------------------------------------------------
// File: TutorialData.cs
//
// Author: Aditya Dinesh
// Date: 15/7/2020
//
// Summary:	Class containing tutorial data which will be displayed on HUD prompts for Tutorialization
///-------------------------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TutorialInfo
{
    public bool mShowControlImg;
    public Controls mMainControl;
    public string mTutorialBoxTitle;
    [TextArea(2, 5)]
    public string mMainControlText;
    public Controls mSecondaryControl;
    [TextArea(2, 5)]
    public string mSecondaryControlText;
}    
