﻿///-------------------------------------------------------------------------------------------------
// File: RaccoonBlink.cs
//
// Author: Dakshvir Singh Rehill
// Date: 3/6/2020
//
// Summary:	Responsbile for Handling the Blink Mechanic of Raccoon
///-------------------------------------------------------------------------------------------------
using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class RaccoonBlink : SpecialAbility
{
    [Header("Camera Settings")]
    [Tooltip("Target Group Averaging weight given to blink pos")]
    [SerializeField] float mBlinkPosWeight = 5.0f;
    [Tooltip("Target Group Averaging radius given to blink pos")]
    [SerializeField] float mBlinkPosRadius = 2.0f;
    [Header("Blink Settings")]
    [Tooltip("The maximum height around the character that the blink could go to")]
    [SerializeField] float mMaxBlinkableHeight = 3.1f;
    [Tooltip("Blink fixed radius")]
    [SerializeField] float mFixedBlinkRadius = 5.5f;
    [Tooltip("The Minimum blink distance from the Raccoon")]
    [SerializeField] float mMinimumRadius = 1.1f;
    [Tooltip("The Sound Effect for Blink Start")]
    [SerializeField] string mBlinkStartSFX = "Blinking";
    [Tooltip("The Sound Effect for Blink Execute")]
    [SerializeField] string mBlinkExecuteSFX = "Blink";
    /// <summary>
    /// The current active blink position
    /// </summary>
    Blink mCurrentBlinkable = null;
    /// <summary>
    /// The active target group cache value
    /// </summary>
    CinemachineTargetGroup mMainTargetGroup;

    void OnDisable() //for protection against using ability when shapeshifting
    {
        if (mAbilityMode)
        {
            AbilityCanceled(new InputAction.CallbackContext());
        }
    }

    protected override void Start()
    {
        base.Start();
        mMainTargetGroup = GameManager.Instance.mSceneHandler.mCameraController.mShapeShiftInst.mMainTargetGroup;
    }

    protected override void PerformAbility(InputAction.CallbackContext pObj)
    {
        GameObject aBlinkObj = ObjectPoolingManager.Instance.GetPooledObject(mAbilityPool.mPoolName);
        mCurrentBlinkable = aBlinkObj.GetComponent<Blink>();
        mCurrentBlinkable.mCharacter = mCharacter;
        aBlinkObj.transform.position = transform.position + transform.forward *
            mFixedBlinkRadius;
        aBlinkObj.transform.rotation = transform.rotation;
        List<CinemachineTargetGroup.Target> aTargets = new List<CinemachineTargetGroup.Target>();
        aTargets.AddRange(mMainTargetGroup.m_Targets);
        aTargets.Add(new CinemachineTargetGroup.Target()
        {
            radius = mBlinkPosRadius,
            weight = mBlinkPosWeight,
            target = aBlinkObj.transform
        });
        mMainTargetGroup.m_Targets = aTargets.ToArray();
        mCurrentBlinkable.mMaxBlinkableHeight = mMaxBlinkableHeight;
        mCurrentBlinkable.mRadius = mFixedBlinkRadius;
        mCurrentBlinkable.mMinimumRadius = mMinimumRadius;
        mCurrentBlinkable.StartBlink();

        if (!string.IsNullOrEmpty(mBlinkStartSFX))
        {
            AudioManager.Instance.PlaySound(mBlinkStartSFX);
        }
    }

    protected override void AbilityCanceled(InputAction.CallbackContext pObj)
    {
        if(!mAbilityMode)
        {
            return;
        }
        base.AbilityCanceled(pObj);
        mCurrentBlinkable.EndBlink();
        List<CinemachineTargetGroup.Target> aTargets = new List<CinemachineTargetGroup.Target>();
        aTargets.AddRange(mMainTargetGroup.m_Targets);
        int aTargetToRemove = -1;
        for (int aI = 0; aI < aTargets.Count; aI++)
        {
            if (aTargets[aI].target == mCurrentBlinkable.transform)
            {
                aTargetToRemove = aI;
                break;
            }
        }
        aTargets.RemoveAt(aTargetToRemove);
        mMainTargetGroup.m_Targets = aTargets.ToArray();
        ObjectPoolingManager.Instance.ReturnPooledObject(mAbilityPool.mPoolName, mCurrentBlinkable.gameObject);
        mCurrentBlinkable = null;
    }

    protected override void ActivateAbility(InputAction.CallbackContext pObj)
    {
        if(!mAbilityMode)
        {
            return;
        }
        if (!string.IsNullOrEmpty(mBlinkExecuteSFX))
        {
            AudioManager.Instance.PlaySound(mBlinkExecuteSFX);
        }
        base.ActivateAbility(pObj);
        mCharacter.transform.position = mCurrentBlinkable.transform.position;
        AbilityCanceled(pObj);
    }
}
