﻿///-------------------------------------------------------------------------------------------------
// File: GuideBaseState.cs
//
// Author: Dakshvir Singh Rehill
// Date: 22/7/2020
//
// Summary:	Base State Behaviour for all Guide State Behaviours
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GuideBaseState : StateMachineBehaviour
{
    protected Guide mGuideRef;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(mGuideRef == null)
        {
            mGuideRef = animator.gameObject.GetComponent<Guide>();
        }
    }
}
