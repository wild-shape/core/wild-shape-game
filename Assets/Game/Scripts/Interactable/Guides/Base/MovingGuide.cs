﻿///-------------------------------------------------------------------------------------------------
// File: MovingGuide.cs
//
// Author: Dakshvir Singh Rehill, Aditya Dinesh
// Date: 10/7/2020
//
// Summary:	Inherited script for Guide NPC who can walk using steering behaviours
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MovingGuide : Guide
{
    [Header("Moving Guide Settings")]
    [Tooltip("List of NPC Location Dialogs")]
    public List<NPCLocationDialog> mDynamicConversationDialogs;
    [Header("Steering Behavior Variables")]
    [Tooltip("Animation Speed for NPC")]
    [SerializeField] float mAnimSpeedMultiplier = 3.0f;
    [Tooltip("Animation Listener component of NPC")]
    [SerializeField] AnimationListener mAnimationListener;
    [Tooltip("The Rigidbody component of this NPC Guide")]
    [SerializeField] Rigidbody mRigidbody;
    [Tooltip("The Steering Agent component of this NPC Guide")]
    public SteeringAgent mSteeringAgent;
    [Tooltip("The Path Follow Steering Behavior of NPC Guide")]
    public PathFollowSteeringBehaviour mMovingBehaviour;
    [Header("Reward Assignments")]
    public GameObject mCollectible;
    [HideInInspector] public int mDynamicIndex = -1;
    [HideInInspector] public bool mMoving = false;
    void OnEnable()
    {
        mGuideAnimator.SetFloat("AnimSpeed", mAnimSpeedMultiplier);
    }

    protected override void Start()
    {
        base.Start();
        mInteractableBy = InteractableByType.Both;
        mAnimationListener.mOnAnimatorMove.AddListener(OnGuideAnimatorMove);
        if(mNPCDialogue.mSummarize)
        {
            for(int aI = 0; aI < mDynamicConversationDialogs.Count; aI ++)
            {
                NPCLocationDialog aDynamicDialog = mDynamicConversationDialogs[aI];
                aDynamicDialog.mDialogInfo.mSummarize = true;
                mDynamicConversationDialogs[aI] = aDynamicDialog;
            }
        }
        mDynamicIndex = 0;
        mNPCDialogue = mDynamicConversationDialogs[mDynamicIndex].mDialogInfo;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        if(mAnimationListener != null)
        {
            mAnimationListener.mOnAnimatorMove.RemoveListener(OnGuideAnimatorMove);
        }
    }

    void OnGuideAnimatorMove()
    {
        if(Time.deltaTime <= 0.0f)
        {
            return;
        }
        mRigidbody.isKinematic = !mMoving;
        Vector3 aVelocity = mGuideAnimator.deltaPosition / Time.deltaTime;
        aVelocity.y = mRigidbody.velocity.y;
        mRigidbody.velocity = aVelocity;
    }

    public override void EndConversation()
    {
        if (!mTalking)
        {
            return;
        }

        if(mDynamicIndex >= (mDynamicConversationDialogs.Count - 1))
        {
            base.EndConversation();
        }
        else
        {
            TalkingTriggerReset();
        }
    }

    protected override void NextConversation(InputAction.CallbackContext pContext)
    {
        if(!mMoving)
        {
            base.NextConversation(pContext);
        }
    }

}
