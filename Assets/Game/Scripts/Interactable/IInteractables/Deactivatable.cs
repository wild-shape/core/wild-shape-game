﻿///-------------------------------------------------------------------------------------------------
// File: Deactivatable.cs
//
// Author: Dakshvir Singh Rehill
// Date: 2/6/2020
//
// Summary:	Attached to enemy gameobjects and core to deactivate them
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Deactivatable : IInteractable
{
    [Header("Deactivate Animation Variables")]
    [Tooltip("The Deactivate trigger animator parameter name for the bear")]
    [SerializeField] string mDeactivateTrigger = "Deactivate";
    [Tooltip("The Deactivate speed animator parameter name for the bear")]
    [SerializeField] string mDeactivateSpeedFloat = "AttackSpeed";
    [Header("Deactivate Variables")]
    [Tooltip("The speed of the deactivate animation of the bear")]
    [SerializeField] float mDeactivateAnimSpeed = 3.0f;
    [Tooltip("Center Transform that the bear will aim the deactivation too")]
    [SerializeField] Transform mDeactivateTarget;
    [Tooltip("Max angle at which the bear can deactivate the object")]
    [SerializeField] float mAngle = 25;
    /// <summary>
    /// Things that can be deactivated by this deactivable
    /// </summary>
    public readonly List<IDeactivatable> mDeactivatables = new List<IDeactivatable>();

    /// <summary>
    /// Boolean to check if the deactivable has been deactivated
    /// </summary>
    bool mDeactivated = false;

    protected override void Start()
    {
        base.Start();
        //mInteractableID = AnalyticsManager.Instance.RegisterInteractable(InteractableType.Deactivatable);
        if (SaveGameManager.Instance.mLoadExists)
        {
            if (GameManager.Instance.mGameData.mBaseData.Count > 0) //deactivate if core is deactivated
            {
                foreach (BaseData aBD in GameManager.Instance.mGameData.mBaseData)
                {
                    if (Mathf.Abs(mDeactivateTarget.position.sqrMagnitude - aBD.mCoreID) < 1)
                    {
                        mDeactivated = true;
                        break;
                    }
                }
            }
        }
    }

    protected override void Update()
    {
        base.Update();
        if (mDeactivated && mDeactivatables.Count > 0) //if deactivated then deactivate all
        {
            if (!mDeactivatables[0].mDeactivate)
            {
                DeactivateAll();
            }
        }
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
    }

    protected override void OnTriggerEnter(Collider pOther)
    {
        if (mDeactivated)
        {
            return;
        }
        base.OnTriggerEnter(pOther);
        if (mActivePlayerCharacter == null)
        {
            return;
        }
        if (mActivePlayerCharacter.gameObject.GetInstanceID() != pOther.gameObject.GetInstanceID())
        {
            return;
        }
        if (mActivePlayerCharacter != null)
        {
            mActivePlayerCharacter.mGameAction.PlayerBasic.Interact.performed += DeactivateStarted;
        }
    }

    protected override void EndInteraction()
    {
        if (!mDeactivated)
        {
            mActivePlayerCharacter.mGameAction.PlayerBasic.Interact.performed -= DeactivateStarted;
        }
        base.EndInteraction();
    }

    /// <summary>
    /// Callback to when the interact button is rpessed for this deactivable
    /// </summary>
    /// <param name="pContext"></param>
    void DeactivateStarted(InputAction.CallbackContext pContext)
    {
        if (GameManager.Instance.mState == GameManager.State.Pause)
        {
            return;
        }
        Vector3 aDist = mDeactivateTarget.position - mActivePlayerCharacter.transform.position;
        aDist.y = mActivePlayerCharacter.transform.forward.y;
        if (Mathf.Abs(Vector3.Angle(mActivePlayerCharacter.transform.forward, aDist)) <= mAngle)
        {
            mActivePlayerCharacter.mGameAction.PlayerBasic.Interact.performed -= DeactivateStarted;
            mActivePlayerCharacter.mAnimator.SetFloat(mDeactivateSpeedFloat, mDeactivateAnimSpeed);
            mActivePlayerCharacter.mAnimator.SetTrigger(mDeactivateTrigger);
            mActivePlayerCharacter.mDeactivateObject.AddListener(DeactivateHandler);
        }
    }

    /// <summary>
    /// Function callback to the time the bear's paw hits the deactivable object
    /// </summary>
    /// <param name="pAnimID"></param>
    void DeactivateHandler(int pAnimID)
    {
        mActivePlayerCharacter.mDeactivateObject.RemoveListener(DeactivateHandler);
        DeactivateAll();
        mIsActive = false;
        EndInteraction();
    }

    /// <summary>
    /// Function used to deactivate all IDeactivable if Deactivatable deactivated
    /// </summary>
    void DeactivateAll()
    {
        foreach (IDeactivatable aDeactivable in mDeactivatables)
        {
            aDeactivable.mDeactivate = true;
        }
        if (mDeactivated) //if already deactivated don't save again
        {
            return;
        }
        mDeactivated = true;
        BaseData aBaseData = new BaseData
        {
            mCoreID = mDeactivateTarget.position.sqrMagnitude
        };
        GameManager.Instance.mGameData.mBaseData.Add(aBaseData);
        SaveGameManager.Instance.SavePlayerData();
    }

    protected override void CheckCanInteract()
    {
        base.CheckCanInteract();
        if (mActivePlayerCharacter == null)
        {
            return;
        }
        Vector3 aDist = mDeactivateTarget.position - mActivePlayerCharacter.transform.position;
        aDist.y = mActivePlayerCharacter.transform.forward.y;
        //can't interact
        if (Mathf.Abs(Vector3.Angle(mActivePlayerCharacter.transform.forward, aDist)) > mAngle)
        {
            mPromptsMenu.SetDisabledAffordance(true);
        }
        else
        {
            mPromptsMenu.SetEnabledAffordance();
        }
    }

}
