﻿///-------------------------------------------------------------------------------------------------
// File: OverlayMenu.cs
//
// Author: Dakshvir Singh Rehill
// Date: 27/7/2020
//
// Summary:	Menu base class that is inherited by MainMenu and PauseMenu to allow them to open other menus on top of them
///-------------------------------------------------------------------------------------------------
using Autodesk.Fbx;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OverlayMenu : Menu
{
    [Tooltip("The Selectable that needs to be selected when this menu is opened")]
    protected Selectable mInitSelectable;
    protected Selectable mCurrentSelected = null;
    MenuClassifier mCurrentSubMenu = null;
    public override void Start()
    {
        base.Start();
        if(mStartMenuState == StartMenuState.Active)
        {
            mInitSelectable.Select();
        }
    }

    /// <summary>
    /// Transfers ownership to the overlay menu
    /// </summary>
    /// <param name="pOverlayClassifier">The Menu Classifier that needs to be set as active</param>
    public virtual void OpenOverlay(MenuClassifier pOverlayClassifier, Selectable pSelected)
    {
        mCurrentSelected = pSelected;
        SubMenu aMenu = MenuManager.Instance.GetMenu<SubMenu>(pOverlayClassifier);
        if(aMenu == null)
        {
            Debug.LogErrorFormat("Menu Not a Sub Menu {0}", pOverlayClassifier.mMenuName);
            return;
        }
        aMenu.mInitSelectable.Select();
        aMenu.SetMenuActive(CloseOverlay);
    }
    /// <summary>
    /// Displays the Overlay but doesn't transfer ownership to the overlay menu
    /// </summary>
    /// <param name="pOverlayClassifier">The Menu Classifier that needs to be displayed</param>
    public virtual void DisplayOverlay(MenuClassifier pOverlayClassifier)
    {
        if(mCurrentSubMenu != null && mCurrentSelected != null)
        {
            SubMenu aMenu = MenuManager.Instance.GetMenu<SubMenu>(mCurrentSubMenu);
            if (aMenu == null)
            {
                Debug.LogErrorFormat("Menu Not a Sub Menu {0}", mCurrentSubMenu.mMenuName);
                return;
            }
            aMenu.BackButtonPressed();
            mCurrentSelected = null;
            HideOverlay(mCurrentSubMenu);
        }
        if(pOverlayClassifier == null)
        {
            return;
        }
        mCurrentSubMenu = pOverlayClassifier;
        MenuManager.Instance.ShowMenu(pOverlayClassifier);
    }
    /// <summary>
    /// Hides the Overlay when selection changes
    /// </summary>
    /// <param name="pOverlayClassifier">The Menu Classifier that needs to be hidden</param>
    public virtual void HideOverlay(MenuClassifier pOverlayClassifier)
    {
        if(mCurrentSelected != null)
        {
            return;
        }
        MenuManager.Instance.HideMenu(pOverlayClassifier);
    }

    public virtual void CloseOverlay()
    {
        mCurrentSelected.Select();
        mCurrentSelected = null;
    }

    public override void ShowMenu(string pOptions)
    {
        base.ShowMenu(pOptions);
        mInitSelectable.Select();
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public override void HideMenu(string pOptions)
    {
        base.HideMenu(pOptions);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
}
