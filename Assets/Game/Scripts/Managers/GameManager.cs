﻿///-------------------------------------------------------------------------------------------------
// File: GameManager.cs
//
// Author: Dakshvir Singh Rehill
// Date: 22/6/2020
//
// Summary:	Manager responsible for the game's transition i.e. circle of life
///-------------------------------------------------------------------------------------------------
using BehaviorDesigner.Runtime;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GameManager : Singleton<GameManager>
{
    /// <summary>
    /// Current state of the entire game
    /// </summary>
    [System.Serializable]
    public enum State
    {
        MainMenu,
        Game,
        Pause
    }

    [Tooltip("The Active Player Character at the start of the game")]
    public CharacterForm mPlayerForm = CharacterForm.Bear;
    [Header("Scene Assignments")]
    [Tooltip("Core Game Scene")]
    public SceneReference mCoreScene;
    [Tooltip("Game UI Scene")]
    [SerializeField] SceneReference mGameUIScene;
    [Tooltip("Loading Sequence for the game i.e. the order in which the game scenes will be loaded")]
    public LoadingSequence mGameLoadingSequence;
    [Header("Menu Classifier Assignments")]
    [Tooltip("Menu Identifier for Main Menu")]
    public MenuClassifier mMainMenu;
    [Tooltip("Menu Identifier for Options Menu")]
    public MenuClassifier mOptionsMenu;
    [Tooltip("Menu Identifier for HUD Stats Menu")]
    public MenuClassifier mHUDStatsMenu;
    [Tooltip("Menu Identifier for HUD Prompts Menu")]
    public MenuClassifier mHUDPromptsMenu;
    [Tooltip("Menu Identifier for HUD Effects Menu")]
    public MenuClassifier mHUDEffectsMenu;
    [Tooltip("Menu Identifier for Pause Menu")]
    public MenuClassifier mPauseMenu;
    [Tooltip("Menu Identifier for Dialog Menu")]
    public MenuClassifier mInGameDialogMenu;
    [Tooltip("Menu Identifier for Off Game Menu")]
    public MenuClassifier mOffGameDialogMenu;
    /// <summary>
    /// The Settings file for the game controls, audio and video files
    /// </summary>
    [HideInInspector] public GameSettingsData mSettingsData;
    /// <summary>
    /// The Player save data of the player for the entire game
    /// </summary>
    [HideInInspector] public GameData mGameData;
    /// <summary>
    /// The callback event that is called when controls are changed to allow binding on all actions
    /// </summary>
    [HideInInspector] public readonly ControlsChangedEvent mOnControlsChanged = new ControlsChangedEvent();
    /// <summary>
    /// The current scene's scene handler to allow for access in other classes using singleton
    /// </summary>
    [HideInInspector] public SceneHandler mSceneHandler;
    /// <summary>
    /// The current state of the game
    /// </summary>
    [HideInInspector] public State mState = State.MainMenu;
    /// <summary>
    /// The Boolean that lets the game specific scenes know that the game has been started
    /// </summary>
    [HideInInspector] public bool mGameStarted = false;
    /// <summary>
    /// The index of the current scene in the scriptable object
    /// </summary>
    [HideInInspector] public int mCurrentSceneIndex;
    /// <summary>
    /// The Input Action Bindings that are used to setup default controls
    /// </summary>
    GameActions mGameActions;

    void Awake()
    {
        LeanTween.init(1000); //initialize leantween
        mGameActions = new GameActions();
    }

    void Start()
    {
        if (mGameLoadingSequence == null || mGameLoadingSequence.mGameSceneSequence.Length <= 0)
        {
            Debug.LogError("Game Loading Asset not found or no scene selected in the asset");
            return;
        }
        SaveGameManager.Instance.LoadPlayerData();
        SaveGameManager.Instance.LoadSettingsData();
        if (!SaveGameManager.Instance.mLoadExists) //reset game data if its a new game
        {
            mGameData.ResetGameData();
        }
        if (SaveGameManager.Instance.mSettingsSaved) //load settings for audio
        {
            AudioManager.Instance.SetAudioVolumeFromSave();
            CheckGameActionIDs();
        }
        else
        {
            SetDefaultSettings(); //set default settings data
        }
        MultiSceneManager.Instance.mOnSceneLoad.AddListener(OnSceneLoad);
        MultiSceneManager.Instance.mOnSceneUnload.AddListener(OnSceneUnload);
        foreach (SceneReference aScene in mGameLoadingSequence.mGameSceneSequence)
        {
            if (string.IsNullOrEmpty(aScene))
            {
                continue;
            }
            MainCameraController.Instance.mGame3DScenes.Add(aScene);
        }
        if (!string.IsNullOrEmpty(mGameLoadingSequence.mBG3DScene)
            && !mGameLoadingSequence.mDebugMode)
        {
            MainCameraController.Instance.mGame3DScenes.Add(mGameLoadingSequence.mBG3DScene);
            MultiSceneManager.Instance.LoadScenes(new List<string> { mGameUIScene, mGameLoadingSequence.mBG3DScene }); //load UI scene
        }
        else
        {
            MultiSceneManager.Instance.LoadScene(mGameUIScene); //load UI scene
        }
    }

    void OnDestroy()
    {
        mGameActions.Dispose();
        if (MultiSceneManager.IsValidSingleton())
        {
            MultiSceneManager.Instance.mOnSceneLoad.RemoveListener(OnSceneLoad);
            MultiSceneManager.Instance.mOnSceneUnload.RemoveListener(OnSceneUnload);
        }
    }

    void CheckGameActionIDs()
    {
        for (int aI = 0; aI < mSettingsData.mCurrentControls.mCurrentControlMapping.Count; aI++)
        {
            InputAction aAction = mGameActions.asset.FindAction(mSettingsData.mCurrentControls.mCurrentControlMapping[aI].mActionID);
            if(aAction == null)
            {
                ResetControls();
            }
        }

    }
    /// <summary>
    /// Set the default settings at the first time start of the game and save it
    /// </summary>
    void SetDefaultSettings()
    {
        mSettingsData = new GameSettingsData();
        ResetControls();
        #region Audio Settings
        mSettingsData.mAudioData = new AudioSettingsData();
        mSettingsData.mAudioData.mAudioVolume = new float[5];
        for (int aI = 0; aI < mSettingsData.mAudioData.mAudioVolume.Length; aI++)
        {
            mSettingsData.mAudioData.mAudioVolume[aI] = AudioManager.Instance.GetMixerGroupVolume((AudioGroup)aI);
        }
        #endregion
        #region Video Settings
        mSettingsData.mVideoData = new VideoSettingsData();
        mSettingsData.mVideoData.mBrightness = 6;
        mSettingsData.mVideoData.mGraphics = 1;
        mSettingsData.mVideoData.mIsFullscreen = true;
        for (int aI = 0; aI < Screen.resolutions.Length; aI++)
        {
            if (Screen.resolutions[aI].width == Screen.currentResolution.width && Screen.resolutions[aI].height == Screen.currentResolution.height)
            {
                mSettingsData.mVideoData.mResolution = aI;
                break;
            }
        }
        #endregion
        SaveGameManager.Instance.SaveSettingsData();
    }

    void ResetControls()
    {
        #region Control Settings
        mGameActions.Enable();
        mSettingsData.mCurrentControls = new ControlsSaveData();
        SetCurrentControlsStruct();
        mGameActions.Disable();
        #endregion
    }

    #region Control Settings
    /// <summary>
    /// Use the control type and input action to set the gamepad and keyboard path
    /// </summary>
    /// <param name="pControlType">Type of Controls</param>
    /// <param name="pAction">Action of the control</param>
    void SetActionMapChangeableControls(Controls pControlType, InputAction pAction)
    {
        ChangeableControls aControls = new ChangeableControls
        {
            mControlType = pControlType,
            mActionID = pAction.id.ToString(),
            mActionName = pAction.name,
        };
        int aC = 0;
        foreach (InputBinding aBinding in pAction.bindings)
        {
            if (string.IsNullOrEmpty(aBinding.groups))
            {
                continue;
            }
            if (aBinding.groups.Contains("Gamepad"))
            {
                aControls.mGamepadPath = aBinding.effectivePath;
                aC++;
            }
            else if (aBinding.groups.Contains("Keyboard"))
            {
                aControls.mKeyboardPath = aBinding.effectivePath;
                aC++;
            }
            if (aC == 2)
            {
                break;
            }
        }
        mSettingsData.mCurrentControls.mCurrentControlMapping.Add(aControls);

    }
    /// <summary>
    /// Set default controls for all changeable controls
    /// </summary>
    void SetCurrentControlsStruct()
    {
        mSettingsData.mCurrentControls.mCurrentControlMapping = new List<ChangeableControls>();
        SetActionMapChangeableControls(Controls.Shapeshift, mGameActions.PlayerBasic.ShapeShift);
        SetActionMapChangeableControls(Controls.SpecialAbility, mGameActions.PlayerAdvanced.AbilityMode);
        SetActionMapChangeableControls(Controls.CancelAbility, mGameActions.PlayerAdvanced.CancelAbility);
        SetActionMapChangeableControls(Controls.Run, mGameActions.Movement.RunModifier);
        SetActionMapChangeableControls(Controls.Interact, mGameActions.PlayerBasic.Interact);
        mSettingsData.mCurrentControls.mFlipCamera = false;
        mSettingsData.mCurrentControls.mCameraSensitivity = 2;
        SaveGameManager.Instance.SaveSettingsData();
    }

    #endregion

    /// <summary>
    /// Function called when the changeable controls change
    /// </summary>
    /// <param name="pControl">The control that changed</param>
    public void SetControls(ChangeableControls pControl)
    {
        for (int aI = 0; aI < mSettingsData.mCurrentControls.mCurrentControlMapping.Count; aI++)
        {
            if (mSettingsData.mCurrentControls.mCurrentControlMapping[aI].mControlType == pControl.mControlType)
            {
                mSettingsData.mCurrentControls.mCurrentControlMapping[aI] = pControl;
                mOnControlsChanged.Invoke(pControl);
                break;
            }
        }
        SaveGameManager.Instance.SaveSettingsData();
    }

    /// <summary>
    /// Update Game Action with latest controls as soon as the game action is created
    /// </summary>
    /// <param name="pAction"></param>
    public void UpdateAllChangeableControls(GameActions pAction)
    {
        for (int aI = 0; aI < mSettingsData.mCurrentControls.mCurrentControlMapping.Count; aI++)
        {
            OnControlsChanged(mSettingsData.mCurrentControls.mCurrentControlMapping[aI], pAction);
        }
    }

    /// <summary>
    /// The Method that changes the binding in all game actions
    /// </summary>
    /// <param name="pControls">The control that changed</param>
    /// <param name="pActionScriptable">The Game Action instance</param>
    public void OnControlsChanged(ChangeableControls pControls, GameActions pActionScriptable)
    {
        InputAction aAction = pActionScriptable.asset.FindAction(pControls.mActionID);
        aAction.Disable();
        int aC = 0;
        for (int aI = 0; aI < aAction.bindings.Count; aI++)
        {
            if (string.IsNullOrEmpty(aAction.bindings[aI].groups))
            {
                continue;
            }
            if (aAction.bindings[aI].groups.Contains("Keyboard"))
            {
                InputBinding aBindingOverride = aAction.bindings[aI];
                aBindingOverride.overridePath = pControls.mKeyboardPath;
                aAction.ApplyBindingOverride(aAction.GetBindingIndex(aBindingOverride.groups), aBindingOverride);
                aC++;
            }
            else if (aAction.bindings[aI].groups.Contains("Gamepad"))
            {
                InputBinding aBindingOverride = aAction.bindings[aI];
                aBindingOverride.overridePath = pControls.mGamepadPath;
                aAction.ApplyBindingOverride(aAction.GetBindingIndex(aBindingOverride.groups), aBindingOverride);
                aC++;
            }
            if (aC == 2)
            {
                break;
            }
        }
        aAction.Enable();
    }

    /// <summary>
    /// Callback when any scene loads
    /// </summary>
    /// <param name="pLoadedScenes">The list of scenes that just got loaded</param>
    void OnSceneLoad(List<string> pLoadedScenes)
    {
        if (pLoadedScenes.Contains(mGameUIScene))
        {
            if (string.IsNullOrEmpty(mGameLoadingSequence.mBG3DScene)
            && !mGameLoadingSequence.mDebugMode)
            {
                MenuManager.Instance.ShowMenu(mMainMenu);
            }
            return;
        }
        if (pLoadedScenes.Contains(mGameLoadingSequence.mGameSceneSequence[mCurrentSceneIndex]))
        {
            MenuManager.Instance.ShowMenu(mHUDStatsMenu);
        }
    }
    /// <summary>
    /// Callback when any scene unloads
    /// </summary>
    /// <param name="pUnloadedScenes">The list of scenes that just got unloaded</param>
    void OnSceneUnload(List<string> pUnloadedScenes)
    {
        if (pUnloadedScenes.Contains(mGameLoadingSequence.mGameSceneSequence[mCurrentSceneIndex]) && mGameStarted)
        {
            mCurrentSceneIndex = mGameData.mGameSceneIndex;
            MultiSceneManager.Instance.LoadScene(mGameLoadingSequence.mGameSceneSequence[mCurrentSceneIndex]);
            ChangeGamePause(false);
        }
        else if (pUnloadedScenes.Contains(mGameLoadingSequence.mGameSceneSequence[mCurrentSceneIndex]))
        {
            MenuManager.Instance.HideMenu(mPauseMenu);
            if (string.IsNullOrEmpty(mGameLoadingSequence.mBG3DScene)
                || mGameLoadingSequence.mDebugMode)
            {
                MenuManager.Instance.ShowMenu(mMainMenu);
            }
            else
            {
                MultiSceneManager.Instance.LoadScene(mGameLoadingSequence.mBG3DScene);
            }
        }
    }
    /// <summary>
    /// Function that pauses all gameplay and input actions
    /// </summary>
    /// <param name="pPause"></param>
    public void ChangeGamePause(bool pPause)
    {
        SharedBool aPauseVar = (SharedBool)GlobalVariables.Instance.GetVariable("mGamePaused");
        aPauseVar.Value = pPause;
        if (pPause)
        {
            mState = State.Pause;
        }
        else
        {
            mState = State.Game;
        }
    }
    /// <summary>
    /// Function that starts the scene index that the player currently is at
    /// </summary>
    public void StartGame()
    {
        if (string.IsNullOrEmpty(mGameLoadingSequence.mGameSceneSequence[mGameData.mGameSceneIndex]))
        {
            Debug.LogError("Scene is Null or Not Set");
            return;
        }
        mCurrentSceneIndex = mGameData.mGameSceneIndex;
        MenuManager.Instance.HideMenu(mMainMenu);
        MultiSceneManager.Instance.LoadScene(mGameLoadingSequence.mGameSceneSequence[mGameData.mGameSceneIndex]);
        mState = State.Game;
    }
    /// <summary>
    /// Function that exits the game and goes to MainMenu
    /// </summary>
    public void ExitGame()
    {
        ChangeGamePause(false);
        SaveGameManager.Instance.SavePlayerData();
        mGameStarted = false;
        List<string> aScenesToUnload = new List<string>();
        foreach (string aScenePath in mGameLoadingSequence.mGameSceneSequence)
        {
            if (MultiSceneManager.Instance.mCurrentLoadedScenes.Contains(aScenePath))
            {
                aScenesToUnload.Add(aScenePath);
            }
        }
        MultiSceneManager.Instance.UnloadScenes(aScenesToUnload);
    }

    /// <summary>
    /// Function called when player dies to reload the game scene at the last checkpoint
    /// </summary>
    public void PlayerDead()
    {
        ChangeGamePause(true);
        mGameData.mPlayerData.mPlayerHealth = 1.0f;
        SaveGameManager.Instance.SavePlayerData();
        LoadNextScene(mCurrentSceneIndex);
    }
    /// <summary>
    /// This function loads the next scene that needs to be loaded and hides HUD menus at the same time
    /// </summary>
    /// <param name="pSceneIndex">The index of the scene that needs to be loaded</param>
    public void LoadNextScene(int pSceneIndex)
    {
        MenuManager.Instance.HideMenu(mHUDPromptsMenu);
        MenuManager.Instance.HideMenu(mHUDEffectsMenu);
        if (mGameLoadingSequence == null || mGameLoadingSequence.mGameSceneSequence == null
           || mGameLoadingSequence.mGameSceneSequence.Length <= pSceneIndex)
        {
            Debug.LogError("Scene Not In Loading Sequence");
            return;
        }
        MenuManager.Instance.HideMenu(mHUDStatsMenu);
        mGameData.mGameSceneIndex = pSceneIndex;
        List<string> aScenesToUnload = new List<string>();
        foreach (string aScenePath in mGameLoadingSequence.mGameSceneSequence)
        {
            if (MultiSceneManager.Instance.mCurrentLoadedScenes.Contains(aScenePath))
            {
                aScenesToUnload.Add(aScenePath);
            }
        }
        MultiSceneManager.Instance.UnloadScenes(aScenesToUnload);
    }

}
