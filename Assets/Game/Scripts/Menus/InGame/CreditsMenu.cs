﻿///-------------------------------------------------------------------------------------------------
// File: CreditsMenu.cs
//
// Author: Dakshvir Singh Rehill
// Date: 29/6/2020
//
// Summary:	Menu that will show end credits after the final end cut scene
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreditsMenu : Menu
{
    [Tooltip("Total time to scroll through the credits")]
    [SerializeField] float mCreditsScrollTime = 1.0f;
    [Tooltip("Total delay after which scrolling starts")]
    [SerializeField] float mCreditsScrollDelay = 0.5f;
    [Tooltip("The scrollbar of the scrollview")]
    [SerializeField] Scrollbar mCreditsScroller;

    public void StartCredits()
    {
        GameManager.Instance.ChangeGamePause(true);
        if (mCreditsScroller.value != 1)
        {
            mCreditsScroller.value = 1.0f;
        }
        if(LeanTween.isTweening(mCreditsScroller.gameObject))
        {
            LeanTween.cancel(mCreditsScroller.gameObject);
        }
        MenuManager.Instance.ShowMenu(mMenuClassifier);
        LeanTween.value(mCreditsScroller.gameObject, 1, 0, mCreditsScrollTime).setDelay(mCreditsScrollDelay)
            .setOnUpdate((pVal) => { mCreditsScroller.value = pVal; })
            .setOnComplete(QuitToMain);
    }

    void EndCredits()
    {
        GameManager.Instance.ChangeGamePause(false);
        if (LeanTween.isTweening(mCreditsScroller.gameObject))
        {
            LeanTween.cancel(mCreditsScroller.gameObject);
        }
        MenuManager.Instance.HideMenu(mMenuClassifier);
    }

    void QuitToMain()
    {
        SaveGameManager.Instance.SavePlayerData();
        EndCredits();
        GameManager.Instance.ExitGame();
    }

}
