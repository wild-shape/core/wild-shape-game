﻿///-------------------------------------------------------------------------------------------------
// File: Collectible.cs
//
// Author: Aditya Dinesh
// Date: 11/6/2020
//
// Summary:	Increases the number of collectibles collected out of the total collectibles in the game 
///-------------------------------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.InputSystem;

public class Collectible : MonoBehaviour
{

    bool mCollected = false;
    [Tooltip("The Collectible Data Asset of this Collectible")]
    [SerializeField] CollectibleData mDataAsset;
    [Tooltip("Collectible Active Particle System")]
    [SerializeField] ParticleSystem mCollectible;
    [Tooltip("Collectible Collected FX")]
    [SerializeField] ParticleSystem mCollectedFX;
    [Tooltip("Sound Effect for Collectible Collected")]
    [SerializeField] string mCollectibleSFX = "Collectable";
    [SerializeField] string mNearCollectibleSFX = "NearCollectable";
    [SerializeField] AudioAssistant mAudioAssistant;
    [Tooltip("The Data required to display Collectable HUD prompt")]
    /// <summary>
    /// Dialog Menu reference that allows to display the collectable HUD prompt
    /// </summary>
    InGameDialogMenu mDialogMenu;
    
    
    void Start()
    {
        if (GameManager.Instance.mGameData.mCollectiblesCollected.Contains(mDataAsset.mCollectibleID))
        {
            mCollected = true;
            Destroy(gameObject);
            return;
        }
        mCollectible.Play();

        if (!string.IsNullOrEmpty(mNearCollectibleSFX) && mAudioAssistant != null)
        {
            mAudioAssistant.PlaySound(mNearCollectibleSFX, true);
        }
        mDialogMenu = MenuManager.Instance.GetMenu<InGameDialogMenu>(GameManager.Instance.mInGameDialogMenu);
    }

    void OnTriggerEnter(Collider other)
    {
        if(mCollected)
        {
            return;
        }
        if(other.GetComponent<PlayerCharacter>() != null)
        {
            mCollected = true;
            mCollectible.Stop();
            mCollectedFX.Play();

            if (!string.IsNullOrEmpty(mNearCollectibleSFX))
            {
                Destroy(mAudioAssistant.gameObject);
            }
            if (!string.IsNullOrEmpty(mCollectibleSFX))
            {
                AudioManager.Instance.PlaySound(mCollectibleSFX);
            }
            GameManager.Instance.mGameData.mCollectiblesCollected.Add(mDataAsset.mCollectibleID);
            GameManager.Instance.mGameData.mPlayerData.mCollectiblesCollected++;
            SaveGameManager.Instance.SavePlayerData();
            mDialogMenu.ShowCollectableHUDPromptDialogueBox(mDataAsset.mCollectibleID);
        }
    }

    void Update()
    {
        if(!mCollected)
        {
            return;
        }
        if(mCollectedFX.isStopped)
        {
            Destroy(gameObject);
        }
    }
}
