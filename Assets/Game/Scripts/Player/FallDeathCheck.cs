﻿///-------------------------------------------------------------------------------------------------
// File: FallDeathCheck.cs
//
// Author: Dakshvir Singh Rehill
// Date: 6/6/2020
//
// Summary:	Check for Ground and Die after a few seconds if no ground found
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallDeathCheck : MonoBehaviour
{
    [Tooltip("The maximum time the player can fall but not die")]
    [SerializeField] float mGroundCheckTime = 0.5f;
    [Tooltip("The maximum distance that the ground can be from the player to allow for safe drop")]
    [SerializeField] float mGroundCheckDistance = 3.0f;
    [Tooltip("The mask of the ground that needs to be used to check raycast to ground")]
    [SerializeField] LayerMask mGroundMask;
    /// <summary>
    /// The current no ground time
    /// </summary>
    float mCurrentTime = 0.0f;
    [Tooltip("Set it to true to visually see the distance raycast from the player character")]
    [SerializeField] bool mDebugDraw = true;

    void Update()
    {
        if(GameManager.Instance.mState == GameManager.State.Pause)
        {
            return;
        }
        if(GameManager.Instance.mGameData.mPlayerData.mPlayerHealth <= 0)
        {
            return;
        }
        //if ground found set time to zero
        if(Physics.Raycast(transform.position, -transform.up, mGroundCheckDistance, mGroundMask, QueryTriggerInteraction.Ignore))
        {
            mCurrentTime = 0.0f;
        }
        else //else increase time till the time you kill the player
        {
            mCurrentTime += Time.deltaTime;
            if(mCurrentTime >= mGroundCheckTime)
            {
                GameManager.Instance.mGameData.mPlayerData.mPlayerHealth -= GameManager.Instance.mGameData.mPlayerData.mPlayerHealth;
                GameManager.Instance.PlayerDead();
            }
        }
    }

    void OnDrawGizmos()
    {
        if(!mDebugDraw)
        {
            return;
        }
        Debug.DrawLine(transform.position, transform.position - transform.up * mGroundCheckDistance, Color.red);
    }

}
