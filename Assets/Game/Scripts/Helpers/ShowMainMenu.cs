﻿///-------------------------------------------------------------------------------------------------
// File: ShowMainMenu.cs
//
// Author: Dakshvir Singh Rehill
// Date: 29/7/2020
//
// Summary:	Script that calls show main menu at start of Main Menu BG Scene
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowMainMenu : MonoBehaviour
{
    
    void Start()
    {
        MenuManager.Instance.ShowMenu(GameManager.Instance.mMainMenu);
    }
}
