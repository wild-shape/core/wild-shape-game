﻿///-------------------------------------------------------------------------------------------------
// File: FinalCollectableChecker.cs
//
// Author: Aditya Dinesh
// Date: 14/8/2020
//
// Summary:	Checks if all Other collectables have been collected to Spawn the Final Collectable
///-------------------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalCollectableChecker : MonoBehaviour
{
    [Tooltip("The Final Collectable Prefab")]
    [SerializeField] GameObject mFinalCollectable;
    List<int> mCollectablesID;
    int mCollectableCounter = 0;

    void Start()
    {
        mCollectablesID = GameManager.Instance.mGameData.mCollectiblesCollected;
    }

    void OnTriggerEnter(Collider pOther)
    {

        if (pOther.GetComponent<PlayerCharacter>() != null)
        {
            if (!mFinalCollectable.activeInHierarchy)
            {
                CheckAllCollected();
                if (mCollectableCounter > 8)
                {
                    mFinalCollectable.SetActive(true);
                }
            }
        }
    }

    void CheckAllCollected()
    {
        foreach (int aID in mCollectablesID)
        {
            if (aID > -1 || aID < 9)
            {
                mCollectableCounter++;
            }
        }
    }
}
