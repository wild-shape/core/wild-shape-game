﻿///-------------------------------------------------------------------------------------------------
// File: HUDStatsMenu.cs
//
// Author: Dakshvir Singh Rehill
// Date: 6/6/2020
//
// Summary:	Menu that displays latest stats in the progression system
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDStatsMenu : Menu
{
    [System.Serializable]
    public struct FormHUDStats
    {
        [Tooltip("The Array of each ability bar that needs to be filled when ability changes")]
        public Image[] mAbilityBar;
        [Tooltip("The Array of each glow bar that needs to be set active when ability is on")]
        public Image[] mAbilitySmallGlow;
        [Tooltip("The Array of each glow bar that needs to be animated when ability is on")]
        public Image[] mAbilityBigGlow;
        [Tooltip("The GameObject that has the form specific UI")]
        public GameObject mFormObject;
        [Tooltip("The Maximum Health bar of this Form")]
        public float mFullHealth;
        /// <summary>
        /// Used to ensure leantween index doesn't go out of bounds due to recalls
        /// </summary>
        [HideInInspector] public bool[] mAnimationCalled;
    }

    [Tooltip("The Bar that has the health of the player")]
    [SerializeField] Image mHealthBarFilled;
    [Tooltip("The Bar that just shows the number of hits the player can endure")]
    [SerializeField] Image mHealthBarEmpty;
    [Tooltip("The Array of HUD Stat References for each shape")]
    [SerializeField] FormHUDStats[] mHUDStats;
    [Tooltip("The Alpha Effect Delay Between Each Other")]
    [SerializeField] float mAlphaBetweenDelay = 0.25f;
    [Tooltip("The Alpha Effect Delay At Start")]
    [SerializeField] float mAlphaDelayAtStart = 0.25f;
    [Tooltip("The Alpha Change Duration")]
    [SerializeField] float mAlphaTime = 1.0f;
    [Tooltip("The Image for each control scheme")]
    [SerializeField] List<Sprite> mControlSchemes;
    [Tooltip("The Movement HUD Image")]
    [SerializeField] Image mMovementHUD;
    void OnEnable()
    {
        if (GameManager.Instance.mSceneHandler == null)
        {
            return;
        }
        for (int aI = 0; aI < mHUDStats.Length; aI++)
        {
            mHUDStats[aI].mAnimationCalled = new bool[mHUDStats[aI].mAbilityBigGlow.Length];
        }
        if (mMovementHUD.gameObject.activeInHierarchy)
        {
            foreach (bool aMechanicsBool in GameManager.Instance.mGameData.mTutorialData)
            {
                if (aMechanicsBool)
                {
                    mMovementHUD.gameObject.SetActive(false);
                    break;
                }
            }
        }
    }

    void Update()
    {
        if (GameManager.Instance.mSceneHandler == null)
        {
            return;
        }
        if (mMovementHUD.gameObject.activeInHierarchy)
        {
            mMovementHUD.sprite = mControlSchemes[(int)InputPromptHandler.Instance.mActiveControlSchemeType];
        }
        UpdateAll();
    }

    void UpdateAll()
    {
        int aCurChar = GameManager.Instance.mGameData.mPlayerData.mCurChar;
        ChangeActivePlayer(aCurChar);
        float aCurrentHealth = GameManager.Instance.mGameData.mPlayerData.mPlayerHealth;
        mHealthBarEmpty.fillAmount = mHUDStats[aCurChar].mFullHealth;
        if(GameManager.Instance.mSceneHandler.mCameraController.mShapeShiftInst.mPlayerCharacters[aCurChar].mStealthyCharacter)
        {
            if(aCurrentHealth > 0.5f)
            {
                aCurrentHealth = 1.0f;
            }
            else if(aCurrentHealth > 0.0f)
            {
                aCurrentHealth = 0.69f;
            }
        }
        aCurrentHealth *= mHUDStats[aCurChar].mFullHealth;
        mHealthBarFilled.fillAmount = aCurrentHealth;
        SetupSpecialAbility(aCurChar);
    }

    void SetupSpecialAbility(int pCurChar)
    {
        float aSpecialAbilityAmnt = GameManager.Instance.mGameData.mPlayerData.mSpecialAbility;
        for (int aI = 0; aI < mHUDStats[pCurChar].mAbilityBar.Length; aI++)
        {
            //Calculate the full fill of this ability bar as that would trigger animation and fill amount
            float aFullFill = (aI + 1) * GameManager.Instance.mSceneHandler.mCameraController.
                                         mShapeShiftInst.mPlayerCharacters[pCurChar].mRequiredStrength;
            if (aSpecialAbilityAmnt >= aFullFill)
            {
                mHUDStats[pCurChar].mAbilityBar[aI].fillAmount = 1.0f;
                mHUDStats[pCurChar].mAbilitySmallGlow[aI].gameObject.SetActive(true);
                if (!mHUDStats[pCurChar].mAnimationCalled[aI])
                {
                    mHUDStats[pCurChar].mAnimationCalled[aI] = true;
                    int aCurIx = aI;
                    LeanTween.value(mHUDStats[pCurChar].mAbilityBigGlow[aCurIx].gameObject, 0, 1, mAlphaTime)
                        .setDelay(mAlphaDelayAtStart)
                        .setOnComplete(() =>
                        {
                            LeanTween.value(mHUDStats[pCurChar].mAbilityBigGlow[aCurIx].gameObject, 1, 0, mAlphaTime)
                            .setDelay(mAlphaBetweenDelay).setOnComplete(() =>
                            {
                                mHUDStats[pCurChar].mAnimationCalled[aCurIx] = false;
                            }).setOnUpdate
                        ((float pVal) =>
                        UpdateAlpha(pVal, pCurChar, aCurIx));
                        }).setOnUpdate
                        ((float pVal) =>
                        UpdateAlpha(pVal, pCurChar, aCurIx));
                }
            }
            else
            {
                if (LeanTween.isTweening(mHUDStats[pCurChar].mAbilityBigGlow[aI].gameObject))
                {
                    LeanTween.cancel(mHUDStats[pCurChar].mAbilityBigGlow[aI].gameObject);
                    mHUDStats[pCurChar].mAnimationCalled[aI] = false;
                }
                UpdateAlpha(0, pCurChar, aI);
                mHUDStats[pCurChar].mAbilitySmallGlow[aI].gameObject.SetActive(false);
                mHUDStats[pCurChar].mAbilityBar[aI].fillAmount = aSpecialAbilityAmnt;
            }
        }
    }

    void UpdateAlpha(float pVal, int pCharIx, int pGlowIx)
    {
        Color aColor = mHUDStats[pCharIx].mAbilityBigGlow[pGlowIx].color;
        aColor.a = pVal;
        mHUDStats[pCharIx].mAbilityBigGlow[pGlowIx].color = aColor;
    }

    void ChangeActivePlayer(int pCurChar)
    {
        if (mHUDStats[pCurChar].mFormObject.activeInHierarchy)
        {
            return;
        }
        int aPrevChar = (pCurChar + 1) % mHUDStats.Length;
        for (int aI = 0; aI < mHUDStats[aPrevChar].mAbilityBar.Length; aI++)
        {
            if (LeanTween.isTweening(mHUDStats[aPrevChar].mAbilityBigGlow[aI].gameObject))
            {
                LeanTween.cancel(mHUDStats[aPrevChar].mAbilityBigGlow[aI].gameObject);
                mHUDStats[aPrevChar].mAnimationCalled[aI] = false;
            }
            UpdateAlpha(0, aPrevChar, aI);
            mHUDStats[aPrevChar].mAbilitySmallGlow[aI].gameObject.SetActive(false);
            mHUDStats[aPrevChar].mAbilityBar[aI].fillAmount = 0;
        }

        mHUDStats[aPrevChar].mFormObject.SetActive(false);
        mHUDStats[pCurChar].mFormObject.SetActive(true);
    }


    public void DeactivateMovementHUD()
    {
        if(mMovementHUD.gameObject.activeInHierarchy)
        {
            mMovementHUD.gameObject.SetActive(false);
        }
    }

}
