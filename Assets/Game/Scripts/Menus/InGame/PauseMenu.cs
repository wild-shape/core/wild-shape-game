﻿///-------------------------------------------------------------------------------------------------
// File: PauseMenu.cs
//
// Author: Dakshvir Singh Rehill
// Date: 23/6/2020
//
// Summary:	Script that handles the behaviour of the pause menu
///-------------------------------------------------------------------------------------------------
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : OverlayMenu
{
    [Tooltip("Continue game button")]
    [SerializeField] Button mContinueBtn;
    [Tooltip("Control menu button")]
    [SerializeField] Button mControlsBtn;
    [Tooltip("Video Settings Button")]
    [SerializeField] Button mVideoSettings;
    [Tooltip("Audio Settings Button")]
    [SerializeField] Button mAudioSettings;
    [Tooltip("Quit to main menu button")]
    [SerializeField] Button mQuitBtn;

    public override void Start()
    {
        base.Start();
        mInitSelectable = mContinueBtn;
    }

    void OnEnable()
    {
        mContinueBtn.interactable =
        mVideoSettings.interactable =
        mAudioSettings.interactable =
        mControlsBtn.interactable =
        mQuitBtn.interactable = true;
        if(GameManager.Instance.mGameStarted)
        {
            MenuManager.Instance.HideMenu(GameManager.Instance.mHUDStatsMenu);
        }
    }

    public void ContinueGame()
    {
        MenuManager.Instance.ShowMenu(GameManager.Instance.mHUDStatsMenu);
        mContinueBtn.interactable = false;
        MenuManager.Instance.HideMenu(mMenuClassifier);
        GameManager.Instance.ChangeGamePause(false);
    }

    public void QuitToMain()
    {
        SaveGameManager.Instance.SavePlayerData();
        mQuitBtn.interactable = false;
        MenuManager.Instance.HideMenu(mMenuClassifier);
        GameManager.Instance.ExitGame();
    }

}
