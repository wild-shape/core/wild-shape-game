﻿///-------------------------------------------------------------------------------------------------
// File: InteractableAffordance.cs
//
// Author: Dakshvir Singh Rehill
// Date: 16/7/2020
//
// Summary:	Script responsible to change the instensity of the material according to player distance
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableAffordance : IInteractable
{
    [Header("Emmission Properties")]
    [Tooltip("The Color Gradient that allows for glow effect in emmissive texture")]
    [SerializeField] [GradientUsage(true)] Gradient mEmissiveGradient;
    [Tooltip("The distance between the player and center of this collider at which the intensity should be maximum")]
    [SerializeField] [Min(0)] float mMaxIntensityDistance = 2.5f;
    [Tooltip("The distance between the player and center of this collider at which the intensity should be minimum")]
    [SerializeField] [Min(2.5f)] float mMinIntensityDistance = 6.5f;
    [Tooltip("The Renderer with the Emissive Material")]
    [SerializeField] GameObject mRenderObject;
    List<MeshRenderer> mEmissiveRenderers;
    /// <summary>
    /// The Emmissive Material copy of this interactable
    /// </summary>
    Material mEmissiveMaterial;
    float mCurrentIntensity;
    float mRange;
    protected override void Start()
    {
        mEmissiveRenderers = new List<MeshRenderer>(mRenderObject.GetComponentsInChildren<MeshRenderer>());
        if (mEmissiveRenderers.Count <= 0)
        {
            Debug.LogError("No Emmissive Renderers Attached");
            Destroy(gameObject);
            return;
        }
        base.Start();
        if (mMaxIntensityDistance > mMinIntensityDistance)
        {
            mMaxIntensityDistance = mMaxIntensityDistance + mMinIntensityDistance;
            mMinIntensityDistance = mMaxIntensityDistance - mMinIntensityDistance;
            mMaxIntensityDistance = mMaxIntensityDistance - mMinIntensityDistance;
        }
        if (mMaxIntensityDistance == mMinIntensityDistance) //check to make sure divide by zero error doesn't occur
        {
            mMaxIntensityDistance = 0.0f;
        }
        mEmissiveMaterial = new Material(mEmissiveRenderers[0].material);
        mCurrentIntensity = 0.0f;
        mRange = 1 / (mMinIntensityDistance - mMaxIntensityDistance);
        SetCurrentIntensity();
    }

    void SetCurrentIntensity()
    {
        mEmissiveMaterial.SetColor("_EmissionColor", mEmissiveGradient.Evaluate(mCurrentIntensity));
        foreach (MeshRenderer aRenderer in mEmissiveRenderers)
        {
            aRenderer.material = mEmissiveMaterial;
        }
    }


    protected override void Update()
    {
        base.Update();
        if (mActivePlayerCharacter != null)
        {
            float aDist = (mActivePlayerCharacter.transform.position - transform.position).magnitude;
            float aIntensityPercentage = 1 - ((aDist - mMaxIntensityDistance) * mRange);
            if (aIntensityPercentage < 0)
            {
                aIntensityPercentage = 0.0f;
            }
            if (aIntensityPercentage > 1)
            {
                aIntensityPercentage = 1.0f;
            }
            if (aIntensityPercentage != mCurrentIntensity)
            {
                mCurrentIntensity = aIntensityPercentage;
                SetCurrentIntensity();
            }
        }
        else
        {
            EndInteraction();
        }

    }

    public void StopAffordance()
    {
        if (mActivePlayerCharacter != null)
        {
            EndInteraction();
        }
        mIsActive = false;
    }

    protected override void EndInteraction()
    {
        base.EndInteraction();
        mCurrentIntensity = 0.0f;
        SetCurrentIntensity();
    }
}
