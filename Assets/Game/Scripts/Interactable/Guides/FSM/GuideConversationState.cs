﻿///-------------------------------------------------------------------------------------------------
// File: GuideConversationState.cs
//
// Author: Dakshvir Singh Rehill, Aditya Dinesh
// Date: 22/7/2020
//
// Summary:	The State of Conversation for the NPC with the player
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuideConversationState : GuideBaseState
{
    protected int mCurrentConversationIx = -1;
    [SerializeField] string mTalkAnimBoolName = "talk";
    bool mConvoStart;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        if (mGuideRef.mNPCDialogue.mSummarize && mCurrentConversationIx < mGuideRef.mNPCDialogue.mBody.Count)
        {
            mCurrentConversationIx = mGuideRef.mNPCDialogue.mBody.Count - 1;
        }
        DialogBoxStrings aDBString = new DialogBoxStrings();
        aDBString.mHeadingText = mGuideRef.mNPCDialogue.mName;
        mCurrentConversationIx++;
        if (mGuideRef.mNPCDialogue.mSummarize && mCurrentConversationIx >= mGuideRef.mNPCDialogue.mBody.Count)
        {
            int aSummaryIx = mCurrentConversationIx - mGuideRef.mNPCDialogue.mBody.Count;
            if (aSummaryIx >= mGuideRef.mNPCDialogue.mSummary.Count)
            {
                mCurrentConversationIx = mGuideRef.mNPCDialogue.mBody.Count - 1;
                mGuideRef.EndConversation();
                mConvoStart = false;
                return;
            }
            aDBString.mBodyText = mGuideRef.mNPCDialogue.mSummary[aSummaryIx];
        }
        else
        {
            if (mCurrentConversationIx == mGuideRef.mNPCDialogue.mBody.Count)
            {
                mGuideRef.mNPCDialogue.mSummarize = true;
                mCurrentConversationIx = mGuideRef.mNPCDialogue.mBody.Count - 1;
                mGuideRef.EndConversation();
                mConvoStart = false;
                return;
            }
            aDBString.mBodyText = mGuideRef.mNPCDialogue.mBody[mCurrentConversationIx];
        }
        mGuideRef.mDialogMenu.mDialogStrings = aDBString;
        mGuideRef.mGuideAnimator.SetBool(mTalkAnimBoolName, true);
        mGuideRef.mDialogMenu.ShowNPCDialogueBox();
        if (!string.IsNullOrEmpty(mGuideRef.mNPCDialogue.mOpenDialogueBox) && !mConvoStart)
        {
            AudioManager.Instance.PlaySound(mGuideRef.mNPCDialogue.mOpenDialogueBox);
            mConvoStart = true;
        }
    }
}
