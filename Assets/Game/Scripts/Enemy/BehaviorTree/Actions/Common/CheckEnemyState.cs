﻿///-------------------------------------------------------------------------------------------------
// File: CheckEnemyState.cs
//
// Author: Dakshvir Singh Rehill
// Date: 15/7/2020
//
// Summary:	Enemy Action that checks if there is an enemy in the vision cone and whether that enemy is deactivated or not
///-------------------------------------------------------------------------------------------------
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[TaskCategory("Enemy")]
[TaskDescription("Enemy Action that checks if there is an enemy in the vision cone and whether that enemy is deactivated or not")]
public class CheckEnemyState : EnemyActionBase
{
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The Enemies that have been deactivated in the entire game")]
    [SerializeField] SharedEnemyList mDeactivatedEnemies;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The Detection Meter of this enemy")]
    [SerializeField] SharedFloat mDetectionMeter;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The Detected Threshold of this enemy")]
    [SerializeField] SharedFloat mDetectedThreshold;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The Distance between the Deactivated Enemy and the inspect point")]
    [SerializeField] SharedFloat mDistanceDifference = 1.0f;
    readonly List<Enemy> mCheckedEnemies = new List<Enemy>();
    float mRaycastDistSqr = -1f;
    float mRaycastAngle = -1f;
    public override void OnStart()
    {
        base.OnStart();
        if (mRaycastDistSqr < 0.0f)
        {
            foreach (Enemy.VisionConePoint aConePoint in mEnemyInstance.mVisionCones)
            {
                float aDistSqr = aConePoint.mMaxViewingDistance * aConePoint.mMaxViewingDistance;
                if (mRaycastDistSqr <= aDistSqr)
                {
                    mRaycastDistSqr = aDistSqr;
                    mRaycastAngle = aConePoint.mViewingAngle;
                }
            }
        }
    }

    public override TaskStatus OnUpdate()
    {
        if(mDetectionMeter.Value >= mDetectedThreshold.Value)
        {
            return TaskStatus.Failure;
        }
        if (mEnemyInstance.mInspectPoints.Count > 0)
        {
            return TaskStatus.Success;
        }
        foreach(Enemy aEnemy in mDeactivatedEnemies.Value)
        {
            if(mCheckedEnemies.Contains(aEnemy))
            {
                continue;
            }
            Vector3 aDistance = aEnemy.transform.position - mEnemyInstance.transform.position;
            if(aDistance.sqrMagnitude <= mRaycastDistSqr)
            {
                float aAngle = Mathf.Abs(Vector3.Angle(mEnemyInstance.mOrigin.forward, aDistance));
                if(aAngle <= mRaycastAngle)
                {
                    //enemy found
                    mCheckedEnemies.Add(aEnemy);
                    mEnemyInstance.mInspectPoints.Add(aEnemy.transform.position - mEnemyInstance.mOrigin.forward * mDistanceDifference.Value);
                    return TaskStatus.Success;
                }
            }
        }
        //float aRightOffset = -0.25f;
        //float aAngle = -mRaycastAngle;
        //for (int aI = 0; aI < 3; aI ++)
        //{
        //    Vector3 aStartPos = mEnemyInstance.mOrigin.position + 
        //        mEnemyInstance.mOrigin.forward * mEnemyInstance.mLookOffset +
        //        mEnemyInstance.mOrigin.right * aRightOffset;
        //    aRightOffset += 0.25f;
        //    RaycastHit aHit;
        //    if (Physics.Raycast(aStartPos,Quaternion.Euler(0,aAngle,0) * mEnemyInstance.mOrigin.forward, out aHit, mRaycastDist, mEnemyLayerMask.Value))
        //    {
        //        Enemy aEnemy = aHit.collider.GetComponentInParent<Enemy>();
        //        if (mCheckedEnemies.Contains(aEnemy))
        //        {
        //            return TaskStatus.Failure;
        //        }
        //        if (aEnemy.mDeactivate || aEnemy.mFrozen)
        //        {
        //            mCheckedEnemies.Add(aEnemy);
        //            mEnemyInstance.mInspectPoints.Add(aEnemy.transform.position - mEnemyInstance.mOrigin.forward * mDistanceDifference.Value);
        //            return TaskStatus.Success;
        //        }
        //    }
        //    aAngle += mRaycastAngle;

        //}
        return TaskStatus.Failure;
    }
}
