﻿///-------------------------------------------------------------------------------------------------
// File: Patrol.cs
//
// Author: Aditya Dinesh, Dakshvir Singh Rehill
// Date: 30/5/2020
//
// Summary:	Action that circles through waypoints and patrols them
///-------------------------------------------------------------------------------------------------
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections.Generic;
using UnityEngine;

[TaskCategory("Enemy/Melee")]
[TaskDescription("Patrol Nav Mesh Enemy to Waypoints and back")]
public class Patrol : MoveAgent
{
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The Waypoint that is currently being traversed is set here to be used in wait time calculation")]
    [SerializeField] SharedTransform mCurrentWaypoint;
    /// <summary>
    /// A List of all Waypoints that will be used to patrol
    /// </summary>
    List<Transform> mWaypoints;
    /// <summary>
    /// The current index of the waypoint that is getting traversed
    /// </summary>
    int mWaypointIndex = 0;

    public override void OnStart()
    {
        base.OnStart();
        if (mWaypoints == null) //create waypoint list if waypoints is null
        {
            mWaypoints = new List<Transform>();
            MeleeEnemy aEnemyInst = (MeleeEnemy)mEnemyInstance;
            bool aCheckInPointSet = aEnemyInst.mCheckInPoint == null;
            for (int aI = 0; aI < aEnemyInst.mWaypointParent.transform.childCount; aI++)
            {
                if (!aCheckInPointSet)
                {
                    if((aEnemyInst.mWaypointParent.transform.GetChild(aI).position - aEnemyInst.mCheckInPoint.transform.position).sqrMagnitude <= 0.2f)
                    {
                        aCheckInPointSet = true;
                        mWaypoints.Add(aEnemyInst.mCheckInPoint.transform);
                    }
                    else
                    {
                        mWaypoints.Add(aEnemyInst.mWaypointParent.transform.GetChild(aI));
                    }
                }
                else
                {
                    mWaypoints.Add(aEnemyInst.mWaypointParent.transform.GetChild(aI));
                }
            }
            if (mWaypoints.Count <= 0)
            {
                mWaypoints.Add(aEnemyInst.mWaypointParent.transform);
            }
            if(!aCheckInPointSet)
            {
                mWaypoints.Add(aEnemyInst.mCheckInPoint.transform);
            }
            mWaypointIndex = 0;
        }

        if (mWaypointIndex < mWaypoints.Count && !Inspect()) // if there are waypoints left and no inspect point is set then set waypoint as destination
        {
            mAgent.speed = mEnemyInstance.mEnemySpeed;
            mEnemy.mEnemyAnimator.SetFloat(mEnemy.mWalkSpeedFloatName, mEnemy.mNormalWalkSpeed);
            mCurrentWaypoint = mWaypoints[mWaypointIndex];
            mDestination = mWaypoints[mWaypointIndex].position;
            mAgent.SetDestination(mDestination);
        }
    }

    /// <summary>
    /// Function checks if there are inspect points for this enemy
    /// Assigns that inspect point as the destination
    /// </summary>
    /// <returns>true if inspect points exist and false otherwise</returns>
    bool Inspect()
    {
        if (mEnemyInstance.mInspectPoints.Count <= 0)
        {
            return false;
        }
        mDestination = mEnemyInstance.mInspectPoints[0];
        mEnemyInstance.mInspectPoints.RemoveAt(0);
        mAgent.speed = mEnemyInstance.mEnemyAlertSpeed;
        mEnemy.mEnemyAnimator.SetFloat(mEnemy.mWalkSpeedFloatName, mEnemy.mAlertWalkSpeed);
        mAgent.SetDestination(mDestination);
        return true;
    }

    public override TaskStatus OnUpdate()
    {
        TaskStatus aReturnStatus = base.OnUpdate();
        if (aReturnStatus == TaskStatus.Success)
        {
            if (!Inspect()) //if no inspect point is set then increment waypoint index in a loopable way
            {
                mWaypointIndex = (mWaypointIndex + 1) % mWaypoints.Count;
                return TaskStatus.Success;
            }
            else
            {
                return TaskStatus.Running;
            }
        }
        return aReturnStatus;
    }
}
