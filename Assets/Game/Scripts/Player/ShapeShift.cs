﻿///-------------------------------------------------------------------------------------------------
// File: ShapeShift.cs
//
// Author: Dakshvir Singh Rehill, Aditya Dinesh
// Date: 20/5/2020
//
// Summary:	Script that handles common mechanic features of both the player characters
///-------------------------------------------------------------------------------------------------
using Cinemachine;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class ShapeShift : MonoBehaviour
{
    [Tooltip("The Sound for shapeshift")]
    [SerializeField] string mShapeshift = "ShapeShift";
    [Tooltip("The Sound for cant shapeshift")]
    [SerializeField] string mCantShapeshift = "CantShapeshift";
    [Tooltip("Target Group used by the camera")]
    public CinemachineTargetGroup mMainTargetGroup;
    [Tooltip("Target Group Averaging weight given to active shape")]
    [SerializeField] float mActiveCharacterWeight = 10.0f;
    [Tooltip("Active Player Shapes in the world")]
    public PlayerCharacter[] mPlayerCharacters;
    [Tooltip("Obstacle Layer")]
    public LayerMask mObstacles;
    [Tooltip("Raycast Height off the ground")]
    [SerializeField] float mRaycastDelta = 0.3f;
    [Tooltip("Shapeshift Error Factor")]
    [SerializeField] float mErrorFactor = 0.5f;
    [Tooltip("Maximum Distance allowed to move the player before shapeshift if there is no space in current position")]
    [SerializeField] [Range(1, 10)] float mMaxCrctnDist = 2.5f;
    [Tooltip("Ray March to check shapeshift will be moved by this step value")]
    [SerializeField] [Range(0.1f, 2.5f)] float mRaymarchAmnt = 0.2f;
    [Tooltip("Pool Classifier for the Shapeshift Effect for Player")]
    public PoolClassifier mShapeshiftEffect;
    /// <summary>
    /// If player has pressed the unique movement this is true
    /// </summary>
    [HideInInspector] public bool mUniqueMovement = false;
    /// <summary>
    /// If player has pressed the run this is true
    /// </summary>
    [HideInInspector] public bool mRunPressed = false;
    /// <summary>
    /// If the player has been frozen this is true
    /// </summary>
    [HideInInspector] public bool mFrozen = false;
    /// <summary>
    /// The input scriptable object for this shape instance
    /// </summary>
    [HideInInspector] public GameActions mInputActions;
    /// <summary>
    /// The effects menu to show the frozen effect
    /// </summary>
    HUDEffectsMenu mHUDEffects = null;
    /// <summary>
    /// The stats menu instance to update when shape changes
    /// </summary>
    [HideInInspector] public HUDStatsMenu mHUDStats = null;
    /// <summary>
    /// The current idle animation's name
    /// </summary>
    string mIdleAnimationName;
    /// <summary>
    /// The input binding for this control action
    /// </summary>
    UnityAction<ChangeableControls> mControlAction;
    /// <summary>
    /// The Raymarchstep for move player to shift
    /// </summary>
    float mCurrentRaymarchStep = 0.0f;

    void Awake()
    {
        mInputActions = new GameActions();
        GameManager.Instance.UpdateAllChangeableControls(mInputActions);
    }

    void OnEnable()
    {
        mInputActions.Enable();
    }
    void OnDisable()
    {
        mInputActions.Disable();
    }

    void Start()
    {
        mControlAction = (pControls) => GameManager.Instance.OnControlsChanged(pControls, mInputActions);
        GameManager.Instance.mOnControlsChanged.AddListener(mControlAction);
        mHUDEffects = MenuManager.Instance.GetMenu<HUDEffectsMenu>(GameManager.Instance.mHUDEffectsMenu);
        mHUDStats = MenuManager.Instance.GetMenu<HUDStatsMenu>(GameManager.Instance.mHUDStatsMenu);
    }
    /// <summary>
    /// Setup the initial callbacks and camera settings for the active shape
    /// </summary>
    public void SetupShapeshift()
    {
        mInputActions.PlayerBasic.ShapeShift.performed += ShapeShiftTriggered;
        mInputActions.Movement.RunModifier.performed += RunPressed;
        mInputActions.Movement.RunModifier.canceled += RunCanceled;
        mInputActions.GameState.Pause.performed += PauseGame;
        int aCurChar = GameManager.Instance.mGameData.mPlayerData.mCurChar;
        mPlayerCharacters[(aCurChar + 1) % mPlayerCharacters.Length].gameObject.SetActive(false);
        for (int aI = 0; aI < mMainTargetGroup.m_Targets.Length; aI++)
        {
            if (mMainTargetGroup.m_Targets[aI].target == mPlayerCharacters[0].mCameraLookAt)
            {
                mMainTargetGroup.m_Targets[aI].radius = mPlayerCharacters[0].mTargetGroupRadius;
                mMainTargetGroup.m_Targets[aI].weight = aCurChar == 0 ? mActiveCharacterWeight : 0.0f;
                mPlayerCharacters[0].mShapeShiftInstance = this;
            }
            else if (mMainTargetGroup.m_Targets[aI].target == mPlayerCharacters[1].mCameraLookAt)
            {
                mMainTargetGroup.m_Targets[aI].radius = mPlayerCharacters[1].mTargetGroupRadius;
                mMainTargetGroup.m_Targets[aI].weight = aCurChar == 1 ? mActiveCharacterWeight : 0.0f;
                mPlayerCharacters[1].mShapeShiftInstance = this;
            }
        }
        RegisterIdleAnimation();
    }

    void OnDestroy()
    {
        mInputActions.PlayerBasic.ShapeShift.performed -= ShapeShiftTriggered;
        mInputActions.Movement.RunModifier.performed -= RunPressed;
        mInputActions.Movement.RunModifier.canceled -= RunCanceled;
        mInputActions.GameState.Pause.performed -= PauseGame;
        if (GameManager.IsValidSingleton())
        {
            GameManager.Instance.mOnControlsChanged.RemoveListener(mControlAction);
        }
        mInputActions.Dispose();
    }
    /// <summary>
    /// Change the idle animation depending on the latest player characters
    /// </summary>
    void RegisterIdleAnimation()
    {
        if (!string.IsNullOrEmpty(mIdleAnimationName))
        {
            mPlayerCharacters[GameManager.Instance.mGameData.mPlayerData.mCurChar].mAnimationListener.UnregisterOnAnimationCompleted(
                Animator.StringToHash(mIdleAnimationName), OnIdleComplete);
        }
        mPlayerCharacters[GameManager.Instance.mGameData.mPlayerData.mCurChar].mCurrentIdleIx = Random.Range(0, mPlayerCharacters[GameManager.Instance.mGameData.mPlayerData.mCurChar].mIdleAnimations);
        int aCurrentIdleIx = mPlayerCharacters[GameManager.Instance.mGameData.mPlayerData.mCurChar].mCurrentIdleIx
            / (mPlayerCharacters[GameManager.Instance.mGameData.mPlayerData.mCurChar].mPlayerMovement.mState == PlayerMovement.NavState.Unique
            ? mPlayerCharacters[GameManager.Instance.mGameData.mPlayerData.mCurChar].mUniqueIdleDivider : 1);
        mIdleAnimationName = mPlayerCharacters[GameManager.Instance.mGameData.mPlayerData.mCurChar].mBaseIdleAnimName;
        if (mUniqueMovement)
        {
            mIdleAnimationName += " " + mPlayerCharacters[GameManager.Instance.mGameData.mPlayerData.mCurChar].mUniqueMovementAnimBool;
        }
        mIdleAnimationName += aCurrentIdleIx;
        mPlayerCharacters[GameManager.Instance.mGameData.mPlayerData.mCurChar].mAnimator.SetInteger(mPlayerCharacters[GameManager.Instance.mGameData.mPlayerData.mCurChar].mIdleIxAnimInt, aCurrentIdleIx);
        mPlayerCharacters[GameManager.Instance.mGameData.mPlayerData.mCurChar].mAnimationListener.RegisterOnAnimationCompleted(
            Animator.StringToHash(mIdleAnimationName), OnIdleComplete);
    }


    public void ToggleShapeshift(bool pAction)
    {
        if (pAction == true)
        {
            mInputActions.PlayerBasic.Enable();
            mInputActions.PlayerAdvanced.Enable();
            mInputActions.Movement.Enable();
        }
        else
        {
            mInputActions.PlayerBasic.Disable();
            mInputActions.PlayerAdvanced.Disable();
            mInputActions.Movement.Disable();
        }
    }

    /// <summary>
    /// Used to disallow shapeshift
    /// </summary>
    /// 
    public void FreezePlayers()
    {
        mInputActions.PlayerBasic.Disable();
        mInputActions.PlayerAdvanced.Disable();
        mInputActions.Movement.Disable();
        if (mHUDEffects != null)
        {
            mHUDEffects.SetFreeze();
        }
    }
    /// <summary>
    /// Used to allow shapeshift
    /// </summary>
    public void UnFreezePlayer()
    {
        mInputActions.PlayerAdvanced.Enable();
        mInputActions.PlayerBasic.Enable();
        mInputActions.Movement.Enable();
        MenuManager.Instance.HideMenu(GameManager.Instance.mHUDEffectsMenu);
    }
    /// <summary>
    /// Function called when escape key is pressed
    /// </summary>
    /// <param name="pContext"></param>
    void PauseGame(InputAction.CallbackContext pContext)
    {
        if (GameManager.Instance.mGameData.mPlayerData.mPlayerHealth <= 0.0f)
        {
            return;
        }
        if (GameManager.Instance.mState != GameManager.State.Pause)
        {
            MenuManager.Instance.ShowMenu(GameManager.Instance.mPauseMenu);
            GameManager.Instance.ChangeGamePause(true);
        }
    }
    /// <summary>
    /// Function called when run key is pressed
    /// </summary>
    /// <param name="pContext"></param>
    void RunPressed(InputAction.CallbackContext pContext)
    {
        mRunPressed = true;
    }
    /// <summary>
    /// Function called when run key is unpressed
    /// </summary>
    /// <param name="pContext"></param>
    void RunCanceled(InputAction.CallbackContext pContext)
    {
        mRunPressed = false;
    }
    /// <summary>
    /// Called when unique movement is changed
    /// </summary>
    public void UniqueMovementChange()
    {
        mUniqueMovement = !mUniqueMovement;
        RegisterIdleAnimation();
    }

    /// <summary>
    /// Callback to randomly choose different idle animations
    /// </summary>
    /// <param name="pIdleHash"></param>
    void OnIdleComplete(int pIdleHash)
    {
        if (pIdleHash == Animator.StringToHash(mIdleAnimationName))
        {
            RegisterIdleAnimation();
        }
    }



    /// <summary>
    /// Function that changes the player character if the character can be spawned at that size
    /// </summary>
    /// <param name="pContext"></param>
    void ShapeShiftTriggered(InputAction.CallbackContext pContext)
    {
        if (GameManager.Instance.mState == GameManager.State.Pause)
        {
            return;
        }
        PlayerMovement.NavState aState = mPlayerCharacters[GameManager.Instance.mGameData.mPlayerData.mCurChar].mPlayerMovement.mState;
        if (aState != PlayerMovement.NavState.Unique && aState != PlayerMovement.NavState.Fall)
        {
            int aPrevChar = GameManager.Instance.mGameData.mPlayerData.mCurChar;
            int aNewChar = (GameManager.Instance.mGameData.mPlayerData.mCurChar + 1) % mPlayerCharacters.Length;
            mPlayerCharacters[aNewChar].transform.position = mPlayerCharacters[aPrevChar].transform.position;
            mPlayerCharacters[aNewChar].transform.rotation = mPlayerCharacters[aPrevChar].transform.rotation;
            Vector3 aCenter = mPlayerCharacters[aNewChar].mYStablizingCollider.transform.position
                + mPlayerCharacters[aNewChar].mYStablizingCollider.transform.up * mRaycastDelta;
            Vector3 aEndDistance = mPlayerCharacters[aNewChar].mYStablizingCollider.height *
                mPlayerCharacters[aNewChar].mYStablizingCollider.transform.forward * 0.5f * mErrorFactor;
            if (Physics.CheckCapsule(aCenter + aEndDistance, aCenter - aEndDistance,
                mPlayerCharacters[aNewChar].mYStablizingCollider.radius, mObstacles, QueryTriggerInteraction.Ignore))
            {
                if (!ShiftByMove())
                {
                    CannotShapeshift();
                    return;
                }
            }
            if (AudioManager.IsValidSingleton())
            {
                AudioManager.Instance.PlaySound(mShapeshift);
            }
            GameManager.Instance.mGameData.mPlayerData.mCurChar = aNewChar;
            mPlayerCharacters[aPrevChar].gameObject.SetActive(false);
            mPlayerCharacters[aNewChar].gameObject.SetActive(true);
            mPlayerCharacters[aNewChar].CauseShapeshiftEffect();
            for (int aI = 0; aI < mMainTargetGroup.m_Targets.Length; aI++)
            {
                if (mMainTargetGroup.m_Targets[aI].target == mPlayerCharacters[aPrevChar].mCameraLookAt)
                {
                    mMainTargetGroup.m_Targets[aI].weight = 0.0f;
                }
                else if (mMainTargetGroup.m_Targets[aI].target == mPlayerCharacters[GameManager.Instance.mGameData.mPlayerData.mCurChar].mCameraLookAt)
                {
                    mMainTargetGroup.m_Targets[aI].weight = mActiveCharacterWeight;
                }
            }
            RegisterIdleAnimation();
        }
        else
        {
            CannotShapeshift();
        }
    }

    /// <summary>
    /// Function to try and move player and shapeshift if possible
    /// </summary>
    /// <returns></returns>
    bool ShiftByMove()
    {
        int aNewChar = (GameManager.Instance.mGameData.mPlayerData.mCurChar + 1) % mPlayerCharacters.Length;
        Vector3 aCurCenter = mPlayerCharacters[aNewChar].mYStablizingCollider.transform.position
        + mPlayerCharacters[aNewChar].mYStablizingCollider.transform.up * mRaycastDelta;
        Vector3 aCurEndDist = mPlayerCharacters[aNewChar].mYStablizingCollider.height *
            mPlayerCharacters[aNewChar].mYStablizingCollider.transform.forward * 0.5f * mErrorFactor;
        //check behind the player's current position
        if (CheckAreaByDirection(ref aCurCenter, ref aCurEndDist, ref aNewChar, -mPlayerCharacters[aNewChar].transform.forward))
        {
            return true;
        }
        //check on the right side of player's current position
        if (CheckAreaByDirection(ref aCurCenter, ref aCurEndDist, ref aNewChar, mPlayerCharacters[aNewChar].transform.right))
        {
            return true;
        }
        //check on the left side of player's current position
        if (CheckAreaByDirection(ref aCurCenter, ref aCurEndDist, ref aNewChar, -mPlayerCharacters[aNewChar].transform.right))
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// Moves the check capsule in the direction specified to check if player can be shapeshifted or not
    /// </summary>
    /// <param name="pCurCenter">The Center Of the Check Capsule</param>
    /// <param name="pCurEndDist">The End of the Check Capsule</param>
    /// <param name="pNewChar">The Shapeshift character index</param>
    /// <param name="pDirection">The direction in which to check</param>
    /// <returns></returns>
    bool CheckAreaByDirection(ref Vector3 pCurCenter, ref Vector3 pCurEndDist, ref int pNewChar, Vector3 pDirection)
    {
        mCurrentRaymarchStep = mRaymarchAmnt;
        Vector3 aCenter = pCurCenter + pDirection * mRaymarchAmnt;
        while (mCurrentRaymarchStep < mMaxCrctnDist)
        {
            if (!Physics.CheckCapsule(aCenter + pCurEndDist, aCenter - pCurEndDist,
                mPlayerCharacters[pNewChar].mYStablizingCollider.radius,
                mObstacles, QueryTriggerInteraction.Ignore))
            {
                mPlayerCharacters[pNewChar].transform.position += pDirection * mCurrentRaymarchStep;
                return true;
            }
            aCenter += pDirection * mRaymarchAmnt;
            mCurrentRaymarchStep += mRaymarchAmnt;
        }
        return false;
    }


    /// <summary>
    /// Cannot Shapeshift VFX when Shapeshift isn't possible
    /// </summary>
    void CannotShapeshift()
    {
        if (mHUDEffects != null)
        {
            mHUDEffects.SetNoAbility();
        }
        if (AudioManager.IsValidSingleton())
        {
            AudioManager.Instance.PlaySound(mCantShapeshift);
        }
    }
}
