﻿///-------------------------------------------------------------------------------------------------
// File: DamageLaser.cs
//
// Author: Dakshvir Singh Rehill
// Date: 14/07/2020
//
// Summary:	This laser will block pathways to ensure core is deactivated before moving on to the next level
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageLaser : MonoBehaviour, IDeactivatable
{
    public bool mDeactivate { get; set; }
    [Tooltip("The Box Collider of this laser")]
    [SerializeField] BoxCollider mLaserCollider;
    [Tooltip("The Mesh Renderer Object to Allow for Dynamic NavmeshObstacle")]
    [SerializeField] Transform mMeshBlock;
    [Tooltip("Deactivatables that can deactivate this laser")]
    [SerializeField] List<Deactivatable> mDeactivatables;
    [Header("Laser Spawning Setup")]
    [Tooltip("The list of lasers attached to this damager")]
    [SerializeField] List<LaserEffectObject> mLaserEffectObjects;
    [Tooltip("The time to re-attack after an attack has been caused")]
    [SerializeField] float mAttackTime = 0.5f;
    [Tooltip("Audio Assistant component for 3D Sfx")]
    [SerializeField] AudioAssistant mAudioAssistant;
    [Tooltip("Sound Effect for Laser Door")]
    [SerializeField] string mLaserDoorSFX = "LaserDoor";
    [Tooltip("Sound Effect for Player Hit")]
    [SerializeField] string mPlayerHitSFX = "PlayerHit";
    [Tooltip("Sound Effect for Broken Door")]
    [SerializeField] string mBrokenDoorSFX = "BrokenLaserDoor";
    /// <summary>
    /// The instance of the current player shape
    /// </summary>
    PlayerCharacter mShapeReference;
    /// <summary>
    /// The current time between attacks
    /// </summary>
    float mCurrentTime = 0.0f;
    [Tooltip("The Pool Classifier for the Player Damage Fx")]
    [SerializeField] PoolClassifier mMeleeAttackFx;
    private bool mPlayerHit;
    private bool mLaserShot;

    void Start()
    {
        if (mLaserEffectObjects.Count <= 0)
        {
            gameObject.SetActive(false);
            return;
        }
        foreach(LaserEffectObject aLaserEffect in mLaserEffectObjects)
        {
            aLaserEffect.mLaserParent = this;
        }
        if(mDeactivatables.Count == 0)
        {
            mDeactivate = true;
        }
        else
        {
            if (!string.IsNullOrEmpty(mLaserDoorSFX) && mAudioAssistant != null)
            {
                mAudioAssistant.PlaySound(mLaserDoorSFX, true);
            }
        }
        foreach(Deactivatable aDeactivatable in mDeactivatables)
        {
            aDeactivatable.mDeactivatables.Add(this);
        }
        if(mMeshBlock != null)
        {
            mMeshBlock.localScale = mLaserCollider.size;
        }
    }

    private void Update()
    {
        if(mDeactivate && !mLaserShot)
        {
            if(mLaserCollider.enabled)//to make blink possible
            {
                mLaserCollider.enabled = false;
                if(mMeshBlock != null)
                {
                    mMeshBlock.gameObject.SetActive(false);
                }
            }
            if (!string.IsNullOrEmpty(mBrokenDoorSFX) && mAudioAssistant != null)
            {
                mAudioAssistant.PlaySound(mBrokenDoorSFX, true);
                mLaserShot = true;
            }
        }

    }

    void OnTriggerEnter(Collider pCollider)
    {
        if (mShapeReference != null)
        {
            if (mShapeReference.gameObject.activeInHierarchy)
            {
                return;
            }
        }
        mShapeReference = pCollider.gameObject.GetComponent<PlayerCharacter>();
        if (mShapeReference != null)
        {
            if (!mPlayerHit && !string.IsNullOrEmpty(mPlayerHitSFX))
            {
                AudioManager.Instance.PlaySound(mPlayerHitSFX);
                mPlayerHit = true;
            }
            Attack();
        }

    }

    void OnTriggerStay(Collider pCollider)
    {
        if (mShapeReference == null)
        {
            return;
        }
        if (mShapeReference.gameObject.GetInstanceID() != pCollider.gameObject.GetInstanceID())
        {
            return;
        }
        Attack();
    }

    void OnTriggerExit(Collider pCollider)
    {
        if (mShapeReference == null)
        {
            return;
        }
        if (mShapeReference.gameObject.GetInstanceID() == pCollider.gameObject.GetInstanceID())
        {
            mShapeReference = null;
            mCurrentTime = 0.0f;
            mPlayerHit = false;
        }
    }


    /// <summary>
    /// Function Called each time player character stays in the attack zone
    /// </summary>
    void Attack()
    {
        if (GameManager.Instance.mState == GameManager.State.Pause)// if game paused don't attack
        {
            return;
        }
        if (GameManager.Instance.mGameData.mPlayerData.mPlayerHealth <= 0) // if player dead don't attack
        {
            return;
        }
        if(mDeactivate)
        {
            return;
        }
        if (mCurrentTime > 0.0f)
        {
            mCurrentTime -= Time.deltaTime;
        }
        if (mCurrentTime <= 0.0f) //if all conditions meet and it is attack time then attack player and update stats
        {
            mCurrentTime = mAttackTime;
            GameManager.Instance.mGameData.mPlayerData.mPlayerHealth -= mShapeReference.mDamage;
            mShapeReference.mAnimator.SetTrigger(mShapeReference.mDamagePlayerTrigger);
            StartCoroutine(PlayMeleeAttackFx());
        }
    }

    /// <summary>
    /// Play Visual Effect to show player is damaged
    /// </summary>
    /// <returns></returns>
    IEnumerator PlayMeleeAttackFx()
    {
        GameObject aAttackFxObject = ObjectPoolingManager.Instance.GetPooledObject(mMeleeAttackFx.mPoolName);
        ParticleSystem aAttackParticle = aAttackFxObject.GetComponent<ParticleSystem>();
        aAttackFxObject.transform.position = mShapeReference.transform.position;
        aAttackParticle.Clear();
        aAttackFxObject.SetActive(true);
        aAttackParticle.Play();
        yield return new WaitWhile(() => aAttackParticle.isPlaying);
        aAttackFxObject.SetActive(false);
        ObjectPoolingManager.Instance.ReturnPooledObject(mMeleeAttackFx.mPoolName, aAttackFxObject);
    }

}
