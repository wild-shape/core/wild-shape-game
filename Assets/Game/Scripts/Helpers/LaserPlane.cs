﻿///-------------------------------------------------------------------------------------------------
// File: LaserPlane.cs
//
// Author: Dakshvir Singh Rehill
// Date: 2/8/2020
//
// Summary:	Used to bake lightmaps and then disabled
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class LaserPlane : MonoBehaviour
{
    [SerializeField] BoxCollider mSizeCol;
    void Start()
    {
        transform.localScale = mSizeCol.size;
    }

    
    void Update()
    {
        
    }
}
