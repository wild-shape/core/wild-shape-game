﻿///-------------------------------------------------------------------------------------------------
// File: EnemyCheckIn.cs
//
// Author: Dakshvir Singh Rehill
// Date: 17/6/2020
//
// Summary:	A Checkin point which allows one enemy to go check for another enemy if it missed
// its checkpoint time
///-------------------------------------------------------------------------------------------------
using BehaviorDesigner.Runtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCheckIn : MonoBehaviour
{
    /// <summary>
    /// Helper Inner Class that allows for storing information about the enemies that
    /// are waiting to be checked in as well as the ones that are currently checked in
    /// </summary>
    public class CheckedInEnemy
    {
        /// <summary>
        /// The time the checkedin enemy will stay at the checkin point
        /// </summary>
        public float mCurrentWaitTime;
        /// <summary>
        /// The index of the order of the enemy checkin
        /// </summary>
        public int mEnemyIx;
        /// <summary>
        /// The reference to the enemy's instance
        /// </summary>
        public MeleeEnemy mEnemyInstance;
    }
    [Tooltip("This time is the time each enemy will wait if this is their charging point")]
    public float mChargingWait = 5.0f;
    [Tooltip("All the enemies that will charge at this point in the order of their charging")]
    [SerializeField] List<MeleeEnemy> mEnemyOrder;
    [Tooltip("The maximum distance before which each enemy will stop near the checkpoint")]
    [SerializeField] float mStoppingDistance = 0.5f;
    /// <summary>
    /// The current enemy that is charging at the checkinpoint
    /// </summary>
    CheckedInEnemy mCurrentChargingEnemy = null;
    /// <summary>
    /// The list of all enemies that are checked in but currently not charging
    /// </summary>
    readonly List<CheckedInEnemy> mEnemiesWaiting = new List<CheckedInEnemy>();
    /// <summary>
    /// Once an enemy reaches the checkin point, their visited becomes true
    /// </summary>
    readonly List<bool> mVisited = new List<bool>();

    void Awake()
    {
        if (mEnemyOrder == null)
        {
            return;
        }
        for (int aI = 0; aI < mEnemyOrder.Count; aI++)
        {
            mVisited.Add(false);
            mEnemyOrder[aI].mCheckInPoint = gameObject;
        }
    }

    void Update()
    {
        if(mCurrentChargingEnemy == null)
        {
            return;
        }
        List<CheckedInEnemy> aEnemyToRemove = new List<CheckedInEnemy>();
        //loop through all waiting enemies to check if any detected the player
        //if someone detected the player, allow that enemy to leave and chase the player
        foreach(CheckedInEnemy aEnemyWaiting in mEnemiesWaiting)
        {
            if(aEnemyWaiting.mEnemyInstance.mDetectionMeterVal.Value < aEnemyWaiting.mEnemyInstance.mUndetectionVal)
            {
                aEnemyWaiting.mEnemyInstance.mAgent.isStopped = true;
            }
            else
            {
                aEnemyToRemove.Add(aEnemyWaiting);
            }
        }
        foreach(CheckedInEnemy aEnemyToR in aEnemyToRemove)
        {
            mEnemiesWaiting.Remove(aEnemyToR);
        }
        aEnemyToRemove.Clear();
        //run the enemy wait timer
        mCurrentChargingEnemy.mCurrentWaitTime -= Time.deltaTime;
        if(mCurrentChargingEnemy.mCurrentWaitTime <= 0) //if enemy wait time is done
        {
            if(mCurrentChargingEnemy.mEnemyIx != 0)
            {
                if(!mVisited[mCurrentChargingEnemy.mEnemyIx - 1]) // check if enemy before this enemy in order reached or not if not then go look for the enemy
                {
                    MeleeEnemy aEnemy = mEnemyOrder[mCurrentChargingEnemy.mEnemyIx - 1];
                    if(aEnemy.mWaypointParent != null)
                    {
                        for(int aI = 0; aI < aEnemy.mWaypointParent.transform.childCount; aI ++)
                        {
                            if(aEnemy.mWaypointParent.transform.GetChild(aI) != transform)
                            {
                                mCurrentChargingEnemy.mEnemyInstance.mInspectPoints.Add(aEnemy.mWaypointParent.transform.GetChild(aI).position);
                            }
                        }
                    }
                    //set lost enemy in AI tree to allow for easy finding
                    mCurrentChargingEnemy.mEnemyInstance.mTree.SetVariableValue("mLostEnemy", aEnemy);
                }
            }
            mCurrentChargingEnemy = null;
            if(mEnemiesWaiting.Count > 0) //if there are enemies waiting, allow them to become the next charging enemy
            {
                mCurrentChargingEnemy = mEnemiesWaiting[0];
                mEnemiesWaiting.RemoveAt(0);
                mCurrentChargingEnemy.mEnemyInstance.mAgent.isStopped = false;
                mVisited[mCurrentChargingEnemy.mEnemyIx] = true;
            }
        }
    }
    /// <summary>
    /// Function called when the checkin point is triggered
    /// </summary>
    /// <param name="pOther"></param>
    public void TriggerEvent(Collider pOther)
    {
        if (mEnemyOrder == null)
        {
            return;
        }
        MeleeEnemy aEnemy = pOther.GetComponent<MeleeEnemy>();
        if (aEnemy != null) // check if its enemy
        {
            if (aEnemy.mAgent == null)
            {
                return;
            }
            if (aEnemy.mDetectionMeterVal.Value > aEnemy.mUndetectionVal) //don't add for checkin if chasing player
            {
                return;
            }
            if (!mEnemyOrder.Contains(aEnemy)) // check if the enemy is supposed to charge here
            {
                return;
            }
            if ((transform.position - aEnemy.mAgent.destination).magnitude <= mStoppingDistance) // check if enemy is within stopping distance
            {
                if (mCurrentChargingEnemy == null) // if there is no charging enemy set this one as charging enemy
                {
                    mCurrentChargingEnemy = new CheckedInEnemy()
                    {
                        mEnemyInstance = aEnemy,
                        mEnemyIx = mEnemyOrder.IndexOf(aEnemy),
                        mCurrentWaitTime = mChargingWait
                    };
                    mVisited[mCurrentChargingEnemy.mEnemyIx] = true;
                }
                else //else add it to the waiting list
                {
                    mEnemiesWaiting.Add(new CheckedInEnemy()
                    {
                        mEnemyInstance = aEnemy,
                        mEnemyIx = mEnemyOrder.IndexOf(aEnemy),
                        mCurrentWaitTime = mChargingWait
                    });
                    aEnemy.mAgent.isStopped = true;
                }
            }
        }
    }
}
