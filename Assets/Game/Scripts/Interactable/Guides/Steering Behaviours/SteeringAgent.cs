﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringAgent : MonoBehaviour
{
    [Tooltip("The Mass of the Agent")]
    public float mMass = 1.0f;
    [Tooltip("The Max Speed of the Agent")]
    public float mMaxSpeed = 1.0f;
    [Tooltip("The Max Force of the Agent")]
    public float mMaxForce = 10.0f;
    /// <summary>
    ///The Velocity of the Agent
    /// </summary>
    [HideInInspector] public Vector3 mVelocity = Vector3.zero;

    private List<SteeringBehaviourBase> mSteeringBehaviours = new List<SteeringBehaviourBase>();

    [Tooltip("The Angular Dampening of the Agent")]
    public float mAngularDampeningTime = 5.0f;
    [Tooltip("The Dead Zone for the Agent")]
    public float mDeadZone = 10.0f;


    private void Start()
    {
        mSteeringBehaviours.AddRange(GetComponentsInChildren<SteeringBehaviourBase>());
        foreach (SteeringBehaviourBase aBehaviour in mSteeringBehaviours)
        {
            aBehaviour.mSteeringAgent = this;
        }
    }

    private void Update()
    {
        Vector3 aSteeringForce = calculateSteeringForce();
        aSteeringForce.y = 0.0f;
        Vector3 aAcceleration = aSteeringForce * (1.0f / mMass);
        mVelocity = mVelocity + (aAcceleration * Time.deltaTime);
        mVelocity = Vector3.ClampMagnitude(mVelocity, mMaxSpeed);
        float aSpeed = mVelocity.magnitude;
        if (aSpeed > 0.0f)
        {
            float aAngle = Vector3.Angle(transform.parent.forward, mVelocity);
            if (Mathf.Abs(aAngle) <= mDeadZone)
            {
                transform.parent.LookAt(transform.parent.position + mVelocity);
            }
            else
            {
                transform.parent.rotation = Quaternion.Slerp(transform.parent.rotation,
                                                      Quaternion.LookRotation(mVelocity),
                                                      Time.deltaTime * mAngularDampeningTime);
            }
        }
    }

    private Vector3 calculateSteeringForce()
    {
        Vector3 aTotalForce = Vector3.zero;
        foreach (SteeringBehaviourBase aBehaviour in mSteeringBehaviours)
        {
            if (aBehaviour.enabled)
            {
                aTotalForce = aTotalForce + (aBehaviour.calculateForce() * aBehaviour.mWeight);
                aTotalForce = Vector3.ClampMagnitude(aTotalForce, mMaxForce);
            }
        }
        return aTotalForce;
    }
}
