﻿///-------------------------------------------------------------------------------------------------
// File: RockFall.cs
//
// Author: Dakshvir Singh Rehill
// Date: 15/07/2020
//
// Summary:	Moves the Rock from one point to another when function is called
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockFall : MonoBehaviour
{
    [Tooltip("The Rigidbody of the Rock")]
    [SerializeField] Rigidbody mRockParent;
    [Tooltip("Audio Assisstant component for 3D sound")]
    [SerializeField] AudioAssistant mAudioAssistant;
    [Tooltip("The Sound Effect for Rock Destroy")]
    [SerializeField] string mRockBreakSFX = "RockDestroy";

    bool mRockDestroyed;

    public void DropRocks(Collider pCollider)
    {
        if (!mRockParent.isKinematic)
        {
            return;
        }
        if (pCollider.GetComponent<PlayerCharacter>() == null)
        {
            return;
        }
        mRockParent.isKinematic = false;
    }

    public void PlaySound()
    {
        if (!mRockDestroyed)
        {
            if (!string.IsNullOrEmpty(mRockBreakSFX) && mAudioAssistant != null)
            {
                mAudioAssistant.PlaySound(mRockBreakSFX);
                mRockDestroyed = true;
            }
        }
    }
}
