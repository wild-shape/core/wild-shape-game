﻿///-------------------------------------------------------------------------------------------------
// File: TargetLookAt.cs
//
// Author: Dakshvir Singh Rehill
// Date: 4/6/2020
//
// Summary:	Scripts that follows the player while player in view
///-------------------------------------------------------------------------------------------------
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;
[TaskCategory("Enemy/Ranged")]
[TaskDescription("Keep Looking At Player While Player in View")]
public class TargetLookAt : EnemyActionBase
{
    [BehaviorDesigner.Runtime.Tasks.Tooltip("Bool to check if can see has to be checked or last known position has to be checked")]
    [SerializeField] SharedBool mLastKnownLookAt = false;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("Bool to check if the enemy can see player")]
    [SerializeField] SharedBool mCanSee;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The last known position of the player")]
    [SerializeField] SharedVector3 mPlayerLastKnown;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The deadzone after which the enemy won't rotate as it wud b looking at player")]
    [SerializeField] SharedFloat mRotationDeadzone = 5;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The maximum radius that the enemy should look around after loosing the player")]
    [SerializeField] SharedFloat mMaxWanderRadius = 8.0f;

    /// <summary>
    /// The last point that was checked by the enemy
    /// </summary>
    Vector3 mLastPoint = Vector3.zero;
    /// <summary>
    /// The shape reference for the player to get the active player character
    /// </summary>
    ShapeShift mPlayerShapeInstance;
    /// <summary>
    /// The instance of the ranged enemy instead of base class instance
    /// </summary>
    RangedEnemy mEnemyInstanceRanged;

    public override void OnStart()
    {
        base.OnStart();
        mEnemyInstanceRanged = (RangedEnemy)mEnemyInstance;
        if (mLastKnownLookAt.Value) // if last known needs to be checked set last point as the last known value
        {
            mLastPoint = mPlayerLastKnown.Value;
        }
        if (mPlayerShapeInstance == null) //set shape instance if null
        {
            mPlayerShapeInstance = GameManager.Instance.mSceneHandler.mCameraController.mShapeShiftInst;
        }
    }


    public override TaskStatus OnUpdate()
    {
        Vector3 aTargetPosition = Vector3.zero;
        Vector3 aTargetDistance = Vector3.zero;
        float aRotationSpeed = 0;
        if (!mLastKnownLookAt.Value)//if last known isn't to be checked
        {
            if (mCanSee.Value)//check if enemy can see player and set target as player
            {
                int aCurChar = GameManager.Instance.mGameData.mPlayerData.mCurChar;
                mPlayerLastKnown.Value = mPlayerShapeInstance.mPlayerCharacters[aCurChar].mCameraLookAt.position;
                aTargetPosition = mPlayerShapeInstance.mPlayerCharacters[aCurChar].mCameraLookAt.position;
                aTargetDistance = aTargetPosition - mEnemyInstance.mOrigin.transform.position;
                aRotationSpeed = mEnemyInstance.mEnemyAlertSpeed;
                mEnemyInstance.mInspectPoints.Clear();
            }
            else
            {
                return TaskStatus.Failure;
            }
        }
        else if (mLastPoint.sqrMagnitude <= float.Epsilon) //if last point isn't set i.e. 0,0,0 return failure
        {
            return TaskStatus.Failure;
        }
        else //use the last point to move enemy and if in deadzone choose a random last point
        {
            aTargetDistance = mLastPoint - mEnemyInstance.mOrigin.transform.position;
            aRotationSpeed = mEnemyInstance.mEnemySpeed;
            if (Mathf.Abs(Vector3.Angle(mEnemyInstanceRanged.mXZRotationPvt.forward, aTargetDistance)) <= mRotationDeadzone.Value)
            {
                SetNewWanderPoint();
                aTargetDistance = mLastPoint - mEnemyInstance.mOrigin.transform.position;
            }
        }
        //rotate to target distance after finding the rotation values
        Quaternion aDesiredRotation = Quaternion.LookRotation(aTargetDistance);
        mEnemyInstanceRanged.mYRotationPvt.rotation = Quaternion.Slerp(mEnemyInstanceRanged.mYRotationPvt.rotation, Quaternion.Euler(0, aDesiredRotation.eulerAngles.y, 0), aRotationSpeed * Time.deltaTime);
        mEnemyInstanceRanged.mXZRotationPvt.rotation = Quaternion.Slerp(mEnemyInstanceRanged.mXZRotationPvt.rotation, aDesiredRotation, aRotationSpeed * Time.deltaTime);
        return TaskStatus.Running;
    }

    /// <summary>
    /// Sets a new wander point around the last known point of the player
    /// </summary>
    void SetNewWanderPoint()
    {
        float aTheta = Random.value * Mathf.PI * 2.0f;
        float aRadius = mMaxWanderRadius.Value * Random.value;
        mLastPoint = mPlayerLastKnown.Value + new Vector3(aRadius * Mathf.Cos(aTheta), 0, aRadius * Mathf.Sin(aTheta));
    }

}
