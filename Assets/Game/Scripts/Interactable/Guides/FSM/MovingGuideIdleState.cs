﻿///-------------------------------------------------------------------------------------------------
// File: MovingGuideIdleState.cs
//
// Author: Dakshvir Singh Rehill
// Date: 22/7/2020
//
// Summary:	Idle State Behaviour for Moving Guide's Finite State Machine
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingGuideIdleState : GuideIdleState
{
    MovingGuide mMovingInst;
    [SerializeField] string mMoveToLocationName = "MoveToLocation";
    [SerializeField] string mIdleName = "Idle";
    [SerializeField] string mNextConvName = "NextConversation";
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.ResetTrigger(mMoveToLocationName);
        animator.ResetTrigger(mIdleName);
        animator.ResetTrigger(mNextConvName);
        base.OnStateEnter(animator, stateInfo, layerIndex);
        if (mMovingInst == null)
        {
            mMovingInst = (MovingGuide)mGuideRef;
        }
        if (mMovingInst.mNPCDialogue.mSummarize)
        {
            NPCLocationDialog aLocationDialog = mMovingInst.mDynamicConversationDialogs[mMovingInst.mDynamicIndex];
            aLocationDialog.mDialogInfo = mMovingInst.mNPCDialogue;
            mMovingInst.mDynamicConversationDialogs[mMovingInst.mDynamicIndex] = aLocationDialog;
            animator.SetTrigger(mMovingInst.mDynamicConversationDialogs[mMovingInst.mDynamicIndex].mConditionCheckTriggerName);
        }
    }
}
