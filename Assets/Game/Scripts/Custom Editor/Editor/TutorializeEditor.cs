﻿///-------------------------------------------------------------------------------------------------
// File: TutorializeEditor.cs
//
// Author: Aditya Dinesh
// Date: 28/7/2020
//
// Summary:	Custom Inspector display for Tutorialize
///-------------------------------------------------------------------------------------------------

using UnityEditor;

[CustomEditor(typeof(Tutorialize))]
public class TutorializeEditor : Editor
{
    SerializedProperty mTutorialInfo;
    SerializedProperty mMechanicsToTeach;

    Tutorialize mTutorialize = null;

    private void OnEnable()
    {
        mTutorialize = target as Tutorialize;

        mTutorialInfo = serializedObject.FindProperty("mTutorialInfo");
        mMechanicsToTeach = serializedObject.FindProperty("mMechanicsToTeach");
    }

    public override void OnInspectorGUI()
    {
        if (mTutorialize != null)
        {
            EditorGUILayout.PropertyField(mTutorialInfo);
            EditorGUILayout.PropertyField(mMechanicsToTeach);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
