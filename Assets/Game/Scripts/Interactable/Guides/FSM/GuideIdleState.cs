﻿///-------------------------------------------------------------------------------------------------
// File: GuideIdleState.cs
//
// Author: Dakshvir Singh Rehill
// Date: 22/7/2020
//
// Summary:	Idle State Behaviour for Guide's Finite State Machine
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuideIdleState : GuideBaseState
{
    [SerializeField] string mTalkAnimBoolName = "talk";

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        mGuideRef.mGuideAnimator.SetBool(mTalkAnimBoolName, false);
    }
}
