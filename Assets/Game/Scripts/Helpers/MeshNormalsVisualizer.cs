﻿///-------------------------------------------------------------------------------------------------
// File: MeshNormalsVisualizer.cs
//
// Author: Dakshvir Singh Rehill
// Date: 30/7/2020
//
// Summary:	Editor Utility that allows us to see mesh normals in scene view
///-------------------------------------------------------------------------------------------------
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class MeshNormalsVisualizer : MonoBehaviour
{
#if UNITY_EDITOR
    [SerializeField]
    private MeshFilter mMeshFilter = null;
    [SerializeField]
    private bool mDisplayWireframe = false;
    [SerializeField]
    private NormalsDrawData mFaceNormals = new NormalsDrawData(new Color(0.13f, 0.86f, 0.86f, 0.6f), true);
    [SerializeField]
    private NormalsDrawData mVertexNormals = new NormalsDrawData(new Color(0.78f, 1, 0.76f, 0.49f), false);

    [System.Serializable]
    private class NormalsDrawData
    {
        [SerializeField]
        protected DrawType mDraw = DrawType.Selected;
        protected enum DrawType { Never, Selected, Always }
        [SerializeField]
        protected float mLength = 0.3f;
        [SerializeField]
        protected Color mNormalColor;
        private Color mBaseColor = new Color(1, 0.52f, 0, 1);
        private const float mBaseData = 0.0125f;


        public NormalsDrawData(Color pNormalColor, bool pDraw)
        {
            mNormalColor = pNormalColor;
            mDraw = pDraw ? DrawType.Selected : DrawType.Never;
        }

        public bool CanDraw(bool pIsSelected)
        {
            return (mDraw == DrawType.Always) || (mDraw == DrawType.Selected && pIsSelected);
        }

        public void Draw(Vector3 pFrom, Vector3 pDirection)
        {
            if (Camera.current.transform.InverseTransformDirection(pDirection).z < 0f)
            {
                Gizmos.color = mBaseColor;
                Gizmos.DrawWireSphere(pFrom, mBaseData);

                Gizmos.color = mNormalColor;
                Gizmos.DrawRay(pFrom, pDirection * mLength);
            }
        }
    }

    void OnDrawGizmosSelected()
    {
        EditorUtility.SetSelectedRenderState(GetComponent<Renderer>(), !mDisplayWireframe ? EditorSelectedRenderState.Hidden : EditorSelectedRenderState.Wireframe);
        OnDrawNormals(true);
    }

    void OnDrawGizmos()
    {
        if (!Selection.Contains(this))
            OnDrawNormals(false);
    }

    private void OnDrawNormals(bool isSelected)
    {
        if (mMeshFilter == null)
        {
            mMeshFilter = GetComponent<MeshFilter>();
            if (mMeshFilter == null)
                return;
        }

        Mesh aMesh = mMeshFilter.sharedMesh;

        //Draw Face Normals
        if (mFaceNormals.CanDraw(isSelected))
        {
            int[] aTriangles = aMesh.triangles;
            Vector3[] aVertices = aMesh.vertices;

            for (int i = 0; i < aTriangles.Length; i += 3)
            {
                Vector3 aPoint0 = transform.TransformPoint(aVertices[aTriangles[i]]);
                Vector3 aPoint1 = transform.TransformPoint(aVertices[aTriangles[i + 1]]);
                Vector3 aPoint2 = transform.TransformPoint(aVertices[aTriangles[i + 2]]);
                Vector3 aTriCenter = (aPoint0 + aPoint1 + aPoint2) / 3;

                Vector3 aNormalDir = Vector3.Cross(aPoint1 - aPoint0, aPoint2 - aPoint0);
                aNormalDir /= aNormalDir.magnitude;

                mFaceNormals.Draw(aTriCenter, aNormalDir);
            }
        }

        //Draw Vertex Normals
        if (mVertexNormals.CanDraw(isSelected))
        {
            Vector3[] aVertices = aMesh.vertices;
            Vector3[] aNormals = aMesh.normals;
            for (int i = 0; i < aVertices.Length; i++)
            {
                mVertexNormals.Draw(transform.TransformPoint(aVertices[i]), transform.TransformVector(aNormals[i]));
            }
        }
    }
#endif
}