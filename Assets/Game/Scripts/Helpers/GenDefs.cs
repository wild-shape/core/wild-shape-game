﻿///-------------------------------------------------------------------------------------------------
// File: GenDefs.cs
//
// Author: Dakshvir Singh Rehill
// Date: 19/5/2020
//
// Summary:	Has Static Helper Functions and common Data Structures Defined
///-------------------------------------------------------------------------------------------------
using UnityEngine;
using System;
using System.Collections.Generic;
public class GenHelpers
{
    /// <summary>
    /// Used to shuffle a list of objects
    /// </summary>
    /// <typeparam name="T">Type of list</typeparam>
    /// <param name="pList">List variable</param>
    public static void Shuffle<T>(ref List<T> pList)
    {
        int aN = pList.Count;
        while (aN > 1)
        {
            aN--;
            int aK = UnityEngine.Random.Range(0, aN + 1);
            T aValue = pList[aK];
            pList[aK] = pList[aN];
            pList[aN] = aValue;
        }
    }
    /// <summary>
    /// Function to get the closest point on a line
    /// </summary>
    /// <param name="pOrigin">Line start</param>
    /// <param name="pEnd">Line end</param>
    /// <param name="pPoint">Point to check</param>
    /// <returns>Closest point</returns>
    public static Vector2 GetClosestPoint(Vector2 pOrigin, Vector2 pEnd, Vector2 pPoint)
    {
        Vector2 aDirection = pEnd - pOrigin;
        float aMaxMag = aDirection.magnitude;
        aDirection.Normalize();
        Vector2 aDistFromOrigin = pPoint - pOrigin;
        float aDotProduct = Vector2.Dot(aDistFromOrigin, aDirection);
        aDotProduct = Mathf.Clamp(aDotProduct, 0f, aMaxMag);
        return pOrigin + aDirection * aDotProduct;
    }
    /// <summary>
    /// Function to check if layer is in layermask
    /// </summary>
    /// <param name="layer">layer</param>
    /// <param name="layermask">layermask</param>
    /// <returns></returns>
    public static bool IsInLayerMask(int pLayer, LayerMask pLayermask)
    {
        return pLayermask == (pLayermask | (1 << pLayer));
    }


}

/// <summary>
/// Used for pooled objects that need to set some default functionality while they are inactive 
/// </summary>
public interface IInitable
{
    /// <summary>
    /// Function to allow for setting up variables and components before start or enable
    /// </summary>
    void Init();
}

/// <summary>
/// Used on all animation listeners so that state behaviours could inform animation completion
/// </summary>
public interface IAnimationCompleted
{
    /// <summary>
    /// The Function that is called on animation completed
    /// </summary>
    /// <param name="pShortHashName">Hash name of the animation</param>
    void AnimationCompleted(int pShortHashName);
}

/// <summary>
/// Analytics Data for the player as a whole
/// </summary>

[Serializable]
public struct PlayerAnalyticsData
{
    public string mPlayerID;
    public string mPlayerControlScheme;
    public float mTimeAsRaccoon;
    public float mTimeAsBear;
    public int mEnemiesAlerted;
    public int mPushUsed;
    public int mHideUsed;
    public int mDeactivateUsed;
    public int mShapeshiftUsed;
    public int mFreezeUsed;
    public int mBlinkUsed;
}

/// <summary>
/// Analytics for individual analytics in the game
/// </summary>
[Serializable]
public struct InteractableAnalyticsData
{
    public string mPlayerID;
    public int mInteractableID;
    public InteractableType mInteractable;
    public int mNoOfInteractions;
}

/// <summary>
/// Analytics for all enemies
/// </summary>
[Serializable]
public struct EnemyAnalyticsData
{
    public string mPlayerID;
    public int mEnemyID;
    public Enemy.Type mEnemyType;
    public int mTimesFrozen;
    public int mTimesUnAlerted;
    public int mTimesAlerted;
    public int mTimesAttacked;
    public int mTimesAttackSuccessful;
    public EnemyDeathType mDeathBy;
}

/// <summary>
/// The type of death that enemy felt
/// </summary>
[Serializable]
public enum EnemyDeathType
{
    NotDead,
    Push,
    Fall,
    Deactivate
}

/// <summary>
/// Different type of interactables in game
/// </summary>
[Serializable]
public enum InteractableType
{
    Pushable,
    Hideable,
    PressurePlate,
    Breakable,
    Deactivatable,
    Replenishable
}

/// <summary>
/// The type of controls that the player can change
/// </summary>
[Serializable]
public enum Controls
{
    Shapeshift,
    SpecialAbility,
    CancelAbility,
    Run,
    Interact
}
[Serializable]
public enum LockedControls
{
    Movement,
    Camera,
    Pause
}

/// <summary>
/// A struct that contains information about bindings of each changeable controls
/// </summary>
[Serializable]
public struct ChangeableControls
{
    public Controls mControlType;
    public string mActionName;
    public string mActionID;
    public string mKeyboardPath;
    public string mGamepadPath;
}

/// <summary>
/// Settings Data for Player controls
/// </summary>
[Serializable]
public struct ControlsSaveData
{
    public List<ChangeableControls> mCurrentControlMapping;
    public float mCameraSensitivity;
    public bool mFlipCamera;
    /// <summary>
    /// find control and return that
    /// </summary>
    /// <param name="pControlType">type of control</param>
    /// <returns>Changeable Control</returns>
    public ChangeableControls GetControl(Controls pControlType)
    {
        foreach (ChangeableControls aControlVal in mCurrentControlMapping)
        {
            if (aControlVal.mControlType == pControlType)
            {
                return aControlVal;
            }
        }
        return new ChangeableControls();
    }

}

/// <summary>
/// Audio Volume settings for the game
/// </summary>
[Serializable]
public struct AudioSettingsData
{
    public float[] mAudioVolume;
}

/// <summary>
/// Video Res settings for the player
/// </summary>
[Serializable]
public struct VideoSettingsData
{
    public float mBrightness;
    public int mResolution;
    public int mGraphics;
    public bool mIsFullscreen;
}

/// <summary>
/// The Form of the player
/// </summary>
[System.Serializable]
public enum CharacterForm
{
    Raccoon,
    Bear
}

/// <summary>
/// Save data for the player
/// </summary>
[Serializable]
public struct PlayerData
{
    public int mCurChar;
    public bool mCreditsShown;
    public float mPlayerHealth;
    public float mSpecialAbility;
    public int mCollectiblesCollected;
}

/// <summary>
/// Save data once player passes a checkpoint
/// </summary>
[Serializable]
public struct CheckpointData
{
    public int mSceneIndex;
    public float mCheckpointID;
    public Vector3 mPosition;
    public Vector3 mRotation;
}
/// <summary>
/// Save data for each core visited by the player
/// </summary>
[Serializable]
public struct BaseData
{
    public float mCoreID;
}
///// <summary>
///// Save data for each Collectible collected by player
///// </summary>
//[Serializable]
//public struct CollectibleData
//{
//    public float mCollectibleID;
//}
/// <summary>
/// Overall settings data struct
/// </summary>
[Serializable]
public struct GameSettingsData
{
    public ControlsSaveData mCurrentControls;
    public AudioSettingsData mAudioData;
    public VideoSettingsData mVideoData;

}
[Serializable]
public enum TeachableMechanics
{
    Shapeshift,
    BlinkUp,
    AOEEnemy,
    Hide,
    AOEButton,
    BlinkAcross
}
/// <summary>
/// Overall game data struct that will be stored as save game
/// </summary>
[Serializable]
public struct GameData
{
    public PlayerData mPlayerData;
    public bool[] mTutorialData;
    public CheckpointData mLastCheckpoint;
    public List<CheckpointData> mCheckpointsVisited;
    public List<float> mTalkedNPCs;
    public int mGameSceneIndex;
    public List<BaseData> mBaseData;
    public List<int> mCollectiblesCollected;
    /// <summary>
    /// Function called to ensure the current game save data is up to date with the game changes
    /// </summary>
    public void UpdateDataVersion()
    {
        if(mCheckpointsVisited == null)
        {
            mCheckpointsVisited = new List<CheckpointData>();
        }
        if(mBaseData == null)
        {
            mBaseData = new List<BaseData>();
        }
        if(mTalkedNPCs == null)
        {
            mTalkedNPCs = new List<float>();
        }
        if(mCollectiblesCollected == null)
        {
            mCollectiblesCollected = new List<int>();
        }
        int aTutLength = Enum.GetValues(typeof(TeachableMechanics)).Length;
        if (mTutorialData == null)
        {
            mTutorialData = new bool[aTutLength];
        }
        if(mTutorialData.Length != aTutLength)
        {
            mTutorialData = new bool[aTutLength];
        }
    }
    /// <summary>
    /// add checkpoint or update it if already exists
    /// </summary>
    /// <param name="pData">Checkpoint Data</param>
    public void UpdateCheckpointsVisited(CheckpointData pData)
    {
        for(int aI = 0; aI < mCheckpointsVisited.Count; aI ++)
        {
            if(Mathf.Abs(mCheckpointsVisited[aI].mCheckpointID - pData.mCheckpointID) <= float.Epsilon)
            {
                mCheckpointsVisited[aI] = pData;
                return;
            }
        }
        mLastCheckpoint = pData;
        mCheckpointsVisited.Add(pData);
    }
    public bool IsCheckpointVisited(float pCheckpointID)
    {
        foreach( CheckpointData aData in mCheckpointsVisited)
        {
            if(Mathf.Abs(aData.mCheckpointID - pCheckpointID) <= float.Epsilon)
            {
                return true;
            }
        }
        return false;
    }
    /// <summary>
    /// Resets all game data instantly
    /// used for new game
    /// </summary>
    public void ResetGameData()
    {
        mPlayerData = new PlayerData
        {
            mCurChar = (int)GameManager.Instance.mPlayerForm,
            mPlayerHealth = 1,
            mCollectiblesCollected = 0,
            mSpecialAbility = 1,
            mCreditsShown = false
        };
        mTutorialData = new bool[Enum.GetValues(typeof(TeachableMechanics)).Length];
        mLastCheckpoint.mSceneIndex = -1;
        mTalkedNPCs = new List<float>();
        mCheckpointsVisited = new List<CheckpointData>();
        mGameSceneIndex = 0;
        mBaseData = new List<BaseData>();
        mCollectiblesCollected = new List<int>();
        SaveGameManager.Instance.SavePlayerData();
    }

}

[Serializable]
public enum ControlSchemeType
{
    PS,
    XBox,
    KeyboardMouse
}

[Serializable]
public struct PromptContainer
{
    public InputPromptSettings mPromptSettings;
    public Sprite mPromptImage;
}

[Serializable]
public struct InputPromptSettings
{
    public ControlSchemeType mSchemeType;
    public string mInputBinding;
    public string mFallbackText;
}

[Serializable]
public struct InputPromptContainer
{
    public List<InputPromptSettings> mLockedControls;
    public List<InputPromptSettings> mUnlockedControls;
}


/// <summary>
/// All the strings that need to be displayed in the dialog box
/// </summary>
[System.Serializable]
public struct DialogBoxStrings
{
    [Tooltip("The text that comes at the top of the dialog box")]
    public string mHeadingText;
    [Tooltip("The text that comes in the middle of the dialog box")]
    public string mBodyText;
    [Tooltip("The text on the first button of the dialog box")]
    public string mButtonOneText;
    [Tooltip("The text on the second button if second button is needed on the dialog box")]
    public string mButtonTwoText;
}
