﻿///-------------------------------------------------------------------------------------------------
// File: HUDPromptsMenu.cs
//
// Author: Dakshvir Singh Rehill,Aditya Dinesh
// Date: 2/6/2020
//
// Summary:	Interaction Prompts Menu to the user from HUD
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using UnityEngine.UI;

public class HUDPromptsMenu : Menu
{
    [System.Serializable]
    public struct PromptAffordance
    {
        [Tooltip("The Color that needs to be set for the text")]
        public Color mPromptColor;
        [Tooltip("The Sprite that needs to be set for the prompt BG")]
        public Sprite[] mPromptSprite;
    }
    [Tooltip("The display in the form of Text")]
    [SerializeField] TextMeshProUGUI mFallbackText;
    [Tooltip("The display in the form of Image")]
    [SerializeField] Image mControlImage;
    [Tooltip("The parent of both controls")]
    [SerializeField] Image mPromptObject;
    [Tooltip("The Affordance Settings for Interact Possible")]
    [SerializeField] PromptAffordance mEnabledInteract;
    [Tooltip("The Affordance Settings for Interact Not Possible")]
    [SerializeField] PromptAffordance mDisabledInteract;
    Sprite mCurrentEnabledInt = null;
    /// <summary>
    /// Show the text of the control prompt
    /// </summary>
    /// <param name="pControl"></param>
    public void ShowPromptsMenu(Controls pControl)
    {
        mPromptObject.gameObject.SetActive(false);
        mControlImage.gameObject.SetActive(false);
        mFallbackText.gameObject.SetActive(false);
        ChangeableControls aControlValues = GameManager.Instance.mSettingsData.mCurrentControls.GetControl(pControl);
        string aControlPath = InputPromptHandler.Instance.mActiveControlSchemeType != ControlSchemeType.KeyboardMouse ?
            aControlValues.mGamepadPath.Substring(aControlValues.mGamepadPath.LastIndexOf(">/") + 2) :
            aControlValues.mKeyboardPath.Substring(aControlValues.mKeyboardPath.LastIndexOf(">/") + 2);
        PromptContainer aPrompt;
        if (InputPromptHandler.Instance.mUIPrompts[InputPromptHandler.Instance.mActiveControlSchemeType]
            .TryGetValue(aControlPath, out aPrompt))
        {
            mPromptObject.sprite = InputPromptHandler.Instance.mInteractImg;
            Sprite aMBTNSprite = null;
            if (InputPromptHandler.Instance.mActiveControlSchemeType == ControlSchemeType.KeyboardMouse)
            {
                aMBTNSprite = aPrompt.mPromptSettings.mFallbackText.Contains("MBTN") ? InputPromptHandler.Instance.mMBtnImg[int.Parse(aPrompt.mPromptSettings.mFallbackText.Substring(4))] : null;
                if (aMBTNSprite != null)
                {
                    mPromptObject.sprite = aMBTNSprite;
                }
                else
                {
                    mPromptObject.sprite = aPrompt.mPromptSettings.mFallbackText.Length >= 3 ? InputPromptHandler.Instance.mLongKeyImg : InputPromptHandler.Instance.mKeyImg;
                }
            }
            if(aMBTNSprite == null)
            {
                mFallbackText.text = aPrompt.mPromptSettings.mFallbackText;
                mControlImage.sprite = aPrompt.mPromptImage;
                if (mControlImage.sprite != null)
                {
                    mControlImage.gameObject.SetActive(true);
                }
                else
                {
                    mFallbackText.gameObject.SetActive(true);
                }
            }
            else
            {
                mFallbackText.text = "";
            }
            MenuManager.Instance.ShowMenu(mMenuClassifier);
            mCurrentEnabledInt = mPromptObject.sprite;
            mPromptObject.gameObject.SetActive(true);
        }
    }

    /// <summary>
    /// Function is called when player inside interactable trigger but interaction conditions aren't met
    /// </summary>
    public void SetDisabledAffordance(bool pCorrectChar = false)
    {
        mFallbackText.color = mDisabledInteract.mPromptColor;
        if (!pCorrectChar)
        {
            mFallbackText.gameObject.SetActive(false);
            int aSpriteIx = (GameManager.Instance.mGameData.mPlayerData.mCurChar + 1) % mDisabledInteract.mPromptSprite.Length;
            mPromptObject.sprite = mDisabledInteract.mPromptSprite[aSpriteIx];
        }
        else
        {
            mPromptObject.sprite = mDisabledInteract.mPromptSprite[2];
        }
    }
    /// <summary>
    /// Function is called when player inside interactable trigger and interaction conditions are met
    /// </summary>
    public void SetEnabledAffordance()
    {
        mFallbackText.color = mEnabledInteract.mPromptColor;
        mFallbackText.gameObject.SetActive(true);
        mPromptObject.sprite = mCurrentEnabledInt;
    }

}
