﻿///-------------------------------------------------------------------------------------------------
// File: CameraController.cs
//
// Author: Dakshvir Singh Rehill
// Date: 19/5/2020
//
// Summary:	Script Responsible for Using new Input System to interact with free look camera
// and hold references to all cameras
///-------------------------------------------------------------------------------------------------
using Cinemachine;
using UnityEngine;
using UnityEngine.Events;

public class CameraController : MonoBehaviour
{
    /// <summary>
    /// Struct to set the controls of the game free look cameras
    /// </summary>
    [System.Serializable]
    public struct CameraControls
    {
        [Tooltip("Max Speed of Camera on this Axis")]
        public float mMaxSpeed;
        [Tooltip("Min Speed of Camera on this Axis")]
        public float mMinSpeed;
        [Tooltip("The Default Invert Axis of the Camera")]
        public bool mDefaultFlip;
    }
    [Tooltip("The Speed and Flip of the Y Axis of the Camera")]
    public CameraControls mYAxis;
    [Tooltip("The rotation speed and flip of the X Axis of the Camera")]
    public CameraControls mXAxis;
    [Tooltip("The current active free look cam of the game")]
    public CinemachineFreeLook mFreeLookCam;
    [Tooltip("The current active free look cam collider")]
    public CinemachineCollider mFreeLookCollision;
    [Tooltip("The current active crawling through cam of the game")]
    public CinemachineVirtualCamera mCrawlThroughCam;
    [Tooltip("The current active crawl cam collision")]
    public CinemachineCollider mCrawlThroughCollision;
    [Tooltip("The class that holds references to both the player shapes")]
    public ShapeShift mShapeShiftInst;
    /// <summary>
    /// The Game Action that governs the movement of the camera
    /// </summary>
    GameActions mGameActions;
    /// <summary>
    /// The unity action that allows for the input binding to be changed in this game action as well
    /// </summary>
    UnityAction<ChangeableControls> mControlAction;

    void Awake()
    {
        mGameActions = new GameActions();
        GameManager.Instance.UpdateAllChangeableControls(mGameActions);
    }
    
    void OnEnable()
    {
        mGameActions.Enable();
        OnCameraSpeedChanged();
    }
    void OnDisable()
    {
        mGameActions.Disable();
    }

    void Start()
    {
        CinemachineCore.GetInputAxis = GetAxisNew; //override cinemachine axis input using input system
        mControlAction = (pControls) => GameManager.Instance.OnControlsChanged(pControls, mGameActions);
        GameManager.Instance.mOnControlsChanged.AddListener(mControlAction);
    }

    void OnDestroy()
    {
       if(GameManager.IsValidSingleton())
        {
            GameManager.Instance.mOnControlsChanged.RemoveListener(mControlAction);
        }
        mGameActions.Dispose();
    }

    public void OnCameraSpeedChanged() //if camera speed is changed using control settings then update the speed
    {
        mFreeLookCam.m_YAxis.m_MaxSpeed = mYAxis.mMinSpeed + (mYAxis.mMaxSpeed - mYAxis.mMinSpeed) * 
            GameManager.Instance.mSettingsData.
            mCurrentControls.mCameraSensitivity;
        mFreeLookCam.m_XAxis.m_MaxSpeed = mXAxis.mMinSpeed + (mXAxis.mMaxSpeed - mXAxis.mMinSpeed) * 
            GameManager.Instance.mSettingsData.
            mCurrentControls.mCameraSensitivity;
        mFreeLookCam.m_YAxis.m_InvertInput = GameManager.Instance.mSettingsData.
            mCurrentControls.mFlipCamera ? !mYAxis.mDefaultFlip : mYAxis.mDefaultFlip;
        mFreeLookCam.m_XAxis.m_InvertInput = GameManager.Instance.mSettingsData.
            mCurrentControls.mFlipCamera ? !mXAxis.mDefaultFlip : mXAxis.mDefaultFlip;
    }

    float GetAxisNew(string pAxisName)
    {
        Vector2 aLookDelta = mGameActions.Camera.Look.ReadValue<Vector2>();
        if (GameManager.Instance.mState == GameManager.State.Pause)
        {
            aLookDelta = Vector2.zero;
        }
        aLookDelta.Normalize();
        if (pAxisName == "Mouse X")
        {
            return aLookDelta.x;
        }
        else if (pAxisName == "Mouse Y")
        {
            return aLookDelta.y;
        }
        return 0;
    }

}
