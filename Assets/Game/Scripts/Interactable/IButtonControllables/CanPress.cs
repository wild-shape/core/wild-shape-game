﻿///-------------------------------------------------------------------------------------------------
// File: CanPress.cs
//
// Author: Dakshvir Singh Rehill
// Date: 30/5/2020
//
// Summary:	Attached to objects that can press pressure plates
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanPress : MonoBehaviour
{
    [Tooltip("Set it to true if this pressure object can only press pressure plates and not break objects")]
    public bool mButtonOnly = false;
}
