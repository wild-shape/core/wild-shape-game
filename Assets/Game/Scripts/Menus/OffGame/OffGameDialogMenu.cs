﻿///-------------------------------------------------------------------------------------------------
// File: OffGameDialogMenu.cs
//
// Author: Dakshvir Singh Rehill
// Date: xx/x/xxxx
//
// Summary:	Summary of this script
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class OffGameDialogMenu : Menu
{
    DialogBoxStrings mDialogStrings;
    [Tooltip("The Dialog Box Object that has references to the dialog box buttons")]
    public DialogBox mDialogBoxUI;
    [Tooltip("The Dialog Box Object that has waiting for input dialog box buttons")]
    public DialogBox mWaitingForInput;

    /// <summary>
    /// Function called to display dialog menu and show dialog box with the set dialog strings
    /// </summary>
    /// <param name="pDialogBoxStrings">The strings that need to be set in the dialog box</param>
    /// <param name="pBtnOneCallback">The callback that needs to occur when button one is pressed</param>
    /// <param name="pBtnTwoCallback">The callback that needs to occur when button two is pressed</param>
    public void ShowDialogBox(DialogBoxStrings pDialogBoxStrings, UnityAction pBtnOneCallback = null, UnityAction pBtnTwoCallback = null)
    {
        mDialogBoxUI.mHeadingText.text = pDialogBoxStrings.mHeadingText;
        mDialogBoxUI.mBodyText.text = pDialogBoxStrings.mBodyText;
        mDialogBoxUI.mButtonOneText.text = pDialogBoxStrings.mButtonOneText;
        mDialogBoxUI.mButtonOne.interactable = true;
        if (pBtnOneCallback != null) //add listener if callback set
        {
            mDialogBoxUI.mButtonOne.onClick.AddListener(pBtnOneCallback);
        }
        mDialogBoxUI.mButtonOne.onClick.AddListener(HideDialogBox);
        Navigation aBtnNav = mDialogBoxUI.mButtonOne.navigation;
        if (pBtnTwoCallback != null) //add button two if callback set
        {
            mDialogBoxUI.mButtonTwo.interactable = true;
            mDialogBoxUI.mButtonTwo.gameObject.SetActive(true);
            mDialogBoxUI.mButtonTwo.onClick.AddListener(pBtnTwoCallback);
            mDialogBoxUI.mButtonTwoText.text = pDialogBoxStrings.mButtonTwoText;
            mDialogBoxUI.mButtonTwo.onClick.AddListener(HideDialogBox);
            aBtnNav.selectOnDown = aBtnNav.selectOnUp = aBtnNav.selectOnLeft = aBtnNav.selectOnRight = mDialogBoxUI.mButtonTwo;
        }
        else
        {
            mDialogBoxUI.mButtonTwo.interactable = false;
            mDialogBoxUI.mButtonTwo.gameObject.SetActive(false);
            aBtnNav.selectOnDown = aBtnNav.selectOnUp = aBtnNav.selectOnLeft = aBtnNav.selectOnRight = mDialogBoxUI.mButtonOne;
        }
        mDialogBoxUI.mButtonOne.navigation = aBtnNav;
        mDialogBoxUI.mButtonOne.Select();
        mDialogBoxUI.gameObject.SetActive(true);
        MenuManager.Instance.ShowMenu(mMenuClassifier);
    }

    /// <summary>
    /// Show Dialog Box when waiting for input binding input
    /// </summary>
    /// <param name="pDialogStrings">Dialog Box string</param>
    public void ShowWaitForInputDialog(DialogBoxStrings pDialogStrings)
    {
        mWaitingForInput.mHeadingText.text = pDialogStrings.mHeadingText;
        mWaitingForInput.mBodyText.text = pDialogStrings.mBodyText;
        mWaitingForInput.gameObject.SetActive(true);
        mWaitingForInput.mButtonOne.Select();
        MenuManager.Instance.ShowMenu(mMenuClassifier);

    }

    /// <summary>
    /// Hide Dialog Box and Dialog box menu using this function
    /// </summary>
    public void HideDialogBox()
    {
        mWaitingForInput.gameObject.SetActive(false);
        mDialogBoxUI.gameObject.SetActive(false);
        mDialogBoxUI.mButtonTwo.interactable = mDialogBoxUI.mButtonOne.interactable = false;
        mDialogBoxUI.mButtonOne.onClick.RemoveAllListeners();
        mDialogBoxUI.mButtonTwo.onClick.RemoveAllListeners();
        MenuManager.Instance.HideMenu(mMenuClassifier);
    }


}
