﻿///-------------------------------------------------------------------------------------------------
// File: LaserEndPoint.cs
//
// Author: Aditya Dinesh, Dakshvir Singh Rehill
// Date: 28/5/2020
//
// Summary:	Handles the behavior of a immobilizing player for sometime
///-------------------------------------------------------------------------------------------------

using BehaviorDesigner.Runtime;
using System.Collections;
using UnityEngine;

public class LaserEndPoint : MonoBehaviour
{
    [Tooltip("The layers that doesn't need to be checked on trigger enter")]
    [SerializeField] LayerMask mIgnoreLayer;
    [Tooltip("The Enemy Reference to whom this Laser End Point belongs to")]
    [SerializeField] Enemy mEnemy;
    [Tooltip("The Pool Classifier for the Player Damage Fx")]
    [SerializeField] PoolClassifier mRangedAttackFx;
    [Tooltip("Sound Effect for Ranged Shoot")]
    [SerializeField] string mShootLaser = "RangedShoot";
    [Tooltip("The Line Renderer for the laser that the ranged enemy shoots")]
    public ParticleSystem mRangedProjectileEfct;
    /// <summary>
    /// Boolean to check if enemy has shot or not
    /// </summary>
    bool mHasShot;
    /// <summary>
    /// The Time till the player stays frozen to be set via the player character
    /// </summary>
    SharedFloat mFreezeTimer;
    /// <summary>
    /// A boolean that stores whether the player was shot or not
    /// </summary>
    SharedBool mShotPlayer;
    /// <summary>
    /// Ensures that the laser point doesn't keep shooting the player
    /// </summary>
    [HideInInspector] public bool mActive = false;
    void Start()
    {
        //get the shared variables from behavior tree and set urself as false
        mFreezeTimer = (SharedFloat) mEnemy.mTree.GetVariable("mFreezeTimer");
        mShotPlayer = (SharedBool) mEnemy.mTree.GetVariable("mShotPlayer");
        mActive = false;
    }

    private void Update()
    {
        if(mActive && !mRangedProjectileEfct.isPlaying)
        {
            mRangedProjectileEfct.Play();
        }
        else if(!mActive && mRangedProjectileEfct.isPlaying)
        {
            mRangedProjectileEfct.Stop();
        }
        if(mActive && !mHasShot)
        {
            if (!string.IsNullOrEmpty(mShootLaser) && mEnemy.mAudioAssistantSFX != null)
            {
                mEnemy.mAudioAssistantSFX.PlaySound(mShootLaser);
                mHasShot = true;
            }
        }
        if(!mActive && mHasShot)
        {
            mHasShot = false;
        }
    }

    private void OnTriggerEnter(Collider pCollider)
    {
        if(!mActive)
        {
            return;
        }
        if(pCollider.isTrigger) //no need to interact with a trigger collider
        {
            return;
        }
        if(GenHelpers.IsInLayerMask(pCollider.gameObject.layer, mIgnoreLayer)) //check if collider is in ignore layer
        {
            return;
        }
        if(pCollider.attachedRigidbody != null) //if no rigidbody then it means its not part of player
        {
            PlayerCharacter aCharacter = pCollider.attachedRigidbody.GetComponent<PlayerCharacter>(); //get player
            if(aCharacter != null)//if no player don't freeze
            {
                if(aCharacter.mShapeShiftInstance.mFrozen)//if already frozen don't freeze
                {
                    return;
                }
                mFreezeTimer.Value = aCharacter.FreezePlayer();//freeze the player and get the freeze time and assign it
                StartCoroutine(PlayRangedAttackFx(aCharacter.transform.position));
                mShotPlayer.Value = true; //set shot player to true so that ranged enemy doesn't shoot again

                
            }
        }
    }
    /// <summary>
    /// Play Visual Effect to depict that the player is frozen
    /// </summary>
    /// <param name="pPosition">Position of the player</param>
    /// <returns></returns>
    IEnumerator PlayRangedAttackFx(Vector3 pPosition)
    {
        GameObject aAttackFxObject = ObjectPoolingManager.Instance.GetPooledObject(mRangedAttackFx.mPoolName);
        ParticleSystem aAttackParticle = aAttackFxObject.GetComponent<ParticleSystem>();
        aAttackFxObject.transform.position = pPosition;
        aAttackParticle.Clear();
        aAttackFxObject.SetActive(true);
        aAttackParticle.Play();
        yield return new WaitWhile(() => mFreezeTimer.Value > 0.0f);
        aAttackParticle.Stop();
        aAttackFxObject.SetActive(false);
        ObjectPoolingManager.Instance.ReturnPooledObject(mRangedAttackFx.mPoolName, aAttackFxObject);
    }


}
