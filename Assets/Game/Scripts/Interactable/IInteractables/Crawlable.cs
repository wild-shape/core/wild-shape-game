﻿///-------------------------------------------------------------------------------------------------
// File: Hideable.cs
//
// Author: Dakshvir Singh Rehill, Aditya Dinesh
// Date: 27/5/2020
//
// Summary:	Attached to objects that can be hidden in by raccoon
///-------------------------------------------------------------------------------------------------
using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;

public class Crawlable : IInteractable
{
    /// <summary>
    /// The state of the Raccoon when it crawls through
    /// or starts crawling or ends crawling or near crawl space
    /// </summary>
    public enum State
    {
        Idle,
        Hiding,
        Hidden,
        UnHiding
    }
    [Header("Crawl Variables")]
    [Tooltip("The Crawled In position of the player")]
    [SerializeField] Transform[] mCrawledInPoints;
    [Tooltip("The minimum distance left to switch to crawled in mode")]
    [SerializeField] float mCrawledInDistance = 0.5f;
    [Tooltip("The hidecam zone distance")]
    [SerializeField] float mHideCamDistError = 0.25f;
    [Tooltip("The normal position of the player")]
    [SerializeField] Transform[] mNormalPositions;
    [Tooltip("The minimum distance left to switch to normal mode")]
    [SerializeField] float mNormalCameraDistance = 0.5f;
    [Header("Camera Settings")]
    [Tooltip("Crawl Through Cam Priority")]
    [SerializeField] int mCamPriority = 20;
    [Tooltip("Max rotation angle of hidden cam")]
    [SerializeField] float mMaxCamRotation = 60.0f;
    /// <summary>
    /// The state of the current character in crawlable
    /// </summary>
    State mHideState = State.Idle;
    /// <summary>
    /// The index of the active normal/crawled in points
    /// </summary>
    int mTransformIx = -1;
    /// <summary>
    /// The offset for follow of the camera
    /// </summary>
    Vector3 mInitFollowOffset;
    /// <summary>
    /// The offset of the look at of the camera
    /// </summary>
    Vector3 mInitTrackedObjectOffset;

    /// <summary>
    /// Composer component of the hide cam
    /// </summary>
    CinemachineComposer mComposer;
    /// <summary>
    /// Transposer component of the hide cam
    /// </summary>
    CinemachineTransposer mTransposer;
    /// <summary>
    /// The follow camera that gets active when player is in crawlable
    /// </summary>
    CinemachineVirtualCamera mCrawlThroughFollowCam;
    /// <summary>
    /// True if player is inside the crawlable
    /// </summary>
    bool mInteracting = false;
    protected override void Start()
    {
        base.Start();
        mCrawlThroughFollowCam = GameManager.Instance.mSceneHandler.mCameraController.mCrawlThroughCam;
        mComposer = mCrawlThroughFollowCam.GetCinemachineComponent<CinemachineComposer>();
        mTransposer = mCrawlThroughFollowCam.GetCinemachineComponent<CinemachineTransposer>();
        mInitFollowOffset = mTransposer.m_FollowOffset;
        mInitTrackedObjectOffset = mComposer.m_TrackedObjectOffset;
        //mInteractableID = AnalyticsManager.Instance.RegisterInteractable(InteractableType.Hideable);
        mHideCamDistError += mCrawledInDistance;
        mInteractableBy = InteractableByType.Stealth;
    }

    protected override void OnTriggerEnter(Collider pOther)
    {
        base.OnTriggerEnter(pOther);
        if (GameManager.Instance.mState == GameManager.State.Pause)
        {
            return;
        }
        if (mActivePlayerCharacter == null)
        {
            return;
        }
        if(mActivePlayerCharacter.gameObject.GetInstanceID() != pOther.gameObject.GetInstanceID())
        {
            return;
        }
        if (mActivePlayerCharacter != null)
        {
            //mActivePlayerCharacter.mGameAction.PlayerBasic.Interact.performed += CrawlMovementModeToggle;
            mActivePlayerCharacter.mShapeShiftInstance.UniqueMovementChange();
        }
        mInteracting = true;
    }

    /// <summary>
    /// Function that is called when interact button is clicked for crawl mode
    /// </summary>
    /// <param name="pContext"></param>
    void CrawlMovementModeToggle(InputAction.CallbackContext pContext)
    {
        if (GameManager.Instance.mState == GameManager.State.Pause)
        {
            return;
        }
        mActivePlayerCharacter.mShapeShiftInstance.UniqueMovementChange();
    }

    protected override void EndInteraction()
    {
        mActivePlayerCharacter.mGameAction.PlayerBasic.Interact.performed -= CrawlMovementModeToggle;
        if (mActivePlayerCharacter.mShapeShiftInstance.mUniqueMovement)
        {
            mActivePlayerCharacter.mShapeShiftInstance.UniqueMovementChange();
        }
        base.EndInteraction();
        mCrawlThroughFollowCam.Priority = 0;
        mInteracting = false;
    }

    protected override void Update()
    {
        base.Update();
        if (mActivePlayerCharacter == null)
        {
            mHideState = State.Idle;
        }
        if(!mInteracting)
        {
            return;
        }
        switch (mHideState) //checks the player position and switches the hide state
        {
            case State.Idle:
                mCrawlThroughFollowCam.Priority = 0;
                if (mActivePlayerCharacter != null)
                {
                    float aMinDist = float.MaxValue;
                    for (int aI = 0; aI < mNormalPositions.Length; aI++) //check distance from position and decide index
                    {
                        Vector3 aDistVec1 = mActivePlayerCharacter.transform.position - mNormalPositions[aI].position;
                        aDistVec1 = Vector3.Project(aDistVec1, mNormalPositions[aI].forward);
                        float aCurDist = aDistVec1.magnitude;
                        if (aMinDist > aCurDist)
                        {
                            aMinDist = aCurDist;
                            mTransformIx = aI;
                        }
                    }
                    mHideState = State.Hiding;
                }
                return;
            case State.Hiding:
                Vector3 aHideDistVec = mActivePlayerCharacter.transform.position - mCrawledInPoints[mTransformIx].position;
                aHideDistVec = Vector3.Project(aHideDistVec, mCrawledInPoints[mTransformIx].forward);
                float aHidPosDist = aHideDistVec.magnitude;
                if (aHidPosDist <= mCrawledInDistance)
                {
                    mHideState = State.Hidden;
                    //AnalyticsManager.Instance.mPlayerAnalytics.mHideUsed++;
                    //InteractableAnalyticsData aData = AnalyticsManager.Instance.mInteractableAnalytics[mInteractableID];
                    //aData.mNoOfInteractions++;
                    //AnalyticsManager.Instance.mInteractableAnalytics[mInteractableID] = aData;
                }
                return;
            case State.Hidden:
                Vector3 aDirection = mActivePlayerCharacter.transform.position - mCrawledInPoints[mTransformIx].position;
                aDirection = Vector3.Project(aDirection, mCrawledInPoints[mTransformIx].forward);
                float aUnHidPosDist = aDirection.magnitude;
                if (aUnHidPosDist > mHideCamDistError)
                {
                    mComposer.m_TrackedObjectOffset = mActivePlayerCharacter.transform.rotation * mInitTrackedObjectOffset;
                    mTransposer.m_FollowOffset = mActivePlayerCharacter.transform.rotation * mInitFollowOffset;
                    mCrawlThroughFollowCam.Priority = mCamPriority;
                    mHideState =  State.UnHiding;
                }
                return;
            case State.UnHiding:
                for (int aNormalPos = 0; aNormalPos < mNormalPositions.Length; aNormalPos++) //check distance and change state accordingly
                {
                    Vector3 aUnhideDist = mActivePlayerCharacter.transform.position - mNormalPositions[aNormalPos].position;
                    aUnhideDist = Vector3.Project(aUnhideDist, mNormalPositions[aNormalPos].forward);
                    float aNormalPosDist = aUnhideDist.magnitude;
                    if (aNormalPosDist <= mNormalCameraDistance)
                    {
                        mHideState = mTransformIx == aNormalPos ? State.Hiding : State.Idle;
                        break;
                    }
                }
                return;
        }
    }

}
