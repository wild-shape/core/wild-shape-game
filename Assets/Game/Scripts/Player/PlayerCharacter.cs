﻿///-------------------------------------------------------------------------------------------------
// File: PlayerCharacter.cs
//
// Author: Dakshvir Singh Rehill
// Date: 19/5/2020
//
// Summary:	Container Class for each different shape of the player
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class PlayerCharacter : MonoBehaviour
{
    [Header("VFX Assignments")]
    [Tooltip("The Scale of the shapeshift effect of this player character")]
    [SerializeField] [Range(0.1f, 10.0f)] float mScaleMultiplier = 1.0f;
    [Header("Component Assignments")]
    [Tooltip("Player's Rigidbody Component")]
    public Rigidbody mRigidbody;
    [Tooltip("Player's Animator Component")]
    public Animator mAnimator;
    [Tooltip("Animation Listener used for on complete, on move callbacks")]
    public AnimationListener mAnimationListener;
    [Tooltip("Player Movement Script")]
    public PlayerMovement mPlayerMovement;
    [Tooltip("Collider Used to Stablize Y Jitter while moving")]
    public CapsuleCollider mYStablizingCollider;
    [Header("Animation Assignments")]
    [Tooltip("The Trigger To Freeze Player in the Animator")]
    public string mFreezePlayerTrigger = "Frozen";
    [Tooltip("The Trigger To Unfreeze Player in the Animator")]
    public string mUnFreezePlayerTrigger = "UnFrozen";
    [Tooltip("The Trigger To Damage Player in the Animator")]
    public string mDamagePlayerTrigger = "Damage";
    [Tooltip("The Trigger To Kill Player in the Animator")]
    public string mKillPlayerTrigger = "Death";
    [Tooltip("Number of Idle Animations in the Animator")]
    public int mIdleAnimations = 3;
    [Tooltip("Number of Unique Idle Animations in the Animator")]
    public int mUniqueIdleDivider = 3;
    [Tooltip("Idle Animation Base Name")]
    public string mBaseIdleAnimName = "Idle";
    [Tooltip("Idle Index Parameter Name")]
    public string mIdleIxAnimInt = "IdleIx";
    [Tooltip("Movement Magnitude Parameter Name")]
    public string mIdleMagAnimFloat = "IdleMag";
    [Tooltip("Horizontal Movement Parameter Name")]
    public string mHorizontalAnimFloat = "Horizontal";
    [Tooltip("Vertical Movement Parameter Name")]
    public string mVerticalAnimFloat = "Vertical";
    [Tooltip("Push state for bear and crawl state for Raccoon")]
    public string mUniqueMovementAnimBool = "Sneak";
    [Tooltip("Player Fall Animation Trigger")]
    public string mFallAnimTrigger = "Fall";
    [Tooltip("Player Falling to Almost Landing Trigger")]
    public string mFallingAnimTrigger = "FallStartToClose";
    [Tooltip("Player Almost Falling to Landing Trigger")]
    public string mFallEndAnimTrigger = "FallCloseToEnd";
    [Header("Functionality Assignments")]
    [Tooltip("Required Ability Strength")]
    public float mRequiredStrength = 0.5f;
    [Tooltip("Look At Transform for the Camera")]
    public Transform mCameraLookAt;
    [Tooltip("Maximum radius around the player that the camera will try to look at")]
    public float mTargetGroupRadius = 3.0f;
    [Tooltip("To Check Distinction Between Two Shape Logic")]
    public bool mStealthyCharacter = false;
    [Tooltip("How much damage per hit can the player take from Enemy")]
    public float mDamage = 0.35f;
    [Tooltip("A percentage value that determines how much of the regular damage can the ranged enemy do")]
    [Range(0, 1)] public float mDamageMod = 0.5f;
    [Tooltip("The amount of time the freezing enemy keeps the player frozen")]
    [SerializeField] float mStayFrozenTime = 5.0f;
    [Header("Enemy Detection Settings")]
    [Tooltip("A percentage value of how much the detection rate will decrease if the player is in view and not moving")]
    [Range(0, 1)] public float mIdleDetectionDecrement = 0.30f;
    [Tooltip("A percentage value of how much the detection rate will decrease if the player is in view and just walking")]
    [Range(0, 1)] public float mWalkDetectionDecrement = 0.15f;
    [Tooltip("A percentage value of how much the detection rate will increase if the player is in view and running")]
    [Range(0, 1)] public float mRunDetectionIncrement = 0.25f;
    [Tooltip("A percentage value of how much the detection rate will increase/decrease depending on the positive/negative sign if the player is in view but pushing/crawling")]
    [Range(-1, 1)] public float mUniqueDetectionChange = 0.55f;
    [Tooltip("The total time it takes for the detection factor to reach the run detection increment from other modes")]
    public float mToRunDetectionTime = 1.0f;
    [Tooltip("The total time it takes for the detection factor to change the mode of detection changes")]
    public float mDetectionModeChangeTime = 1.0f;
    [Header("Sound Variables")]
    [Tooltip("Sound Effect for Player Stun")]
    [SerializeField] string mPlayerStun = "RangedStunPlayer";
    [HideInInspector] public bool mHidden = false;
    /// <summary>
    /// The current active modifier of the detection factor in change of detection of player
    /// </summary>
    [HideInInspector] public float mDetectionModifier = 0.0f;
    /// <summary>
    /// Used to randomly switch idle animations
    /// </summary>
    [HideInInspector] public int mCurrentIdleIx = 0;
    /// <summary>
    /// deactivate object animation event
    /// </summary>
    [HideInInspector] public readonly AnimationCompletedEvent mDeactivateObject = new AnimationCompletedEvent();
    /// <summary>
    /// Active game action of the player that governs all interactions as well as movement of the player
    /// </summary>
    [HideInInspector] public GameActions mGameAction;
    /// <summary>
    /// The current active shapeshift instance that has access to target group and other player character
    /// </summary>
    [HideInInspector] public ShapeShift mShapeShiftInstance = null;
    /// <summary>
    /// The time this player has left to stay frozen
    /// </summary>
    float mFreezeTime = 0.0f;
    /// <summary>
    /// Callback for changing controls
    /// </summary>
    UnityAction<ChangeableControls> mControlAction;
    /// <summary>
    /// Particle System Reference for the current shapeshifting
    /// </summary>
    ParticleSystem mShapeshiftParticle;
    bool mDead = false;
    void Awake()
    {
        mGameAction = new GameActions();
        GameManager.Instance.UpdateAllChangeableControls(mGameAction);
        mDetectionModifier = -mIdleDetectionDecrement;
    }
    void OnEnable()
    {
        mGameAction.Enable();
    }
    void OnDisable()
    {
        mGameAction.Disable();
        mDetectionModifier = -mIdleDetectionDecrement;
        StopAllCoroutines();
        if (mShapeshiftParticle != null && ObjectPoolingManager.IsValidSingleton())
        {
            SendBackToPool();
        }
    }

    void Start()
    {
        mControlAction = (pControls) => GameManager.Instance.OnControlsChanged(pControls, mGameAction);
        GameManager.Instance.mOnControlsChanged.AddListener(mControlAction);
    }

    void OnDestroy()
    {
        if (GameManager.IsValidSingleton() && GameManager.Instance.mOnControlsChanged != null)
        {
            GameManager.Instance.mOnControlsChanged.RemoveListener(mControlAction);
        }
        mGameAction.Dispose();
    }

    void AdvancedGameActionEnable()
    {
        if (!GameManager.Instance.mGameLoadingSequence.mDebugMode)
        {
            if (GameManager.Instance.mGameData.mTalkedNPCs.Count <= 0)
            {
                mGameAction.PlayerAdvanced.Disable();
            }
            else
            {
                mGameAction.PlayerAdvanced.Enable();
            }
        }
    }


    void Update()
    {
        if(mDead)
        {
            return;
        }
        AdvancedGameActionEnable();
        if(GameManager.Instance.mGameData.mPlayerData.mPlayerHealth <= 0.0f)
        {
            KillPlayer();
        }
        if (!mShapeShiftInstance.mFrozen)
        {
            return;
        }
        mFreezeTime += Time.deltaTime;
        if (mFreezeTime >= mStayFrozenTime)
        {
            UnFreezePlayer();
        }
    }
    /// <summary>
    /// Function is called to play death animation and disable input actions as player is dead
    /// </summary>
    void KillPlayer()
    {
        mDead = true;
        GameManager.Instance.mGameData.mPlayerData.mPlayerHealth = 0.0f;
        mAnimator.SetTrigger(mKillPlayerTrigger);
        mGameAction.Disable();
        mShapeShiftInstance.ToggleShapeshift(false);
    }

    /// <summary>
    /// Called by the Ranged Enemy to Freeze the player and slightly damages the player too
    /// </summary>
    /// <returns></returns>
    public float FreezePlayer()
    {
        mAnimator.SetTrigger(mFreezePlayerTrigger);
        GameManager.Instance.mGameData.mPlayerData.mPlayerHealth -= mDamage * mDamageMod;
        mShapeShiftInstance.mFrozen = true;
        mGameAction.Disable();
        if (GameManager.Instance.mGameData.mPlayerData.mPlayerHealth > 0.0f)
        {
            mShapeShiftInstance.FreezePlayers();
        }
        if (!string.IsNullOrEmpty(mPlayerStun) && !AudioManager.Instance.IsSoundPlaying(mPlayerStun))
        {
            AudioManager.Instance.PlaySound(mPlayerStun);
        }
        return mStayFrozenTime;
    }
    /// <summary>
    /// Called when the player has been frozen for enough time
    /// </summary>
    public void UnFreezePlayer()
    {
        mAnimator.SetTrigger(mUnFreezePlayerTrigger);
        mFreezeTime = 0.0f;
        mShapeShiftInstance.mFrozen = false;
        mShapeShiftInstance.UnFreezePlayer();
        mGameAction.Enable();
        if (!string.IsNullOrEmpty(mPlayerStun) && AudioManager.Instance.IsSoundPlaying(mPlayerStun))
        {
            AudioManager.Instance.StopSound(mPlayerStun);
        }
    }

    /// <summary>
    /// Callback for Deactivating Object
    /// </summary>
    /// <param name="pAnimParam">Deactivating Anim</param>
    public void DeactivateObject(int pAnimParam)
    {
        mDeactivateObject.Invoke(pAnimParam);
    }
    /// <summary>
    /// Callback for allowing to walk after falling
    /// </summary>
    /// <param name="pAnimParam"></param>
    public void AllowWalk(int pAnimParam)
    {
        mPlayerMovement.AllowWalkAfterFall();
    }
    public void GameOver(int pAnimParam)
    {
        if (!string.IsNullOrEmpty(mPlayerStun) && AudioManager.Instance.IsSoundPlaying(mPlayerStun))
        {
            AudioManager.Instance.StopSound(mPlayerStun);
        }
        GameManager.Instance.PlayerDead();
    }
    /// <summary>
    /// Function that is called when player shapeshifts to this shape
    /// </summary>
    public void CauseShapeshiftEffect()
    {
        if (mShapeshiftParticle != null)
        {
            StopAllCoroutines();
            SendBackToPool();
        }
        StartCoroutine(PlayShapeshiftEffect());
    }

    /// <summary>
    /// Coroutine that plays the shapeshift effect on the player
    /// </summary>
    /// <returns></returns>
    IEnumerator PlayShapeshiftEffect()
    {
        GameObject aShapeParticleObj = ObjectPoolingManager.Instance.GetPooledObject(mShapeShiftInstance.mShapeshiftEffect.mPoolName);
        aShapeParticleObj.transform.position = transform.position;
        aShapeParticleObj.transform.localScale *= mScaleMultiplier;
        aShapeParticleObj.SetActive(true);
        mShapeshiftParticle = aShapeParticleObj.GetComponent<ParticleSystem>();
        mShapeshiftParticle.Clear();
        mShapeshiftParticle.Play();
        while (true)
        {
            if (!mShapeshiftParticle.isPlaying)
            {
                break;
            }
            aShapeParticleObj.transform.position = transform.position;
            yield return null;
        }
        SendBackToPool();
    }

    void SendBackToPool()
    {
        mShapeshiftParticle.gameObject.transform.localScale /= mScaleMultiplier;
        mShapeshiftParticle.gameObject.SetActive(false);
        ObjectPoolingManager.Instance.ReturnPooledObject(mShapeShiftInstance.mShapeshiftEffect.mPoolName, mShapeshiftParticle.gameObject);
        mShapeshiftParticle = null;
    }

}
