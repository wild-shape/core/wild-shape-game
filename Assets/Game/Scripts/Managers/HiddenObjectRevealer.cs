﻿///-------------------------------------------------------------------------------------------------
// File: HiddenObjectRevealer.cs
//
// Author: Dakshvir Singh Rehill
// Date: 29/7/2020
//
// Summary:	This script is used to clean up objects that are hidden due to redundant plugins that are not being used anymore
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class HiddenObjectRevealer : MonoBehaviour
{
    // The functions are not called constantly like they are in play mode.
    // - Update is only called when something in the scene changed.
    void Update()
    {
        GameObject[] obj = (GameObject[])GameObject.FindObjectsOfType(typeof(GameObject));
        foreach (GameObject o in obj)
            o.hideFlags = HideFlags.None; // https://docs.unity3d.com/ScriptReference/HideFlags.html
    }
}
