﻿///-------------------------------------------------------------------------------------------------
// File: AnimationListener.cs
//
// Author: Dakshvir Singh Rehill
// Date: 19/5/2020
//
// Summary:	Animation Listener allows other scripts to register to OnAnimatorMove 
// Animation event and also allows Animation Completed events to be fired.
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimationListener : MonoBehaviour, IAnimationCompleted
{
	/// <summary>
	/// Event to expose the On Animator Move callback to scripts on other game objects
	/// </summary>
	[HideInInspector] public readonly UnityEvent mOnAnimatorMove = new UnityEvent();
	/// <summary>
	/// Event to expose the On Animator IK callback to scripts on other game objects
	/// </summary>
	[HideInInspector] public readonly AnimationCompletedEvent mOnAnimatorIK = new AnimationCompletedEvent();
	/// <summary>
	/// Dictionary to maintain all callbacks registered to be called on the completion of the animation
	/// </summary>
	readonly Dictionary<int, AnimationCompletedEvent> mOnAnimationCompleted = new Dictionary<int, AnimationCompletedEvent>();
	
	void OnAnimatorMove()
	{
		mOnAnimatorMove.Invoke();
	}

	void OnAnimatorIK(int pLayerIndex)
	{
		mOnAnimatorIK.Invoke(pLayerIndex);
	}
	/// <summary>
	/// Used by State Behaviour to Call Event callbacks registered for the completion of the animation
	/// </summary>
	/// <param name="pShortHashName">The Hash Code of the Animation that just got completed</param>
	public void AnimationCompleted(int pShortHashName)
	{
		AnimationCompletedEvent aEvent;
		if (mOnAnimationCompleted.TryGetValue(pShortHashName, out aEvent))
		{
			aEvent.Invoke(pShortHashName);
		}
	}

	/// <summary>
	/// Used to Register event callbacks that need to be called at the completion of the animation
	/// </summary>
	/// <param name="pShortHashName">The Hash Code of the Animation whose completion callback needs to be registered</param>
	/// <param name="pOnAnimationCompletedAction">The completion event callback that needs to be called at completion of the animation</param>
	public void RegisterOnAnimationCompleted(int pShortHashName, UnityAction<int> pOnAnimationCompletedAction)
	{
		AnimationCompletedEvent aEvent;
		if (!mOnAnimationCompleted.TryGetValue(pShortHashName, out aEvent))
		{
			aEvent = new AnimationCompletedEvent();
			mOnAnimationCompleted.Add(pShortHashName, aEvent);
		}
		aEvent.AddListener(pOnAnimationCompletedAction);
	}

	/// <summary>
	/// Used to unregister event callbacks that need to be called at the completion of the animation
	/// </summary>
	/// <param name="pShortHashName">The Hash Code of the Animation whose completion callback needs to be unregistered</param>
	/// <param name="pOnAnimationCompletedAction">The completion event callback that needs to be called at completion of the animation</param>
	public void UnregisterOnAnimationCompleted(int pShortHashName, UnityAction<int> pOnAnimationCompletedAction)
	{
		AnimationCompletedEvent aEvent;
		if (mOnAnimationCompleted.TryGetValue(pShortHashName, out aEvent))
		{
			aEvent.RemoveListener(pOnAnimationCompletedAction);
		}
	}

}