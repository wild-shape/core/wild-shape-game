This version of Behaviour Designer is a seat license for gdap@sheridancollege.ca and can only be used for educational purpose.

If the user intends to use Behaviour Designer for commercial use they must purchase their own and abide by the licensing rules set by Opsive.