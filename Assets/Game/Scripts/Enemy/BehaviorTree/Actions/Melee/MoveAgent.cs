﻿///-------------------------------------------------------------------------------------------------
// File: MoveAgent.cs
//
// Author: Dakshvir Singh Rehill
// Date: 10/6/2020
//
// Summary:	Action that moves the agent to a point and returns success when the point is reached
///-------------------------------------------------------------------------------------------------
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;
using UnityEngine.AI;

public abstract class MoveAgent : EnemyActionBase
{
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The Distance between the enemy and destination before enemy stops")]
    [SerializeField] SharedFloat mStoppingDistance;
    /// <summary>
    /// The destination of the enemy
    /// </summary>
    protected Vector3 mDestination;
    /// <summary>
    /// Check if off mesh link is triggered
    /// </summary>
    bool mOffMeshLinkTriggered = false;
    /// <summary>
    /// The current off mesh link data
    /// </summary>
    OffMeshLinkData mOffMeshLinkData;
    /// <summary>
    /// The time step to be used to complete off mesh link using enemy speed
    /// </summary>
    float mTStep = 0.0f;
    /// <summary>
    /// The current time in the entire off mesh link traversal
    /// </summary>
    float mCurrentT = 0.0f;
    /// <summary>
    /// The start position of the off mesh link
    /// </summary>
    Vector3 mStartPosition;
    /// <summary>
    /// The end position of the off mesh link
    /// </summary>
    Vector3 mEndPosition;
    /// <summary>
    /// The direction of the traversal of the off mesh link for rotation of the enemy
    /// </summary>
    Vector3 mLookDirection;

    /// <summary>
    /// The NavMeshAgent of the Melee Enemy
    /// </summary>
    protected NavMeshAgent mAgent;
    /// <summary>
    /// Reference to Melee Enemy
    /// </summary>
    protected MeleeEnemy mEnemy;
    public override void OnStart()
    {
        base.OnStart();
        if (mAgent == null) //cache agent if null
        {
            mEnemy = (MeleeEnemy)mEnemyInstance;
            mAgent = mEnemy.mAgent;
        }
        mEnemy.mEnemyAnimator.SetBool(mEnemy.mWalkAnimBoolName, true);
    }

    public override TaskStatus OnUpdate()
    {
        Vector3 aDestination = mDestination;
        aDestination.y = 0;
        Vector3 aEnemyPos = mAgent.transform.position;
        aEnemyPos.y = 0;
        if((aDestination - aEnemyPos).magnitude <= mStoppingDistance.Value) // check if enemy reached the destination
        {
            return TaskStatus.Success;
        }
        else if(mAgent.isOnOffMeshLink) // check if enemy on off mesh link for breakable bridge traversal
        {
            if(mOffMeshLinkTriggered) // if off mesh link already triggered just move the enemy till the time step is 1
            {
                if(mCurrentT < 1.0f) // if current time in time step is less than 1, move the enemy
                {
                    MoveManually();
                }
                else // when time step is 1 complete the off mesh traversal and switch navmesh traversal on
                {
                    mAgent.transform.position = mEndPosition;
                    mAgent.updatePosition = true;
                    mAgent.updateRotation = true;
                    mAgent.CompleteOffMeshLink();
                    mOffMeshLinkTriggered = false;
                }
            }
            else // else trigger off mesh link, calculate the off mesh link data, time step, end position, start position and move the enemy
            {
                mOffMeshLinkTriggered = true;
                mAgent.updatePosition = false;
                mAgent.updateRotation = false;
                mOffMeshLinkData = mAgent.currentOffMeshLinkData;
                mStartPosition = mAgent.transform.position;
                mEndPosition = (mOffMeshLinkData.endPos + Vector3.up * mAgent.baseOffset);
                mLookDirection = mEndPosition - mStartPosition;
                mTStep =1/( mLookDirection.magnitude / mAgent.speed);
                mCurrentT = 0.0f;
                MoveManually();
            }
        }
        return TaskStatus.Running;
    }

    /// <summary>
    /// Move the Agent using Lerp and Slerp instead of using the navmesh
    /// </summary>
    void MoveManually()
    {
        mAgent.transform.position = Vector3.Lerp(mStartPosition, mEndPosition, mCurrentT);
        mAgent.transform.rotation = Quaternion.Slerp(
            mAgent.transform.rotation, Quaternion.LookRotation(mLookDirection),
            mCurrentT);
        mCurrentT += mTStep * Time.deltaTime;
    }

    public override void OnEnd()
    {
        mEnemy.mEnemyAnimator.SetBool(mEnemy.mWalkAnimBoolName, false);
    }
}
