﻿///-------------------------------------------------------------------------------------------------
// File: CrawlableEditor.cs
//
//Author: Aditya Dinesh
// Date: 28/7/2020
//
// Summary:	Custom Inspector display for Crawlable
///-------------------------------------------------------------------------------------------------

using UnityEditor;

[CustomEditor(typeof(Crawlable))]
public class CrawlableEditor : Editor
{
    SerializedProperty mInteractableBy;
    SerializedProperty mCrawledInPoints;
    SerializedProperty mCrawledInDistance;
    SerializedProperty mHideCamDistError;
    SerializedProperty mNormalPositions;
    SerializedProperty mNormalCameraDistance;
    SerializedProperty mCamPriority;
    SerializedProperty mMaxCamRotation;

    Crawlable mCrawlable = null;

    private void OnEnable()
    {
        mCrawlable = target as Crawlable;

        mInteractableBy = serializedObject.FindProperty("mInteractableBy");
        mCrawledInPoints = serializedObject.FindProperty("mCrawledInPoints");
        mCrawledInDistance = serializedObject.FindProperty("mCrawledInDistance");
        mHideCamDistError = serializedObject.FindProperty("mHideCamDistError");
        mNormalPositions = serializedObject.FindProperty("mNormalPositions");
        mNormalCameraDistance = serializedObject.FindProperty("mNormalCameraDistance");
        mCamPriority = serializedObject.FindProperty("mCamPriority");
        mMaxCamRotation = serializedObject.FindProperty("mMaxCamRotation");
    }

    public override void OnInspectorGUI()
    {
        if (mCrawlable != null)
        {
            EditorGUILayout.PropertyField(mInteractableBy);
            EditorGUILayout.PropertyField(mCrawledInPoints);
            EditorGUILayout.PropertyField(mCrawledInDistance);
            EditorGUILayout.PropertyField(mHideCamDistError);
            EditorGUILayout.PropertyField(mNormalPositions);
            EditorGUILayout.PropertyField(mNormalCameraDistance);
            EditorGUILayout.PropertyField(mCamPriority);
            EditorGUILayout.PropertyField(mMaxCamRotation);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
