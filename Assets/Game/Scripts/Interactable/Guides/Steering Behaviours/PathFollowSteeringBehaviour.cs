﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PathFollowSteeringBehaviour : ArriveSteeringBehaviour
{
    [Header("Path Follow Variables")]
    [Tooltip("The minimum distance between the agent and Waypoint required for the agent to reach")]
	public float mWaypointSeekDist = 0.5f;
    [Tooltip("If Debug Lines need to be Drawn in Scene View")]
    public bool mDebugDraw;

    int mCurrentWaypointIndex = 0;
    private NavMeshPath mPath;

	[Tooltip("The weight given to this steering behavior when there is a path to follow")]
	public float mActivatedWeight = 1.0f;
	[Tooltip("The weight given to this steering behavior when there is no path to follow")]
	public float mReachedWeight = 0.0f;

    void Start()
    {
        mPath = new NavMeshPath();
		mWeight = mReachedWeight;
    }

	public override Vector3 calculateForce()
	{
		if (mCurrentWaypointIndex >= mPath.corners.Length - 1)
		{
			if(mArrived)
            {
				mWeight = mReachedWeight;
				return -mSteeringAgent.mVelocity;
            }
			return base.calculateForce();
		}
		else if ((mPath.corners[mCurrentWaypointIndex] - transform.parent.position).magnitude < mWaypointSeekDist)
		{
            mCurrentWaypointIndex++;
			mTarget = mPath.corners[mCurrentWaypointIndex];
		}
		return base.calculateForce();
	}

    public void ChangePath(Vector3 pDestination)
    {
        mCurrentWaypointIndex = 0;
        NavMesh.CalculatePath(transform.position, pDestination, NavMesh.AllAreas, mPath);
        if (mPath != null && mPath.corners.Length > 0)
        {
            mTarget = mPath.corners[0];
			mWeight = mActivatedWeight;
		}
	}

	private void OnDrawGizmos()
	{
		if (mPath != null && mDebugDraw)
		{
			for(int i = 1; i < mPath.corners.Length; i++)
			{
				DebugExtension.DebugWireSphere(mPath.corners[i - 1], Color.blue, 0.5f);
				Debug.DrawLine(mPath.corners[i - 1], mPath.corners[i], Color.blue);
			}
		}
	}

}
