﻿///-------------------------------------------------------------------------------------------------
// File: MeleeEnemy.cs
//
// Author: Dakshvir Singh Rehill
// Date: 08/07/2020
//
// Summary:	Inherits Enemy but also stores some melee specific variables
///-------------------------------------------------------------------------------------------------
using DigitalRuby.LightningBolt;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeEnemy : Enemy
{
    [Header("Melee Specific Assignments")]
    [Tooltip("Melee Enemy uses Unity NavMesh to move which is why a NavMeshAgent is required to be assigned here")]
    public UnityEngine.AI.NavMeshAgent mAgent;
    [Tooltip("Enemy Waypoint parent for Melee Enemy")]
    public GameObject mWaypointParent;
    [Tooltip("The Lightning Bolt Script that controls the Tasers of the Melee Enemy")]
    public LightningBoltScript mEnemyTaser;
    [Tooltip("Melee Enemy's Animator that controls the Walk Animation")]
    public Animator mEnemyAnimator;
    [Tooltip("Melee Enemy's Walk Animator Boolean")]
    public string mWalkAnimBoolName = "Walk";
    [Tooltip("Melee Enemy's Walk Speed Animator Float")]
    public string mWalkSpeedFloatName = "EnemyAnimSpeed";
    [Tooltip("Melee Enemy's Normal Walk Anim Speed")]
    [Range(0.25f, 4)] public float mNormalWalkSpeed = 1.0f;
    [Tooltip("Melee Enemy's Alert Walk Anim Speed")]
    [Range(0.5f, 6)] public float mAlertWalkSpeed = 1.0f;
    /// <summary>
    /// Enemy Charging point which can be null if no check in is required
    /// </summary>
    [HideInInspector] public GameObject mCheckInPoint;
    protected override void Start()
    {
        base.Start();
        mAgent.speed = mEnemySpeed;
        if (!string.IsNullOrEmpty(mMeleeHoverSFX) && mAudioAssistantAmbience != null)
        {
            mAudioAssistantAmbience.PlaySound(mMeleeHoverSFX, true);
        }
        mEnemyAnimator.SetFloat(mWalkSpeedFloatName, mNormalWalkSpeed);
    }

    protected override void Update()
    {
        base.Update();
        mEnemyTaser.ManualMode = mDeactivate || mFrozen;
    }
}
