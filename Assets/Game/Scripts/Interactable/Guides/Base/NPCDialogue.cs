﻿///-------------------------------------------------------------------------------------------------
// File: NPCDialogue.cs
//
// Author: Aditya Dinesh
// Date: /5/2020
//
// Summary:	The structs needed for NPC Dialogues
///-------------------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct NPCDialogue 
{
    [Tooltip("The Heading of the dialog box")]
    public string mName;
    [Tooltip("The List of All the different body texts that are circulated")]
    [TextArea(3,15)]
    public List<string> mBody;
    [Tooltip("The List of All the different summary text that are circulated in summary")]
    [TextArea(3,5)]
    public List<string> mSummary;
    [Tooltip("The Sound Effect for opening Dialog box")]
    public string mOpenDialogueBox;
    [HideInInspector] public bool mSummarize;
}

[System.Serializable]
public struct NPCLocationDialog
{
    [Tooltip("The dialog details of NPC before condition met")]
    public NPCDialogue mDialogInfo;
    [Tooltip("The position where the moving guide would go to after condition is met")]
    public Transform mConditionMetPosition;
    [Tooltip("The Condition Check Trigger Name")]
    public string mConditionCheckTriggerName;
}
