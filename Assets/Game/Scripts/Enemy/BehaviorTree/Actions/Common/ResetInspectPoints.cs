﻿///-------------------------------------------------------------------------------------------------
// File: ResetInspectPoints.cs
//
// Author: Dakshvir Singh Rehill
// Date: 17/6/2020
//
// Summary:	Resets the inspect points of the enemy
///-------------------------------------------------------------------------------------------------
using BehaviorDesigner.Runtime.Tasks;
[TaskCategory("Enemy")]
[TaskDescription("Resets the inspect points of the enemy")]
public class ResetInspectPoints : EnemyActionBase
{
    public override TaskStatus OnUpdate()
    {
        mEnemyInstance.mInspectPoints.Clear();
        return TaskStatus.Success;
    }
}
