﻿///-------------------------------------------------------------------------------------------------
// File: Chase.cs
//
// Author: Aditya Dinesh, Dakshvir Singh Rehill
// Date: 1/6/2020
//
// Summary:	Chase player till player vanishes then reach last known position
///-------------------------------------------------------------------------------------------------

using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;
using UnityEngine.AI;

[TaskCategory("Enemy/Melee")]
[TaskDescription("Chase player till player vanishes then reach last known position")]
public class Chase : MoveAgent
{
    [BehaviorDesigner.Runtime.Tasks.Tooltip("Boolean to determine whether the player is visible or not")]
    [SerializeField] SharedBool mCanSee;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The last known position of the player before loosing the player")]
    [SerializeField] SharedVector3 mPlayerLastKnown;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The magnitude of the difference between player position and last known position")]
    [SerializeField] float mDestinationChangeMagnitude = 1.0f;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("Boolean to allow enemy to bypass can see rule")]
    [SerializeField] bool mCheckCanSee = true;
    /// <summary>
    /// The shapeshift instance that has the latest active player character
    /// </summary>
    ShapeShift mPlayerShapeInstance;
    public override void OnStart()
    {
        base.OnStart();
        if (mPlayerShapeInstance == null) //cache reference if null
        {
            mPlayerShapeInstance = GameManager.Instance.mSceneHandler.mCameraController.mShapeShiftInst;
        }
        mAgent.speed = mEnemyInstance.mEnemyAlertSpeed;
        mEnemy.mEnemyAnimator.SetFloat(mEnemy.mWalkSpeedFloatName, mEnemy.mAlertWalkSpeed);
    }

    public override TaskStatus OnUpdate()
    {
        if (mCanSee.Value || !mCheckCanSee) //get player last known position
        {
            mPlayerLastKnown.Value = mPlayerShapeInstance.mPlayerCharacters[GameManager.Instance.mGameData.mPlayerData.mCurChar].mCameraLookAt.position;
        }
        else
        {
            return TaskStatus.Failure;
        }

        //only update destination after change magnitude for optimization purposes
        if ((mPlayerLastKnown.Value - mAgent.destination).magnitude >= mDestinationChangeMagnitude)
        {
            mAgent.SetDestination(mPlayerLastKnown.Value);
        }
        mDestination = mPlayerLastKnown.Value;
        return base.OnUpdate();
    }

    public override void OnEnd()
    {
        mAgent.speed = mEnemyInstance.mEnemySpeed;
    }

}

