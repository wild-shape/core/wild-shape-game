﻿///-------------------------------------------------------------------------------------------------
// File: LoadingSequence.cs
//
// Author: Dakshvir Singh Rehill
// Date: 25/6/2020
//
// Summary:	Scriptable Object to determine the loading sequence of the game
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LoadingSequence", menuName = "Optimization/Loading Sequence")]
public class LoadingSequence : ScriptableObject
{
    [Tooltip("Set this to true if you don't want to load main menu and save game")]
    public bool mDebugMode;
    [Tooltip("The 3D Background Scene that will be loaded in the background of the Main Menu")]
    public SceneReference mBG3DScene;
    [Tooltip("The sequence of scenes for this build")]
    public SceneReference[] mGameSceneSequence;
}
