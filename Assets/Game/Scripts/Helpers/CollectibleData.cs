﻿///-------------------------------------------------------------------------------------------------
// File: CollectibleData.cs
//
// Author: Dakshvir Singh Rehill
// Date: 12/8/2020
//
// Summary:	Scriptable Asset that contains the story of the collectible
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum GameSceneName
{
    TutorialBase,
    Outerworld,
    FinalBase
}

[CreateAssetMenu(fileName = "CollectibleData", menuName = "CollectibleAsset")]
public class CollectibleData : ScriptableObject
{
    [Tooltip("Enter Unique ID of the Collectible")]
    public int mCollectibleID;
    [Tooltip("The Scene this collectible will be in")]
    public GameSceneName mSceneID;
    [Tooltip("The story that will be displayed with this collectible")]
    [TextArea(1,3)] public string mCollectibleStory;
}
