﻿///-------------------------------------------------------------------------------------------------
// File: CheckPoint.cs
//
// Author: Aditya Dinesh, Dakshvir Singh Rehill
// Date: 10/6/2020
//
// Summary:	Script to Control Checkpoint Behavior
///-------------------------------------------------------------------------------------------------

using UnityEngine;

public class CheckPoint : IInteractable
{
    [Header("CheckPoint Variables")]
    [Tooltip("Spawn Point of the Player when they respawn")]
    [SerializeField] Transform mSpawnPoint;
    [Header("CheckPoint VFX Variables")]
    [Tooltip("GameObject containig the Checkpoint Particles")]
    [SerializeField] GameObject mCheckpointParticles;
    [Header("CheckPoint Animate Variables")]
    [Tooltip("Transform of Rock game object")]
    [SerializeField] Transform mRock;
    //[Tooltip("This will be the Material of the Rock after Checkpoint has been triggered")]
    //[SerializeField] Material mRockMaterial;
    [Tooltip("The position where the Rock will move to before swinging")]
    [SerializeField] Transform mLerpDest;
    [Tooltip("Stopping distance for Rock Lerp")]
    [SerializeField] float mLerpStoppingDistance = 5.0f;
    [Tooltip("Speed of Rock Lerp")]
    [SerializeField] float mLerpSpeed = 5.0f;
    [Tooltip("Speed of Rock Swing")]
    [SerializeField] float mRockSwingSpeed = 5.0f;
    [Tooltip("Amount/Threshold of Rock Swing")]
    [SerializeField] float mRockSwingAmount = 5.0f;
    [Header("CheckPoint Sound Variables")]
    [Tooltip("Audio Assisstant component for 3D sound")]
    [SerializeField] AudioAssistant mAudioAssisstant;
    [Tooltip("Sound Effect for Passing Checkpoint")]
    [SerializeField] string mPassCheckpointSFX = "PassCheckpoint";
    [Tooltip("Ambient Sound for Water Checkpoint <if this prefab is not a water checkpoint leave this blank>")]
    [SerializeField] string mWaterCheckpointAmbience;
    Vector3 mRockStartPos;
    bool mPlaySound = true;
    bool mStartLerp = false;
    bool mLerped;
    protected override void Start()
    {
        if (mSpawnPoint == null)
        {
            Destroy(gameObject);
            return;
        }
        if (GameManager.Instance.mGameData.IsCheckpointVisited(mSpawnPoint.position.sqrMagnitude))
        {
            mStartLerp = true;
            mPlaySound = false;
        }
        base.Start();
        if (!string.IsNullOrEmpty(mWaterCheckpointAmbience) && mAudioAssisstant != null)
        {
            mAudioAssisstant.PlaySound(mWaterCheckpointAmbience, true);
        }
    }

    protected override void Update()
    {
        base.Update();
        if (mStartLerp && !mLerped)
        {
            mCheckpointParticles.SetActive(true);
            LerpToPos(mLerpDest.position);
        }
        if (mLerped)
        {
            //mRock.GetComponent<MeshRenderer>().material = mRockMaterial;
            RockSwing();
        }
    }

    protected override void OnTriggerEnter(Collider pOther)
    {
        base.OnTriggerEnter(pOther);

        if (mActivePlayerCharacter != null)
        {
            CheckpointData aData = new CheckpointData
            {
                mSceneIndex = GameManager.Instance.mCurrentSceneIndex,
                mCheckpointID = mSpawnPoint.position.sqrMagnitude,
                mPosition = mSpawnPoint.position,
                mRotation = mSpawnPoint.rotation.eulerAngles
            };
            GameManager.Instance.mGameData.UpdateCheckpointsVisited(aData);
            SaveGameManager.Instance.SavePlayerData();

            if (!string.IsNullOrEmpty(mPassCheckpointSFX) && mPlaySound)
            {
                AudioManager.Instance.PlaySound(mPassCheckpointSFX);
                mPlaySound = false;
            }
            mStartLerp = true;
        }
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        if (!string.IsNullOrEmpty(mWaterCheckpointAmbience) && mAudioAssisstant != null && AudioManager.IsValidSingleton())
        {
            AudioManager.Instance.StopSound(mWaterCheckpointAmbience);
        }
    }

    public void RockSwing()
    {
        Vector3 aNewPos = mRock.position;

        aNewPos.y = mRockStartPos.y + (Mathf.Sin(Time.time * mRockSwingSpeed) * mRockSwingAmount);
        aNewPos.x = mRock.position.x;
        aNewPos.z = mRock.position.z;

        mRock.position = aNewPos;

    }

    void LerpToPos(Vector3 pLerpToPos)
    {
        if (Vector3.Distance(mRock.position, pLerpToPos) <= mLerpStoppingDistance)
        {
            mLerped = true;
            mRockStartPos = mRock.position;

            return;
        }
        mRock.position = Vector3.Lerp(mRock.position, pLerpToPos, Time.deltaTime * mLerpSpeed);

    }

    ///// <summary>
    ///// Function called when player passes through the replenish health section
    ///// </summary>
    ///// <param name="pCollider"></param>
    //public void ReplenishHealth(Collider pCollider)
    //{
    //    if(mActivePlayerCharacter == null)
    //    {
    //        return;
    //    }
    //    //if health is full return
    //    if (GameManager.Instance.mGameData.mPlayerData.mPlayerHealth >= 1.0f)
    //    {
    //        return;
    //    }
    //    //check if collider and player are same
    //    if (pCollider.gameObject.GetInstanceID() != mActivePlayerCharacter.gameObject.GetInstanceID())
    //    {
    //        return;
    //    }
    //    //InteractableAnalyticsData aData = AnalyticsManager.Instance.mInteractableAnalytics[mInteractableID];
    //    //aData.mNoOfInteractions++;
    //    //AnalyticsManager.Instance.mInteractableAnalytics[mInteractableID] = aData;
    //    float aHealthToAdd = mHealthPerCrossing;
    //    if(mTotalHealthReserve < mHealthPerCrossing)
    //    {
    //        aHealthToAdd = mTotalHealthReserve;
    //    }
    //    mTotalHealthReserve -= aHealthToAdd;
    //    GameManager.Instance.mGameData.mPlayerData.mPlayerHealth += aHealthToAdd;
    //    if(GameManager.Instance.mGameData.mPlayerData.mPlayerHealth > 1.0f)
    //    {
    //        GameManager.Instance.mGameData.mPlayerData.mPlayerHealth = 1.0f;
    //    }
    //    CheckpointData aCPData = new CheckpointData
    //    {
    //        mSceneIndex = GameManager.Instance.mCurrentSceneIndex,
    //        mCheckpointID = mSpawnPoint.position.sqrMagnitude,
    //        mHealthReserve = mTotalHealthReserve,
    //        mPosition = mSpawnPoint.position,
    //        mRotation = mSpawnPoint.rotation.eulerAngles
    //    };
    //    mStatsMenu.UpdateAll();
    //    GameManager.Instance.mGameData.UpdateCheckpointsVisited(aCPData);
    //    SaveGameManager.Instance.SavePlayerData();
    //}
}
