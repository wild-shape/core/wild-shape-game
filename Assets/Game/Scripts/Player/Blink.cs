﻿///-------------------------------------------------------------------------------------------------
// File: Blink.cs
//
// Author: Dakshvir Singh Rehill
// Date: 3/6/2020
//
// Summary:	Actual Object to Blink To
///-------------------------------------------------------------------------------------------------
using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blink : MonoBehaviour
{
    /// <summary>
    /// Used to keep a count of all the hits to ensure the object with maximum hits is chosen for the blink position
    /// </summary>
    [System.Serializable]
    public struct HitCount
    {
        /// <summary>
        /// The Raycast Hit object of the Hit
        /// </summary>
        public RaycastHit mHit;
        /// <summary>
        /// Number of hits caused by this collider
        /// </summary>
        public int mHitCount;
    }
    /// <summary>
    /// The current character script attached to the Raccoon
    /// </summary>
    [HideInInspector] public PlayerCharacter mCharacter;
    [Tooltip("Points to raycast to ground")]
    [SerializeField] Transform[] mRaycastPoints;
    [Tooltip("Delta Value for Raycasting to ground")]
    [SerializeField] float mRaycastDelta = 1.0f;
    [Tooltip("Mask of all blinkable areas")]
    [SerializeField] LayerMask mBlinkable;
    [Tooltip("Mask of All the blockers that can't be blinked to ")]
    [SerializeField] LayerMask mBlockers;
    [Tooltip("The Animator Component of Raccoon Copy")]
    [SerializeField] Animator mRaccoonCopy;
    [Tooltip("To Optimize the Number of Recursive calls of ground check function")]
    [SerializeField] int mMaximumRecursiveCalls = 3;
    [Tooltip("The Minimum blink distance from the Raccoon")]
    [HideInInspector] public float mMinimumRadius;
    /// <summary>
    /// Maximum height to blink to
    /// </summary>
    [HideInInspector] public float mMaxBlinkableHeight;
    /// <summary>
    /// The fixed radius of the blink object
    /// </summary>
    [HideInInspector] public float mRadius;
    /// <summary>
    /// The current radius of the blink object
    /// </summary>
    float mCurrentRadius;
    int mRecallCount = 0;
    public void StartBlink()
    {
        gameObject.SetActive(true);
        CheckIsGrounded();
    }

    public void EndBlink()
    {
        gameObject.SetActive(false);
    }

    void Update()
    {
        if (mCharacter == null)
        {
            return;
        }
        DuplicateAnimatorParameters();
        mCurrentRadius = mRadius;
        Vector3 aDesiredPosition = new Vector3(mCharacter.transform.position.x, transform.position.y, mCharacter.transform.position.z)
                           + mCharacter.transform.forward.normalized * mCurrentRadius;
        transform.position = aDesiredPosition;
        CheckIsGrounded();
    }
    /// <summary>
    /// Function gets all animator values of the actual player and set sthem in the copy animator
    /// </summary>
    void DuplicateAnimatorParameters()
    {
        mRaccoonCopy.gameObject.transform.rotation = mCharacter.transform.rotation;
        foreach (AnimatorControllerParameter aAnimatorParam in mCharacter.mAnimator.parameters)
        {
            switch (aAnimatorParam.type)
            {
                case AnimatorControllerParameterType.Bool:
                    mRaccoonCopy.SetBool(aAnimatorParam.nameHash, mCharacter.mAnimator.GetBool(aAnimatorParam.nameHash));
                    break;
                case AnimatorControllerParameterType.Float:
                    mRaccoonCopy.SetFloat(aAnimatorParam.nameHash, mCharacter.mAnimator.GetFloat(aAnimatorParam.nameHash));
                    break;
                case AnimatorControllerParameterType.Int:
                    mRaccoonCopy.SetInteger(aAnimatorParam.nameHash, mCharacter.mAnimator.GetInteger(aAnimatorParam.nameHash));
                    break;
                case AnimatorControllerParameterType.Trigger: //no way to know whether trigger is set or not so left unset
                    break;
            }
        }
    }

    void CheckIsGrounded()
    {
        Dictionary<Collider, HitCount> aRaycastHits = new Dictionary<Collider, HitCount>();
        foreach (Transform aRaycastPoint in mRaycastPoints) //find the collider that is getting hit by points
        {
            RaycastHit aHit;
            if (Physics.Raycast(aRaycastPoint.transform.position
                + transform.up * mMaxBlinkableHeight,
                -transform.up,
                out aHit,
                mMaxBlinkableHeight * 2.0f,
                mBlinkable,
                QueryTriggerInteraction.Ignore))
            {
                //check if the hit y is lower than current y
                if (aHit.point.y < mCharacter.transform.position.y)
                {
                    continue;
                }
                HitCount aHitCount = new HitCount();
                if (aRaycastHits.TryGetValue(aHit.collider, out aHitCount)) //if collider exists increase hit count
                {
                    aHitCount.mHitCount++;
                    aRaycastHits[aHit.collider] = aHitCount;
                }
                else
                {
                    aHitCount.mHit = aHit;
                    aHitCount.mHitCount = 1;
                    aRaycastHits.Add(aHit.collider, aHitCount);
                }
            }
        }
        if (aRaycastHits.Count > 0)//if any collider hit
        {
            float aHeighest = float.MinValue;
            Collider aHeighestCol = null;
            foreach (HitCount aCount in aRaycastHits.Values)//find the heighest collider
            {
                if (aCount.mHit.point.y > aHeighest)
                {
                    aHeighest = aCount.mHit.point.y;
                    aHeighestCol = aCount.mHit.collider;
                }
            }
            HitCount aHeighestCount = aRaycastHits[aHeighestCol];
            Vector3 aDesiredPosition = new Vector3(mCharacter.transform.position.x,
                                        aHeighestCount.mHit.point.y, mCharacter.transform.position.z)
                                        + mCharacter.transform.forward.normalized * mCurrentRadius;
            transform.position = aDesiredPosition; //set position to heighest collider
        }
        else
        {
            //no ground found send blink back
            Vector3 aDesiredPosition = transform.position;
            float aLandHeight = 0;
            while (!ReduceBlinkRadius(ref aDesiredPosition))
            {
                bool aLandFound = true;
                aLandHeight = mCharacter.transform.position.y;
                foreach (Transform aRaycastPoint in mRaycastPoints) //check if ground found on all points
                {
                    RaycastHit aHit = new RaycastHit();
                    aLandFound = aLandFound && Physics.Raycast(aDesiredPosition + aRaycastPoint.localPosition
                                    + transform.up * mMaxBlinkableHeight,
                                    -transform.up,
                                    out aHit,
                                    mMaxBlinkableHeight * 2.0f,
                                    mBlinkable,
                                    QueryTriggerInteraction.Ignore);
                    if (aLandFound)
                    {
                        if (aHit.point.y > aLandHeight)
                        {
                            aLandHeight = aHit.point.y;
                        }
                    }
                }
                aDesiredPosition.y = aLandHeight;
                if (aLandFound && aLandHeight >= mCharacter.transform.position.y)
                {
                    RaycastHit aRHit = new RaycastHit();
                    Physics.Raycast(aDesiredPosition + mRaycastPoints[0].localPosition
                                    + transform.up * mMaxBlinkableHeight,
                                    -transform.up,
                                    out aRHit,
                                    mMaxBlinkableHeight * 2.0f,
                                    mBlinkable,
                                    QueryTriggerInteraction.Ignore);
                    float aHeightDiff = Mathf.Abs(aLandHeight - aRHit.point.y);
                    if (aHeightDiff <= mMaxBlinkableHeight * 0.4f)
                    {
                        break;
                    }
                }
            }
            transform.position = aDesiredPosition;
        }
        Transform aBlockedRaycastPoint = null;
        foreach (Transform aRaycastPoint in mRaycastPoints) //find blockers to move blink point away
        {
            if (Physics.Raycast(aRaycastPoint.transform.position
                - transform.up * mRaycastDelta,
                transform.up,
                mRaycastDelta * 2.0f,
                mBlockers))
            {
                aBlockedRaycastPoint = aRaycastPoint;
                break;
            }
        }
        if (aBlockedRaycastPoint != null) //if blocker found send blink back
        {
            Vector3 aDesiredPosition = transform.position;
            while (!ReduceBlinkRadius(ref aDesiredPosition))
            {
                bool aBlockersNotFound = true;
                foreach (Transform aRaycastPoint in mRaycastPoints) //check for blockers
                {
                    aBlockersNotFound = aBlockersNotFound && !Physics.Raycast(aDesiredPosition + aRaycastPoint.localPosition
                                    - transform.up * mRaycastDelta,
                                    transform.up,
                                    mRaycastDelta * 2.0f,
                                    mBlockers,
                                    QueryTriggerInteraction.Ignore);
                }
                if (aBlockersNotFound)
                {
                    break;
                }
            }
            transform.position = aDesiredPosition;
            if (mRecallCount >= mMaximumRecursiveCalls)
            {
                mCurrentRadius = 0.0f;
                transform.position = mCharacter.transform.position;
                return;
            }
            mRecallCount++;
            CheckIsGrounded();
        }
        else
        {
            mRecallCount = 0;
        }
        if (mCurrentRadius <= 0)
        {
            return;
        }
        //Final Check for Nothing in Between player and blink point
        Vector3 aRaycastPos = mCharacter.transform.position - transform.forward * mRaycastDelta;
        aRaycastPos.y = transform.position.y;
        aRaycastPos += transform.up * mRaycastDelta;
        RaycastHit aInBetweenHit;
        Vector3 aDesiredPosInBw = transform.position;
        while (Physics.Raycast(aRaycastPos, mCharacter.transform.forward, out aInBetweenHit, mCurrentRadius, mBlockers))
        {
            aDesiredPosInBw.x = aInBetweenHit.point.x;
            aDesiredPosInBw.z = aInBetweenHit.point.z;
            if(ReduceBlinkRadius(ref aDesiredPosInBw))
            {
                break;
            }
        }
        transform.position = aDesiredPosInBw;
    }

    /// <summary>
    /// Function that reduces the blink radius with time
    /// </summary>
    /// <param name="pDesiredPosition"> The position that needs to be brought closer to player</param>
    /// <returns>False if there is still radius left and true if radius is 0</returns>
    bool ReduceBlinkRadius(ref Vector3 pDesiredPosition)
    {
        mCurrentRadius -= Time.deltaTime;
        if (mCurrentRadius < mMinimumRadius)
        {
            mCurrentRadius = 0.0f;
            pDesiredPosition = new Vector3(mCharacter.transform.position.x,
                pDesiredPosition.y, mCharacter.transform.position.z);
            return true;
        }
        pDesiredPosition = new Vector3(mCharacter.transform.position.x,
            pDesiredPosition.y, mCharacter.transform.position.z)
            + mCharacter.transform.forward.normalized * mCurrentRadius;
        return false;
    }
}
