﻿///-------------------------------------------------------------------------------------------------
// File: BlockPushedCheckState.cs
//
// Author: Dakshvir Singh Rehill
// Date: 22/7/2020
//
// Summary:	Moving Guide State that is active if the guide is waiting for the player to move the block
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BlockPushedCheckState : GuideBaseState
{
    MovingGuide mMoveInst;
    [SerializeField] string mMoveStateTrigger = "MoveToLocation";
    NavMeshPath mPath;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        if(mMoveInst == null)
        {
            mMoveInst = (MovingGuide)mGuideRef;
            mPath = new NavMeshPath();
        }
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (NavMesh.CalculatePath(mMoveInst.transform.position, mMoveInst.mDynamicConversationDialogs[mMoveInst.mDynamicIndex].mConditionMetPosition.position, NavMesh.AllAreas,mPath))
        {
            if(mPath.status == 
                NavMeshPathStatus.PathComplete)
            {
                animator.SetTrigger(mMoveStateTrigger);
            }
        }
    }
}
