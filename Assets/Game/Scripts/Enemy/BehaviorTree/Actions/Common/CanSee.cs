///-------------------------------------------------------------------------------------------------
// File: CanSee.cs
//
// Author: Aditya Dinesh, Dakshvir Singh Rehill
// Date: 27/5/2020
//
// Summary:	Returns success if the player is in any of the enemy vision cones
///-------------------------------------------------------------------------------------------------


using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

[TaskCategory("Enemy")]
[TaskDescription("Returns success if the player is in any of the enemy vision cones")]
public class CanSee : EnemyActionBase
{
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The global target layer that has obstacles as well as player")]
    [SerializeField] SharedLayerMask mTargetLayer;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The boolean that needs to be set if player is visible")]
    [SerializeField] SharedBool mCanSee;
    /// <summary>
    /// The reference to the player's shapeshift instance to get the active shape
    /// </summary>
    ShapeShift mPlayerShapeInstance;

    public override void OnStart()
    {
        base.OnStart();
        if (mPlayerShapeInstance == null) //set if it is null
        {
            mPlayerShapeInstance = GameManager.Instance.mSceneHandler.mCameraController.mShapeShiftInst;
        }
    }


    public override TaskStatus OnUpdate()
    {
        int aCurChar = GameManager.Instance.mGameData.mPlayerData.mCurChar;
        Vector3 aTargetPosition = mPlayerShapeInstance.mPlayerCharacters[aCurChar].mCameraLookAt.position;
        mCanSee.Value = false;
        foreach (Enemy.VisionConePoint aVisionPoint in mEnemyInstance.mVisionCones)
        {
            Vector3 aRelativeVisionConePoint = mEnemyInstance.mOrigin.position + aVisionPoint.mOffset;
            Vector3 aOriginForward = mEnemyInstance.mOrigin.forward;
            if(aVisionPoint.mRotationOffset.sqrMagnitude > 0.0f)
            {
                aOriginForward = Quaternion.Euler(aVisionPoint.mRotationOffset) * aOriginForward;
            }
            // Calculate distance and angle between us and the target
            Vector3 aDistance = aTargetPosition - aRelativeVisionConePoint;
            float aMagnitude = aDistance.magnitude;
            aDistance.Normalize();
            float aAngle = Vector3.Angle(aOriginForward, aDistance);
            // If the target is within the specified range
            if (Mathf.Abs(aAngle) <= aVisionPoint.mViewingAngle  &&
                aMagnitude <= aVisionPoint.mMaxViewingDistance)
            {
                RaycastHit aHit;
                if (Physics.Raycast(aRelativeVisionConePoint,
                    aDistance, out aHit,
                    aVisionPoint.mMaxViewingDistance,
                    mTargetLayer.Value))
                {
                    if(aHit.collider.attachedRigidbody != null)
                    {
                        if(aHit.collider.attachedRigidbody.gameObject.GetInstanceID() ==
                            mPlayerShapeInstance.mPlayerCharacters[aCurChar].gameObject.GetInstanceID()
                            && !mPlayerShapeInstance.mPlayerCharacters[aCurChar].mHidden)
                        {
                            mCanSee.Value = true;
                            break;
                        }
                    }
                }
            }
        }
        return mCanSee.Value ? TaskStatus.Success : TaskStatus.Failure;
    }
}