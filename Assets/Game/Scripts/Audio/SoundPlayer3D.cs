﻿///-------------------------------------------------------------------------------------------------
// File: 3DSoundPlayer.cs
//
// Author: Aditya Dinesh
// Date: /5/2020
//
// Summary:	Play a List of Ambient Sounds that will always be looping
///-------------------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SoundData3D
{
    public string mSoundName;
    public AudioAssistant mAudioAssistant;
}


public class SoundPlayer3D : MonoBehaviour
{
    public List<SoundData3D> mSoundList;

    void Start()
    {
        if(mSoundList.Count > 0)
        {
            foreach(SoundData3D aSoundData3D in mSoundList)
            {
                if(!string.IsNullOrEmpty(aSoundData3D.mSoundName) && aSoundData3D.mAudioAssistant != null)
                {
                    aSoundData3D.mAudioAssistant.PlaySound(aSoundData3D.mSoundName,true);
                }
            }
        }
    }
}
