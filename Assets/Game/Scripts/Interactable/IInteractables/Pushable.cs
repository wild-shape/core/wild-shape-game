﻿///-------------------------------------------------------------------------------------------------
// File: Pushable.cs
//
// Author: Dakshvir Singh Rehill
// Date: 25/5/2020
//
// Summary:	Attached to objects that can be pushed by bear
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Pushable : IInteractable
{
    [Header("Push Movement Variables")]
    [Tooltip("The Directions in which the object can be pushed")]
    [SerializeField] List<Transform> mPushDirections;
    [Header("Push Object Variables")]
    [Tooltip("The rigidbody of the pushable object to allow moving and physics")]
    [SerializeField] Rigidbody mRigidBody;
    [Tooltip("This multiplier is the scaling that is caused on the speed of the bear push animation and applied to the pushbale object")]
    [SerializeField] float mSpeedMultiplier = 0.65F;
    [Tooltip("The distance between the pushable object and bear after which the push starts")]
    [SerializeField] float mPushableDistance = 3.0f;
    [Tooltip("The Maximum Angle Between Pushable And Bear")]
    [SerializeField] [Range(0, 75)] float mMaxAngleDifference = 45.0f;
    [Tooltip("The Sound Effect for Pushing Object")]
    [SerializeField] string mPushObjectSFX = "PushObject";
    [Tooltip("The Sound Effect for Crate Destroy")]
    [SerializeField] string mCrateDestroySFX = "CrateImpact";
    [Header("Fall Variables")]
    [Tooltip("The max distance in Y position after which the pushable object will respawn")]
    [SerializeField] float mYCheck = 2.0f;
    [Header("Pushable Effect")]
    [Tooltip("The Particle System that is being used for the effect")]
    [SerializeField] List<ParticleSystem> mPushableEffect;
    [Tooltip("The Parent object of the particle systems")]
    [SerializeField] Transform mParticleParent;
    [Tooltip("The Effect When Crate Falls")]
    [SerializeField] PoolClassifier mCrateFallFx;
    /// <summary>
    /// If destroyed don't trigger enter
    /// </summary>
    bool mDestroyed = false;
    /// <summary>
    /// A boolean that determines whether the push is active orn not
    /// </summary>
    bool mPushActive = false;
    /// <summary>
    /// The spawn position of the pushable object
    /// </summary>
    Vector3 mOriginalPosition;

    protected override void Start()
    {
        base.Start();
        //mInteractableID = AnalyticsManager.Instance.RegisterInteractable(InteractableType.Pushable);
        mOriginalPosition = mRigidBody.position;
    }

    protected override void OnTriggerEnter(Collider pOther)
    {
        if (mDestroyed)
        {
            return;
        }
        base.OnTriggerEnter(pOther);
        if (mActivePlayerCharacter == null)
        {
            return;
        }
        if (mActivePlayerCharacter.gameObject.GetInstanceID() != pOther.gameObject.GetInstanceID())
        {
            return;
        }
        if (mActivePlayerCharacter != null)
        {
            mPushActive = true;
            mActivePlayerCharacter.mAnimationListener.mOnAnimatorMove.AddListener(OnBearAnimatorMove);
        }
    }

    protected override void Update()
    {
        if (mDestroyed)
        {
            return;
        }
        if (Mathf.Abs(mRigidBody.position.y - mOriginalPosition.y) >= mYCheck) //destroy
        {
            BreakPushable();
        }
        base.Update();
    }

    protected override void EndInteraction()
    {
        if (mActivePlayerCharacter == null)
        {
            return;
        }
        if (mPushActive)
        {
            UnregisterOnAnimatorMove();
        }
        if (mActivePlayerCharacter.mShapeShiftInstance.mUniqueMovement)
        {
            mActivePlayerCharacter.mShapeShiftInstance.UniqueMovementChange();
        }
        base.EndInteraction();
    }

    /// <summary>
    /// The function that uses the animator move of the bear to move the pushable object
    /// </summary>
    void OnBearAnimatorMove()
    {
        if (Time.deltaTime < 0)
        {
            return;
        }
        Vector3 aDist = mRigidBody.position - mActivePlayerCharacter.transform.position;
        aDist.y = mActivePlayerCharacter.transform.forward.y;
        if (Mathf.Abs(Vector3.Angle(mActivePlayerCharacter.transform.forward, aDist)) <= mMaxAngleDifference &&
            aDist.magnitude <= mPushableDistance) // the angle and distance should be in the mode
        {
            if (mPushableEffect.Count > 0)
            {
                if (!mPushableEffect[0].isPlaying)
                {
                    foreach (ParticleSystem aEffect in mPushableEffect)
                    {
                        aEffect.Clear();
                        aEffect.Play();
                    }
                }
            }
            if (!AudioManager.Instance.IsSoundPlaying(mPushObjectSFX))
            {
                AudioManager.Instance.PlaySound(mPushObjectSFX, true);
            }
            mParticleParent.transform.rotation = mActivePlayerCharacter.transform.rotation;
            Vector3 aForwardDirection = transform.forward;
            float aSmallestAngle = 360f;
            foreach (Transform aForward in mPushDirections)
            {
                if (Vector3.Dot(mActivePlayerCharacter.transform.forward, aForward.forward) < 0)
                {
                    continue;
                }
                float aAngle = Vector3.Angle(mActivePlayerCharacter.transform.forward, aForward.forward);
                if (aSmallestAngle > aAngle)
                {
                    aForwardDirection = aForward.forward;
                    aSmallestAngle = aAngle;
                }
            }
            ToggleUniqueMovement(true);
            mRigidBody.MovePosition(mRigidBody.position + aForwardDirection * mSpeedMultiplier * Time.deltaTime);
        }
        else //if its not in the range the set the bool as false
        {
            ToggleUniqueMovement(false);
            mParticleParent.localRotation = Quaternion.identity;
            if (mPushableEffect.Count > 0)
            {
                if (mPushableEffect[0].isPlaying)
                {
                    foreach (ParticleSystem aEffect in mPushableEffect)
                    {
                        aEffect.Clear();
                        aEffect.Stop();
                    }
                }
            }
            if (AudioManager.IsValidSingleton() && !string.IsNullOrEmpty(mPushObjectSFX))
            {
                if (AudioManager.Instance.IsSoundPlaying(mPushObjectSFX))
                {
                    AudioManager.Instance.StopSound(mPushObjectSFX);
                }
            }
        }
    }

    void ToggleUniqueMovement(bool pPushActive)
    {
        if (mActivePlayerCharacter.mPlayerMovement.mState == PlayerMovement.NavState.Unique && pPushActive)
        {
            return;
        }
        if (mActivePlayerCharacter.mPlayerMovement.mState != PlayerMovement.NavState.Unique && !pPushActive)
        {
            return;
        }
        mActivePlayerCharacter.mShapeShiftInstance.UniqueMovementChange();
    }

    /// <summary>
    /// End push when bear is not in standing mode
    /// </summary>
    void UnregisterOnAnimatorMove()
    {
        mPushActive = false;
        mActivePlayerCharacter.mAnimationListener.mOnAnimatorMove.RemoveListener(OnBearAnimatorMove);
        if (AudioManager.IsValidSingleton() && !string.IsNullOrEmpty(mPushObjectSFX))
        {
            if (AudioManager.Instance.IsSoundPlaying(mPushObjectSFX))
            {
                AudioManager.Instance.StopSound(mPushObjectSFX);
            }
        }
    }

    /// <summary>
    /// Function called to break the crate into pieces and vanish it
    /// </summary>
    public void BreakPushable()
    {
        mDestroyed = true;
        if (AudioManager.IsValidSingleton() && !string.IsNullOrEmpty(mCrateDestroySFX))
        {
            AudioManager.Instance.PlaySound(mCrateDestroySFX);
        }
        EndInteraction();
        StartCoroutine(CauseCrateFallFX());
    }

    IEnumerator CauseCrateFallFX()
    {
        GameObject aObj = ObjectPoolingManager.Instance.GetPooledObject(mCrateFallFx.mPoolName);
        ParticleSystem aCrateFx = aObj.GetComponent<ParticleSystem>();
        aCrateFx.Clear();
        aObj.transform.position = transform.position;
        aObj.SetActive(true);
        aCrateFx.Play();
        yield return new WaitWhile(() => aCrateFx.isPlaying);
        aObj.SetActive(false);
        ObjectPoolingManager.Instance.ReturnPooledObject(mCrateFallFx.mPoolName, aObj);
        Destroy(mRigidBody.gameObject);
    }

}