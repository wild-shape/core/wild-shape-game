﻿///-------------------------------------------------------------------------------------------------
// File: HideVFXChecker.cs
//
// Author: Dakshvir Singh Rehill
// Date: 18/7/2020
//
// Summary:	This Object Determines which object will be active on the raccoon depending on the state of the raccoon
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideVFXChecker : MonoBehaviour
{
    [Tooltip("Player Character Instance of the Raccoon Character")]
    public PlayerCharacter mCharacter;
    [Tooltip("The Renderer to which the material swap needs to be done")]
    [SerializeField] Renderer mPlayerRenderer;
    [Tooltip("The Material That Needs to Be Set in Normal Mode")]
    [SerializeField] Material mNormalMaterial;
    [Tooltip("The Material That Needs to Be Set in Hide Mode")]
    [SerializeField] Material mHideMaterial;
    [Tooltip("The Y Height from which the hideable needs to be checked")]
    [SerializeField] float mYHeight = 3.0f;
    [Tooltip("The Layer for Hideable Objects")]
    [SerializeField] LayerMask mHideableLayer;
    [Tooltip("The Layer for NonHideable Objects only")]
    [SerializeField] LayerMask mNonHideableLayer;
    [Tooltip("Hidden Particle Effect GameObject")]
    [SerializeField] ParticleSystem mHiddenParticle;
    [Tooltip("If Debug Mode True then make gizmos")]
    [SerializeField] bool mDrawGizmos = false;
    [Tooltip("Sound Effect for Hide")]
    [SerializeField] string mHideSFX = "EnterBush";
    [Tooltip("Sound Effect for UnHide")]
    [SerializeField] string mUnHideSFX = "ExitBush";

    void Update()
    {
        RaycastHit aHit;
        if(Physics.Raycast(transform.position + transform.up * mYHeight, -transform.up,out aHit, mYHeight + 0.1f, mHideableLayer))
        {
            if(!GenHelpers.IsInLayerMask(aHit.collider.gameObject.layer, mNonHideableLayer))
            {
                HideEffectTriggered();
            }
            else
            {
                UnhideEffectTriggered();
            }
        }
        else
        {
            UnhideEffectTriggered();
        }
    }

    private void OnDisable()
    {
        UnhideEffectTriggered();
    }

    void HideEffectTriggered()
    {
        if(mCharacter.mHidden)
        {
            return;
        }
        if(!string.IsNullOrEmpty(mHideSFX))
        {
            AudioManager.Instance.PlaySound(mHideSFX);
        }
        mCharacter.mHidden = true;
        mHiddenParticle.gameObject.SetActive(true);
        mHiddenParticle.Play();
        mPlayerRenderer.material = mHideMaterial;
    }

    void UnhideEffectTriggered()
    {
        if(!mCharacter.mHidden)
        {
            return;
        }
        if (!string.IsNullOrEmpty(mHideSFX) && AudioManager.IsValidSingleton())
        {
            AudioManager.Instance.PlaySound(mUnHideSFX);
        }
        mCharacter.mHidden = false;
        mHiddenParticle.Clear();
        mHiddenParticle.Stop();
        mHiddenParticle.gameObject.SetActive(false);
        mPlayerRenderer.material = mNormalMaterial;
    }

    void OnDrawGizmos()
    {
        if(!mDrawGizmos)
        {
            return;
        }
        Debug.DrawLine(transform.position + transform.up * mYHeight, transform.position - transform.up * 0.1f, Color.red);
    }

}
