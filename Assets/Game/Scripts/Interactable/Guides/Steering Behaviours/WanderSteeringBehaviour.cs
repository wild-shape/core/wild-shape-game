﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderSteeringBehaviour : SteeringBehaviourBase
{
	public float WanderDistance = 2.0f;
	public float WanderRadius = 1.0f;
	public float WanderJitter = 20.0f;

	private Vector3 WanderTarget;

	private void Start()
	{
		float theta = (float)Random.value * Mathf.PI * 2;
		WanderTarget = new Vector3(WanderRadius * (float)Mathf.Cos(theta),
								   0.0f,
								   WanderRadius * (float)Mathf.Sin(theta));
	}

	public override Vector3 calculateForce()
	{
		float jitterTimeSlice = WanderJitter * Time.deltaTime;

		WanderTarget = WanderTarget + new Vector3(Random.Range(-1.0f, 1.0f) * jitterTimeSlice,
												  0.0f,
												  Random.Range(-1.0f, 1.0f) * jitterTimeSlice);
		WanderTarget.Normalize();
		WanderTarget = WanderTarget * WanderRadius;

		mTarget = WanderTarget + new Vector3(0, 0, WanderDistance);
        mTarget = transform.rotation * mTarget + transform.position;

		Vector3 wanderForce = mTarget - transform.position;
		wanderForce.Normalize();
		wanderForce = wanderForce * mSteeringAgent.mMaxSpeed;

		return wanderForce;
	}

	private void OnDrawGizmos()
	{
		Vector3 circleCenter = transform.rotation * new Vector3(0.0f, 0.0f, WanderDistance) + transform.position;
		DebugExtension.DrawCircle(circleCenter, Vector3.up, Color.green, WanderRadius);

		Debug.DrawLine(transform.position, mTarget, Color.blue);

		DebugExtension.DebugArrow(transform.position, transform.forward * WanderDistance, Color.red);
	}
}
