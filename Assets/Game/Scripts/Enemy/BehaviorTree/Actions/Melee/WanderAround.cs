﻿///-------------------------------------------------------------------------------------------------
// File: WanderAround.cs
//
// Author: Dakshvir Singh Rehill
// Date: 10/6/2020
//
// Summary:	Enemy Wanders around for sometime
///-------------------------------------------------------------------------------------------------
using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;

[TaskCategory("Enemy/Melee")]
[TaskDescription("Wanders the enemy around last known player position in hopes of searching for him")]
public class WanderAround : MoveAgent
{
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The maximum radius around the player's last known position that the enemy should look for the player")]
    [SerializeField] float mMaxWanderRadius = 8.0f;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The last known position of the player")]
    [SerializeField] SharedVector3 mPlayerLastKnown;


    public override void OnStart()
    {
        base.OnStart();
        //set the speed as alert speed as enemy is alerted
        mAgent.speed = mEnemyInstance.mEnemyAlertSpeed;
        mDestination = mPlayerLastKnown.Value;
        mAgent.SetDestination(mDestination);
    }

    public override TaskStatus OnUpdate()
    {
        TaskStatus aReturnStatus = base.OnUpdate();
        if(aReturnStatus == TaskStatus.Success)
        {
            SetNewInspectPoint();
            return TaskStatus.Running;
        }
        return aReturnStatus;
    }

    void SetNewInspectPoint() //once the destination is reached find a new point on the wander circle
    {
        float aTheta = Random.value * Mathf.PI * 2.0f;
        float aRadius = mMaxWanderRadius * Random.value;
        //calculate the new position using circle value taking player's last known position as the center
        mDestination = mPlayerLastKnown.Value + new Vector3(aRadius * Mathf.Cos(aTheta),0, aRadius * Mathf.Sin(aTheta));
        mAgent.SetDestination(mDestination);
    }

    public override void OnEnd()
    {
        mAgent.speed = mEnemyInstance.mEnemySpeed;
    }

}
