﻿///-------------------------------------------------------------------------------------------------
// File: SceneHandler.cs
//
// Author: Aditya Dinesh, Dakshvir Singh Rehill
// Date: 3/6/2020
//
// Summary:	System to Scene specific Progression
///-------------------------------------------------------------------------------------------------

using BehaviorDesigner.Runtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneHandler : MonoBehaviour
{
    [Tooltip("The Rate of Regeneration of the special ability bar for the player")]
    public float mSARegeneratioRate = 0.1f;
    [Tooltip("The Camera Settings mono behavior of this scene")]
    public CameraController mCameraController;
    [Tooltip("The Name of the Sound Clip that will be played as Background Music for this scene")]
    [SerializeField] string mBackGroundMusic;
    [Tooltip("The Name of the Sound Clip that will be played as Background Ambience for this scene")]
    [SerializeField] string mBackGroundAmbience;
    [Tooltip("The Name of the Sound Effect that will be played Player Respawn")]
    [SerializeField] string mRespawnSFX = "CheckpointSoundEffect";
    [Tooltip("All the scene changers in the scene i.e. the point where loading scene is displayed and the scene changes")]
    [SerializeField] List<SceneTransitionTriggerable> mSceneChangers;
    [Tooltip("All the cores in the scene that need to be deactivated")]
    [SerializeField] List<Core> mCores;
    [Tooltip("The Menu Classifier for Credits Menu")]
    [SerializeField] MenuClassifier mCreditsClassifier;
    /// <summary>
    /// The reference to the credits menu in the game
    /// </summary>
    CreditsMenu mCreditsMenu;
    [Tooltip("ObjectPoolingClassifier for Res Effect")]
    [SerializeField] PoolClassifier mResurrectionClassifier;
    [Tooltip("The Delay After Which Res Effect should be played")]
    [SerializeField] float mResEffectDelay = 0.25f;
    void Awake()
    {
        GameManager.Instance.mSceneHandler = this; //set this as the scene manager
    }

    void Start()
    {
        mCreditsMenu = MenuManager.Instance.GetMenu<CreditsMenu>(mCreditsClassifier);
        if (!string.IsNullOrEmpty(mBackGroundMusic)) //play sound if music is set
        {
            AudioManager.Instance.PlaySound(mBackGroundMusic, true);
        }
        if (!string.IsNullOrEmpty(mBackGroundAmbience)) //play sound if ambience is set
        {
            AudioManager.Instance.PlaySound(mBackGroundAmbience, true);
        }
        if (!GameManager.Instance.mGameStarted) //set game as started as scene has been loaded
        {
            GameManager.Instance.mGameStarted = true;
            SaveGameManager.Instance.mLoadExists = true; //make sure reload allows checkpoint check
        }
        Transform aMainCharForm = mCameraController.mShapeShiftInst.mPlayerCharacters[0].transform;
        Transform aSecCharForm = mCameraController.mShapeShiftInst.mPlayerCharacters[1].transform;
        if (GameManager.Instance.mGameData.mCheckpointsVisited.Count > 0) //if there are checkpoints
        {
            CheckpointData aData = GameManager.Instance.mGameData.mLastCheckpoint;
            if (aData.mSceneIndex == GameManager.Instance.mGameData.mGameSceneIndex)
            {
                transform.position = aData.mPosition;
                aMainCharForm.position = aSecCharForm.position = aData.mPosition;
                aMainCharForm.rotation = aSecCharForm.rotation = Quaternion.Euler(aData.mRotation);
                StartCoroutine(PlayResEffect(aData.mPosition));
                if (!string.IsNullOrEmpty(mRespawnSFX)) //play sound if music is set
                {
                    AudioManager.Instance.PlaySound(mRespawnSFX);
                }
            }
        }
        mCameraController.mShapeShiftInst.SetupShapeshift();
    }

    void OnDestroy()
    {
        SharedEnemyList aDeactivatedEnemies = (SharedEnemyList)GlobalVariables.Instance.GetVariable("mDeactivatedEnemies");
        aDeactivatedEnemies.Value.Clear();
        if (AudioManager.IsValidSingleton())
        {
            if (!string.IsNullOrEmpty(mBackGroundMusic) && AudioManager.Instance.IsSoundPlaying(mBackGroundMusic))
            {
                AudioManager.Instance.StopSound(mBackGroundMusic);
            }
            if (!string.IsNullOrEmpty(mBackGroundAmbience) && AudioManager.Instance.IsSoundPlaying(mBackGroundAmbience))
            {
                AudioManager.Instance.StopSound(mBackGroundAmbience);
            }
        }
        if (GameManager.IsValidSingleton()
            &&
            GameManager.Instance.mSceneHandler == this)
        {
            GameManager.Instance.mSceneHandler = null;
        }
    }

    private void Update()
    {
        if (GameManager.Instance.mState == GameManager.State.Pause) // if game paused don't regenerate
        {
            return;
        }
        if (Cursor.lockState != CursorLockMode.Locked)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
        if (GameManager.Instance.mGameData.mPlayerData.mSpecialAbility < 1.0f
            && GameManager.Instance.mGameData.mPlayerData.mPlayerHealth > 0.0f) //regenerate and update all
        {
            GameManager.Instance.mGameData.mPlayerData.mSpecialAbility += mSARegeneratioRate * Time.deltaTime;
        }
        if (!GameManager.Instance.mGameStarted || GameManager.Instance.mGameData.mPlayerData.mCreditsShown)
        {
            return;
        }
        if (mSceneChangers == null || mSceneChangers.Count <= 0) //check if there are no scene changers
        {
            if (mCores != null && mCores.Count > 0) //and all cores have been deactivated
            {
                foreach (Core aCore in mCores)
                {
                    if (!aCore.mDeactivate)
                    {
                        return;
                    }
                }
                GameManager.Instance.mGameData.mPlayerData.mCreditsShown = true; //show credits
                SaveGameManager.Instance.SavePlayerData();
                mCreditsMenu.StartCredits();
            }
        }
    }

    /// <summary>
    /// Function that plays the resurrection effect after some delay
    /// </summary>
    /// <param name="pEffectPos">The position where the effect needs to happen</param>
    IEnumerator PlayResEffect(Vector3 pEffectPos)
    {
        yield return new WaitForSeconds(mResEffectDelay);
        GameObject aResEffect = ObjectPoolingManager.Instance.GetPooledObject(mResurrectionClassifier.mPoolName);
        aResEffect.transform.position = pEffectPos;
        aResEffect.SetActive(true);
        ParticleSystem aParticleEf = aResEffect.GetComponent<ParticleSystem>();
        aParticleEf.Play();
        yield return new WaitWhile(() => aParticleEf.isPlaying);
        aParticleEf.gameObject.SetActive(false);
        ObjectPoolingManager.Instance.ReturnPooledObject(mResurrectionClassifier.mPoolName, aParticleEf.gameObject);
    }

}