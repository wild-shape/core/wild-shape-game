﻿///-------------------------------------------------------------------------------------------------
// File: ProgressMenu.cs
//
// Author: Dakshvir Singh Rehill
// Date: 13/8/2020
//
// Summary:	This is the menu that shows how much game is completed and all the collectibles available
// with the collectibles that have been collected
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ProgressMenu : SubMenu
{
    public readonly Dictionary<int, CollectibleData> mAllCollectibleData = new Dictionary<int, CollectibleData>();

    [SerializeField] TextMeshProUGUI mCollectibleTxt;

    public override void Awake()
    {
        base.Awake();
        var aCollectibleData = Resources.LoadAll<CollectibleData>("CollectibleData");
        foreach (CollectibleData aData in aCollectibleData)
        {
            mAllCollectibleData.Add(aData.mCollectibleID, aData);
        }
    }

    public void ShowText(int pCollectibleIx)
    {
        CollectibleData aData;
        if (!mAllCollectibleData.TryGetValue(pCollectibleIx, out aData))
        {
            mCollectibleTxt.text = "";
            return;
        }
        mCollectibleTxt.text = aData.mCollectibleStory;
    }

    public void GetCollectedTotalInScene(out int pTotalCollected, out int pTotalInScene)
    {
        GameSceneName aScName = (GameSceneName)GameManager.Instance.mGameData.mGameSceneIndex;
        pTotalCollected = 0;
        pTotalInScene = 0;
        foreach (KeyValuePair<int, CollectibleData> aCollectibles in mAllCollectibleData)
        {
            if (aCollectibles.Value.mSceneID == aScName)
            {
                pTotalInScene++;
                if (GameManager.Instance.mGameData.mCollectiblesCollected.Contains(aCollectibles.Key))
                {
                    pTotalCollected++;
                }
            }
        }
    }

}
