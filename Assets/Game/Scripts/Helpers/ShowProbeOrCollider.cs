﻿///-------------------------------------------------------------------------------------------------
// File: ShowProbOrCollider.cs
//
// Author: Dakshvir Singh Rehill
// Date: 17/7/2020
//
// Summary: SceneUtility to allow designers to see all child colliders without selecting each of them
// or see all probes without selecting each of them
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowProbeOrCollider : MonoBehaviour
{
    [System.Serializable]
    public enum ExtensionType
    {
        BoxCollider,
        ReflectionProbe,
        OcclusionArea
    }
    [System.Serializable]
    public struct ExtensionData
    {
        public ExtensionType mExtType;
        public Color mGizmoColor;
    }
    [SerializeField]
    ExtensionData mExtensionData
        = new ExtensionData
        {
            mExtType = ExtensionType.BoxCollider,
            mGizmoColor = Color.red
        };
    [SerializeField] bool mDebugDraw = true;
    void OnDrawGizmos()
    {
        if (mDebugDraw)
        {
            Gizmos.color = mExtensionData.mGizmoColor;
            switch (mExtensionData.mExtType)
            {
                case ExtensionType.BoxCollider:
                    BoxCollider[] aColliders = gameObject.GetComponentsInChildren<BoxCollider>();
                    foreach (BoxCollider aCollider in aColliders)
                    {
                        Gizmos.matrix = Matrix4x4.TRS(aCollider.transform.position, aCollider.transform.rotation, aCollider.transform.lossyScale);
                        Gizmos.DrawCube(aCollider.center, aCollider.size);
                    }
                    break;
                case ExtensionType.ReflectionProbe:
                    ReflectionProbe[] aRProbe = gameObject.GetComponentsInChildren<ReflectionProbe>();
                    foreach (ReflectionProbe aReflectP in aRProbe)
                    {
                        Gizmos.matrix = Matrix4x4.TRS(aReflectP.transform.position, aReflectP.transform.rotation, aReflectP.transform.lossyScale);
                        Gizmos.DrawCube(aReflectP.center, aReflectP.size);
                    }
                    break;
                case ExtensionType.OcclusionArea:
                    OcclusionArea[] aOArea = gameObject.GetComponentsInChildren<OcclusionArea>();
                    foreach (OcclusionArea aArea in aOArea)
                    {
                        Gizmos.matrix = Matrix4x4.TRS(aArea.transform.position, aArea.transform.rotation, aArea.transform.lossyScale);
                        Gizmos.DrawCube(aArea.center, aArea.size);
                    }
                    break;
            }
        }
    }
}
