﻿///-------------------------------------------------------------------------------------------------
// File: Guide.cs
//
// Author: Dakshvir Singh Rehill, Aditya Dinesh
// Date: 7/7/2020
//
// Summary:	The Guide script is the base class for all guides in the game
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Guide : IInteractable
{
    [Header("Stationary Guide Settings")]
    [Tooltip("The List of Dialogues for the NPC (It is auto set for the Moving Guide")]
    public NPCDialogue mNPCDialogue;
    [Tooltip("The Trigger point after which new scene will load")]
    public SceneTransitionTriggerable mNextSceneTrigger;
    [Header("Guide Common Settings")]
    [Tooltip("The Animator Reference for the NPC character")]
    public Animator mGuideAnimator;
    [Header("Guide AI Settings")]
    [Tooltip("The Reference to the FSM component of this NPC character")]
    [SerializeField] protected Animator mFSMInstance;
    [Tooltip("The FSM Trigger Name for Idle State")]
    [SerializeField] protected string mIdleFSMTriggerName = "Idle";
    [Tooltip("The FSM Trigger Name for Next Conversation")]
    [SerializeField] protected string mConvFSMTriggerName = "NextConversation";
    [HideInInspector] public float mNPC_ID;
    [HideInInspector] public InGameDialogMenu mDialogMenu;
    protected bool mTalking = false;
    protected override void Start()
    {
        base.Start();
        mNPC_ID = transform.position.sqrMagnitude;
        mInteractableBy = InteractableByType.Both;
        if (!mNPCDialogue.mSummarize)
        {
            mNPCDialogue.mSummarize = CheckIfTalked();
        }
        if (mDialogMenu == null)
        {
            mDialogMenu = MenuManager.Instance.GetMenu<InGameDialogMenu>(GameManager.Instance.mInGameDialogMenu);
        }
    }

    public virtual void EndConversation()
    {
        TalkingTriggerReset();
        SaveNPCConversation();
    }

    protected  void SaveNPCConversation()
    {
        if (!CheckIfTalked())
        {
            GameManager.Instance.mGameData.mTalkedNPCs.Add(mNPC_ID);
            SaveGameManager.Instance.SavePlayerData();
        }
    }

    protected void TalkingTriggerReset()
    {
        if (!mTalking)
        {
            return;
        }
        mTalking = false;
        mFSMInstance.ResetTrigger(mConvFSMTriggerName);
        mFSMInstance.SetTrigger(mIdleFSMTriggerName);
        mDialogMenu.HideDialogBox();
        mActivePlayerCharacter.mGameAction.Movement.Enable();
        mActivePlayerCharacter.mShapeShiftInstance.ToggleShapeshift(true);
        if (mNextSceneTrigger != null)
        {
            mNextSceneTrigger.mActive = true;
        }
    }

    bool CheckIfTalked()
    {
        List<float> aTalkedNPCs = GameManager.Instance.mGameData.mTalkedNPCs;

        for (int aI = 0; aI < aTalkedNPCs.Count; aI++)
        {
            if (Mathf.Abs(aTalkedNPCs[aI] - mNPC_ID) <= float.Epsilon)
            {
                return true;
            }
        }
        return false;
    }

    protected override void OnTriggerEnter(Collider pOther)
    {
        base.OnTriggerEnter(pOther);
        if (mActivePlayerCharacter == null)
        {
            return;
        }
        if (mActivePlayerCharacter.gameObject.GetInstanceID() != pOther.gameObject.GetInstanceID())
        {
            return;
        }
        if (mActivePlayerCharacter != null)
        {
            mActivePlayerCharacter.mGameAction.PlayerBasic.Interact.performed += NextConversation;
        }
    }
    protected virtual void NextConversation(InputAction.CallbackContext pContext)
    {
        mFSMInstance.ResetTrigger(mIdleFSMTriggerName);
        mActivePlayerCharacter.mPlayerMovement.MakeStateIdleWithoutBlend();
        mTalking = true;
        mActivePlayerCharacter.mGameAction.Movement.Disable();
        mActivePlayerCharacter.mShapeShiftInstance.ToggleShapeshift(false);
        mFSMInstance.SetTrigger(mConvFSMTriggerName);
    }

    protected override void EndInteraction()
    {
        EndConversation();
        mActivePlayerCharacter.mGameAction.PlayerBasic.Interact.performed -= NextConversation;
        base.EndInteraction();
    }


}