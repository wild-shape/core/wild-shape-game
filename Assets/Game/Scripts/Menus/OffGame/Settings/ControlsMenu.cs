﻿///-------------------------------------------------------------------------------------------------
// File: ControlsMenu.cs
//
// Author: Dakshvir Singh Rehill
// Date: 24/6/2020
//
// Summary:	Menu that will allow the user to change control scheme
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class ControlsMenu : SubMenu
{
    [Tooltip("The Maximum sensitivity allowed for the camera")]
    [SerializeField] float mMaxCameraSensitivity = 10.0f;
    /// <summary>
    /// The Input Binding Action Scriptable for the game
    /// </summary>
    GameActions mGameActions;
    /// <summary>
    /// The Dictionary that holds the Controls MonoBehavior reference for all changeable controls
    /// </summary>
    readonly Dictionary<Controls, ModularControls> mActionVsUI = new Dictionary<Controls, ModularControls>();
    readonly Dictionary<LockedControls, ModularControls> mLockedVsUI = new Dictionary<LockedControls, ModularControls>();
    [Tooltip("UI Prefab for the control settings")]
    [SerializeField] GameObject mModularControlPrefab;
    [Tooltip("The Scroll View of the Control Settings")]
    [SerializeField] ScrollRect mScrollRect;
    [Tooltip("The Scroll Rect Content Rect Transform")]
    [SerializeField] RectTransform mControlContent;
    [Tooltip("The Scroll Rect VLG")]
    [SerializeField] VerticalLayoutGroup mScrollRectLG;
    [Tooltip("The slider that changes camera sensitivity")]
    [SerializeField] Slider mCameraSensitivity;
    [Tooltip("The Toggle for inverting the camera axis")]
    [SerializeField] Toggle mInvertCameraToggle;
    [Tooltip("The Scrollbar for the control scroll rect")]
    [SerializeField] Scrollbar mControlsScroller;
    [Tooltip("Dialog Box strings to display which control scheme you would like to change")]
    [SerializeField]
    DialogBoxStrings mWaitForInputDialog
        = new DialogBoxStrings()
        {
            mHeadingText = "Waiting For Input",
            mBodyText = "Please press the key you want to bind for action <i><b>{0}</b></i> .",
            mButtonOneText = "",
            mButtonTwoText = ""
        };
    /// <summary>
    /// Reference to the dialog menu
    /// </summary>
    OffGameDialogMenu mDialogMenu;

    /// <summary>
    /// Current Active Control scheme reference
    /// </summary>
    ControlSchemeType mActiveUIScheme = ControlSchemeType.KeyboardMouse;
    /// <summary>
    /// The index of the scroll
    /// </summary>
    int mScrollIx = 0;
    /// <summary>
    /// Total Number of Controls in the scroll
    /// </summary>
    int mTotalControls = -1;
    public int mActiveIndex
    {
        get
        {
            return mScrollIx;
        }
        set
        {
            mScrollIx = value;
            float aScrollVertPos = 1.0f - (float)mScrollIx / mTotalControls;
            mScrollRect.verticalNormalizedPosition = aScrollVertPos;
        }
    }

    public override void Awake()
    {
        base.Awake();
        mGameActions = new GameActions();
        GameManager.Instance.UpdateAllChangeableControls(mGameActions);
        mCameraSensitivity.maxValue = mMaxCameraSensitivity;
    }

    public override void Start()
    {
        base.Start();
        #region Add Changeable Controls
        Navigation aControlsNav;
        Controls aLastCtrl = Controls.Interact;
        mTotalControls = -1;
        foreach (Controls aControl in System.Enum.GetValues(typeof(Controls))) //create controls in the scroll rect
        {
            GameObject aModularObj = Instantiate(mModularControlPrefab, mControlContent, false);
            ModularControls aControls = aModularObj.GetComponent<ModularControls>();
            aControls.mIndex = ++mTotalControls;
            aControls.mControlsMenu = this;
            if (mActionVsUI.Count == 0)
            {
                aControlsNav = mBackBtn.navigation;
                aControlsNav.selectOnDown = aControls.mButton;
                mBackBtn.navigation = aControlsNav;
                aControlsNav = aControls.mButton.navigation;
                aControlsNav.selectOnLeft = aControlsNav.selectOnUp = mBackBtn;
                aControls.mButton.navigation = aControlsNav;
                mInitSelectable = aControls.mButton;
            }
            else
            {
                aControlsNav = aControls.mButton.navigation;
                aControlsNav.selectOnLeft = aControlsNav.selectOnUp = mActionVsUI[aLastCtrl].mButton;
                aControls.mButton.navigation = aControlsNav;
                aControlsNav = mActionVsUI[aLastCtrl].mButton.navigation;
                aControlsNav.selectOnRight = aControlsNav.selectOnDown = aControls.mButton;
                mActionVsUI[aLastCtrl].mButton.navigation = aControlsNav;
            }
            aControls.mActionName.text = aControl.ToString();
            mActionVsUI.Add(aControl, aControls);
            mControlContent.sizeDelta += new Vector2(0,
                aControls.mSelfTransform.sizeDelta.y + mScrollRectLG.spacing);
            aLastCtrl = aControl;
            Controls aTempControl = aControl;
            aControls.mButton.onClick.AddListener(() => OpenKeyBindingDialog(aTempControl));
        }
        #endregion
        #region Add Locked Controls
        LockedControls aLastLControl = LockedControls.Pause;
        foreach (LockedControls aControl in System.Enum.GetValues(typeof(LockedControls))) //create controls in the scroll rect
        {
            GameObject aModularObj = Instantiate(mModularControlPrefab, mControlContent, false);
            ModularControls aControls = aModularObj.GetComponent<ModularControls>();
            aControls.mControlsMenu = this;
            aControls.mIndex = ++mTotalControls;
            if (mLockedVsUI.Count == 0)
            {
                aControlsNav = mActionVsUI[aLastCtrl].mButton.navigation;
                aControlsNav.selectOnRight = aControlsNav.selectOnDown = aControls.mButton;
                mActionVsUI[aLastCtrl].mButton.navigation = aControlsNav;
                aControlsNav = aControls.mButton.navigation;
                aControlsNav.selectOnLeft = aControlsNav.selectOnUp = mActionVsUI[aLastCtrl].mButton;
                aControls.mButton.navigation = aControlsNav;
            }
            else
            {
                aControlsNav = aControls.mButton.navigation;
                aControlsNav.selectOnLeft = aControlsNav.selectOnUp = mLockedVsUI[aLastLControl].mButton;
                aControls.mButton.navigation = aControlsNav;
                aControlsNav = mLockedVsUI[aLastLControl].mButton.navigation;
                aControlsNav.selectOnRight = aControlsNav.selectOnDown = aControls.mButton;
                mLockedVsUI[aLastLControl].mButton.navigation = aControlsNav;
            }
            aControls.mActionName.text = aControl.ToString();
            mLockedVsUI.Add(aControl, aControls);
            mControlContent.sizeDelta += new Vector2(0, aControls.mSelfTransform.sizeDelta.y);
            aLastLControl = aControl;
            aControls.mButton.interactable = false;
        }
        aControlsNav = mLockedVsUI[aLastLControl].mButton.navigation;
        aControlsNav.selectOnRight = aControlsNav.selectOnDown = mCameraSensitivity;
        mLockedVsUI[aLastLControl].mButton.navigation = aControlsNav;
        aControlsNav = mCameraSensitivity.navigation;
        aControlsNav.selectOnUp = mLockedVsUI[aLastLControl].mButton;
        mCameraSensitivity.navigation = aControlsNav;
        aControlsNav = mInvertCameraToggle.navigation;
        aControlsNav.selectOnUp = mLockedVsUI[aLastLControl].mButton;
        mInvertCameraToggle.navigation = aControlsNav;
        #endregion
        mActiveIndex = 0;
        SetActiveControlSchemeUI(false);
        mDialogMenu = MenuManager.Instance.GetMenu<OffGameDialogMenu>(GameManager.Instance.mOffGameDialogMenu);
    }

    void OnDestroy()
    {
        mGameActions.Dispose();
    }

    void SetActiveControlSchemeUI(bool pCheckChange = true)
    {
        if (pCheckChange)
        {
            if (mActiveUIScheme == InputPromptHandler.Instance.mActiveControlSchemeType)
            {
                return;
            }
        }
        mActiveUIScheme = InputPromptHandler.Instance.mActiveControlSchemeType;
        foreach (KeyValuePair<Controls, ModularControls> aControlUI in mActionVsUI)
        {
            SetChangeableControlUI(aControlUI.Key, aControlUI.Value);
        }
        foreach (KeyValuePair<LockedControls, ModularControls> aControlUI in mLockedVsUI)
        {
            string aControlPath = "";
            switch (aControlUI.Key)
            {
                case LockedControls.Movement:
                    aControlPath = mActiveUIScheme != ControlSchemeType.KeyboardMouse ? "leftStick" : "Dpad";
                    break;
                case LockedControls.Camera:
                    aControlPath = mActiveUIScheme != ControlSchemeType.KeyboardMouse ? "rightStick" : "delta";
                    break;
                case LockedControls.Pause:
                    aControlPath = mActiveUIScheme != ControlSchemeType.KeyboardMouse ? "start" : "escape";
                    break;
            }
            SetModularControlScheme(aControlPath, aControlUI.Value);
        }
    }

    void SetChangeableControlUI(Controls pControl, ModularControls pControlUI)
    {
        ChangeableControls aControlValues = GameManager.Instance.mSettingsData.mCurrentControls.GetControl(pControl);
        string aControlPath = mActiveUIScheme != ControlSchemeType.KeyboardMouse ?
            aControlValues.mGamepadPath.Substring(aControlValues.mGamepadPath.LastIndexOf(">/") + 2) :
            aControlValues.mKeyboardPath.Substring(aControlValues.mKeyboardPath.LastIndexOf(">/") + 2);
        SetModularControlScheme(aControlPath, pControlUI);

    }

    void SetModularControlScheme(string pControlPath, ModularControls pControlUI)
    {
        PromptContainer aPrompt;
        if (InputPromptHandler.Instance.mUIPrompts[mActiveUIScheme].TryGetValue(pControlPath, out aPrompt))
        {
            pControlUI.mControlTxt.text = aPrompt.mPromptSettings.mFallbackText;
            pControlUI.mControlImg.sprite = aPrompt.mPromptImage;
            if (pControlUI.mControlImg.sprite != null)
            {
                pControlUI.mControlTxt.gameObject.SetActive(false);
                pControlUI.mControlImg.gameObject.SetActive(true);
            }
            else
            {
                pControlUI.mControlImg.gameObject.SetActive(false);
                pControlUI.mControlTxt.gameObject.SetActive(true);
            }
        }

    }

    void OnEnable()
    {
        mGameActions.Enable();
        mCameraSensitivity.value = GameManager.Instance.mSettingsData.mCurrentControls.mCameraSensitivity;
        mInvertCameraToggle.isOn = GameManager.Instance.mSettingsData.mCurrentControls.mFlipCamera;
        SetActiveControlSchemeUI();
        ToggleInteractable(true);
    }
    void OnDisable()
    {
        mGameActions.Disable();
    }

    public void OnInvertCameraChanged(bool pToggleVal)
    {
        GameManager.Instance.mSettingsData.mCurrentControls.mFlipCamera = pToggleVal;
        SaveGameManager.Instance.SaveSettingsData();
        CallCameraController();
    }

    void CallCameraController()
    {
        if (GameManager.Instance.mSceneHandler != null)
        {
            if (GameManager.Instance.mSceneHandler.mCameraController != null)
            {
                GameManager.Instance.mSceneHandler.mCameraController.OnCameraSpeedChanged();
            }
        }
    }

    public void OnCameraSensitivityChanged(float pVal)
    {
        GameManager.Instance.mSettingsData.mCurrentControls.mCameraSensitivity = pVal;
        SaveGameManager.Instance.SaveSettingsData();
        CallCameraController();
    }


    public void OpenKeyBindingDialog(Controls pSelectedControl)
    {
        ToggleInteractable(false);
        DialogBoxStrings aDBStrings = mWaitForInputDialog;
        aDBStrings.mBodyText = string.Format(aDBStrings.mBodyText, pSelectedControl.ToString());
        aDBStrings.mHeadingText = string.Format(aDBStrings.mHeadingText, pSelectedControl.ToString());
        mDialogMenu.ShowWaitForInputDialog(aDBStrings);
        if (mActiveUIScheme == ControlSchemeType.KeyboardMouse)
        {
            AllowKeyboardKeybinding(pSelectedControl);
        }
        else
        {
            AllowGamepadKeybinding(pSelectedControl);
        }
    }

    /// <summary>
    /// Allow Binding of Keyboard controls
    /// </summary>
    /// <param name="pControl"></param>
    void AllowKeyboardKeybinding(Controls pControl)
    {
        ChangeableControls aChangeableControlVal = GameManager.Instance.mSettingsData.mCurrentControls.GetControl(pControl);
        InputAction aAction = mGameActions.asset.FindAction(aChangeableControlVal.mActionID);
        aAction.Disable();
        var aRebindOp = aAction.PerformInteractiveRebinding()
            .WithBindingGroup("Keyboard&Mouse")
            .OnMatchWaitForAnother(1.0f)
            //remove all camera and movement 
            .WithControlsExcluding("<Pointer>/position")
             .WithControlsExcluding("<Pointer>/delta")
             .WithControlsExcluding("<Keyboard>/up")
             .WithControlsExcluding("<Keyboard>/down")
             .WithControlsExcluding("<Keyboard>/left")
             .WithControlsExcluding("<Keyboard>/right")
             .WithControlsExcluding("<Keyboard>/w")
             .WithControlsExcluding("<Keyboard>/s")
             .WithControlsExcluding("<Keyboard>/a")
             .WithControlsExcluding("<Keyboard>/d")
             .WithControlsHavingToMatchPath("<Keyboard>")
             .WithControlsHavingToMatchPath("<Mouse>")
             .OnComplete((pRebindOp) =>
            {
                OnRebindingOperationComplete(pRebindOp, pControl);
            }).
             Start();
    }
    /// <summary>
    /// When binding is complete make sure other scriptable objects have binding updated as well
    /// </summary>
    /// <param name="pRebindOp"></param>
    /// <param name="pChangedControl"></param>
    void OnRebindingOperationComplete(InputActionRebindingExtensions.RebindingOperation pRebindOp, Controls pChangedControl)
    {
        ChangeableControls aChangeableControlVal = GameManager.Instance.mSettingsData.mCurrentControls.GetControl(pChangedControl);
        int aC = 0;
        foreach (InputBinding aBinding in pRebindOp.action.bindings)
        {
            if (string.IsNullOrEmpty(aBinding.groups))
            {
                continue;
            }
            if (aBinding.groups.Contains("Gamepad"))
            {
                aChangeableControlVal.mGamepadPath = aBinding.effectivePath;
                aC++;
            }
            else if (aBinding.groups.Contains("Keyboard"))
            {
                aChangeableControlVal.mKeyboardPath = aBinding.effectivePath;
                aC++;
            }
            if (aC == 2)
            {
                break;
            }
        }
        GameManager.Instance.SetControls(aChangeableControlVal);
        pRebindOp.action.Enable();
        pRebindOp.Dispose();
        ModularControls aControlUI = mActionVsUI[pChangedControl];
        SetChangeableControlUI(pChangedControl, aControlUI);
        aControlUI.mButton.Select();
        ToggleInteractable(true);
        mDialogMenu.HideDialogBox();

    }
    /// <summary>
    /// Allow binding for gamepad
    /// </summary>
    /// <param name="pControl"></param>
    void AllowGamepadKeybinding(Controls pControl)
    {
        ChangeableControls aChangeableControlVal = GameManager.Instance.mSettingsData.mCurrentControls.GetControl(pControl);
        InputAction aAction = mGameActions.asset.FindAction(new System.Guid(aChangeableControlVal.mActionID));
        aAction.Disable();
        var aRebindOp = aAction.PerformInteractiveRebinding().WithBindingGroup("Gamepad")
             //remove all camera and movement 
             .WithControlsExcluding("<Gamepad>/leftStick")
             .WithControlsExcluding("<Gamepad>/rightStick")
             .WithControlsHavingToMatchPath("<Gamepad>")
             .OnComplete((pRebindOp) =>
             {
                 OnRebindingOperationComplete(pRebindOp, pControl);
             }).
             Start();
    }

    void ToggleInteractable(bool pEnable)
    {
        foreach (ModularControls aControl in mActionVsUI.Values)
        {
            aControl.mButton.interactable = pEnable;
        }
        mCameraSensitivity.interactable = pEnable;
        //mControlsScroller.interactable = pEnable;
        mInvertCameraToggle.interactable = pEnable;
    }

}
