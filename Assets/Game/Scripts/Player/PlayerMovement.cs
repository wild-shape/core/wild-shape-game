﻿///-------------------------------------------------------------------------------------------------
// File: PlayerMovement.cs
//
// Author: Dakshvir Singh Rehill
// Date: 20/5/2020
//
// Summary:	Responsbile for controlling just the movement mechanic of main player
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;

public class PlayerMovement : MonoBehaviour
{
    /// <summary>
    /// The current navigation state of the player
    /// </summary>
    public enum NavState
    {
        Idle,
        Move,
        Run,
        Unique,
        Fall
    }
    /// <summary>
    /// The current falling state of the player to allow for animation switching
    /// </summary>
    public enum FallState
    {
        NotFalling,
        FallStart,
        AlmostLand,
        Land
    }
    [Tooltip("The Player Character MonoBehavior Reference")]
    [SerializeField] PlayerCharacter mPlayerCharacter;
    [Header("Animation Values")]
    [Tooltip("Animator Parameter name for Speed of Move animations")]
    [SerializeField] string mModelSpeedAnimFloat = "ModelSpeed";
    [Tooltip("Speed of the move animations")]
    [SerializeField] float mModelAnimSpeed = 2.0f;
    [Tooltip("The Blend Tree values for Basic Walk Mode")]
    [Range(0, 2)]
    [SerializeField] float mBasicSpeedMultiplier = 2.0f;
    [Tooltip("The Blend Tree values for Run Mode")]
    [Range(2, 4)]
    [SerializeField] float mRunSpeedMultiplier = 4.0f;
    [Tooltip("Speed of the Unique Movement")]
    [SerializeField] float mUniqueAnimSpeed = 2.0f;
    [Tooltip("Speed of Player Fall")]
    [SerializeField] float mFallAnimSpeed = 2.0f;
    [Header("Movement Values")]
    [Tooltip("Stick input deadzone after which walking starts")]
    [SerializeField] float mStartWalkingThreshold = 0.1f;
    [Tooltip("The speed smoothing time when nav mode changes from idle<->walk<->run")]
    [SerializeField] float mSpeedChangeLerpTime = 0.5f;
    [Header("Fall Values")]
    [Tooltip("The Max distance from ground at which almost landing anim is triggered")]
    [SerializeField] float mAlmostLandingDistance = 2.0f;
    [Tooltip("The Max distance from ground at which landing anim is triggered")]
    [SerializeField] float mLandingDistance = 1.0f;
    [Tooltip("The delay before starting the fall animation")]
    [SerializeField] float mFallAnimDelay = 0.1f;
    [Header("Raycast Settings")]
    [Tooltip("The Maximum Falling Speed that needs to be reached after which the fall animation is triggered")]
    [SerializeField] float mFallYSpeed = 0.65f;
    [Tooltip("Raycast Distance to check the ground normal")]
    [SerializeField] float mGCheckDist = 0.2f;
    [Tooltip("The height of the unique movement mode")]
    [SerializeField] float mUniqueHeight = 5.0f;
    [Tooltip("From Raycast Point")]
    [SerializeField] Transform mFrontCheckpoint;
    [Tooltip("Back Raycast Point")]
    [SerializeField] Transform mBackCheckpoint;
    [Header("Sound Parameters")]
    [Tooltip("Raccoon/Bear Walk Sound Effect")]
    [SerializeField] string mPlayerWalk = "";
    [Tooltip("Raccoon/Bear Run Sound Effect")]
    [SerializeField] string mPlayerRunSFX = "RaccoonBearForestRun";
    /// <summary>
    /// The regular height of the Y Stablizing collider
    /// </summary>
    float mNormalHeight;
    /// <summary>
    /// The time after which player starts to fall
    /// </summary>
    float mNoGroundTimer = 0.0f;
    /// <summary>
    /// The current navigation state of the player
    /// </summary>
    [HideInInspector] public NavState mState = NavState.Idle;
    /// <summary>
    /// The current fall state of the player
    /// </summary>
    FallState mFallState = FallState.NotFalling;
    /// <summary>
    /// The amount of movement in the forward direction of the player
    /// </summary>
    float mForward;
    /// <summary>
    /// The amount of turn required to move the player
    /// </summary>
    float mTurn;
    /// <summary>
    /// The current multiplier to the blend tree speed
    /// </summary>
    float mSpeedMultiplier = 1.0f;
    /// <summary>
    /// The normal vector of the ground on which the player is
    /// </summary>
    Vector3 mGroundNormal = Vector3.up;

    void OnEnable()
    {
        mPlayerCharacter.mAnimator.SetFloat(mModelSpeedAnimFloat, mModelAnimSpeed);
        mPlayerCharacter.mAnimator.SetFloat(mPlayerCharacter.mUniqueMovementAnimBool + "Speed", mUniqueAnimSpeed);
        mNormalHeight = mPlayerCharacter.mYStablizingCollider.radius * 2.0f;
        if (!mPlayerCharacter.mStealthyCharacter)
        {
            mUniqueHeight = mPlayerCharacter.mYStablizingCollider.radius * 2.0f;
        }
    }
    void OnDisable()
    {
        mSpeedMultiplier = 1.0f;
        CheckAndStopSound(mPlayerRunSFX);
        CheckAndStopSound(mPlayerWalk);
    }

    void Start()
    {
        mPlayerCharacter.mAnimationListener.mOnAnimatorMove.AddListener(OnPlayerAnimatorMove);
    }

    void OnDestroy()
    {
        if (mPlayerCharacter != null)
        {
            mPlayerCharacter.mAnimationListener.mOnAnimatorMove.RemoveListener(OnPlayerAnimatorMove);
        }
    }
    /// <summary>
    /// The function to take the root motion of the player and give it to the rigidbody preserving the Y velocity of the body
    /// </summary>
    void OnPlayerAnimatorMove()
    {
        if (Time.deltaTime > 0)
        {
            Vector3 aVelocity = mPlayerCharacter.mAnimator.deltaPosition / Time.deltaTime;
            aVelocity.y = mPlayerCharacter.mRigidbody.velocity.y;
            mPlayerCharacter.mRigidbody.velocity = aVelocity;
        }
    }

    void Update()
    {
        NavUpdate();

        if(mState == NavState.Idle || mState == NavState.Unique)
        {
            CheckAndStopSound(mPlayerRunSFX);
            CheckAndStopSound(mPlayerWalk);
        }
        if(mState == NavState.Move)
        {
            CheckAndStopSound(mPlayerRunSFX);
            CheckAndPlaySound(mPlayerWalk);
        }
        if(mState == NavState.Run)
        {
            CheckAndStopSound(mPlayerWalk);
            CheckAndPlaySound(mPlayerRunSFX);
        }
    }

    /// <summary>
    /// Function to smooth transition between idle<->walk<->run
    /// </summary>
    /// <param name="pFrom">Previous speed</param>
    /// <param name="pTo">Desired speed</param>
    /// <param name="pSetState">Nav State</param>
    void MovementModeLerp(float pFromSpeed, float pToSpeed, float pFromDetection, float pToDetection, float pDetectionTime, NavState pSetState)
    {
        if (LeanTween.isTweening(gameObject))
        {
            LeanTween.cancel(gameObject);
        }
        LeanTween.value(gameObject, pFromSpeed, pToSpeed, mSpeedChangeLerpTime).
            setOnUpdate((float pVal) => mSpeedMultiplier = pVal);
        LeanTween.value(gameObject, pFromDetection, pToDetection, pDetectionTime).
            setOnUpdate((float pVal) => mPlayerCharacter.mDetectionModifier = pVal);
        mState = pSetState;

    }
    /// <summary>
    /// Function to continuously move if no action is being performed
    /// </summary>
    void NavUpdate()
    {
        CheckIsGrounded();
        Vector2 aStickVal = mPlayerCharacter.mGameAction.Movement.Move.ReadValue<Vector2>();
        if (GameManager.Instance.mState == GameManager.State.Pause)
        {
            aStickVal = Vector2.zero;
        }
        Vector3 aMovementVector = aStickVal.y * Camera.main.transform.forward + aStickVal.x * Camera.main.transform.right;
        aMovementVector.Normalize();
        aMovementVector = transform.InverseTransformDirection(aMovementVector);
        aMovementVector = Vector3.ProjectOnPlane(aMovementVector, mGroundNormal);
        float aIdleMag = aStickVal.magnitude;
        aMovementVector.Normalize();
        mPlayerCharacter.mAnimator.SetFloat(mPlayerCharacter.mIdleMagAnimFloat, aIdleMag, 0.1f, Time.deltaTime);
        mTurn = Mathf.Atan2(aMovementVector.x, aMovementVector.z);
        mForward = aMovementVector.z;
        if (mState != NavState.Fall)
        {
            CheckChangeToUniqueMovement();
            ChangeMovementModes(aIdleMag);
        }
        else
        {
            FallUpdate();
        }
    }
    /// <summary>
    /// If player is falling this update is called
    /// </summary>
    void FallUpdate()
    {
        RaycastHit aGroundHit;
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out aGroundHit
            , mAlmostLandingDistance * 5.0f, mPlayerCharacter.mShapeShiftInstance.mObstacles, QueryTriggerInteraction.Ignore))
        {
            //ground found
            float aDist = (transform.position - aGroundHit.point).magnitude;
            if (aDist <= mLandingDistance
                && mFallState != FallState.Land)
            {
                if (mFallState == FallState.FallStart)
                {
                    mPlayerCharacter.mAnimator.SetTrigger(mPlayerCharacter.mFallingAnimTrigger);
                }
                mPlayerCharacter.mAnimator.SetTrigger(mPlayerCharacter.mFallEndAnimTrigger);
                mFallState = FallState.Land;
            }
            else if (aDist <= mAlmostLandingDistance
                && mFallState != FallState.AlmostLand)
            {
                mPlayerCharacter.mAnimator.SetTrigger(mPlayerCharacter.mFallingAnimTrigger);
                mFallState = FallState.AlmostLand;
            }
        }
    }
    /// <summary>
    /// Open up player movement as soon as fall animation touches the ground
    /// </summary>
    public void AllowWalkAfterFall()
    {
        mState = NavState.Idle;
        mFallState = FallState.NotFalling;
    }
    /// <summary>
    /// Turn the player and set animation values for movement if idlemag > threshold
    /// </summary>
    /// <param name="pIdleMag"></param>
    void ChangeMovementModes(float pIdleMag)
    {
        if (pIdleMag > mStartWalkingThreshold)
        {
            TurnExtra();
            switch (mState)
            {
                case NavState.Idle:
                    MovementModeLerp(mSpeedMultiplier, mBasicSpeedMultiplier,
                        mPlayerCharacter.mDetectionModifier, -mPlayerCharacter.mWalkDetectionDecrement,
                        mPlayerCharacter.mDetectionModeChangeTime,
                        NavState.Move);
                    break;
                case NavState.Unique:
                    if (!LeanTween.isTweening(gameObject) &&
                        (mSpeedMultiplier < mBasicSpeedMultiplier || mSpeedMultiplier > mBasicSpeedMultiplier))
                    {
                        MovementModeLerp(mSpeedMultiplier, mBasicSpeedMultiplier,
                            mPlayerCharacter.mDetectionModifier, mPlayerCharacter.mUniqueDetectionChange,
                            mPlayerCharacter.mDetectionModeChangeTime,
                            NavState.Unique);
                    }
                    break;
                case NavState.Move:
                    if (mPlayerCharacter.mShapeShiftInstance.mRunPressed)
                    {
                        MovementModeLerp(mSpeedMultiplier, mRunSpeedMultiplier,
                            mPlayerCharacter.mDetectionModifier, mPlayerCharacter.mRunDetectionIncrement,
                            mPlayerCharacter.mToRunDetectionTime,
                            NavState.Run);
                    }
                    break;
                case NavState.Run:
                    if (!mPlayerCharacter.mShapeShiftInstance.mRunPressed)
                    {
                        MovementModeLerp(mSpeedMultiplier, mBasicSpeedMultiplier,
                            mPlayerCharacter.mDetectionModifier, -mPlayerCharacter.mWalkDetectionDecrement,
                            mPlayerCharacter.mDetectionModeChangeTime,
                            NavState.Move);
                    }
                    break;
            }
            mPlayerCharacter.mAnimator.SetFloat(mPlayerCharacter.mVerticalAnimFloat, mForward * mSpeedMultiplier, 0.1f, Time.deltaTime);
            mPlayerCharacter.mAnimator.SetFloat(mPlayerCharacter.mHorizontalAnimFloat, mTurn * mSpeedMultiplier, 0.1f, Time.deltaTime);
        }
        else
        {
            MovementModeLerp(mSpeedMultiplier, 1.0f,
                mPlayerCharacter.mDetectionModifier, mState != NavState.Unique ?
                -mPlayerCharacter.mIdleDetectionDecrement :
                mPlayerCharacter.mUniqueDetectionChange,
                mPlayerCharacter.mDetectionModeChangeTime,
                mState != NavState.Unique ?
                NavState.Idle : NavState.Unique);
        }
    }
    /// <summary>
    /// Check if height is available for the player to go into unique movement
    /// </summary>
    void CheckChangeToUniqueMovement()
    {
        if (mPlayerCharacter.mShapeShiftInstance.mUniqueMovement
            && mState != NavState.Unique)
        {
            CheckUniqueMovementSpace();
        }
        else if (!mPlayerCharacter.mShapeShiftInstance.mUniqueMovement
            && mState == NavState.Unique)
        {
            CheckNormalMovementSpace();
        }
        mPlayerCharacter.mAnimator.SetBool(mPlayerCharacter.mUniqueMovementAnimBool, mState == NavState.Unique);
        mPlayerCharacter.mYStablizingCollider.enabled = (mState != NavState.Unique && mState != NavState.Fall);

    }
    /// <summary>
    /// Check the height of the unique movement collision
    /// </summary>
    void CheckUniqueMovementSpace()
    {
        if (!Physics.Raycast(transform.position, transform.up, mUniqueHeight, mPlayerCharacter.mShapeShiftInstance.mObstacles, QueryTriggerInteraction.Ignore))
        {
            mState = NavState.Unique;
            if (LeanTween.isTweening(gameObject))
            {
                LeanTween.cancel(gameObject);
            }
        }
    }
    /// <summary>
    /// Check the height of the normal movement collision
    /// </summary>
    void CheckNormalMovementSpace()
    {

        if (Physics.Raycast(mFrontCheckpoint.position, mFrontCheckpoint.up, mNormalHeight, mPlayerCharacter.mShapeShiftInstance.mObstacles, QueryTriggerInteraction.Ignore))
        {
            return;
        }
        if (Physics.Raycast(mBackCheckpoint.position, mBackCheckpoint.up, mNormalHeight, mPlayerCharacter.mShapeShiftInstance.mObstacles, QueryTriggerInteraction.Ignore))
        {
            return;
        }
        mState = NavState.Idle;
    }

    /// <summary>
    /// Extra turning that the animation doesn't provide
    /// </summary>
    void TurnExtra()
    {
        if (mState != NavState.Idle)
        {
            float aTurnSpeed = Mathf.Lerp(180, 360, mForward);
            transform.Rotate(0, mTurn * aTurnSpeed * Time.deltaTime, 0);
        }
    }

    /// <summary>
    /// Raycast to get Ground Normal
    /// </summary>
    void CheckIsGrounded()
    {
        RaycastHit aHitInfo;
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out aHitInfo, mGCheckDist
            , mPlayerCharacter.mShapeShiftInstance.mObstacles, QueryTriggerInteraction.Ignore))
        {
            mGroundNormal = aHitInfo.normal;
        }
        if (mState != NavState.Fall)//check fall and trigger it
        {
            if (mPlayerCharacter.mRigidbody.velocity.y < -mFallYSpeed)//use rigidbody speed to determine ground fall anim
            {
                mNoGroundTimer += Time.deltaTime;
                if (mNoGroundTimer > mFallAnimDelay)
                {
                    mState = NavState.Fall;
                    mPlayerCharacter.mAnimator.SetFloat(mPlayerCharacter.mFallAnimTrigger + "Speed", mFallAnimSpeed);
                    mPlayerCharacter.mAnimator.SetBool(mPlayerCharacter.mUniqueMovementAnimBool, false);
                    mPlayerCharacter.mAnimator.SetTrigger(mPlayerCharacter.mFallAnimTrigger);
                    mFallState = FallState.FallStart;
                    mNoGroundTimer = 0.0f;
                }
            }
            else
            {
                mNoGroundTimer = 0.0f;
            }
        }
    }

    /// <summary>
    /// Function instantly stops the player from moving and is used to ensure the player doesn't run past interactable trigger zones when 
    /// event is triggered
    /// </summary>
    public void MakeStateIdleWithoutBlend()
    {
        mState = NavState.Idle;
        mPlayerCharacter.mAnimator.SetFloat(mPlayerCharacter.mIdleMagAnimFloat, 0.0f);
        mSpeedMultiplier = 1.0f;
    }

    /// <summary>
    /// Plays a sound if not playing already
    /// </summary>
    /// <param name="pSoundName"> The name of the Sound that needs to be played</param>
    void CheckAndPlaySound(string pSoundName)
    {
        if(!string.IsNullOrEmpty(pSoundName) && !AudioManager.Instance.IsSoundPlaying(pSoundName))
        {
            AudioManager.Instance.PlaySound(pSoundName, true);
        }
    }
    /// <summary>
    /// Stops a sound if playing already
    /// </summary>
    /// <param name="pSoundName"> The name of the Sound that needs to be played</param>
    void CheckAndStopSound(string pSoundName)
    {
        if(!AudioManager.IsValidSingleton())
        {
            return;
        }
        if (!string.IsNullOrEmpty(pSoundName) && AudioManager.Instance.IsSoundPlaying(pSoundName))
        {
            AudioManager.Instance.StopSound(pSoundName);
        }
    }

}
