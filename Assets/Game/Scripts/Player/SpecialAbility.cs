﻿///-------------------------------------------------------------------------------------------------
// File: SpecialAbility.cs
//
// Author: Dakshvir Singh Rehill
// Date: 3/6/2020
//
// Summary:	Script that instantiate objects of player's special ability
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SpecialAbility : MonoBehaviour
{
    [Tooltip("Character of this Ability")]
    [SerializeField] protected PlayerCharacter mCharacter;
    [Tooltip("Ability Pool")]
    [SerializeField] protected PoolClassifier mAbilityPool;
    /// <summary>
    /// The Menu that shows the effect of no ability
    /// </summary>
    HUDEffectsMenu mHUDEffects = null;
    /// <summary>
    /// The menu that allows to display the stats of the ability after using it
    /// </summary>
    HUDStatsMenu mHUDStatsMenu = null;
    /// <summary>
    /// Boolean that displays if the ability is being casted
    /// </summary>
    protected bool mAbilityMode = false;

    protected virtual void Start()
    {
        mHUDEffects = MenuManager.Instance.GetMenu<HUDEffectsMenu>(GameManager.Instance.mHUDPromptsMenu);
        mHUDStatsMenu = MenuManager.Instance.GetMenu<HUDStatsMenu>(GameManager.Instance.mHUDStatsMenu);
        mCharacter.mGameAction.PlayerAdvanced.AbilityMode.performed += AbilityPerformed;
        mCharacter.mGameAction.PlayerAdvanced.AbilityMode.canceled += ActivateAbility;
        mCharacter.mGameAction.PlayerAdvanced.CancelAbility.performed += AbilityCanceled;
    }

    protected virtual void OnDestroy()
    {
        if(mCharacter != null)
        {
            mCharacter.mGameAction.PlayerAdvanced.AbilityMode.performed -= AbilityPerformed;
            mCharacter.mGameAction.PlayerAdvanced.AbilityMode.canceled -= ActivateAbility;
            mCharacter.mGameAction.PlayerAdvanced.CancelAbility.performed -= AbilityCanceled;
        }
    }

    void AbilityPerformed(InputAction.CallbackContext pObj)
    {
        if (GameManager.Instance.mState == GameManager.State.Pause)
        {
            return;
        }
        if (!CanPerformAbility())
        {
            if(mHUDEffects != null)
            {
                mHUDEffects.SetNoAbility();
            }
            return;
        }
        mAbilityMode = true;
        PerformAbility(pObj);
    }

    protected virtual bool CanPerformAbility()
    {
        return mCharacter.mPlayerMovement.mState != PlayerMovement.NavState.Unique &&
            GameManager.Instance.mGameData.mPlayerData.mSpecialAbility >= mCharacter.mRequiredStrength;
    }

    protected virtual void AbilityCanceled(InputAction.CallbackContext pObj)
    {
        mAbilityMode = false;
    }

    protected virtual void ActivateAbility(InputAction.CallbackContext pObj)
    {
        GameManager.Instance.mGameData.mPlayerData.mSpecialAbility -= mCharacter.mRequiredStrength;
    }



    protected virtual void PerformAbility(InputAction.CallbackContext pObj)
    {

    }

}
