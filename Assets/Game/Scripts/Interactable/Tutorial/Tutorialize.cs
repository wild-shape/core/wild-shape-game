﻿///-------------------------------------------------------------------------------------------------
// File: Tutorialize.cs
//
// Author: Aditya Dinesh, Dakshvir Singh Rehill
// Date: 15/7/2020
//
// Summary:	Handles tutorialization of Mechanics through HUD Prompts 
///-------------------------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Tutorialize : IInteractable
{
    [Tooltip("The Tutorial Info that needs to be displayed")]
    [SerializeField] TutorialInfo mTutorialInfo;
    [Tooltip("The Type of Tutorial for this Tutorialize Object")]
    [SerializeField] TeachableMechanics mMechanicsToTeach;
    
    [Tooltip("The Delay after a shapeshift for which the prompt won't be shown")]
    [SerializeField] [Range(0.2f, 2)] float mShiftTimePause = 1.0f;
    /// <summary>
    /// Dialog Menu reference that allows to display the tutorial prompt
    /// </summary>
    InGameDialogMenu mDialogMenu;
    /// <summary>
    /// Reference to the HUD stats menu to call movement HUD deactivate
    /// </summary>
    HUDStatsMenu mStatsMenu;
    /// <summary>
    /// Boolean that checks if HUD Prompts need to be displayed again
    /// </summary>
    bool mFinishTeaching = false;
    /// <summary>
    /// Boolean that ensures that TutorialEnd is only called if already interacted with the player
    /// </summary>
    bool mHasInteracted = false;
    /// <summary>
    /// The character form on which the ability is being taught
    /// </summary>
    PlayerCharacter mTeachableCharacter;
    /// <summary>
    /// The delay after which the player will be shown the prompt again
    /// </summary>
    float mShapeshiftDelay = 0.0f;
    protected override void Start()
    {
        base.Start();
        mStatsMenu = MenuManager.Instance.GetMenu<HUDStatsMenu>(GameManager.Instance.mHUDStatsMenu);
        mDialogMenu = MenuManager.Instance.GetMenu<InGameDialogMenu>(GameManager.Instance.mInGameDialogMenu);
        mFinishTeaching = GameManager.Instance.mGameData.mTutorialData[(int)mMechanicsToTeach];
    }

    protected override void Update()
    {
        base.Update();
        if (mFinishTeaching)
        {
            Destroy(gameObject);
            return;
        }
        if (mShapeshiftDelay > 0.0f)
        {
            mShapeshiftDelay -= Time.deltaTime;
        }
    }

    void TaughtAbility(InputAction.CallbackContext pContext)
    {
        if (mTeachableCharacter == null)
        {
            return;
        }
        mFinishTeaching = true;
        GameManager.Instance.mGameData.mTutorialData[(int)mMechanicsToTeach] = true;
        SaveGameManager.Instance.SavePlayerData();
        FinishTutorial();
    }

    void PlayerShapeshifted(InputAction.CallbackContext pContext)
    {
        mShapeshiftDelay = mShiftTimePause;
    }

    protected override void EndInteraction()
    {
        GameManager.Instance.mSceneHandler.mCameraController.mShapeShiftInst
        .mInputActions.PlayerBasic.ShapeShift.performed -= PlayerShapeshifted;
        base.EndInteraction();
    }

    protected override void OnTriggerEnter(Collider pOther)
    {
        if (mShapeshiftDelay > 0.0f)
        {
            return;
        }
        base.OnTriggerEnter(pOther);
        if (mActivePlayerCharacter == null)
        {
            return;
        }
        if (mActivePlayerCharacter.gameObject.GetInstanceID() != pOther.gameObject.GetInstanceID())
        {
            return;
        }
        if (!mFinishTeaching)
        {
            mStatsMenu.DeactivateMovementHUD();
            mActivePlayerCharacter.mPlayerMovement.MakeStateIdleWithoutBlend();
            GameManager.Instance.ChangeGamePause(true);
            mDialogMenu.ShowHUDPromptDialogueBox(mTutorialInfo);
            mHasInteracted = true;
            mActivePlayerCharacter.mGameAction.PlayerBasic.Interact.performed += EndTutorial;
            GameManager.Instance.mSceneHandler.mCameraController.mShapeShiftInst
            .mInputActions.PlayerBasic.ShapeShift.performed += PlayerShapeshifted;
        }
    }

    void FinishTutorial()
    {
        switch (mMechanicsToTeach)
        {
            case TeachableMechanics.Shapeshift:
                ShapeShift aShapeInst = GameManager.Instance.mSceneHandler.mCameraController.mShapeShiftInst;
                aShapeInst.mInputActions.PlayerBasic.ShapeShift.performed -= TaughtAbility;
                break;
            case TeachableMechanics.AOEEnemy:
            case TeachableMechanics.AOEButton:
            case TeachableMechanics.BlinkUp:
            case TeachableMechanics.BlinkAcross:
                mTeachableCharacter.mGameAction.PlayerAdvanced.AbilityMode.performed -= TaughtAbility;
                break;
        }
        mTeachableCharacter = null;
    }

    void EndTutorial(InputAction.CallbackContext pContext)
    {
        if (mActivePlayerCharacter == null
            || GameManager.Instance.mState != GameManager.State.Pause)
        {
            return;
        }
        mActivePlayerCharacter.mGameAction.PlayerBasic.Interact.performed -= EndTutorial;
        GameManager.Instance.ChangeGamePause(false);
        if (mHasInteracted)
        {
            mDialogMenu.HideDialogBox();
            ShapeShift aShapeInst = GameManager.Instance.mSceneHandler.mCameraController.mShapeShiftInst;
            int aActiveCharIx = GameManager.Instance.mGameData.mPlayerData.mCurChar;
            switch (mMechanicsToTeach)
            {
                case TeachableMechanics.Shapeshift:
                    mTeachableCharacter = mActivePlayerCharacter;
                    aShapeInst.mInputActions.PlayerBasic.ShapeShift.performed += TaughtAbility;
                    break;
                case TeachableMechanics.AOEEnemy:
                case TeachableMechanics.AOEButton:
                    mTeachableCharacter = !aShapeInst.mPlayerCharacters[aActiveCharIx].mStealthyCharacter ? mActivePlayerCharacter :
                        aShapeInst.mPlayerCharacters[(aActiveCharIx + 1) % aShapeInst.mPlayerCharacters.Length];
                    mTeachableCharacter.mGameAction.PlayerAdvanced.AbilityMode.performed += TaughtAbility;
                    break;
                case TeachableMechanics.BlinkUp:
                case TeachableMechanics.BlinkAcross:
                    mTeachableCharacter = aShapeInst.mPlayerCharacters[aActiveCharIx].mStealthyCharacter ? mActivePlayerCharacter :
                        aShapeInst.mPlayerCharacters[(aActiveCharIx + 1) % aShapeInst.mPlayerCharacters.Length];
                    mTeachableCharacter.mGameAction.PlayerAdvanced.AbilityMode.performed += TaughtAbility;
                    break;
                case TeachableMechanics.Hide:
                    mTeachableCharacter = mActivePlayerCharacter;
                    TaughtAbility(new InputAction.CallbackContext());
                    break;
            }
            mHasInteracted = false;
        }
    }
}
