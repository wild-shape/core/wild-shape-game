﻿///-------------------------------------------------------------------------------------------------
// File: CollectibleObj.cs
//
// Author: Dakshvir Singh Rehill
// Date: 13/8/2020
//
// Summary:	Class Used to store index of collectible and check whether the collectible is active or not
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CollectibleObj : MonoBehaviour, ISelectHandler, IDeselectHandler
{
    [SerializeField] Image mSelectedImg;
    [SerializeField] Sprite mActiveImg;
    [SerializeField] Sprite mInactiveImg;
    [SerializeField] float mDeselectOpacity = 0.5f;
    [SerializeField] ProgressMenu mProgressMenu;
    [SerializeField] int mCollectibleIx;

    void OnEnable()
    {
        if (GameManager.Instance.mGameData.mCollectiblesCollected.Contains(mCollectibleIx))
        {
            mSelectedImg.sprite = mActiveImg;
            Color aColor = mSelectedImg.color;
            aColor.a = mDeselectOpacity;
            mSelectedImg.color = aColor;
        }
        else
        {
            mSelectedImg.sprite = mInactiveImg;
            Color aColor = mSelectedImg.color;
            aColor.a = 1.0f;
            mSelectedImg.color = aColor;

        }
    }

    public void OnSelect(BaseEventData eventData)
    {
        Color aColor = mSelectedImg.color;
        aColor.a = 1.0f;
        mSelectedImg.color = aColor;
        if (GameManager.Instance.mGameData.mCollectiblesCollected.Contains(mCollectibleIx))
        {
            mProgressMenu.ShowText(mCollectibleIx);
        }
    }
    public void OnDeselect(BaseEventData eventData)
    {
        if (GameManager.Instance.mGameData.mCollectiblesCollected.Contains(mCollectibleIx))
        {
            Color aColor = mSelectedImg.color;
            aColor.a = mDeselectOpacity;
            mSelectedImg.color = aColor;
        }
        mProgressMenu.ShowText(-1);
    }
}
