﻿///-------------------------------------------------------------------------------------------------
// File: DialogBox.cs
//
// Author: Dakshvir Singh Rehill
// Date: 24/6/2020
//
// Summary:	Modular UI Holder for Dialog Box Buttons and Text
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class DialogBox : MonoBehaviour
{
    [Tooltip("The text of the main heading")]
    public TextMeshProUGUI mHeadingText;
    [Tooltip("The text of the main body")]
    public TextMeshProUGUI mBodyText;
    [Tooltip("The text of button one")]
    public TextMeshProUGUI mButtonOneText;
    [Tooltip("The text of button two")]
    public TextMeshProUGUI mButtonTwoText;
    [Tooltip("The instance of button one")]
    public Button mButtonOne;
    [Tooltip("The instance of button two")]
    public Button mButtonTwo;
}
