﻿///-------------------------------------------------------------------------------------------------
// File: Gate.cs
//
// Author: Dakshvir Singh Rehill
// Date: 30/5/2020
//
// Summary:	Mechanical Door that can be controlled using a pressure plate and bear AOE ability
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Gate : MonoBehaviour, IButtonControllable
{
    [System.Serializable]
    public struct DoorSide
    {
        [Tooltip("The Pivot of the door side that should move")]
        public Transform mDoorPivot;
        [Tooltip("The Position of the door side when door is closed")]
        public Transform mClosedDoorPos;
        [Tooltip("The Position of the door side when door is open")]
        public Transform mOpenDoorPos;
    }

    ButtonControllableState mState = ButtonControllableState.Idle;
    [Tooltip("Set this to true if the door is supposed to stay open")]
    [SerializeField] bool mStayOpen = false;
    [Tooltip("Stay Open Door Attachment which is active when door is stay open")]
    [SerializeField] GameObject mDoorAttachment;
    [Tooltip("Left Side of the Door")]
    [SerializeField] DoorSide mLeftDoor;
    [Tooltip("Right Side of the  Door")]
    [SerializeField] DoorSide mRightDoor;
    [Tooltip("Pressure Plate that controls this gate")]
    [SerializeField] PressurePlate mControllingPlate;
    [Tooltip("The time it takes to close the door")]
    [SerializeField] float mClosingTime = 0.5f;
    [Tooltip("Audio Assisstant component for 3D sound")]
    [SerializeField] AudioAssistant mAudioAssisstant;
    [Tooltip("Sound Effect for Opening and Closing Door")]
    [SerializeField] string mDoorOpenCloseSFX = "DoorCloseOpen";
    bool mPressedAnimationCalled = false;
    bool mUnpressedAnimationCalled = false;
    void Start()
    {
        mControllingPlate.mControlledObjects.Add(this); //add interface reference
    }

    void OnDestroy()
    {
        if (mControllingPlate != null)
        {
            mControllingPlate.mControlledObjects.Remove(this);
        }
    }

    void Update()
    {
        mState = mStayOpen ? ButtonControllableState.Pressed : mState;
        switch (mState) //compare state and do animation
        {
            case ButtonControllableState.Idle:
                if ((mLeftDoor.mDoorPivot.position - mLeftDoor.mClosedDoorPos.position).sqrMagnitude > 0.2f)
                {
                    mLeftDoor.mDoorPivot.position = mLeftDoor.mClosedDoorPos.position;
                    mRightDoor.mDoorPivot.position = mRightDoor.mClosedDoorPos.position;
                }
                break;
            case ButtonControllableState.Pressed:
                if (mStayOpen)
                {
                    if (!mDoorAttachment.activeInHierarchy)
                    {
                        mDoorAttachment.SetActive(true);
                        mLeftDoor.mDoorPivot.gameObject.SetActive(false);
                        mRightDoor.mDoorPivot.gameObject.SetActive(false);
                    }
                    return;
                }
                if (!mPressedAnimationCalled)
                {
                    mPressedAnimationCalled = true;
                    LeanTween.move(mLeftDoor.mDoorPivot.gameObject, mLeftDoor.mOpenDoorPos.position, 0.05f);
                    LeanTween.move(mRightDoor.mDoorPivot.gameObject, mRightDoor.mOpenDoorPos.position, 0.05f);
                }
                break;
            case ButtonControllableState.Unpressed:
                if (mStayOpen)
                {
                    return;
                }
                if (!mUnpressedAnimationCalled)
                {
                    mUnpressedAnimationCalled = true;
                    LeanTween.move(mLeftDoor.mDoorPivot.gameObject, mLeftDoor.mClosedDoorPos.position, mClosingTime);
                    LeanTween.move(mRightDoor.mDoorPivot.gameObject, mRightDoor.mClosedDoorPos.position, mClosingTime)
                        .setOnComplete(() => { mState = ButtonControllableState.Idle; });
                }
                break;
        }
    }

    public void ButtonDown(PressurePlate pControllingPlate)
    {
        CancelTweens();
        mState = ButtonControllableState.Pressed;
        mPressedAnimationCalled = false;
        PlaySound();
    }

    void CancelTweens()
    {
        if (LeanTween.isTweening(mRightDoor.mDoorPivot.gameObject))
        {
            LeanTween.cancel(mRightDoor.mDoorPivot.gameObject);
        }
        if (LeanTween.isTweening(mLeftDoor.mDoorPivot.gameObject))
        {
            LeanTween.cancel(mLeftDoor.mDoorPivot.gameObject);
        }
    }

    public void ButtonUp(PressurePlate pControllingPlate)
    {
        CancelTweens();
        mState = ButtonControllableState.Unpressed;
        mUnpressedAnimationCalled = false;
        PlaySound();
    }

    void PlaySound()
    {
        if (AudioManager.IsValidSingleton() && !string.IsNullOrEmpty(mDoorOpenCloseSFX) && mAudioAssisstant != null)
        {
            if (AudioManager.Instance.IsSoundPlaying(mDoorOpenCloseSFX))
            {
                AudioManager.Instance.StopSound(mDoorOpenCloseSFX);
            }
            mAudioAssisstant.PlaySound(mDoorOpenCloseSFX);
        }
    }

}
