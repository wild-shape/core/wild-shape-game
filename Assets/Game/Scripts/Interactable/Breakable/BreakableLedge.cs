﻿///-------------------------------------------------------------------------------------------------
// File: BreakableLedge.cs
//
// Author: Aditya Dinesh
// Date: 9/6/2020
//
// Summary:	Logic for Breakble Ledges
///-------------------------------------------------------------------------------------------------

using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BreakableLedge : IBreakable
{
    [Header("Platform Break Variables")]
    [Tooltip(" The Speed at which the platform should shake")]
    [SerializeField] float mSpeed;
    [Tooltip(" The Shake threshold for platform or the amount of shakiness")]
    [SerializeField] float mAmount;
    [Tooltip(" Time interval before shake starts")]
    [SerializeField] float mWaitBeforeShake;
    //[Tooltip("Off Mesh Link")]
    //[SerializeField] OffMeshLink mOffMeshLink;
    [Tooltip("The Sound Effect for Screen Shake")]
    [SerializeField] string mScreenShakeSFX = "RockScreenShake";
    private float mCurWaitTime;
    CinemachineFreeLook mCamera;

    void Start()
    {
        mCamera = GameManager.Instance.mSceneHandler.mCameraController.mFreeLookCam;
    }

    protected override void Update()
    {
        base.Update();
        //if(mOffMeshLink != null && !mOffMeshLink.activated)
        //{
        //    mOffMeshLink.activated = true;
        //}
    }

    protected override void BreakObject()
    {
        StopShake();
        base.BreakObject();
        //mOffMeshLink.activated = false;
    }

    protected override void EndDrop()
    {
        base.EndDrop();
        StopShake();
        if (AudioManager.Instance.IsSoundPlaying(mScreenShakeSFX) && !string.IsNullOrEmpty(mScreenShakeSFX))
        {
            AudioManager.Instance.StopSound(mScreenShakeSFX);
        }
    }


    protected override void BreakingUpdate()
    {
        base.BreakingUpdate();

        if (mCurWaitTime >= mWaitBeforeShake)
        {
            return;
        }
        mCurWaitTime += Time.deltaTime;
        if (mCurWaitTime >= mWaitBeforeShake)
        {
            OnShake();
            
        }
    }

    protected override void BreakingStop()
    {
        base.BreakingStop();
        StopShake();
        if (AudioManager.Instance.IsSoundPlaying(mScreenShakeSFX) && !string.IsNullOrEmpty(mScreenShakeSFX))
        {
            AudioManager.Instance.StopSound(mScreenShakeSFX);
        }
    }
    protected override void BreakingStart()
    {
        base.BreakingStart();
        mCurWaitTime = 0.0f;
        if (!AudioManager.Instance.IsSoundPlaying(mScreenShakeSFX) && !string.IsNullOrEmpty(mScreenShakeSFX))
        {
            AudioManager.Instance.PlaySound(mScreenShakeSFX, true);
        }
    }


    /// <summary>
    /// Function to start shaking the Camera 
    /// </summary>
    void OnShake()
    {
        if (mCamera == null)
        {
            Debug.LogError("There is no Camera attached to this Component");
            return;
        }

        for (int i = 0; i < 3; i++)
        {
            mCamera.GetRig(i).GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = mSpeed;
            mCamera.GetRig(i).GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = mAmount;
        }
    }

    /// <summary>
    /// Function to stop shaking the Camera 
    /// </summary>
    void StopShake()
    {
        for (int i = 0; i < 3; i++)
        {
            //Reset Amplitude and Frequency Gain to 0
            mCamera.GetRig(i).GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = 0;
            mCamera.GetRig(i).GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 0;
        }
    }
}
