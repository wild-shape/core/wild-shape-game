﻿///-------------------------------------------------------------------------------------------------
// File: MoveAndIncrementState.cs
//
// Author: Dakshvir Singh Rehill
// Date: 22/7/2020
//
// Summary:	Moves the NPC to the end goal and once the NPC reaches, increments the dynamic index
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAndIncrementState : GuideBaseState
{
    MovingGuide mMoveInst;
    [SerializeField] string mIdleTriggerName = "Idle";
    [SerializeField] string mSpeedFloatName = "Speed";
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        if (mMoveInst == null)
        {
            mMoveInst = (MovingGuide)mGuideRef;
        }
        mMoveInst.mMoving = true;
        Transform aFinalTransform = mMoveInst.mDynamicConversationDialogs[mMoveInst.mDynamicIndex].mConditionMetPosition;
        mMoveInst.mMovingBehaviour.ChangePath(aFinalTransform.position);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(!mMoveInst.mMoving)
        {
            return;
        }
        //reached goal
        if (Mathf.Abs(mMoveInst.mMovingBehaviour.mWeight - mMoveInst.mMovingBehaviour.mReachedWeight) <= float.Epsilon)
        {
            mMoveInst.mDynamicIndex++;
            if(mMoveInst.mDynamicIndex == mMoveInst.mDynamicConversationDialogs.Count)
            {
                mMoveInst.mDynamicIndex--;
            }
            mMoveInst.mNPCDialogue = mMoveInst.mDynamicConversationDialogs[mMoveInst.mDynamicIndex].mDialogInfo;
            mMoveInst.mMoving = false;
            animator.SetTrigger(mIdleTriggerName);
        }
        else
        {
            mMoveInst.mGuideAnimator.SetFloat(mSpeedFloatName, mMoveInst.mSteeringAgent.mVelocity.magnitude);
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        mMoveInst.mGuideAnimator.SetFloat(mSpeedFloatName, 0.0f);
    }
}
