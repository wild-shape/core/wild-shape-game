﻿///-------------------------------------------------------------------------------------------------
// File: AudioMenu.cs
//
// Author: Dakshvir Singh Rehill
// Date: 24/6/2020
//
// Summary:	Menu class for Audio Settings Menu
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class AudioMenu : SubMenu
{
    /// <summary>
    /// UI Components of Audio Menu
    /// </summary>
    [System.Serializable]
    public struct AudioSettingsUI
    {
        [Tooltip("The Audio Group this Slider Belongs to")]
        public AudioGroup mAudioGroup;
        [Tooltip("The slider of this audio group")]
        public Slider mSlider;
        /// <summary>
        /// The event that needs to be called when the value of the slider is changed
        /// </summary>
        [HideInInspector] public UnityAction<float> mSliderValueChangedEvent;
    }
    [Tooltip("UI Assignments to all the Audio Group Components")]
    [SerializeField] List<AudioSettingsUI> mGroupVol;
    [Tooltip("Max Audio Val")]
    public float mMaxVal = 5f;
    [Tooltip("Min Audio Val")]
    public float mMinVal = -30.0f;
    public override void Start()
    {
        base.Start();
        if (mGroupVol == null)
        {
            return;
        }
        for (int aI = 0; aI < mGroupVol.Count; aI++) //set current mixer group volume on slider and add events
        {
            AudioSettingsUI aUI = mGroupVol[aI];
            float aAudioVal = AudioManager.Instance.GetMixerGroupVolume(aUI.mAudioGroup);
            if (aAudioVal <= -80.0f)
            {
                aUI.mSlider.value = 0.0f;
            }
            else
            {
                aUI.mSlider.value = (((aAudioVal - mMinVal) * (1 - 0)) / (mMaxVal - mMinVal));
            }
            int aIx = aI;
            aUI.mSliderValueChangedEvent = (float pVal) => SetGroupVolume(aUI.mAudioGroup, aIx);
            aUI.mSlider.onValueChanged.AddListener(aUI.mSliderValueChangedEvent);
            mGroupVol[aI] = aUI;
        }
    }

    void OnDestroy()
    {
        if (mGroupVol == null)
        {
            return;
        }
        for (int aI = 0; aI < mGroupVol.Count; aI++)
        {
            mGroupVol[aI].mSlider.onValueChanged.RemoveListener(mGroupVol[aI].mSliderValueChangedEvent);
        }
    }

    void SetGroupVolume(AudioGroup pGroup, int pIx)
    {
        float aVal = ((mGroupVol[pIx].mSlider.value) * (mMaxVal - mMinVal)) + mMinVal;
        aVal += SetAdditiveIndex(mGroupVol[pIx].mSlider.value);
        AudioManager.Instance.SetMixerGroupVolume(pGroup, aVal);
    }

    float SetAdditiveIndex(float pValue)
    {
        if(pValue < 0.01)
        {
            return -100;
        }
        return 0;
    }
}
