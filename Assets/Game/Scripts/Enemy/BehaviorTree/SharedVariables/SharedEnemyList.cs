///-------------------------------------------------------------------------------------------------
// File: SharedEnemy.cs
//
// Author: Dakshvir Singh Rehill
// Date: 11/6/2020
//
// Summary:	Allow shared variable for enemy
///-------------------------------------------------------------------------------------------------
using UnityEngine;
using BehaviorDesigner.Runtime;
using System.Collections.Generic;

[System.Serializable]
public class SharedEnemyList : SharedVariable<List<Enemy>>
{
	public override string ToString() { return mValue == null ? "null" : mValue.ToString(); }
	public static implicit operator SharedEnemyList(List<Enemy> value) { return new SharedEnemyList { mValue = value }; }
}