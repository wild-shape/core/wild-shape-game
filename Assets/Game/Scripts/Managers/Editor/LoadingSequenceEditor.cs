﻿///-------------------------------------------------------------------------------------------------
// File: LoadingSequenceEditor.cs
//
// Author: Dakshvir Singh Rehill
// Date: 29/6/2020
//
// Summary:	This Editor Window will allow Designers to Load Debug Scenes and define the loading sequence of
// scenes in the game. This script also has a shortcut to run the core scene directly by key press
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEditor.ShortcutManagement;
using UnityEditor.SceneManagement;
public class LoadingSequenceEditor : EditorWindow
{
    /// <summary>
    /// The reference to the window to allow for stopping and starting it again.
    /// </summary>
    static LoadingSequenceEditor mWindow;

    [Shortcut("Refresh Loading Sequence Editor", KeyCode.F11)]
    [MenuItem("Wild Shape/Loading Sequence Editor")]
    public static void OpenLoadingSequenceWindow()
    {
        if (mLoadSettings == null)
        {
            SetLoadSettings();
        }
        if (mWindow != null)
        {
            mWindow.Close();
            mWindow = null;
        }

        mWindow = GetWindow<LoadingSequenceEditor>();
        mWindow.minSize = new Vector2(500, 600);
        mWindow.titleContent = new GUIContent("Wild Shape Loading Sequence Editor", "Editor Application Is Used to Setup Loading Sequences for Game");
    }
    /// <summary>
    /// The static reference for the editor window settings
    /// </summary>
    static LoadSequenceSettings mLoadSettings;
    public static void SetLoadSettings() //set loading settings if it starts
    {
        if (mLoadSettings != null)
        {
            return;
        }
        if (!AssetDatabase.IsValidFolder("Assets/Game"))
        {
            AssetDatabase.CreateFolder("Assets", "Game");
        }
        if (!AssetDatabase.IsValidFolder("Assets/Game/LoadEditorSettings"))
        {
            AssetDatabase.CreateFolder("Assets/Game", "LoadEditorSettings");
        }
        mLoadSettings = AssetDatabase.LoadAssetAtPath<LoadSequenceSettings>("Assets/Game/LoadEditorSettings/LoadingEditorSettings.asset");
        if (mLoadSettings == null)
        {
            mLoadSettings = CreateInstance<LoadSequenceSettings>();
            mLoadSettings.mCurrentLoadingSequence = null;
            mLoadSettings.mGameLoadingSequence = null;
            mLoadSettings.mCoreScenePath = null;
            AssetDatabase.CreateAsset(mLoadSettings, "Assets/Game/LoadEditorSettings/LoadingEditorSettings.asset");
            EditorUtility.SetDirty(mLoadSettings);
        }
        EditorApplication.playModeStateChanged += PlayModeChanged;
    }

    static void PlayModeChanged(PlayModeStateChange pPlayModeState)
    {
        if (pPlayModeState == PlayModeStateChange.EnteredEditMode)
        {
            if (EditorSceneManager.GetActiveScene().path == mLoadSettings.mCoreScenePath)
            {
                GameManager.Instance.mGameLoadingSequence = mLoadSettings.mGameLoadingSequence;
                EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
                EditorSceneManager.SaveOpenScenes();
            }
        }
    }

    [Shortcut("Play Core Scene", KeyCode.Alpha5)]
    [MenuItem("Wild Shape/Run or Stop Core Scene")]
    public static void RunCoreScene()
    {
        if (mLoadSettings == null)
        {
            SetLoadSettings();
        }
        if (EditorApplication.isPlaying)
        {
            EditorApplication.ExitPlaymode();
            return;
        }
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        if (mLoadSettings.mCurrentLoadingSequence != null
            && !string.IsNullOrEmpty(mLoadSettings.mCoreScenePath))
        {
            EditorSceneManager.OpenScene(mLoadSettings.mCoreScenePath);
            GameManager.Instance.mGameLoadingSequence = mLoadSettings.mCurrentLoadingSequence;
        }
        EditorApplication.EnterPlaymode();
    }

    void OnGUI()
    {
        EditorGUILayout.BeginVertical();
        {
            EditorGUILayout.Space(30);
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.Space(30);
            SerializedObject aLoadSettingsObj = new SerializedObject(mLoadSettings);
            SerializedProperty aCoreSceneProp = aLoadSettingsObj.FindProperty("mCoreScenePath");
            EditorGUILayout.PropertyField(aCoreSceneProp,
                new GUIContent("Core Scene", "Select The Scenen You Would Like to load first"),
                true);
            aLoadSettingsObj.ApplyModifiedProperties();
            EditorGUILayout.Space(30);
            LoadingSequence aMainSequence = (LoadingSequence)EditorGUILayout.ObjectField(
                new GUIContent("Main Loading Sequence", "Choose the game sequence that will run in the main build"),
                mLoadSettings.mGameLoadingSequence,
                typeof(LoadingSequence), false);
            if (aMainSequence != mLoadSettings.mGameLoadingSequence)
            {
                mLoadSettings.mGameLoadingSequence = aMainSequence;
                EditorUtility.SetDirty(mLoadSettings);
            }
            EditorGUILayout.Space(30);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space(30);
            EditorGUILayout.BeginHorizontal();
            LoadingSequence aSequence = (LoadingSequence)EditorGUILayout.ObjectField(
                new GUIContent("Game Loading Sequence", "Choose the game sequence you would like to run and edit"),
                mLoadSettings.mCurrentLoadingSequence,
                typeof(LoadingSequence), false);
            if (aSequence != mLoadSettings.mCurrentLoadingSequence)
            {
                mLoadSettings.mCurrentLoadingSequence = aSequence;
                EditorUtility.SetDirty(mLoadSettings);
            }
            if (mLoadSettings.mCurrentLoadingSequence == null)
            {
                EditorUtility.SetDirty(mLoadSettings);
                EditorGUILayout.Space(30);
                if (GUILayout.Button(new GUIContent("Create Loading Sequence", "Creates a Brand New Asset in Loading Sequence Folder")))
                {
                    mLoadSettings.mCurrentLoadingSequence = CreateInstance<LoadingSequence>();
                    if (!AssetDatabase.IsValidFolder("Assets/Game/LoadingSequence"))
                    {
                        AssetDatabase.CreateFolder("Assets/Game", "LoadingSequence");
                    }
                    AssetDatabase.CreateAsset(mLoadSettings.mCurrentLoadingSequence,
                        AssetDatabase.GenerateUniqueAssetPath("Assets/Game/LoadingSequence/LoadingSequence.asset"));
                    EditorUtility.SetDirty(mLoadSettings.mCurrentLoadingSequence);
                }
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space(30);
            LoadingSequenceEditorGUI();
            if (GUILayout.Button(new GUIContent("Enter PlayMode", "Plays The Current Loading Sequence in PlayMode")))
            {
                RunCoreScene();
            }
        }
        EditorGUILayout.EndVertical();
    }

    void LoadingSequenceEditorGUI()
    {
        if (mLoadSettings.mCurrentLoadingSequence == null)
        {
            return;
        }
        EditorGUILayout.BeginHorizontal();
        bool aDebugMode = EditorGUILayout.Toggle(new GUIContent("Debug Mode", "Tick This If you don't want to go to menus and debug game scene"),
            mLoadSettings.mCurrentLoadingSequence.mDebugMode);
        if (aDebugMode != mLoadSettings.mCurrentLoadingSequence.mDebugMode)
        {
            mLoadSettings.mCurrentLoadingSequence.mDebugMode = aDebugMode;
            EditorUtility.SetDirty(mLoadSettings.mCurrentLoadingSequence);
        }
        SerializedObject aSceneList = new SerializedObject(mLoadSettings.mCurrentLoadingSequence);
        SerializedProperty aBGScene = aSceneList.FindProperty("mBG3DScene");
        EditorGUILayout.PropertyField(aBGScene,
            new GUIContent("Main Menu Background Scene", "This Scene will be loaded in the background of the main menu"),
            true);
        aSceneList.ApplyModifiedProperties();
        EditorGUILayout.Space(30);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space(30);
        SerializedProperty aSceneListProp = aSceneList.FindProperty("mGameSceneSequence");
        EditorGUILayout.PropertyField(aSceneListProp,
            new GUIContent("Sequential Game Scenes", "This array determines the index of scenes to be loaded. Index 0 will be loaded first"),
            true);
        aSceneList.ApplyModifiedProperties();
        EditorGUILayout.Space(30);
    }
}
