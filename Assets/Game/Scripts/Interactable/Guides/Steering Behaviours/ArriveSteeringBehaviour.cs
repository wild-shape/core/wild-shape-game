﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArriveSteeringBehaviour : SeekSteeringBehaviour
{
    [Header("Arrive Variables")]
    [Tooltip("The minimum distance between Agent and Target required for the Agent to start slowing down")]
    public float mSlowDownDistance = 5.0f;
    [Tooltip("The deceleration speed of Agent")]
    public float mDeceleration = 2.5f;
    [Tooltip("The minimum distance between Agent and Target required for the Agent to stop")]
    public float mStoppingDistance = 0.5f;
    [HideInInspector] public bool mArrived = false;
    public override Vector3 calculateForce()
    {
        Vector3 aToTarget = mTarget - transform.parent.position;
        float aDistanceToTarget = aToTarget.magnitude;
        if (aDistanceToTarget > mSlowDownDistance)
        {
            mArrived = false;
            return base.calculateForce();
        }
        else if (aDistanceToTarget > mStoppingDistance)
        {
            float aSpeed = aDistanceToTarget / mDeceleration;
            if (aSpeed > mSteeringAgent.mMaxSpeed)
            {
                aSpeed = mSteeringAgent.mMaxSpeed;
            }

            aSpeed = aSpeed / aDistanceToTarget;
            Vector3 aDesiredVelocity = aToTarget.normalized * aSpeed;
            mArrived = false;
            return aDesiredVelocity - mSteeringAgent.mVelocity;
        }
        mArrived = true;
        return -mSteeringAgent.mVelocity;
    }
}
