﻿///-------------------------------------------------------------------------------------------------
// File: Enemy.cs
//
// Author: Dakshvir Singh Rehill
// Date: 31/5/2020
//
// Summary:	Enemy Class that is IFreezable, IDeactivatable and Base for both enemies
///-------------------------------------------------------------------------------------------------
using BehaviorDesigner.Runtime;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour, IFreezable, IDeactivatable
{
    /// <summary>
    /// This Type decides the type of the enemy
    /// </summary>
    public enum Type
    {
        Melee,
        Ranged
    };

    /// <summary>
    /// The struct that defines each vision cone point that the enemy can detect from
    /// </summary>
    [System.Serializable]
    public struct VisionConePoint
    {
        [Tooltip("This offset is added to the origin position to change the starting position of the vision cone")]
        public Vector3 mOffset;
        [Tooltip("This rotation offset, rotates the starting position around each axis by the angular values given in it")]
        public Vector3 mRotationOffset;
        [Tooltip("This angle determines the maximum angle between the starting position of the cone and the player")]
        [Range(0, 180)]
        public float mViewingAngle;
        [Tooltip("This distance determines how far the enemy can see from the vision cone's starting position")]
        public float mMaxViewingDistance;
        [Tooltip("Percentage value of how much this vision cone will effect the detection of the player")]
        [Range(0, 1)]
        public float mDetectionStrength;
        /// <summary>
        /// Reference to the light component of vision cone light
        /// </summary>
        [HideInInspector] public Light mConeLight;
    }

    public bool mFrozen { get; set; }
    public bool mDeactivate { get; set; }

    /// <summary>
    /// Boolean value which is true when the enemy is in attack mode and if the player
    /// is in the vicinity of the enemy, the enemy will perform the attack
    /// </summary>
    public bool mAttack { get; set; }
    [Header("Vision Cone Settings")]
    [Tooltip("The original start point of the vision cone as well as the raycast")]
    public Transform mOrigin;
    [Tooltip("These vision cones will be used to perform varied levels of detections for the enemy")]
    public List<VisionConePoint> mVisionCones;
    [Tooltip("The Insta Detect Circle Radius")]
    public float mInstaDetectRadius = 2.5f;
    [Tooltip("The Maximum Angle from the Forward Direction of Enemy where insta detect radius will catch you")]
    [Range(0, 180)] public float mInstaDetectAngle = 180.0f;
    [Tooltip("Maximum Insta Detect Y Distance so that the enemy doesn't detect the player if it is in a different level")]
    public float mYDistanceCheck = 2.0f;
    [Header("Rate of Detection and Threshold of Detection")]
    [Tooltip("A percentage value that determines how much the angle difference from 0 will effect the detection of the player")]
    [Range(0, 1)]
    public float mAngleDetectionStrength = 0.25f;
    [Tooltip("A percentage value that determines how much the closeness of the player will effect the detection of the player")]
    [Range(0, 1)]
    public float mDistanceDetectionStrength = 0.5f;
    [Tooltip("The time it takes for the enemy to reset the detected meter after it can't see the player")]
    public float mReversalTime = 1.0f;
    [Tooltip("The delay between the enemy starting the detection reset and when it can't see the player")]
    public float mReversalDelay = 1.0f;
    [Header("Enemy Movement Settings")]
    [Tooltip("The rotation speed of ranged enemy and the traversal speed of melee enemy")]
    public float mEnemySpeed;
    [Tooltip("Can't See Alert Speed of Enemy")]
    public float mEnemyAlertSpeed = 5.0f;
    [Header("Functionality Assignments")]
    [Tooltip("All the deactivating controllers that can deactivate the enemy if they get deactivated")]
    [SerializeField] List<Deactivatable> mDeactivateControllers;
    [Tooltip("The Behavior Tree that governs the AI of the enemy")]
    public BehaviorTree mTree;
    [Tooltip("The type of the enemy")]
    public Type mEtype = Type.Melee;
    [Tooltip("Fall and destroy distance")]
    public float mFallDistance = 15.0f;
    [Header("Enemy Rendering Settings")]
    [Tooltip("The GameObject with Enemy On Effect of this Enemy")]
    public GameObject mEnemyOnEffect;
    [Tooltip("The Particle Systems that need to be paused when the enemy is frozen")]
    public List<ParticleSystem> mEnemyOnToFreeze;
    [Tooltip("Vision Cone Spot Light Prefab")]
    [SerializeField] GameObject mSpotLight;
    [Tooltip("Enemy Inner Light Gameobject")]
    [SerializeField] GameObject mInnerLightObj;
    [Tooltip("Max intensity of the vision cone spot light")]
    [SerializeField] float mMaxIntensity = 10.0f;
    [Tooltip("The color gradient for different enemy detection percentages")]
    [SerializeField] Gradient mEnemyGradient;
    [Tooltip("The color of the enemy renderers when the enemy gets frozen")]
    [SerializeField] Color mFrozenColor;
    [Tooltip("The color of the enemy renderers when the enemy gets deactivated")]
    [SerializeField] Color mDeactivateColor;
    [Tooltip("The Position where the deactivate effect needs to be spawned")]
    [SerializeField] Transform mDeactivatePos;
    [Tooltip("The Pool Classifier for the Deactivate Fx")]
    [SerializeField] PoolClassifier mDeactivateFx;
    [Header("Debug Variables")]
    [Tooltip("Set to true for On Gizmo Draw in the scene view")]
    [SerializeField] bool mDebugDraw = true;
    [Header("Sound Variables")]
    [Tooltip("Audio Assisstant component for 3D SFX")]
    [SerializeField] public AudioAssistant mAudioAssistantSFX;
    [Tooltip("Audio Assisstant component for 3D Ambience")]
    [SerializeField] protected AudioAssistant mAudioAssistantAmbience;
    [Tooltip("Sound Effect for Enemy Alerted")]
    [SerializeField] protected string mEnemyAlertedSFX = "EnemyAlerted";
    [Tooltip("Sound Effect for Melee Hover")]
    [SerializeField] protected string mMeleeHoverSFX = "MeleeHover";
    [Tooltip("Sound Effect for Ranged Movement")]
    [SerializeField] public string mRangedMovementSFX = "RangedMovement";
    [Tooltip("Sound Effect for Ranged Shoot")]
    [SerializeField] protected string mRangedShootSFX = "RangedShoot";
    [Tooltip("Sound Effect for Player Stun")]
    [SerializeField] protected string mPlayerStunSFX = "RangedStunPlayer";
    [Tooltip("Sound Effect for Enemy Disabled")]
    [SerializeField] protected string mEnemyDisabledSFX = "EnemyDisabled";
    bool mAlertSoundPlayed;
    /// <summary>
    /// The Threshold for Enemy detection used for calculating the percentage of detection for color changes
    /// </summary>
    [HideInInspector] public float mDetectionVal;
    /// <summary>
    /// The Threshold for Undetection after enemy detects the player
    /// used for calculating the percentage of detection for color changes
    /// </summary>
    [HideInInspector] public float mUndetectionVal;
    /// <summary>
    /// The shared float that is the calculated detection meter value
    /// taken from the behavior tree action
    /// </summary>
    [HideInInspector] public SharedFloat mDetectionMeterVal;
    /// <summary>
    /// Used to compare Y position of the enemy before killing it
    /// </summary>
    float mOriginalYPos;
    /// <summary>
    /// This list contains all the points that the enemy needs to inspect after it gets alerted
    /// about the player's wherabouts
    /// </summary>
    [HideInInspector] public readonly List<Vector3> mInspectPoints = new List<Vector3>();
    /// <summary>
    /// The Light component of the Inner Light Gameobject
    /// </summary>
    Light mInnerLight;
    /// booleans to check if the player has already been deactivated/frozen and the color has been changed
    /// so that the color changing isn't repetitive looping through the material/renderer arrays
    bool mDeactivateColorChanged = false;
    bool mFrozenColorChanged = false;
    protected virtual void Start()
    {
        mOriginalYPos = transform.position.y;
        //As IDeactivable is an interface, it can't be assigned in the inspector
        //Which is why assignment is the other way around and done in the start function
        if (mDeactivateControllers != null)
        {
            foreach (Deactivatable aDeactivatable in mDeactivateControllers)
            {
                aDeactivatable.mDeactivatables.Add(this);
            }
        }
        //Get variable values from behavior tree and get reference to detection meter shared float
        mDetectionVal = ((SharedFloat)mTree.GetVariable("mDetectedThreshold")).Value;
        mUndetectionVal = ((SharedFloat)mTree.GetVariable("mUndetectedThreshold")).Value;
        mDetectionMeterVal = (SharedFloat)mTree.GetVariable("mDetectionMeter");
        mDetectionMeterVal.Value = 0.0f;
        for (int aI = 0; aI < mVisionCones.Count; aI++)
        {
            GameObject aVisionConeLight = Instantiate(mSpotLight, mOrigin, false);
            VisionConePoint aVisionCone = mVisionCones[aI];
            aVisionCone.mConeLight = aVisionConeLight.GetComponent<Light>();
            mVisionCones[aI] = aVisionCone;
            aVisionCone.mConeLight.intensity = mMaxIntensity * aVisionCone.mDetectionStrength;
            aVisionCone.mConeLight.color = mEnemyGradient.Evaluate(0);
            aVisionCone.mConeLight.spotAngle = aVisionCone.mViewingAngle;
            aVisionCone.mConeLight.range = aVisionCone.mMaxViewingDistance;
        }
        if(mInnerLightObj != null)
        {
            mInnerLight = mInnerLightObj.GetComponent<Light>();
        }
    }

    protected virtual void Update()
    {
        if (mOriginalYPos - transform.position.y >= mFallDistance)//optimization for falling enemy
        {
            gameObject.SetActive(false);
        }
        if (mDeactivate)
        {
            if (!mDeactivateColorChanged)
            {
                mDeactivateColorChanged = true;
                mEnemyOnEffect.SetActive(false);
                GameObject aDeactivateFx = ObjectPoolingManager.Instance.GetPooledObject(mDeactivateFx.mPoolName);
                aDeactivateFx.transform.parent = mDeactivatePos;
                aDeactivateFx.transform.position = mDeactivatePos.position;
                aDeactivateFx.SetActive(true);
                //change color to deactivate color
                ChangeEnemyRenderColor(mDeactivateColor, new Color(0, 0, 0, 0));

                if (!string.IsNullOrEmpty(mEnemyDisabledSFX) && mAudioAssistantSFX != null)
                {
                    mAudioAssistantSFX.PlaySound(mEnemyDisabledSFX, true);
                }
            }

            return;
        }
        if (mFrozen)
        {
            if (!mFrozenColorChanged)
            {
                mFrozenColorChanged = true;
                foreach (ParticleSystem aEnemyOnEf in mEnemyOnToFreeze)
                {
                    aEnemyOnEf.Pause();
                }
                //change color to frozen color
                ChangeEnemyRenderColor(mFrozenColor, new Color(0, 0, 0, 0));
            }
            return;
        }
        mDeactivateColorChanged = mFrozenColorChanged = false;
        if (mEnemyOnToFreeze.Count > 0)
        {
            if (mEnemyOnToFreeze[0].isPaused)
            {
                foreach (ParticleSystem aEnemyOnEf in mEnemyOnToFreeze)
                {
                    aEnemyOnEf.Play();
                }
            }
        }

        if ((mDetectionMeterVal.Value > mDetectionVal) && !mAlertSoundPlayed)
        {
            if (!string.IsNullOrEmpty(mEnemyAlertedSFX) && mAudioAssistantSFX != null)
            {
                mAudioAssistantSFX.PlaySound(mEnemyAlertedSFX);
                mAlertSoundPlayed = true;
            }
        }

        if ((mDetectionMeterVal.Value < mUndetectionVal) && mAlertSoundPlayed)
        {
            mAlertSoundPlayed = false;
        }

        //change the color to the detection color from gradient
        float aCurrentPercent = mDetectionMeterVal.Value / (mDetectionVal - mUndetectionVal);
        Color aEnemyColor = mEnemyGradient.Evaluate(aCurrentPercent);
        ChangeEnemyRenderColor(aEnemyColor, aEnemyColor);
    }

    /// <summary>
    /// Function to change the render color of all enemy renders when needed
    /// </summary>
    /// <param name="pColor"> The color that needs to be set</param>
    protected virtual void ChangeEnemyRenderColor(Color pColor, Color pConeColor)
    {
        if(mInnerLight != null)
        {
            mInnerLight.color = pColor;
        }
        for (int aI = 0; aI < mVisionCones.Count; aI++)
        {
            mVisionCones[aI].mConeLight.color = pConeColor;
        }
    }

    /// <summary>
    /// Function that draws vision cone lines in the scene view in debug mode
    /// </summary>
    void OnDrawGizmos()
    {
        if (!mDebugDraw)
        {
            return;
        }
#if UNITY_EDITOR
        UnityEditor.Handles.color = Color.blue;
        UnityEditor.Handles.DrawSolidArc(
            transform.position,
            transform.up,
            Quaternion.AngleAxis(-mInstaDetectAngle, mOrigin.up) * transform.forward,
            mInstaDetectAngle * 2.0f, mInstaDetectRadius);
        if (mVisionCones != null && mOrigin != null)
        {
            foreach (VisionConePoint aCone in mVisionCones)
            {
                Vector3 aRelativeVisionConePoint = mOrigin.position + aCone.mOffset;
                Vector3 aOriginForward = mOrigin.forward;
                if (aCone.mRotationOffset.sqrMagnitude > 0.0f)
                {
                    aOriginForward = Quaternion.Euler(aCone.mRotationOffset) * aOriginForward;
                }
                Color aConeColor = Color.white;
                aConeColor.a = aCone.mDetectionStrength;
                UnityEditor.Handles.color = aConeColor;
                UnityEditor.Handles.DrawSolidArc(aRelativeVisionConePoint, mOrigin.up, Quaternion.AngleAxis(-aCone.mViewingAngle, mOrigin.up) * aOriginForward,
                    aCone.mViewingAngle * 2.0f, aCone.mMaxViewingDistance);
                UnityEditor.Handles.DrawSolidArc(aRelativeVisionConePoint, mOrigin.right, Quaternion.AngleAxis(-aCone.mViewingAngle, mOrigin.right) * aOriginForward,
                    aCone.mViewingAngle * 2.0f, aCone.mMaxViewingDistance);
            }
        }
#endif
    }
}