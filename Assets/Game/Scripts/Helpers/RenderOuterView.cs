﻿///-------------------------------------------------------------------------------------------------
// File: RenderOuterView.cs
//
// Author: Dakshvir Singh Rehill
// Date: 31/7/2020
//
// Summary:	Script is used to move the render component of the render texture to allow for dynamic viewing of forest
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderOuterView : MonoBehaviour
{
    [SerializeField] float mMaxFOVDist = 25.0f;
    [SerializeField] float mMinFOVDist = 5.0f;
    [SerializeField] float mMaxFOV = 90;
    [SerializeField] float mMinFOV = 30;
    [SerializeField] Camera mOuterViewCam;
    [SerializeField] Transform mDistPoint;

    [SerializeField] bool mDebugDraw = false;
    void Update()
    {
        int aCurChar = GameManager.Instance.mGameData.mPlayerData.mCurChar;
        Vector3 aPP = GameManager.Instance.mSceneHandler.mCameraController.mShapeShiftInst.mPlayerCharacters[aCurChar].mCameraLookAt.position;
        Vector3 aDistVec = mDistPoint.position - aPP;
        aDistVec.y = 0.0f;
        mOuterViewCam.transform.rotation = Quaternion.LookRotation(aDistVec);
        float aCurFOV = mOuterViewCam.fieldOfView;
        float aPerc = Mathf.Clamp((aDistVec.magnitude - mMinFOVDist) / (mMaxFOVDist - mMinFOVDist), 0, 1);
        float aCalculatedFOV = (1 - aPerc)
            * (mMaxFOV - mMinFOV) + mMinFOV;
        if (Mathf.Abs(aCalculatedFOV - aCurFOV) > float.Epsilon)
        {
            mOuterViewCam.fieldOfView = aCalculatedFOV;
        }
    }

    void OnDrawGizmos()
    {
        if (!mDebugDraw)
        {
            return;
        }
        DebugExtension.DrawPoint(mDistPoint.position + mDistPoint.forward * mMinFOVDist, Color.red);
        DebugExtension.DrawPoint(mDistPoint.position + mDistPoint.forward * mMaxFOVDist, Color.blue);
    }

}
