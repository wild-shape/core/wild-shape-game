﻿///-------------------------------------------------------------------------------------------------
// File: InteractableAffordanceEditor.cs
//
// Author: Aditya Dinesh
// Date: 28/7/2020
//
// Summary:	Custom Inspector display for Interactable Affordance
///-------------------------------------------------------------------------------------------------

using UnityEditor;

[CustomEditor(typeof(InteractableAffordance))]
public class InteractableAffordanceEditor : Editor
{
    SerializedProperty mInteractableBy;
    SerializedProperty mEmissiveGradient;
    SerializedProperty mEmissiveRenderers;
    SerializedProperty mMaxIntensityDistance;
    SerializedProperty mMinIntensityDistance;

    InteractableAffordance mInteractableAffordance = null;

    private void OnEnable()
    {
        mInteractableAffordance = target as InteractableAffordance;

        mInteractableBy = serializedObject.FindProperty("mInteractableBy");
        mEmissiveGradient = serializedObject.FindProperty("mEmissiveGradient");
        mEmissiveRenderers = serializedObject.FindProperty("mRenderObject");
        mMaxIntensityDistance = serializedObject.FindProperty("mMaxIntensityDistance");
        mMinIntensityDistance = serializedObject.FindProperty("mMinIntensityDistance");
    }

    public override void OnInspectorGUI()
    {
        if (mInteractableAffordance != null)
        {
            EditorGUILayout.PropertyField(mInteractableBy);
            EditorGUILayout.PropertyField(mEmissiveGradient);
            EditorGUILayout.PropertyField(mEmissiveRenderers);
            EditorGUILayout.PropertyField(mMaxIntensityDistance);
            EditorGUILayout.PropertyField(mMinIntensityDistance);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
