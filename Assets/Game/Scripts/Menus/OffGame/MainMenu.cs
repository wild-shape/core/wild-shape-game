﻿///-------------------------------------------------------------------------------------------------
// File: MainMenu.cs
//
// Author: Dakshvir Singh Rehill
// Date: 22/6/2020
//
// Summary:	Menu Class for the Main Menu screen
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : OverlayMenu
{
    [Tooltip("The Button for Continuing the Game")]
    [SerializeField] Button mContinueBtn;
    [Tooltip("The Button for Starting a new Game")]
    [SerializeField] Button mNewGameBtn;
    [Tooltip("The Button for Controls Menu")]
    [SerializeField] Button mControlsBtn;
    [Tooltip("The Button for Audio Settings Menu")]
    [SerializeField] Button mAudioSettingsBtn;
    [Tooltip("The Button for Video Settings Menu")]
    [SerializeField] Button mVideoSettingsBtn;
    [Tooltip("The Button for Exiting the game")]
    [SerializeField] Button mExitBtn;
    [Tooltip("The Strings that need to be displayed when player tries to start new game when save game exists")]
    [SerializeField] DialogBoxStrings mDialogBoxStrings
        = new DialogBoxStrings()
        {
            mHeadingText = "Save Game Exists",
            mBodyText = "Warning! Starting a new game will overwrite your game progress!",
            mButtonOneText = "Load Saved Game",
            mButtonTwoText = "Start New Game"
        };
    [Tooltip("The Main Menu Baground Music")]
    [SerializeField] string mMainMenuMusic = "InTheRainforest";
    /// <summary>
    /// The dialog menu of the game
    /// </summary>
    OffGameDialogMenu mDialogBoxMenu;
    /// <summary>
    /// Copy of BG Scene
    /// </summary>
    SceneReference mBGScene;
    public override void Awake()
    {
        base.Awake();
        mInitSelectable = mNewGameBtn;
    }
    public override void Start()
    {
        base.Start();
        mDialogBoxMenu = MenuManager.Instance.GetMenu<OffGameDialogMenu>(GameManager.Instance.mOffGameDialogMenu);
        if(GameManager.Instance.mGameLoadingSequence.mDebugMode)
        {
            mBGScene = new SceneReference();
            StartGame();
        }
        else
        {
            mBGScene = GameManager.Instance.mGameLoadingSequence.mBG3DScene;
        }
    }

    void OnEnable()
    {
        mContinueBtn.interactable = 
        mNewGameBtn.interactable = 
        mAudioSettingsBtn.interactable = 
        mControlsBtn.interactable =
        mExitBtn.interactable = 
        mVideoSettingsBtn.interactable = true;

        if(!string.IsNullOrEmpty(mMainMenuMusic) && !AudioManager.Instance.IsSoundPlaying(mMainMenuMusic))
        {
            AudioManager.Instance.PlaySound(mMainMenuMusic, true);
        }
    }

    public void StartNewGame()
    {
        mNewGameBtn.interactable = false;

        if(AudioManager.Instance.IsSoundPlaying(MenuManager.Instance.mMenuSelectMovementSFX))
        {
            AudioManager.Instance.StopSound(MenuManager.Instance.mMenuSelectMovementSFX);
        }
        AudioManager.Instance.PlaySound(MenuManager.Instance.mMenuSelectionClickSFX);

        if (SaveGameManager.Instance.mLoadExists)
        {
            mDialogBoxMenu.ShowDialogBox(mDialogBoxStrings, StartGame, OverwriteGame);
        }
        else
        {
            StartGame();
        }
    }

    public void OverwriteGame()
    {
        GameManager.Instance.mGameData.ResetGameData();
        SaveGameManager.Instance.mLoadExists = false;
        StartGame();
    }

    public void StartGame()
    {
        if (AudioManager.Instance.IsSoundPlaying(MenuManager.Instance.mMenuSelectMovementSFX))
        {
            AudioManager.Instance.StopSound(MenuManager.Instance.mMenuSelectMovementSFX);
        }
        AudioManager.Instance.PlaySound(MenuManager.Instance.mMenuSelectionClickSFX);
        GameManager.Instance.StartGame();
    }
    
    public void ExitGame()
    {
        mExitBtn.interactable = false;
        if (AudioManager.Instance.IsSoundPlaying(MenuManager.Instance.mMenuSelectMovementSFX))
        {
            AudioManager.Instance.StopSound(MenuManager.Instance.mMenuSelectMovementSFX);
        }
        AudioManager.Instance.PlaySound(MenuManager.Instance.mMenuBackSelectionSFX);
        SaveGameManager.Instance.SavePlayerData();
        Application.Quit();
    }


    public override void ShowMenu(string pOptions)
    {
        base.ShowMenu(pOptions);
    }

    public override void HideMenu(string pOptions)
    {
        if (!string.IsNullOrEmpty(mBGScene))
        {
            MultiSceneManager.Instance.UnloadScene(mBGScene);
        }
        base.HideMenu(pOptions);
    }

    private void OnDisable()
    {
        if(!AudioManager.IsValidSingleton())
        {
            return;
        }
        if (!string.IsNullOrEmpty(mMainMenuMusic) && AudioManager.Instance.IsSoundPlaying(mMainMenuMusic))
        {
            AudioManager.Instance.StopSound(mMainMenuMusic);
        }
    }

}
