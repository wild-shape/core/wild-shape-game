﻿///-------------------------------------------------------------------------------------------------
// File: SimpleLookAt.cs
//
// Author: Dakshvir Singh Rehill
// Date: 17/6/2020
//
// Summary:	Allows the Melee enemy to look at player when in attack range
///-------------------------------------------------------------------------------------------------
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;
using UnityEngine.AI;

[TaskCategory("Enemy/Melee")]
[TaskDescription("Allows the melee to look at the player when in attack range")]
public class SimpleLookAt : EnemyActionBase
{
    /// <summary>
    /// Agent of the Melee Enemy
    /// </summary>
    NavMeshAgent mAgent;
    public override void OnStart()
    {
        base.OnStart();
        if (mAgent == null)
        {
            mAgent = ((MeleeEnemy)mEnemyInstance).mAgent;
        }
    }

    public override TaskStatus OnUpdate()
    {
        int aChar = GameManager.Instance.mGameData.mPlayerData.mCurChar;
        //set the rotation of the agent so that it faces the player
        mAgent.transform.rotation = Quaternion.Slerp(mAgent.transform.rotation,
            Quaternion.LookRotation(GameManager.Instance.mSceneHandler.mCameraController.
            mShapeShiftInst.mPlayerCharacters[aChar].mCameraLookAt.position -
            mAgent.transform.position), 2.0f * Time.deltaTime);
        return TaskStatus.Success;
    }
}
