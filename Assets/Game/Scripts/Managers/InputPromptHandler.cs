﻿///-------------------------------------------------------------------------------------------------
// File: InputPromptHandler.cs
//
// Author: Dakshvir Singh Rehill
// Date: 20/7/2020
//
// Summary:	Singleton that holds reference to all the images and fallback text for HUD Prompts for Input
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XInput;

public class InputPromptHandler : Singleton<InputPromptHandler>
{
    [SerializeField] string mInputPromptsPath = "InputPrompts";
    [HideInInspector] public InputPromptContainer mInputPromptResource;
    public readonly Dictionary<ControlSchemeType, Dictionary<string, PromptContainer>> mUIPrompts = new Dictionary<ControlSchemeType, Dictionary<string, PromptContainer>>();
    [HideInInspector] public ControlSchemeType mActiveControlSchemeType = ControlSchemeType.KeyboardMouse;
    public Sprite[] mMBtnImg;
    public Sprite mKeyImg;
    public Sprite mLongKeyImg;
    public Sprite mInteractImg;

    void Awake()
    {
        mUIPrompts.Add(ControlSchemeType.KeyboardMouse, new Dictionary<string, PromptContainer>());
        mUIPrompts.Add(ControlSchemeType.PS, new Dictionary<string, PromptContainer>());
        mUIPrompts.Add(ControlSchemeType.XBox, new Dictionary<string, PromptContainer>());
        TextAsset aJSONFile = Resources.Load<TextAsset>(mInputPromptsPath);
        if (aJSONFile == null)
        {
            Debug.LogError("Couldn't Find Prompts in Resources");
            return;
        }
        mInputPromptResource = JsonUtility.FromJson<InputPromptContainer>(aJSONFile.text);
    }

    void Start()
    {
        foreach (InputPromptSettings aContainer in mInputPromptResource.mLockedControls)
        {
            AddToDictionary(aContainer);
        }
        foreach (InputPromptSettings aContainer in mInputPromptResource.mUnlockedControls)
        {
            AddToDictionary(aContainer);
        }
        ChangeControlScheme();
        InputSystem.onDeviceChange += OnDeviceChange;
    }

    void AddToDictionary(InputPromptSettings pContainer)
    {
        if (string.IsNullOrEmpty(pContainer.mInputBinding))
        {
            return;
        }
        if (!mUIPrompts[pContainer.mSchemeType].ContainsKey(pContainer.mInputBinding))
        {
            PromptContainer aSettingsContainer = new PromptContainer()
            {
                mPromptSettings = pContainer,
                mPromptImage = LoadSpriteAsResource(pContainer.mSchemeType.ToString() + "/" + pContainer.mInputBinding)
            };
            mUIPrompts[pContainer.mSchemeType].Add(pContainer.mInputBinding, aSettingsContainer);
        }

    }

    void ChangeControlScheme()
    {
        mActiveControlSchemeType = ControlSchemeType.KeyboardMouse;
        foreach (Gamepad aGPad in Gamepad.all)
        {
            if (aGPad is XInputController)
            {
                mActiveControlSchemeType = ControlSchemeType.XBox;
                break;
            }
        }
        if (mActiveControlSchemeType == ControlSchemeType.KeyboardMouse && Gamepad.all.Count > 0)
        {
            mActiveControlSchemeType = ControlSchemeType.PS;
        }
    }

    private void OnDeviceChange(InputDevice pInputDevice, InputDeviceChange pDeviceChange)
    {
        ChangeControlScheme();
    }

    Sprite LoadSpriteAsResource(string pPath)
    {
        try
        {
            Sprite aPromptImage = Resources.Load<Sprite>(pPath);
            return aPromptImage;
        }
        catch (System.Exception aE)
        {
            Debug.LogWarningFormat("Image of {0} not set,{1}\n{2}", pPath, aE.Message, aE.StackTrace);
            return null;
        }
    }
}
