﻿///-------------------------------------------------------------------------------------------------
// File: MainCameraController.cs
//
// Author: Dakshvir Singh Rehill
// Date: 29/7/2020
//
// Summary:	Allows other scripts to setup camera flags depending on the type of the scene
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraController : Singleton<MainCameraController>
{
    [SerializeField] CameraClearFlags mDefaultClearFlag;
    [SerializeField] CameraClearFlags m3DClearFlag;
    Camera mMainCamera;
    [HideInInspector] public readonly List<string> mGame3DScenes = new List<string>();
    void Awake()
    {
        mMainCamera = Camera.main;
    }
    void Start()
    {
        MultiSceneManager.Instance.mOnSceneLoad.AddListener(SetFlagAndScene);
        MultiSceneManager.Instance.mOnSceneUnload.AddListener(SetFlagAndScene);
        mMainCamera.clearFlags = mDefaultClearFlag;
    }
    void OnDestroy()
    {
        if (MultiSceneManager.IsValidSingleton())
        {
            MultiSceneManager.Instance.mOnSceneLoad.RemoveListener(SetFlagAndScene);
            MultiSceneManager.Instance.mOnSceneUnload.RemoveListener(SetFlagAndScene);
        }
    }
    void SetFlagAndScene(List<string> pScenes)
    {
        string a3DScene = null;
        foreach (string aScenePath in mGame3DScenes)
        {
            if (MultiSceneManager.Instance.mCurrentLoadedScenes.Contains(aScenePath))
            {
                a3DScene = aScenePath;
                break;
            }
        }
        if (!string.IsNullOrEmpty(a3DScene))//3d scene active
        {
            mMainCamera.clearFlags = m3DClearFlag;
            MultiSceneManager.Instance.SetActiveScene(a3DScene);
        }
        else//no 3d scene active
        {
            mMainCamera.clearFlags = mDefaultClearFlag;
            MultiSceneManager.Instance.SetActiveScene(GameManager.Instance.mCoreScene);
        }
    }
}
