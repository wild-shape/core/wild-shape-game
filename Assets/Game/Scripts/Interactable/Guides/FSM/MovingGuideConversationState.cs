﻿///-------------------------------------------------------------------------------------------------
// File: MovingGuideConversationState.cs
//
// Author: Dakshvir Singh Rehill
// Date: 23/7/2020
//
// Summary:	This inherits GuideConversationState and allows for dynamic dialog changes
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingGuideConversationState : GuideConversationState
{
    int mDialogIndex = -1;
    MovingGuide mMovingInst;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (mMovingInst == null)
        {
            mDialogIndex = 0;
            base.OnStateEnter(animator, stateInfo, layerIndex);
            mMovingInst = (MovingGuide)mGuideRef;
            return;
        }
        else if (mDialogIndex != mMovingInst.mDynamicIndex)
        {
            mDialogIndex = mMovingInst.mDynamicIndex;
            mCurrentConversationIx = -1;
        }
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }
}
