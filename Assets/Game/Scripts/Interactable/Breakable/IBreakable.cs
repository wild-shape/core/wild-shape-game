﻿///-------------------------------------------------------------------------------------------------
// File: IBreakable.cs
//
// Author: Dakshvir Singh Rehill
// Date: 8/6/2020
//
// Summary:	Objects that can break when Bear or Pushable Object is detected
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IBreakable : MonoBehaviour
{
    [Tooltip("A List of all objects that will fall or break according to pressure")]
    [SerializeField] protected GameObject[] mBreakableObjects;
    [Tooltip("The total time it will take after which the objects will fall or break")]
    [SerializeField] float mBreakableTime = 0.5f;
    [Tooltip("The total time the objects fall for before getting deactivated")]
    [SerializeField] float mFallTime = 1.0f;
    [Tooltip("The total distance these objects will fall to")]
    [SerializeField] float mFallDistance = 4.0f;
    [Tooltip("The XZ Rotation Angle That each falling object will rotate to")]
    [SerializeField] [Range(1, 180)] float mMaxXZRotation = 25.0f;
    /// <summary>
    /// The object that is currently applying pressure to the object
    /// </summary>
    CanPress mPressureObject = null;
    /// <summary>
    /// The current time since pressure object was placed
    /// </summary>
    protected float mCurrentBreakTime = 0.0f;

    //int mInteractableID;
    /// <summary>
    /// Boolean that is true if object is broken
    /// </summary>
    bool mBroken = false;
    protected virtual void OnDestroy()
    {
        if (LeanTween.isTweening(gameObject))
        {
            LeanTween.cancel(gameObject);
        }
    }

    protected virtual void Update()
    {
        if (mBroken)
        {
            return;
        }
        if (mPressureObject != null)
        {
            if (!mPressureObject.gameObject.activeInHierarchy)
            {
                BreakingStop();
                return;
            }
            mCurrentBreakTime -= Time.deltaTime;
            if (mCurrentBreakTime <= 0.0f)
            {
                BreakObject();
            }
            else
            {
                BreakingUpdate();
            }
        }
    }
    /// <summary>
    /// Called when object needs to start the breaking animation and fall down
    /// </summary>
    protected virtual void BreakObject()
    {
        mBroken = true;
        Pushable aPushable = mPressureObject.GetComponentInParent<Pushable>();
        if (aPushable != null)
        {
            aPushable.BreakPushable();
        }
        foreach (GameObject aBreakable in mBreakableObjects)
        {
            float aRotAngle = Random.Range(-mMaxXZRotation, mMaxXZRotation);
            if (Random.value > 0.5f)
            {
                LeanTween.rotateAroundLocal(aBreakable, Vector3.right, aRotAngle, mFallTime);
            }
            else
            {
                LeanTween.rotateAroundLocal(aBreakable, Vector3.forward, aRotAngle, mFallTime);
            }
            LeanTween.moveY(aBreakable, aBreakable.transform.position.y - mFallDistance, mFallTime).setOnComplete(() => aBreakable.SetActive(false));
        }
        LeanTween.value(gameObject, 0, 1, mFallTime).setOnComplete(EndDrop);
    }
    /// <summary>
    /// Deactivate object once drop is complete
    /// </summary>
    protected virtual void EndDrop()
    {
        gameObject.SetActive(false);
    }
    /// <summary>
    /// Called when the object isn't broken yet
    /// </summary>
    protected virtual void BreakingUpdate()
    {

    }
    /// <summary>
    /// Called when the object isn't broken but breaking needs to stop because there is no pressure object
    /// </summary>
    protected virtual void BreakingStop()
    {
        mPressureObject = null;
        mCurrentBreakTime = 0.0f;
    }

    /// <summary>
    /// Called when the pressure is introduced for the first time
    /// </summary>
    protected virtual void BreakingStart()
    {

    }

    protected virtual void OnTriggerEnter(Collider pCollider)
    {
        if (mPressureObject != null)
        {
            return;
        }
        CanPress aCanPress = pCollider.GetComponent<CanPress>();
        if (aCanPress == null)
        {
            return;
        }
        if (!aCanPress.mButtonOnly)
        {
            mCurrentBreakTime = mBreakableTime;
            mPressureObject = aCanPress;
            BreakingStart();
        }
    }

    protected virtual void OnTriggerExit(Collider pCollider)
    {
        if (mPressureObject == null)
        {
            return;
        }
        if (mPressureObject.gameObject.GetInstanceID() != pCollider.gameObject.GetInstanceID())
        {
            return;
        }
        BreakingStop();
    }
}
