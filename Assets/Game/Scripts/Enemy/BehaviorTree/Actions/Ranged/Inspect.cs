﻿///-------------------------------------------------------------------------------------------------
// File: Inspect.cs
//
// Author: Aditya Dinesh, Dakshvir Singh Rehill
// Date: 28/5/2020
//
// Summary:	Check for Player in the Area by inspecting over a list of Inspect Points/ Waypoints
///-------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

[TaskCategory("Enemy/Ranged")]
[TaskDescription("Check for Player in the Area by inspecting over a list of Inspect Points/ Waypoints")]
public class Inspect : EnemyActionBase
{
    /// <summary>
    /// The state of rotation of the melee enemy allowing for two types of rotation
    /// </summary>
    public enum RotationState
    {
        Idle,
        YRotation,
        XZRotation
    }

    [BehaviorDesigner.Runtime.Tasks.Tooltip("The Y Axis Pivot will stop rotating if the target is within this angle")]
    [SerializeField] float mYRotationDeadzone = 15;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The XZ Axis Pivot will stop rotating if the target is within this angle")]
    [SerializeField] float mXZRotationDeadZone = 5;
    /// <summary>
    /// The Current State of Rotation of the Enemy
    /// </summary>
    RotationState mState = RotationState.Idle;
    /// <summary>
    /// List of points that the target will inspect
    /// </summary>
    List<Transform> mInspectPoints;
    /// <summary>
    /// The position of the current inspect point for the Enemy
    /// </summary>
    Vector3 mCurrentInspectionPoint;
    /// <summary>
    /// The current inspect point index which the target is checking
    /// </summary>
    int mIndex = 0;

    /// <summary>
    /// The Ranged Instance for the enemy
    /// </summary>
    RangedEnemy mEnemyInstanceRanged;

    public override void OnStart()
    {
        base.OnStart();
        mEnemyInstanceRanged = (RangedEnemy)mEnemyInstance;
        if (mInspectPoints == null) //set the inspect points list if it is null
        {
            mInspectPoints = new List<Transform>();
            for (int aI = 0; aI < mEnemyInstanceRanged.mInspectPointParent.transform.childCount; aI++)
            {
                mInspectPoints.Add(mEnemyInstanceRanged.mInspectPointParent.transform.GetChild(aI));
            }
            if (mInspectPoints.Count <= 0)
            {
                mInspectPoints.Add(mEnemyInstanceRanged.mInspectPointParent.transform);
            }
            mIndex = 0;
        }
        if (mIndex < mInspectPoints.Count && !InspectPoint()) //set the next inspect point index if no preset points are there
        {
            mCurrentInspectionPoint = mInspectPoints[mIndex].position;
        }

        mState = RotationState.Idle;
    }

    /// <summary>
    /// Function checks if there are inspect points for this enemy
    /// Assigns that inspect point as the destination
    /// </summary>
    /// <returns>true if inspect points exist and false otherwise</returns>
    bool InspectPoint()
    {
        if (mEnemyInstance.mInspectPoints.Count <= 0)
        {
            return false;
        }
        mCurrentInspectionPoint = mEnemyInstance.mInspectPoints[0];
        mEnemyInstance.mInspectPoints.RemoveAt(0);
        
        return true;
    }


    public override TaskStatus OnUpdate()
    {
        Vector3 aTargetDistance = Vector3.zero;
        float aAngle = 0;
        switch (mState) //check the state of the rotation and move enemy accordingly
        {
            case RotationState.Idle:
                mState = RotationState.YRotation;
                return TaskStatus.Running;
            case RotationState.YRotation:
                aTargetDistance = mCurrentInspectionPoint - mEnemyInstanceRanged.mYRotationPvt.position;
                aTargetDistance.y = 0.0f;
                aAngle = Vector3.Angle(mEnemyInstanceRanged.mYRotationPvt.forward, aTargetDistance); //check angle around just Y axis
                if (Mathf.Abs(aAngle) > mYRotationDeadzone) //rotate if Y axis not in deadzone
                {
                    //mEnemyInstanceRanged.mYRotationPvt.rotation = Quaternion.Slerp(
                      //  mEnemyInstanceRanged.mYRotationPvt.rotation, Quaternion.LookRotation(aTargetDistance), Time.deltaTime * mEnemyInstance.mEnemySpeed);
                    mEnemyInstanceRanged.mYRotationPvt.rotation = Quaternion.Slerp(
                        mEnemyInstanceRanged.mYRotationPvt.rotation, 
                        Quaternion.Euler(new Vector3(0, Quaternion.LookRotation(aTargetDistance).eulerAngles.y, 0))
                        , Time.deltaTime * mEnemyInstance.mEnemySpeed);
                    return TaskStatus.Running;
                }
                if (!string.IsNullOrEmpty(mEnemyInstanceRanged.mRangedMovementSFX))
                {
                    mEnemyInstance.mAudioAssistantSFX.PlaySound(mEnemyInstanceRanged.mRangedMovementSFX);
                }
                mState = RotationState.XZRotation;
                return TaskStatus.Running;
            case RotationState.XZRotation:
                aTargetDistance = mCurrentInspectionPoint - mEnemyInstanceRanged.mXZRotationPvt.position;
                aAngle = Vector3.Angle(mEnemyInstanceRanged.mXZRotationPvt.forward, aTargetDistance); //check angle around all axis
                if (Mathf.Abs(aAngle) > mXZRotationDeadZone) //rotate if all axis not in deadzone
                {
                    mEnemyInstanceRanged.mXZRotationPvt.rotation = Quaternion.Slerp(
                        mEnemyInstanceRanged.mXZRotationPvt.rotation, Quaternion.LookRotation(aTargetDistance), Time.deltaTime * mEnemyInstance.mEnemySpeed);
                    return TaskStatus.Running;
                }
                if(!InspectPoint()) //if there is no other inspect point left then increase the index of the preset inspect points
                {
                    mIndex = (mIndex + 1) % mInspectPoints.Count; //move inspect point and return success once reached deadzone
                    return TaskStatus.Success;
                }
                else
                {
                    mState = RotationState.Idle;
                    return TaskStatus.Running;
                }
        }
        return TaskStatus.Failure;
    }
}

