Steps to run the latest project build from Unity:-

1. Go to Assets/Game/Scenes/Game/Core.unity
and press play.

2. Controls:-

WSAD/Arrow Keys/GamePad Left Stick for Player Walk
Hold Left Shift/ GamePad Right Trigger for Player Run
Hold Left Ctrl/Gamepad Right Bumper for Player Crawl/Push
Mouse Delta/GamePad Right Stick for Camera Movement
Q/GamePad Left Bumper for Shapeshift
E/GamePad Button West (Square/X) for Interact/Deactivate
Space/GamePad Left Trigger hold for Bear AOE Immobilize Mode/Raccoon blink mode and release for cast
Right Mouse Click/Button East (Circle/B) to cancel AOE/Blink


a. To debug non game scenes that have player, press F11 to open Loading Sequence Editor
b. Set the scenes that you want to debug in the debug sequence.
c. Press Enter Playmode to play.
d. Press 5 to play/stop debug sequence without going to the Loading Sequence Editor.