﻿///-------------------------------------------------------------------------------------------------
// File: AOEFreeze.cs
//
// Author: Dakshvir Singh Rehill
// Date: 30/5/2020
//
// Summary:	Implementation of Bear's AOE Special Ability
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class AOEFreeze : MonoBehaviour
{
    [Tooltip("Object Pool")]
    [SerializeField] PoolClassifier mPool;
    /// <summary>
    /// The script of the character of this freeze object
    /// </summary>
    [HideInInspector] public PlayerCharacter mCharacter;
    [Tooltip("Rigidbody of the freezing object")]
    [SerializeField] Rigidbody mFreezeBody;
    [Tooltip("The collider of the freezing object")]
    [SerializeField] CapsuleCollider mFreezeCollider;
    [Header("Freeze Size and Time")]
    [Tooltip("The radius of the AOE Freeze")]
    [SerializeField] float mAOERadius = 6.0f;
    [Tooltip("The height of the AOE Freeze")]
    [SerializeField] float mAOEHeight = 1.5f;
    [Tooltip("The Spread Time of the AOE Freeze")]
    [SerializeField] [Range(0, 1)] float mAOETime = 0.45f;
    [Tooltip("The maximum time that the freeze will last")]
    [SerializeField] float mFreezeTime = 6.5f;
    [Header("Freeze Effect Settings")]
    [Tooltip("Particle System That is On The Boundary of the AOE Immobilize")]
    [SerializeField] ParticleSystem mBoundaryParticle;
    [Tooltip("Particle System That Will Show Immobilize Outline")]
    [SerializeField] ParticleSystem mAOEOutlineEffect;
    [Tooltip("Scale difference for Outline Effect")]
    [SerializeField] float mOutlineScale = 0.4167f;
    [Tooltip("Scale difference for Boundary Effect")]
    [SerializeField] float mBoundaryScale = 1.67f;
    /// <summary>
    /// Current Time of the Freeze Effect
    /// </summary>
    float mCurrentFreezeTime = 0.0f;
    /// <summary>
    /// Objects that are currently frozen
    /// </summary>
    readonly List<IFreezable> mCurrentlyFreezed = new List<IFreezable>();
    /// <summary>
    /// True when AOE is actively setup
    /// </summary>
    bool mAOEActive = false;
    /// <summary>
    /// True when the effect of AOE is completed
    /// </summary>
    bool mAOELerpCompleted = false;

    void Update()
    {
        if (!mAOEActive)
        {
            transform.position = mCharacter.transform.position;
            transform.rotation = mCharacter.transform.rotation;
            return;
        }
        if (mCurrentFreezeTime > 0.0f)
        {
            mCurrentFreezeTime -= Time.deltaTime;
            if (mCurrentFreezeTime <= 0.0f) //end freeze
            {
                mAOEOutlineEffect.Play();
                LeanTween.value(gameObject, mFreezeCollider.radius, 0, 1.0f - mAOETime).setOnUpdate(ChangeFreezeRadius);
                LeanTween.value(gameObject, mFreezeCollider.height, 0, 1.0f - mAOETime).setOnUpdate(ChangeFreezeHeight)
                    .setOnComplete(() =>
                    {
                        foreach(IFreezable aFrozen in mCurrentlyFreezed)
                        {
                            aFrozen.mFrozen = false;
                        }
                        mCurrentlyFreezed.Clear();
                        ResetAOE();
                    });
            }
        }
    }
    /// <summary>
    /// Function to reset all AOE settings back to normal
    /// </summary>
    public void ResetAOE()
    {
        mAOELerpCompleted = false;
        mFreezeBody.isKinematic = true;
        mFreezeCollider.radius = 0.0f;
        mFreezeCollider.height = 0.0f;
        mFreezeCollider.enabled = false;
        mAOEOutlineEffect.Stop();
        mBoundaryParticle.Stop();
        mAOEOutlineEffect.gameObject.transform.localScale = new Vector3(0, mAOEOutlineEffect.gameObject.transform.localScale.y, 0);
        mBoundaryParticle.gameObject.transform.localScale = new Vector3(0, mBoundaryParticle.gameObject.transform.localScale.y, 0);
        mCharacter = null;
        gameObject.SetActive(false);
        ObjectPoolingManager.Instance.ReturnPooledObject(mPool.mPoolName, gameObject);
    }
    /// <summary>
    /// Start AOE but don't cause it just show the radius
    /// </summary>
    public void StartAOE()
    {
        mAOELerpCompleted = false;
        mAOEActive = false;
        float aOutlineXZScl = mOutlineScale * mAOERadius;
        mAOEOutlineEffect.gameObject.transform.localScale = new Vector3(aOutlineXZScl, mAOEOutlineEffect.gameObject.transform.localScale.y, aOutlineXZScl);
        gameObject.SetActive(true);
        mAOEOutlineEffect.Simulate(mAOETime, true, true);
    }
    /// <summary>
    /// Start lerping and causing the AOE
    /// </summary>
    public void AOEAnimStart()
    {
        mBoundaryParticle.gameObject.transform.localScale = new Vector3(0, mBoundaryParticle.gameObject.transform.localScale.y, 0);
        mFreezeCollider.enabled = true;
        mFreezeBody.isKinematic = false;
        mAOEOutlineEffect.Clear();
        mAOEOutlineEffect.Simulate(0, true, true);
        mAOEOutlineEffect.Play();
        mBoundaryParticle.Play();
        LeanTween.value(gameObject, 0, mAOERadius, mAOETime).setOnUpdate(ChangeFreezeRadius)
            .setOnComplete(() =>
            {
                mAOELerpCompleted = true;
                mAOEOutlineEffect.Pause();
            });
        LeanTween.value(gameObject, 0, mAOEHeight, mAOETime).setOnUpdate(ChangeFreezeHeight);
        mCurrentFreezeTime = mFreezeTime;
        mAOEActive = true;
    }
    /// <summary>
    /// Function used to dynamically update the radius of the AOE freeze
    /// </summary>
    /// <param name="pRadius">Current Radius of the freeze</param>
    void ChangeFreezeRadius(float pRadius)
    {
        mFreezeCollider.radius = pRadius;
        float aBoundXZScl = mBoundaryScale * pRadius;
        mBoundaryParticle.gameObject.transform.localScale = new Vector3(aBoundXZScl, mBoundaryParticle.gameObject.transform.localScale.y, aBoundXZScl);
    }
    /// <summary>
    /// Function used to dynamically update the height of the AOE freeze
    /// </summary>
    /// <param name="pHeight">Current Height of the freeze</param>
    void ChangeFreezeHeight(float pHeight)
    {
        mFreezeCollider.height = pHeight;
    }

    void OnTriggerEnter(Collider pOther)
    {
        if (mAOELerpCompleted)
        {
            return;
        }
        IFreezable aFreezable = pOther.GetComponent<IFreezable>();
        if (aFreezable != null && !mCurrentlyFreezed.Contains(aFreezable))
        {
            Enemy aEnemy = pOther.GetComponent<Enemy>();
            if (aEnemy != null)
            {
                //EnemyAnalyticsData aData = AnalyticsManager.Instance.mEnemyAnalytics[aEnemy.mEnemyID];
                //aData.mTimesFrozen++;
                //AnalyticsManager.Instance.mEnemyAnalytics[aEnemy.mEnemyID] = aData;
            }
            mCurrentlyFreezed.Add(aFreezable);
            aFreezable.mFrozen = true;
        }
    }

    //to ensure two overlapping freezes don't cancel out
    void OnTriggerStay(Collider pOther)
    {
        if (mAOELerpCompleted)
        {
            return;
        }
        IFreezable aFreezable = pOther.GetComponent<IFreezable>();
        if (aFreezable != null && mCurrentlyFreezed.Contains(aFreezable))
        {
            aFreezable.mFrozen = true;
        }
    }
}
