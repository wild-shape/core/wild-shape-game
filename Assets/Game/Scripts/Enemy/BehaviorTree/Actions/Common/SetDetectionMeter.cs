﻿///-------------------------------------------------------------------------------------------------
// File: SetDetectionMeter.cs
//
// Author: Dakshvir Singh Rehill
// Date: 8/6/2020
//
// Summary:	Action to set the detection meter of the player
///-------------------------------------------------------------------------------------------------

using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

[TaskCategory("Enemy")]
[TaskDescription("Sets Detection Meter of the Player when player is visible/not visible")]
public class SetDetectionMeter : EnemyActionBase
{
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The percentage value of how much the player has been detected")]
    [SerializeField] SharedFloat mDetectionMeter;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The threshold percentage for the player getting undetected")]
    [SerializeField] SharedFloat mUndetectionThreshold;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("Boolean to check if player is visible/not visible")]
    [SerializeField] SharedBool mCanSee;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The Last Known Position of the player to be set if insta detected")]
    [SerializeField] SharedVector3 mLastKnownPos;
    /// <summary>
    /// The shape instance to get the active player character
    /// </summary>
    ShapeShift mPlayerShapeInstance;

    public override void OnStart()
    {
        base.OnStart();
        if (mPlayerShapeInstance == null)//set shape instance if null
        {
            mPlayerShapeInstance = GameManager.Instance.mSceneHandler.mCameraController.mShapeShiftInst;
        }
    }

    public override TaskStatus OnUpdate()
    {
        int aCurChar = GameManager.Instance.mGameData.mPlayerData.mCurChar; //current character index
        Vector3 aTargetPosition = mPlayerShapeInstance.mPlayerCharacters[aCurChar].mCameraLookAt.position;
        //if can't see but player in insta detect
        if (!mCanSee.Value)
        {
            // Calculate distance and angle between us and the target
            Vector3 aDistance = aTargetPosition - mEnemyInstance.transform.position;
            aDistance.y = 0.0f;
            float aMagnitude = aDistance.magnitude;
            if (!mPlayerShapeInstance.mPlayerCharacters[aCurChar].mHidden)
            {
                float aAngle = Mathf.Abs(Vector3.Angle(mEnemyInstance.transform.forward, aDistance));
                if (aAngle <= mEnemyInstance.mInstaDetectAngle && aMagnitude <= mEnemyInstance.mInstaDetectRadius
                    && aTargetPosition.y - mEnemyInstance.transform.position.y <= mEnemyInstance.mYDistanceCheck)
                {
                    mDetectionMeter.Value = 1.0f;
                    mLastKnownPos.Value = aTargetPosition;
                    mEnemyInstance.mTree.SendEvent("PlayerCollided");
                    return TaskStatus.Success;
                }
            }
        }

        if (mCanSee.Value && !mPlayerShapeInstance.mPlayerCharacters[aCurChar].mHidden) //if u can see player
        {
            if (LeanTween.isTweening(gameObject)) //cancel detection reduction tween if can see player again
            {
                LeanTween.cancel(gameObject);
            }
            float aDetectionMeterChange = 0.0f; //total change in detection meter after combining all vision cones
            foreach (Enemy.VisionConePoint aVisionPoint in mEnemyInstance.mVisionCones)
            {
                Vector3 aRelativeVisionConePoint = mEnemyInstance.mOrigin.position + aVisionPoint.mOffset;
                Vector3 aOriginForward = mEnemyInstance.mOrigin.forward;
                if (aVisionPoint.mRotationOffset.sqrMagnitude > 0.0f)
                {
                    aOriginForward = Quaternion.Euler(aVisionPoint.mRotationOffset) * aOriginForward;
                }
                // Calculate distance and angle between us and the target
                Vector3 aDistance = aTargetPosition - aRelativeVisionConePoint;
                float aMagnitude = aDistance.magnitude;
                float aAngle = Mathf.Abs(Vector3.Angle(aOriginForward, aDistance));
                float aAngleDetection = 1 - (aVisionPoint.mViewingAngle - aAngle) * 0.01f; //reverse detection as less is more
                aAngleDetection = aAngleDetection < 0 ? 0 : aAngleDetection;
                float aMagnitudeDetection = 1 - (aVisionPoint.mMaxViewingDistance - aMagnitude) * 0.01f;
                aMagnitudeDetection = aMagnitudeDetection < 0 ? 0 : aMagnitudeDetection; //if less than 0 then make it 0
                float aConeDetectionChange = (aAngleDetection * mEnemyInstance.mAngleDetectionStrength
                    + aMagnitudeDetection * mEnemyInstance.mDistanceDetectionStrength)//add distance and angle strength
                     * aVisionPoint.mDetectionStrength; //multiply by overal vision point strength
                aConeDetectionChange += mPlayerShapeInstance.
                    mPlayerCharacters[aCurChar].mDetectionModifier
                    * aConeDetectionChange; //add the detection modifier percentage change to the change
                aDetectionMeterChange += aConeDetectionChange; //add it to the total detection change
            }
            if (aDetectionMeterChange > 0) //if total detection is greater than 0 then add it to the detection meter
            {
                mDetectionMeter.Value += aDetectionMeterChange * Time.deltaTime;
            }
        }
        else if (!(LeanTween.isTweening(gameObject) ||
            mDetectionMeter.Value <= mUndetectionThreshold.Value)) //if not (already reducing or detection less than undetected value) then reverse detection
        {
            LeanTween.value(gameObject, mDetectionMeter.Value, 0, mEnemyInstance.mReversalTime)
                .setDelay(mEnemyInstance.mReversalDelay)
                .setOnUpdate((float pVal) => { mDetectionMeter.Value = pVal; });
        }
        return TaskStatus.Success;
    }
}
