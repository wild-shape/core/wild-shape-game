﻿///-------------------------------------------------------------------------------------------------
// File: IsLostEnemyFound.cs
//
// Author: Dakshvir Singh Rehill
// Date: 17/6/2020
//
// Summary:	Conditional to look for the lost enemy if there is any available
///-------------------------------------------------------------------------------------------------
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;
[TaskCategory("Enemy/Melee")]
[TaskDescription("Checks if the enemy found the lost enemy or not")]
public class IsLostEnemyFound : EnemyConditionalBase
{
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The Lost Enemy's Enemy Instance that needs to be found")]
    [SerializeField] SharedEnemy mLostEnemy;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The layer in which all enemies reside")]
    [SerializeField] SharedLayerMask mEnemyMask;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The global layer of all targets blockers and player")]
    [SerializeField] SharedLayerMask mTargetLayer;
    /// <summary>
    /// This layer mask combines the target layer and enemy layer so that they are one layer
    /// </summary>
    LayerMask mFinalMask;
    /// <summary>
    ///Boolean used to check if enemy needs to be nulled on end
    /// </summary>
    bool mSetNull = false;
    public override void OnStart()
    {
        base.OnStart();
        mFinalMask = mEnemyMask.Value;
        mFinalMask |= mTargetLayer.Value; //combine target layer and enemy layer
    }

    public override TaskStatus OnUpdate()
    {
        if(mLostEnemy.Value == null) //if no lost enemy return
        {
            return TaskStatus.Failure;
        }

        foreach (Enemy.VisionConePoint aVisionPoint in mEnemyInstance.mVisionCones) // find enemy using vision cones
        {
            Vector3 aRelativeVisionConePoint = mEnemyInstance.mOrigin.position + aVisionPoint.mOffset;
            Vector3 aOriginForward = mEnemyInstance.mOrigin.forward;
            if (aVisionPoint.mRotationOffset.sqrMagnitude > 0.0f)
            {
                aOriginForward = Quaternion.Euler(aVisionPoint.mRotationOffset) * aOriginForward;
            }
            // Calculate distance and angle between us and the target
            Vector3 aDistance = mLostEnemy.Value.transform.position - aRelativeVisionConePoint;
            float aMagnitude = aDistance.magnitude;
            aDistance.Normalize();
            float aAngle = Vector3.Angle(aOriginForward, aDistance);
            // If the target is within the specified range
            if (Mathf.Abs(aAngle) <= aVisionPoint.mViewingAngle &&
                aMagnitude <= aVisionPoint.mMaxViewingDistance)
            {
                RaycastHit aHit;
                if (Physics.Raycast(aRelativeVisionConePoint,
                    aDistance, out aHit,
                    aVisionPoint.mMaxViewingDistance,
                    mFinalMask))
                {
                    if (aHit.collider != null) // if something found
                    {
                        if (aHit.collider.gameObject.GetInstanceID() ==
                            mLostEnemy.Value.gameObject.GetInstanceID()) // if enemy found
                        {
                            mSetNull = true;
                            return TaskStatus.Success;
                        }
                    }
                }
            }
        }
        return TaskStatus.Failure;
    }

    public override void OnEnd()
    {
        if(mSetNull)
        {
            mLostEnemy.Value = null;
        }
    }
}
