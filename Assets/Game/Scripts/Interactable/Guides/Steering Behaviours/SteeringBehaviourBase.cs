﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SteeringBehaviourBase : MonoBehaviour
{
    [Header("Base Variables")]
    [Tooltip("The weight of the Agent")]
	public float mWeight = 1.0f;
    [Tooltip("The target/destination of Agent")]
    public Vector3 mTarget = Vector3.zero;

	public abstract Vector3 calculateForce();

	[HideInInspector] public SteeringAgent mSteeringAgent;

}
