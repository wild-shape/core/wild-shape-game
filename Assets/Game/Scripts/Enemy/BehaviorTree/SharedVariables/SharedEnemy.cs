﻿///-------------------------------------------------------------------------------------------------
// File: SharedEnemy.cs
//
// Author: Dakshvir Singh Rehill
// Date: 17/6/2020
//
// Summary:	Shared Enemy variable created to expose Enemy to Behavior Designer Actions and Conditionals
///-------------------------------------------------------------------------------------------------
using UnityEngine;
using BehaviorDesigner.Runtime;
using System.Collections.Generic;

[System.Serializable]
public class SharedEnemy : SharedVariable<Enemy>
{
	public override string ToString() { return mValue == null ? "null" : mValue.ToString(); }
	public static implicit operator SharedEnemy(Enemy value) { return new SharedEnemy { mValue = value }; }
}