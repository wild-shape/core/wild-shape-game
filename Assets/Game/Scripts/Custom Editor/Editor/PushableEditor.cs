﻿///-------------------------------------------------------------------------------------------------
// File: PushableEditor.cs
//
// Author: Aditya Dinesh
// Date: 4/8/2020
//
// Summary:	Custom Inspector display for Pushable
///-------------------------------------------------------------------------------------------------

using UnityEditor;

[CustomEditor(typeof(Pushable))]
public class PushableEditor : Editor
{
    SerializedProperty mInteractableBy;
    SerializedProperty mPushDirections;
    SerializedProperty mRigidBody;
    SerializedProperty mSpeedMultiplier;
    SerializedProperty mPushableDistance;
    SerializedProperty mMaxAngleDifference;
    SerializedProperty mPushObjectSFX;
    SerializedProperty mCrateDestroySFX;
    SerializedProperty mYCheck;
    SerializedProperty mPushableEffect;
    SerializedProperty mParticleParent;
    SerializedProperty mCrateFallFx;

    Pushable mPushable = null;

    private void OnEnable()
    {
        mPushable = target as Pushable;

        mInteractableBy = serializedObject.FindProperty("mInteractableBy");
        mPushDirections = serializedObject.FindProperty("mPushDirections");
        mRigidBody = serializedObject.FindProperty("mRigidBody");
        mSpeedMultiplier = serializedObject.FindProperty("mSpeedMultiplier");
        mPushableDistance = serializedObject.FindProperty("mPushableDistance");
        mMaxAngleDifference = serializedObject.FindProperty("mMaxAngleDifference");
        mPushObjectSFX = serializedObject.FindProperty("mPushObjectSFX");
        mCrateDestroySFX = serializedObject.FindProperty("mCrateDestroySFX");
        mYCheck = serializedObject.FindProperty("mYCheck");
        mPushableEffect = serializedObject.FindProperty("mPushableEffect");
        mParticleParent = serializedObject.FindProperty("mParticleParent");
        mCrateFallFx = serializedObject.FindProperty("mCrateFallFx");
    }

    public override void OnInspectorGUI()
    {
        if (mPushable != null)
        {
            EditorGUILayout.PropertyField(mInteractableBy);
            EditorGUILayout.PropertyField(mPushDirections);
            EditorGUILayout.PropertyField(mRigidBody);
            EditorGUILayout.PropertyField(mSpeedMultiplier);
            EditorGUILayout.PropertyField(mPushableDistance);
            EditorGUILayout.PropertyField(mMaxAngleDifference);
            EditorGUILayout.PropertyField(mPushObjectSFX);
            EditorGUILayout.PropertyField(mCrateDestroySFX);
            EditorGUILayout.PropertyField(mYCheck);
            EditorGUILayout.PropertyField(mPushableEffect);
            EditorGUILayout.PropertyField(mParticleParent);
            EditorGUILayout.PropertyField(mCrateFallFx);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
