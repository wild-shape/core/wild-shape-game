﻿///-------------------------------------------------------------------------------------------------
// File: IInteractable.cs
//
// Author: Dakshvir Singh Rehill
// Date: 25/5/2020
//
// Summary:	Anything that the player can interact with
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public abstract class IInteractable : MonoBehaviour
{
    public enum InteractableByType
    {
        Stealth,
        Brute,
        Both
    }
    [HideInInspector] public bool mIsActive = true;
    [Tooltip("To determine which type of player will be able to interact with this interactable")]
    [SerializeField] protected InteractableByType mInteractableBy;
    [Tooltip("To determine the type of interaction control that this interactable will use")]
    [SerializeField] Controls mControlType;
    /// <summary>
    /// The current player character that is trying to interact with this interactable
    /// </summary>
    protected PlayerCharacter mActivePlayerCharacter;
    /// <summary>
    /// Character reference just to show prompt affordance
    /// </summary>
    protected PlayerCharacter mCheckInteractionChar;

    /// <summary>
    /// The menu that would show what button to press for this interactable
    /// </summary>
    protected HUDPromptsMenu mPromptsMenu;

    protected virtual void Start()
    {
        mPromptsMenu = MenuManager.Instance.GetMenu<HUDPromptsMenu>(GameManager.Instance.mHUDPromptsMenu);
    }

    protected virtual void Update()
    {
        if (!mIsActive)
        {
            mCheckInteractionChar = null;
        }
        if (mCheckInteractionChar == null)
        {
            return;
        }
        CheckCanInteract();
        if (!mCheckInteractionChar.gameObject.activeInHierarchy)
        {
            mCheckInteractionChar = null;
        }
        if (mActivePlayerCharacter == null)
        {
            return;
        }
        if (!mActivePlayerCharacter.gameObject.activeInHierarchy)
        {
            EndInteraction();
        }
    }

    protected virtual void OnDestroy()
    {

    }

    protected virtual void OnTriggerEnter(Collider pOther)
    {
        if (!mIsActive)
        {
            return;
        }
        if (mCheckInteractionChar != null)
        {
            return;
        }
        PlayerCharacter aCharacter = pOther.GetComponent<PlayerCharacter>();
        if (aCharacter == null)
        {
            return;
        }
        mCheckInteractionChar = aCharacter;
        if (mInteractableBy == InteractableByType.Stealth && !aCharacter.mStealthyCharacter)
        {
            return;
        }
        else if (mInteractableBy == InteractableByType.Brute && aCharacter.mStealthyCharacter)
        {
            return;
        }
        mActivePlayerCharacter = aCharacter;
    }

    protected virtual void OnTriggerExit(Collider pOther)
    {
        if (!mIsActive)
        {
            return;
        }
        if (mCheckInteractionChar == null)
        {
            return;
        }
        if (pOther.gameObject.GetInstanceID() != mCheckInteractionChar.gameObject.GetInstanceID())
        {
            return;
        }
        mCheckInteractionChar = null;
        if (mControlType != Controls.Shapeshift)
        {
            MenuManager.Instance.HideMenu(GameManager.Instance.mHUDPromptsMenu);
        }
        if (mActivePlayerCharacter == null)
        {
            return;
        }
        if (pOther.gameObject.GetInstanceID() != mActivePlayerCharacter.gameObject.GetInstanceID())
        {
            return;
        }
        EndInteraction();
    }

    /// <summary>
    /// Called when the player is away from the interactable to finish interaction
    /// </summary>
    protected virtual void EndInteraction()
    {
        if (mControlType != Controls.Shapeshift
            && mPromptsMenu.gameObject.activeInHierarchy)
        {
            MenuManager.Instance.HideMenu(GameManager.Instance.mHUDPromptsMenu);
        }
        mActivePlayerCharacter = null;
    }
    /// <summary>
    /// Called to display prompts menu
    /// </summary>
    protected virtual void CheckCanInteract()
    {
        if (mControlType == Controls.Shapeshift)
        {
            return;
        }
        if (!mPromptsMenu.gameObject.activeInHierarchy)
        {
            mPromptsMenu.ShowPromptsMenu(mControlType);
        }
        if (mActivePlayerCharacter == null)
        {
            mPromptsMenu.SetDisabledAffordance();
        }
        else
        {
            mPromptsMenu.SetEnabledAffordance();
        }
    }

}
