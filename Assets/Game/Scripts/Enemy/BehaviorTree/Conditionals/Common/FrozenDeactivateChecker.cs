﻿///-------------------------------------------------------------------------------------------------
// File: FrozenDeactivateChecker.cs
//
// Author: Dakshvir Singh Rehill
// Date: 17/7/2020
//
// Summary:	Conditional checks deactivated enemy and frozen enemy (Created as BD's internal comparer was causing issues
///-------------------------------------------------------------------------------------------------
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

[TaskCategory("Enemy")]
[TaskDescription("Conditional Checks whether the Enemy is Deactivated or Frozen or None")]
public class FrozenDeactivateChecker : EnemyConditionalBase
{
    [BehaviorDesigner.Runtime.Tasks.Tooltip("Enum Value Determines whether Deactivate needs to be checked or Frozen")]
    [SerializeField] SharedEnemyMode mCheckingType;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The Bool Value to compare the variable to")]
    [SerializeField] SharedBool mValueToCompare;
    public override TaskStatus OnUpdate()
    {
        switch (mCheckingType.Value)
        {
            case EnemyMode.Deactivate:
                return ((IDeactivatable)mEnemyInstance).mDeactivate == mValueToCompare.Value ? TaskStatus.Success : TaskStatus.Failure;
            case EnemyMode.Frozen:
                return ((IFreezable)mEnemyInstance).mFrozen == mValueToCompare.Value ? TaskStatus.Success : TaskStatus.Failure;
            case EnemyMode.Attack:
                return mEnemyInstance.mAttack == mValueToCompare.Value ? TaskStatus.Success : TaskStatus.Failure;
            default:
                return TaskStatus.Failure;
        }
    }
}
