﻿///-------------------------------------------------------------------------------------------------
// File: VideoMenu.cs
//
// Author: Dakshvir Singh Rehill , Aditya Dinesh
// Date: 24/6/2020
//
// Summary:	Menu that will allow the user to change video settings
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class VideoMenu : SubMenu
{
    /// <summary>
    /// Array of All available screen resolutions
    /// </summary>
    Resolution[] mScreenResolutions;
    [Tooltip("Dropdown menu to select active screen resolution")]
    [SerializeField] Dropdown mResolutionDropdown;
    [Tooltip("The Quality settings drop down for game")]
    [SerializeField] Dropdown mGraphicQuality;
    [Tooltip("Toggle between full screen and windowed")]
    [SerializeField] Toggle mFullscreen;
    public override void Start()
    {
        base.Start();
        #region Initial Resolution Setup

        // Load and Set Default Screen Resolutions
        mScreenResolutions = Screen.resolutions;

        mResolutionDropdown.ClearOptions();

        List<string> aOptions = new List<string>();


        for (int i = 0; i < mScreenResolutions.Length; i++)
        {
            string aOption = mScreenResolutions[i].width + "x" + mScreenResolutions[i].height;
            aOptions.Add(aOption);

        }

        mResolutionDropdown.AddOptions(aOptions);
        mResolutionDropdown.RefreshShownValue();

        #endregion
        LoadVideoSettings();
    }

    public void SetResolution(int pResolutionIndex)
    {
        Resolution aResolution = mScreenResolutions[pResolutionIndex];
        Screen.SetResolution(aResolution.width, aResolution.height, Screen.fullScreen);
        SaveVideoSettings();

    }

    public void SetQuality(int pQualityIndex)
    {
        QualitySettings.SetQualityLevel(pQualityIndex);
        SaveVideoSettings();
    }

    public void SetFullscreen(bool pIsFullscreen)
    {
        Screen.fullScreen = pIsFullscreen;
        SaveVideoSettings();
    }

    void LoadVideoSettings()
    {
        VideoSettingsData aVSD = GameManager.Instance.mSettingsData.mVideoData;
        if(aVSD.mResolution > mScreenResolutions.Length)
        {
            aVSD.mResolution = mScreenResolutions.Length - 1;
        }
        mResolutionDropdown.value = aVSD.mResolution;
        mGraphicQuality.value = aVSD.mGraphics;
        mFullscreen.isOn = aVSD.mIsFullscreen;
        SetResolution(aVSD.mResolution);
        SetQuality(aVSD.mGraphics);
        SetFullscreen(aVSD.mIsFullscreen);
    }

    void SaveVideoSettings()
    {
        VideoSettingsData aVSD = new VideoSettingsData();
        aVSD.mGraphics = mGraphicQuality.value;
        aVSD.mResolution = mResolutionDropdown.value;
        aVSD.mIsFullscreen = mFullscreen.isOn;
        GameManager.Instance.mSettingsData.mVideoData = aVSD;
        SaveGameManager.Instance.SaveSettingsData();
    }
}
