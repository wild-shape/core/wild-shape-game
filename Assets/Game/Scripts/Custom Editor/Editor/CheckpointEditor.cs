﻿///-------------------------------------------------------------------------------------------------
// File: CheckpointEditor.cs
//
// Author: Aditya Dinesh
// Date: 28/7/2020
//
// Summary:	Custom Inspector display for Checkpoint
///-------------------------------------------------------------------------------------------------

using UnityEditor;

[CustomEditor(typeof(CheckPoint))]
public class CheckpointEditor : Editor
{
    SerializedProperty mSpawnPoint;
    SerializedProperty mCheckpointParticles;
    SerializedProperty mRock;
    SerializedProperty mRockMaterial;
    SerializedProperty mLerpDest;
    SerializedProperty mLerpStoppingDistance;
    SerializedProperty mLerpSpeed;
    SerializedProperty mRockSwingSpeed;
    SerializedProperty mRockSwingAmount;
    SerializedProperty mAudioAssisstant;
    SerializedProperty mPassCheckpointSFX;
    SerializedProperty mWaterCheckpointSFX;

    CheckPoint mCheckPoint = null;

    private void OnEnable()
    {
        mCheckPoint = target as CheckPoint;
        mSpawnPoint = serializedObject.FindProperty("mSpawnPoint");
        mCheckpointParticles = serializedObject.FindProperty("mCheckpointParticles");
        mRock = serializedObject.FindProperty("mRock");
        mRockMaterial = serializedObject.FindProperty("mRockMaterial");
        mLerpDest = serializedObject.FindProperty("mLerpDest");
        mLerpStoppingDistance = serializedObject.FindProperty("mLerpStoppingDistance");
        mLerpSpeed = serializedObject.FindProperty("mLerpSpeed");
        mRockSwingSpeed = serializedObject.FindProperty("mRockSwingSpeed");
        mRockSwingAmount = serializedObject.FindProperty("mRockSwingAmount");
        mAudioAssisstant = serializedObject.FindProperty("mAudioAssisstant");
        mPassCheckpointSFX = serializedObject.FindProperty("mPassCheckpointSFX");
        mWaterCheckpointSFX = serializedObject.FindProperty("mWaterCheckpointAmbience");
    }

    public override void OnInspectorGUI()
    {
        if(mCheckPoint != null)
        {
            EditorGUILayout.PropertyField(mSpawnPoint);
            EditorGUILayout.PropertyField(mCheckpointParticles);
            EditorGUILayout.PropertyField(mRock);
            EditorGUILayout.PropertyField(mRockMaterial);
            EditorGUILayout.PropertyField(mLerpDest);
            EditorGUILayout.PropertyField(mLerpStoppingDistance);
            EditorGUILayout.PropertyField(mLerpSpeed);
            EditorGUILayout.PropertyField(mRockSwingSpeed);
            EditorGUILayout.PropertyField(mRockSwingAmount);
            EditorGUILayout.PropertyField(mAudioAssisstant);
            EditorGUILayout.PropertyField(mPassCheckpointSFX);
            EditorGUILayout.PropertyField(mWaterCheckpointSFX);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
