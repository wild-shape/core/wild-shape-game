﻿///-------------------------------------------------------------------------------------------------
// File: SetWaypointWait.cs
//
// Author: Dakshvir Singh Rehill
// Date: 17/6/2020
//
// Summary:	Used to determine if the waypoint is an interactable or just a waypoint
///-------------------------------------------------------------------------------------------------
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;
[TaskCategory("Enemy/Melee")]
[TaskDescription("Used to determine if the waypoint is an interactable or just a waypoint and what wait time should be set accordingly")]
public class SetWaypointWait : EnemyActionBase
{
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The Current Waypoint that the enemy just traversed")]
    [SerializeField] SharedTransform mCurrentWaypoint;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The Maximum Wait Time for the enemy at a normal waypoint")]
    [SerializeField] SharedFloat mMaxWait;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The Minimum Wait Time for the enemy at a normal waypoint")]
    [SerializeField] SharedFloat mMinWait;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("The final wait time that is calculated in this action")]
    [SerializeField] SharedFloat mWaypointWait;

    public override TaskStatus OnUpdate()
    {
        if (mCurrentWaypoint.Value != null) //if waypoint is null no need to check for interaction
        {
            EnemyCheckIn aCheckIn = mCurrentWaypoint.Value.GetComponent<EnemyCheckIn>(); //check if waypoint is a checkin point
            if (aCheckIn != null)
            {
                mWaypointWait.Value = aCheckIn.mChargingWait; //wait for charging time instead of random time
                return TaskStatus.Success;
            }
        }
        mWaypointWait.Value = Random.Range(mMinWait.Value, mMaxWait.Value);
        return TaskStatus.Success;
    }
}
