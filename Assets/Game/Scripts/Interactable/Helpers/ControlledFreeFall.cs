﻿///-------------------------------------------------------------------------------------------------
// File: ControlledFreeFall.cs
//
// Author: Dakshvir Singh Rehill
// Date: 1/6/2020
//
// Summary:	Allows a non-rigidbody to fall at a desired rate
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlledFreeFall : MonoBehaviour
{
    [Tooltip("Layer Mask of the ground")]
    [SerializeField] LayerMask mGroundLayer;
    [Tooltip("The Collider of Object That Needs to Free Fall")]
    [SerializeField] Collider mObjectBounds;
    [Tooltip("The speed of the free fall")]
    [SerializeField] float mFallSpeed = 10.0f;
    [Tooltip("The minimum distance to check ground")]
    [SerializeField] float mGCheckDist = 0.2f;
    [Tooltip("Draw Debug Lines")]
    [SerializeField] bool mDebugDraw = true;
    void Update()
    {
        //box cast the object size to check if there is anything below it
        if (!Physics.BoxCast(transform.position, mObjectBounds.bounds.extents, -Vector3.up, mObjectBounds.transform.rotation, mGCheckDist, mGroundLayer, QueryTriggerInteraction.Ignore))
        {
            mObjectBounds.transform.position -= Vector3.up * mFallSpeed * Time.deltaTime;
        }
    }

    void OnDrawGizmos()
    {
        if (!mDebugDraw)
        {
            return;
        }
        Bounds aBound = new Bounds(transform.position - Vector3.up * mGCheckDist, mObjectBounds.bounds.size);
        DebugExtension.DrawBounds(aBound, Color.blue);
    }



}
