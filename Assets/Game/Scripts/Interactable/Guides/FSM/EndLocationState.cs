﻿///-------------------------------------------------------------------------------------------------
// File: EndLocationState.cs
//
// Author: Dakshvir Singh Rehill
// Date: 22/7/2020
//
// Summary:	Called to allow the NPC to move to home when conversation is done
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLocationState : GuideBaseState
{
    MovingGuide mMoveInst;
    [SerializeField] string mMoveStateTrigger = "MoveToLocation";
    bool mReachedLocation = false;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        if (mMoveInst == null)
        {
            mMoveInst = (MovingGuide)mGuideRef;
        }
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (mMoveInst.mDynamicConversationDialogs[mMoveInst.mDynamicIndex].mDialogInfo.mSummarize && !mReachedLocation)
        {
            mReachedLocation = true;
            if(mMoveInst.mCollectible != null)
            {
                if (!mMoveInst.mCollectible.activeInHierarchy)
                {
                    mMoveInst.mCollectible.SetActive(true);
                }
            }
            animator.SetTrigger(mMoveStateTrigger);
        }
    }
}
