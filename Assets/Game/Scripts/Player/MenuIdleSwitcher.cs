﻿///-------------------------------------------------------------------------------------------------
// File: MenuIdleSwitcher.cs
//
// Author: Dakshvir Singh Rehill
// Date: 1/8/2020
//
// Summary:	Script setup so that different player idle animations are looped in the animator
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AnimationListener))]
[RequireComponent(typeof(Animator))]
public class MenuIdleSwitcher : MonoBehaviour
{
    [Tooltip("Number of Idle Animations in the Animator")]
    public int mIdleAnimations = 3;
    [Tooltip("Idle Animation Base Name")]
    public string mBaseIdleAnimName = "Idle";
    [Tooltip("Idle Index Parameter Name")]
    public string mIdleIxAnimInt = "IdleIx";
    int mCurrentIdleIx = 0;
    string mIdleAnimationName;
    AnimationListener mAnimationListener;
    Animator mAnimator;
    void Start()
    {
        mAnimationListener = GetComponent<AnimationListener>();
        mAnimator = GetComponent<Animator>();
        RegisterIdleAnimation();
    }

    /// <summary>
    /// Callback to randomly choose different idle animations
    /// </summary>
    /// <param name="pIdleHash"></param>
    void OnIdleComplete(int pIdleHash)
    {
        if (pIdleHash == Animator.StringToHash(mIdleAnimationName))
        {
            RegisterIdleAnimation();
        }
    }


    /// <summary>
    /// Change the idle animation
    /// </summary>
    void RegisterIdleAnimation()
    {
        if (!string.IsNullOrEmpty(mIdleAnimationName))
        {
            mAnimationListener.UnregisterOnAnimationCompleted(
                Animator.StringToHash(mIdleAnimationName), OnIdleComplete);
        }
        mCurrentIdleIx = Random.Range(0, mIdleAnimations);
        mIdleAnimationName = mBaseIdleAnimName;
        mIdleAnimationName += mCurrentIdleIx;
        mAnimator.SetInteger(mIdleIxAnimInt, mCurrentIdleIx);
        mAnimationListener.RegisterOnAnimationCompleted(
            Animator.StringToHash(mIdleAnimationName), OnIdleComplete);
    }

}
