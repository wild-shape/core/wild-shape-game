﻿///-------------------------------------------------------------------------------------------------
// File: SetButtonUI.cs
//
// Author: Dakshvir Singh Rehill
// Date: 27/7/2020
//
// Summary:	Script uses the button selectable to switch on image
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class SetButtonUI : MonoBehaviour,ISelectHandler,IDeselectHandler
{
    [Tooltip("The Image that shows that the button is selected")]
    [SerializeField] GameObject mSelectionImage;
    [Tooltip("The Button of this object")]
    Button mSelectedObject;
    [Tooltip("The Menu Classifier that will be opened as a sub menu to this menu")]
    [SerializeField] MenuClassifier mSubMenuClassifier;
    [Tooltip("The Overlay Menu reference of this button's main menu UI")]
    [SerializeField] OverlayMenu mOverlayMenuInstance;
    [Tooltip("This avoids the Select sfx playing instantly on load")]
    [SerializeField] bool mCanPlaySelectSFX = true;
    void Awake()
    {
        mSelectedObject = GetComponent<Button>();
        if(mOverlayMenuInstance != null && mSubMenuClassifier != null)
        {
            mSelectedObject.onClick.AddListener(() => mOverlayMenuInstance.OpenOverlay(mSubMenuClassifier, mSelectedObject));
        }
    }

    public void OnDeselect(BaseEventData eventData)
    {
        mSelectionImage.SetActive(false);
        if (mOverlayMenuInstance != null && mSubMenuClassifier != null)
        {
            mOverlayMenuInstance.HideOverlay(mSubMenuClassifier);
        }
    }

    public void OnSelect(BaseEventData eventData)
    {
        mSelectionImage.SetActive(true);
        if (mCanPlaySelectSFX)
        {
            AudioManager.Instance.PlaySound(MenuManager.Instance.mMenuSelectMovementSFX);
        }
        else
        {
            mCanPlaySelectSFX = true;
        }
        if (mOverlayMenuInstance != null)
        {
            mOverlayMenuInstance.DisplayOverlay(mSubMenuClassifier);
        }
    }

}
