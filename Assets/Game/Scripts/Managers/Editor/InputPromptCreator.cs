﻿///-------------------------------------------------------------------------------------------------
// File: InputPromptCreator.cs
//
// Author: Dakshvir Singh Rehill
// Date: 28/7/2020
//
// Summary:	This script is used to generate a json resource that contains all possible controls a user can set 
// in supported control schemes
///-------------------------------------------------------------------------------------------------
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.ShortcutManagement;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputPromptCreator
{
    [Shortcut("Update Prompt Assets", KeyCode.Alpha9)]
    [MenuItem("Wild Shape/Update Prompts")]
    public static void CreatePromptsAsset()
    {
        InputPromptContainer aContainerObj = new InputPromptContainer();
        string aPath = Application.dataPath + "/Game/InputSettings/Resources/InputPrompts.json";
        if (aContainerObj.mLockedControls == null)
        {
            aContainerObj.mLockedControls = new List<InputPromptSettings>();
        }
        if (aContainerObj.mUnlockedControls == null)
        {
            aContainerObj.mUnlockedControls = new List<InputPromptSettings>();
        }
        #region Create Locked Controls
        #region KeyboardMouse
        InputPromptSettings aSetting = new InputPromptSettings()
        {
            mInputBinding = "escape",
            mFallbackText = "esc",
            mSchemeType = ControlSchemeType.KeyboardMouse
        };
        aContainerObj.mLockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "Dpad",
            mFallbackText = "WASD/Arrow Keys",
            mSchemeType = ControlSchemeType.KeyboardMouse
        };
        aContainerObj.mLockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "delta",
            mFallbackText = "Mouse Movement",
            mSchemeType = ControlSchemeType.KeyboardMouse
        };
        aContainerObj.mLockedControls.Add(aSetting);
        #endregion
        #region XBox
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "start",
            mFallbackText = "Menu",
            mSchemeType = ControlSchemeType.XBox
        };
        aContainerObj.mLockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "leftStick",
            mFallbackText = "Left Stick",
            mSchemeType = ControlSchemeType.XBox
        };
        aContainerObj.mLockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "rightStick",
            mFallbackText = "Right Stick",
            mSchemeType = ControlSchemeType.XBox
        };
        aContainerObj.mLockedControls.Add(aSetting);
        #endregion
        #region PS
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "start",
            mFallbackText = "Options",
            mSchemeType = ControlSchemeType.PS
        };
        aContainerObj.mLockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "leftStick",
            mFallbackText = "Left Stick",
            mSchemeType = ControlSchemeType.PS
        };
        aContainerObj.mLockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "rightStick",
            mFallbackText = "Right Stick",
            mSchemeType = ControlSchemeType.PS
        };
        aContainerObj.mLockedControls.Add(aSetting);
        #endregion
        #endregion
        #region Create Unlocked Controls
        #region KeyboardMouse
        foreach (Key aCode in Enum.GetValues(typeof(Key)))
        {
            string aCodeString = aCode.ToString();
            aCodeString = aCodeString.Substring(0, 1).ToLower() + (aCodeString.Length <= 1 ? "" :
                aCodeString.Substring(1, aCodeString.Length - 1));
            aSetting = new InputPromptSettings()
            {
                mInputBinding = aCodeString,
                mFallbackText = aCodeString.Substring(0, aCodeString.Length > 3 ? 3 : aCodeString.Length).ToUpper(),
                mSchemeType = ControlSchemeType.KeyboardMouse
            };
            aContainerObj.mUnlockedControls.Add(aSetting);
        }
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "rightButton",
            mFallbackText = "Rt Clk",
            mSchemeType = ControlSchemeType.KeyboardMouse
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "leftButton",
            mFallbackText = "Lt Clk",
            mSchemeType = ControlSchemeType.KeyboardMouse
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "middleButton",
            mFallbackText = "Md Clk",
            mSchemeType = ControlSchemeType.KeyboardMouse
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        #endregion
        #region XBox
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "buttonNorth",
            mFallbackText = "Y",
            mSchemeType = ControlSchemeType.XBox
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "buttonSouth",
            mFallbackText = "A",
            mSchemeType = ControlSchemeType.XBox
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "buttonWest",
            mFallbackText = "X",
            mSchemeType = ControlSchemeType.XBox
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "buttonEast",
            mFallbackText = "B",
            mSchemeType = ControlSchemeType.XBox
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "leftShoulder",
            mFallbackText = "LB",
            mSchemeType = ControlSchemeType.XBox
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "rightShoulder",
            mFallbackText = "RB",
            mSchemeType = ControlSchemeType.XBox
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "leftTrigger",
            mFallbackText = "LT",
            mSchemeType = ControlSchemeType.XBox
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "rightTrigger",
            mFallbackText = "RT",
            mSchemeType = ControlSchemeType.XBox
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "select",
            mFallbackText = "Share",
            mSchemeType = ControlSchemeType.XBox
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "leftStickButton",
            mFallbackText = "LS",
            mSchemeType = ControlSchemeType.XBox
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "rightStickButton",
            mFallbackText = "RS",
            mSchemeType = ControlSchemeType.XBox
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        #endregion
        #region PS
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "buttonNorth",
            mFallbackText = "Tri",
            mSchemeType = ControlSchemeType.PS
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "buttonSouth",
            mFallbackText = "X",
            mSchemeType = ControlSchemeType.PS
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "buttonWest",
            mFallbackText = "Sqr",
            mSchemeType = ControlSchemeType.PS
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "buttonEast",
            mFallbackText = "Cir",
            mSchemeType = ControlSchemeType.PS
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "leftShoulder",
            mFallbackText = "L1",
            mSchemeType = ControlSchemeType.PS
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "rightShoulder",
            mFallbackText = "R1",
            mSchemeType = ControlSchemeType.PS
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "leftTrigger",
            mFallbackText = "L2",
            mSchemeType = ControlSchemeType.PS
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "rightTrigger",
            mFallbackText = "R2",
            mSchemeType = ControlSchemeType.PS
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "select",
            mFallbackText = "Share",
            mSchemeType = ControlSchemeType.PS
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "leftStickButton",
            mFallbackText = "LS",
            mSchemeType = ControlSchemeType.PS
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        aSetting = new InputPromptSettings()
        {
            mInputBinding = "rightStickButton",
            mFallbackText = "RS",
            mSchemeType = ControlSchemeType.PS
        };
        aContainerObj.mUnlockedControls.Add(aSetting);
        #endregion
        #endregion
        #region Save JSON File
        try
        {
            using (StreamWriter aWriter = new StreamWriter(aPath, false))
            {
                aWriter.WriteLine(JsonUtility.ToJson(aContainerObj, true));
            }
        }
        catch (Exception aE)
        {
            Debug.LogErrorFormat("Coudn't Save JSON,{0}\n{1}", aE.Message, aE.StackTrace);
        }
        #endregion
    }
}
