﻿///-------------------------------------------------------------------------------------------------
// File: InGameDialogBox.cs
//
// Author: Dakshvir Singh Rehill
// Date: 20/7/2020
//
// Summary:	This dialog box is similar to normal dialog box but is only used ingame
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InGameDialogBox : MonoBehaviour
{
    [Tooltip("The text of the main heading")]
    public TextMeshProUGUI mHeadingText;
    [Tooltip("The text of the main body")]
    public TextMeshProUGUI mBodyText;
    [Tooltip("Continue Container Image")]
    public GameObject mContinueContainer;
    [Tooltip("The Continue Button Text")]
    public TextMeshProUGUI mContinueTxt;
    [Tooltip("The Continue Image")]
    public Image mContinueImg;
    [Tooltip("The Interact BG Img")]
    public Image mInteractBG;
    [Header("Tutorialize Settings")]
    [Tooltip("The Main Object of Entire Secondary")]
    public GameObject mSecondaryBodyObj;
    [Tooltip("The Container for Main Txt and Image")]
    public GameObject mContainer;
    [Tooltip("The Container for Sec Txt and Image")]
    public GameObject mSecContainer;
    [Tooltip("The Main Control Text")]
    public TextMeshProUGUI mMainTxt;
    [Tooltip("The Main Control Image")]
    public Image mMainControlImage;
    [Tooltip("The Secondary Control Text")]
    public TextMeshProUGUI mSecControlTxt;
    [Tooltip("The Secondary Control Image")]
    public Image mSecCtrlImg;
    [Tooltip("The Secondary Body")]
    public TextMeshProUGUI mSecondBody;
    [Tooltip("The Main Image")]
    public Image mMainIntImg;
    [Tooltip("The Second Image")]
    public Image mSecondIntImg;
    void OnDisable()
    {
        mContinueContainer.SetActive(false);
        mContinueImg.gameObject.SetActive(false);
        mContinueTxt.gameObject.SetActive(false);
        if (mSecondaryBodyObj != null)
        {
            mSecondaryBodyObj.SetActive(false);
            mSecondBody.gameObject.SetActive(false);
            mSecCtrlImg.gameObject.SetActive(false);
            mSecControlTxt.gameObject.SetActive(false);
            mMainControlImage.gameObject.SetActive(false);
            mMainTxt.gameObject.SetActive(false);
            mContainer.SetActive(false);
            mSecContainer.SetActive(false);
        }
    }

    public void ShowInfo(TutorialInfo pInfo)
    {
        mHeadingText.text = pInfo.mTutorialBoxTitle;
        mBodyText.text = pInfo.mMainControlText;
        if (!string.IsNullOrEmpty(pInfo.mSecondaryControlText))
        {
            mSecondaryBodyObj.SetActive(true);
            mSecondBody.gameObject.SetActive(true);
            mSecondBody.text = pInfo.mSecondaryControlText;
        }
        if (pInfo.mShowControlImg)
        {
            ChangeableControls aControlValues = GameManager.Instance.mSettingsData.mCurrentControls.GetControl(pInfo.mMainControl);
            PromptContainer aPrompt;
            string aControlPath = InputPromptHandler.Instance.mActiveControlSchemeType != ControlSchemeType.KeyboardMouse ?
                aControlValues.mGamepadPath.Substring(aControlValues.mGamepadPath.LastIndexOf(">/") + 2) :
                aControlValues.mKeyboardPath.Substring(aControlValues.mKeyboardPath.LastIndexOf(">/") + 2);
            if (InputPromptHandler.Instance.mUIPrompts[InputPromptHandler.Instance.mActiveControlSchemeType]
                .TryGetValue(aControlPath, out aPrompt))
            {
                mMainIntImg.sprite = InputPromptHandler.Instance.mInteractImg;
                Sprite aMBTNSprite = null;
                if (InputPromptHandler.Instance.mActiveControlSchemeType == ControlSchemeType.KeyboardMouse)
                {
                    aMBTNSprite = aPrompt.mPromptSettings.mFallbackText.Contains("MBTN") ? InputPromptHandler.Instance.mMBtnImg[int.Parse(aPrompt.mPromptSettings.mFallbackText.Substring(4))] : null;
                    if (aMBTNSprite != null)
                    {
                        mMainIntImg.sprite = aMBTNSprite;
                    }
                    else
                    {
                        mMainIntImg.sprite = aPrompt.mPromptSettings.mFallbackText.Length >= 3 ? InputPromptHandler.Instance.mLongKeyImg : InputPromptHandler.Instance.mKeyImg;
                    }
                }

                mContainer.SetActive(true);
                if (aMBTNSprite == null)
                {
                    mMainTxt.text = aPrompt.mPromptSettings.mFallbackText;
                    mMainControlImage.sprite = aPrompt.mPromptImage == null ? null : aPrompt.mPromptImage;
                    if (mMainControlImage.sprite != null)
                    {
                        mMainControlImage.gameObject.SetActive(true);
                    }
                    else
                    {
                        mMainTxt.gameObject.SetActive(true);
                    }
                }
                else
                {
                    mMainTxt.text = "";
                }
            }
            if (!string.IsNullOrEmpty(pInfo.mSecondaryControlText))
            {
                aControlValues = GameManager.Instance.mSettingsData.mCurrentControls.GetControl(pInfo.mSecondaryControl);
                aControlPath = InputPromptHandler.Instance.mActiveControlSchemeType != ControlSchemeType.KeyboardMouse ?
                    aControlValues.mGamepadPath.Substring(aControlValues.mGamepadPath.LastIndexOf(">/") + 2) :
                    aControlValues.mKeyboardPath.Substring(aControlValues.mKeyboardPath.LastIndexOf(">/") + 2);
                if (InputPromptHandler.Instance.mUIPrompts[InputPromptHandler.Instance.mActiveControlSchemeType]
                    .TryGetValue(aControlPath, out aPrompt))
                {
                    mSecondIntImg.sprite = InputPromptHandler.Instance.mInteractImg;
                    Sprite aMBTNSprite = null;
                    if (InputPromptHandler.Instance.mActiveControlSchemeType == ControlSchemeType.KeyboardMouse)
                    {
                        aMBTNSprite = aPrompt.mPromptSettings.mFallbackText.Contains("MBTN") ? InputPromptHandler.Instance.mMBtnImg[int.Parse(aPrompt.mPromptSettings.mFallbackText.Substring(4))] : null;
                        if (aMBTNSprite != null)
                        {
                            mSecondIntImg.sprite = aMBTNSprite;
                        }
                        else
                        {
                            mSecondIntImg.sprite = aPrompt.mPromptSettings.mFallbackText.Length >= 3 ? InputPromptHandler.Instance.mLongKeyImg : InputPromptHandler.Instance.mKeyImg;
                        }
                    }
                    mSecContainer.SetActive(true);
                    if (aMBTNSprite == null)
                    {
                        mSecControlTxt.text = aPrompt.mPromptSettings.mFallbackText;
                        mSecCtrlImg.sprite = aPrompt.mPromptImage == null ? null : aPrompt.mPromptImage;
                        if (mSecCtrlImg.sprite != null)
                        {
                            mSecCtrlImg.gameObject.SetActive(true);
                        }
                        else
                        {
                            mSecControlTxt.gameObject.SetActive(true);
                        }
                    }
                    else
                    {
                        mSecControlTxt.text = "";
                    }
                }

            }
        }
        SetupContinueImage();
        gameObject.SetActive(true);
    }

    void SetupContinueImage()
    {
        ChangeableControls aControlValues = GameManager.Instance.mSettingsData.mCurrentControls.GetControl(Controls.Interact);
        PromptContainer aPrompt;
        string aControlPath = InputPromptHandler.Instance.mActiveControlSchemeType != ControlSchemeType.KeyboardMouse ?
            aControlValues.mGamepadPath.Substring(aControlValues.mGamepadPath.LastIndexOf(">/") + 2) :
            aControlValues.mKeyboardPath.Substring(aControlValues.mKeyboardPath.LastIndexOf(">/") + 2);
        if (InputPromptHandler.Instance.mUIPrompts[InputPromptHandler.Instance.mActiveControlSchemeType]
            .TryGetValue(aControlPath, out aPrompt))
        {
            mInteractBG.sprite = InputPromptHandler.Instance.mInteractImg;
            Sprite aMBTNSprite = null;
            if (InputPromptHandler.Instance.mActiveControlSchemeType == ControlSchemeType.KeyboardMouse)
            {
                aMBTNSprite = aPrompt.mPromptSettings.mFallbackText.Contains("MBTN") ? InputPromptHandler.Instance.mMBtnImg[int.Parse(aPrompt.mPromptSettings.mFallbackText.Substring(4))] : null;
                if (aMBTNSprite != null)
                {
                    mInteractBG.sprite = aMBTNSprite;
                }
                else
                {
                    mInteractBG.sprite = aPrompt.mPromptSettings.mFallbackText.Length >= 3 ? InputPromptHandler.Instance.mLongKeyImg : InputPromptHandler.Instance.mKeyImg;
                }
            }
            mContinueContainer.SetActive(true);
            if (aMBTNSprite == null)
            {
                mContinueTxt.text = aPrompt.mPromptSettings.mFallbackText;
                mContinueImg.sprite = aPrompt.mPromptImage == null ? null : aPrompt.mPromptImage;
                if (mContinueImg.sprite != null)
                {
                    mContinueImg.gameObject.SetActive(true);
                }
                else
                {
                    mContinueTxt.gameObject.SetActive(true);
                }
            }
            else
            {
                mContinueTxt.text = "";
            }
        }

    }

    public void ShowDialog(DialogBoxStrings pStrings)
    {
        mHeadingText.text = pStrings.mHeadingText;
        mBodyText.text = pStrings.mBodyText;
        SetupContinueImage();
        gameObject.SetActive(true);
    }

}
