///-------------------------------------------------------------------------------------------------
// File: MeleeAttack.cs
//
// Author: Dakshvir Singh Rehill
// Date: 1/6/2020
//
// Summary:	Handles attack of Melee Enemy
///-------------------------------------------------------------------------------------------------
using System.Collections;
using UnityEngine;

public class MeleeAttack : MonoBehaviour
{
	[Tooltip("The maximum y position difference between player and the enemy")]
	[SerializeField] float mMaxYDifference = 1.0f;
	[Tooltip("The time to re-attack after an attack has been caused")]
	[SerializeField] float mAttackTime = 0.5f;
	[Tooltip("The Melee Enemy class reference")]
	[SerializeField] MeleeEnemy mEnemyInstance;
	[Tooltip("The Pool Classifier for the Player Damage Fx")]
	[SerializeField] PoolClassifier mMeleeAttackFx;
    [Tooltip("Sound Effect for Player Hit")]
    [SerializeField] string mPlayerHitSFX = "PlayerHit";
    /// <summary>
    /// The instance of the current player shape
    /// </summary>
    PlayerCharacter mShapeReference;
	/// <summary>
	/// The current time between attacks
	/// </summary>
	float mCurrentTime = 0.0f;

    void OnTriggerEnter(Collider pCollider)
	{
		if(mShapeReference != null)
        {
			if(mShapeReference.gameObject.activeInHierarchy)
            {
				return;
			}
		}
		mShapeReference = pCollider.gameObject.GetComponent<PlayerCharacter>();
		if(mShapeReference != null)
        {
			Attack();
        }

	}

	void OnTriggerStay(Collider pCollider)
    {
		if(mShapeReference == null)
        {
			return;
        }
		if (mShapeReference.gameObject.GetInstanceID() != pCollider.gameObject.GetInstanceID())
        {
			return;
        }
		Attack();
	}

	void OnTriggerExit(Collider pCollider)
	{
		if (mShapeReference == null)
		{
			return;
		}
		if (mShapeReference.gameObject.GetInstanceID() == pCollider.gameObject.GetInstanceID())
		{
			mEnemyInstance.mAttack = false;
			mShapeReference = null;
			mCurrentTime = 0.0f;
		}
	}


	/// <summary>
	/// Function Called each time player character stays in the attack zone
	/// </summary>
	void Attack()
    {
		if(GameManager.Instance.mState == GameManager.State.Pause)// if game paused don't attack
        {
			return;
        }
		if(GameManager.Instance.mGameData.mPlayerData.mPlayerHealth <= 0) // if player dead don't attack
        {
			return;
        }
		if(mEnemyInstance.mDeactivate || mEnemyInstance.mFrozen)// don't attack if enemy deactivated or frozen
        {
			return;
        }
		if(mEnemyInstance.mDetectionMeterVal.Value < mEnemyInstance.mDetectionVal)
        {
			return;
        }
		mEnemyInstance.mAttack = true;
		mEnemyInstance.mAgent.velocity = Vector3.zero;
		mEnemyInstance.mAgent.isStopped = true; //force stop
		if (mCurrentTime > 0.0f)
		{
			mCurrentTime -= Time.deltaTime;
		}
		if (mShapeReference.mCameraLookAt.position.y
	- mEnemyInstance.mAgent.transform.position.y >= mMaxYDifference) //check for max y distance
		{
			return;
		}
		if (mCurrentTime <= 0.0f) //if all conditions meet and it is attack time then attack player and update stats
		{
			mCurrentTime = mAttackTime;
			GameManager.Instance.mGameData.mPlayerData.mPlayerHealth -= mShapeReference.mDamage;
			mShapeReference.mAnimator.SetTrigger(mShapeReference.mDamagePlayerTrigger);
			StartCoroutine(PlayMeleeAttackFx());
		}
	}
	/// <summary>
	/// Play Visual Effect to show player is damaged
	/// </summary>
	/// <returns></returns>
	IEnumerator PlayMeleeAttackFx()
    {
		if (!string.IsNullOrEmpty(mPlayerHitSFX))
		{
			AudioManager.Instance.PlaySound(mPlayerHitSFX);
		}
		GameObject aAttackFxObject = ObjectPoolingManager.Instance.GetPooledObject(mMeleeAttackFx.mPoolName);
		ParticleSystem aAttackParticle = aAttackFxObject.GetComponent<ParticleSystem>();
		aAttackFxObject.transform.position = mShapeReference.transform.position;
		aAttackParticle.Clear();
		aAttackFxObject.SetActive(true);
		aAttackParticle.Play();
		yield return new WaitWhile(() => aAttackParticle.isPlaying);
		aAttackFxObject.SetActive(false);
		ObjectPoolingManager.Instance.ReturnPooledObject(mMeleeAttackFx.mPoolName, aAttackFxObject);
    }

}