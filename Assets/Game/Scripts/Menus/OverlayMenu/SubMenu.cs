﻿///-------------------------------------------------------------------------------------------------
// File: SubMenu.cs
//
// Author: Dakshvir Singh Rehill
// Date: 27/7/2020
//
// Summary:	The Menu that is displayed on top of the other menu
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SubMenu : Menu
{
    [Tooltip("The Selectable that needs to be selected when this menu is opened")]
    public Selectable mInitSelectable;
    [Tooltip("The Back Button for this sub menu")]
    [SerializeField] protected Button mBackBtn;
    [Tooltip("The Overlay Image that shows that menu is inactive")]
    [SerializeField] protected GameObject mInactiveImage;
    public virtual void SetMenuActive(UnityAction pBackCallback)
    {
        mInactiveImage.SetActive(false);
        mBackBtn.onClick.RemoveAllListeners();
        mBackBtn.onClick.AddListener(pBackCallback);
        mBackBtn.onClick.AddListener(BackButtonPressed);
        if (AudioManager.Instance.IsSoundPlaying(MenuManager.Instance.mMenuSelectMovementSFX))
        {
            AudioManager.Instance.StopSound(MenuManager.Instance.mMenuSelectMovementSFX);
        }
        AudioManager.Instance.PlaySound(MenuManager.Instance.mMenuSelectionClickSFX);
    }

    public virtual void BackButtonPressed()
    {
        mInactiveImage.SetActive(true);
        if (AudioManager.Instance.IsSoundPlaying(MenuManager.Instance.mMenuSelectMovementSFX))
        {
            AudioManager.Instance.StopSound(MenuManager.Instance.mMenuSelectMovementSFX);
        }
        AudioManager.Instance.PlaySound(MenuManager.Instance.mMenuBackSelectionSFX);
    }
}
